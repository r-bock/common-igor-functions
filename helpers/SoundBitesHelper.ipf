#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.3
#pragma version = 1
#pragma ModuleName = SoundBites

// by Roland Bock, October 2016
//
// This file contains a selection of helper files to load and play sound bites from a Igor Experiment
// preference file.
//
// ** version 1 (OCT 2016)

static Strconstant ksSB_prefPath = "helpers:SoundPreferences"
static Strconstant ksSB_sbName = "SoundBites.pxp"

static Strconstant ksSB_packageName = "SoundBites"

static Strconstant ksSB_soundFlag = "soundFlag"

// **********************     S T A T I C    H E L P E R    F U N C T I O N S      *********************

static Function /DF getPackage()
	DFREF cDF = GetDatafolderDFR()
	DFREF package = root:Packages:$ksSB_packageName
	if (DatafolderRefStatus(folder) == 1)
		return package
	endif
	SetDatafolder root:
	NewDatafolder /S/O Packages
	NewDatafolder /S/O $ksSB_packageName
	DFREF package = GetDatafolderDFR()
	SetDatafolder cDF
	return package
End
	
static Function setSoundFlag(flag)
	Variable flag
	
	flag = flag < 1 ? 0 : 1
	
	DFREF package = getPackage()
	
	NVAR sound = package:$ksSB_soundFlag
	if (!NVAR_Exists(sound))
		Variable /G package:$ksSB_soundFlag = flag
	else
		sound = flag
	endif
	
	return flag
End

static Function hasSound()
	DFREF package = getPackage()
	NVAR sound = package:$ksSB_soundFlag
	if (!NVAR_Exists(sound))
		return setSoundFlag(0)
	else
		return sound
	endif
End
	
static Function findSoundBites(folder)
	String folder
	
	Variable result = 0
	
	NewPath /O/Q/Z SB_startFolder, folder
	String list = IndexedFile(SB_startFolder, -1, "????") + IndexedDir(SB_startFolder, -1, 0)
	if (WhichListItem("RB Minions", list) < 0)
		// not here so process each directory and alias to find it
		Variable index = 0
		Variable numItems = ItemsInList(list)
		for (index = 0; index < numItems; index += 1)
			String listItem = StringFromList(index, list)
			GetFileFolderInfo /Q/Z/P=SB_startFolder listItem
			if (V_isFolder)
				result = findSoundBites(S_path)
			elseif(V_isAliasShortcut)
				result = findSoundBites(S_aliasPath)
			endif
			if (result > 0)
				NewPath /O/Q/Z SB_startFolder, folder
			else
				break
			endif
		endfor
	else
		GetFileFolderInfo /Q/Z/P=SB_startFolder "RB Minions"
		if (V_isFolder)
			NewPath /O/Q/Z RBSoundBiteLocation, folder + "RB Minions:" + ksSB_prefPath
		elseif (V_isAliasShortcut)
			NewPath /O/Q/Z RBSoundBiteLocation, S_aliasPath + ksSB_prefPath
		else
			setSoundFlag(0)
			result = 1
		endif
	endif
	
	KillPath /Z SB_startFolder

	return result
End
	
static Function hasSoundBites()
	Variable result = findSoundBites(SpecialDirPath("Igor Pro User Files", 0,0,0) + "User Procedures")
	if (result == 0)
		GetFileFolderInfo /Q/Z/P=RBSoundBiteLocation ksSB_sbName
		if (V_Flag == 0 && V_isFile)
			setSoundFlag(1)
		else
			setSoundFlag(0)
			result = 2
		endif
	endif
	
	return result == 0
End
	
	

// ************************    G E N E R A L    F U N C T I O N S     *****************************


// This function loads a soundbite (sound wave) from the sound bite preference file located in RB Minions folder
// within the helpers subfolder. The sound wave has to exist and match the name
Function SB_LoadSoundBite(soundName)
	String soundName
	
	DFREF package = getPackage()	
	DFREF cDF = GetDatafolderDFR()
	
	Variable result = 0
	
	if (hasSoundBites())
		GetFileFolderInfo /Q/Z/P=RBSoundBiteLocation ksSB_sbName
		if (V_flag == 0 && V_isFile)
			SetDatafolder package
			LoadData /O /J=soundName /L=1 /Q /P=RBSoundBiteLocation ksSB_sbName
			SetDatafolder cDF
			setSoundFlag(1)
		endif
	else
		result = 1
	endif
	
	return result
End	
	
Function SB_play(soundName)
	String soundName
	
	DFREF package = getPackage()
	Wave sound = package:$soundName
	if (hasSound() && WaveExists(sound))
		PlaySound sound
	endif
	return 0
End
#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.30

// written by Roland Bock
// ** version 1.30 (November 2017) added font option to the addFileAndDateInfo function; added another
//                                 function to label layout pages with page numbers. 
// ** version 1.16 (November 2014) added a menu item into macros to execute this function on the 
//                    top layout
// ** version 1.15 (March 2012)
// * version 0.1 (Apr 2009)

// ******************************************************************
// **************                  C O N S T A N T S                  *************************
// ******************************************************************
#if defined(WINDOWS)
	static Strconstant ksDefFont = "Arial"
#else
	static Strconstant ksDefFont = "Verdana"
#endif

static StrConstant ksFileInfoBoxName = "fileInfo"
static Strconstant ksPageInfoBoxName = "pageInfoBox"
static Strconstant ksDateInfoBoxName = "dateInfoBox"


Menu "Macros", dynamic
	SubMenu "Layouts"
		"add file name and date", /Q, AddInfo2TopLayout()
		MenuForIgor7(), /Q, AddPageAndDateInfo2Top()
	End
End

Function /S MenuForIgor7()
	String menuItem = "add page numbers and date"
	if (IgorVersion() < 7.00)
		// hide from versions less than 7, since multiple layout pages have only been
		//    introduced since version 7
		menuItem = ""
	endif
	return menuItem
End


// This function ensures that the info function will be executed on the top layout
Function AddInfo2TopLayout()
	String topLayout = StringFromList(0, WinList("*", ";", "WIN:4"))
	if (strlen(topLayout) == 0)
		// no layout available
		return 0
	endif
	
	DoWindow /F $topLayout
	addFileAndDateInfo()
	return 1
End


// This function adds a text box in the upper right hand corner of the layout with the current date
// and the file name.
Function addFileAndDateInfo([theLayout, font])
	String theLayout
	String font
	
	if (ParamIsDefault(font) || strlen(font) == 0 || WhichListItem(font, FontList(";")) < 0)
		font = ksDefFont
	endif
	
	if (ParamIsDefault(theLayout) || strlen(theLayout) == 0)
		TextBox/C/N=$ksFileInfoBoxName/F=0/A=LT/X=0.5/Y=0.5 "\\F'"+ font + "'\\Z09\\f02file: "  + IgorInfo(1) + "; printed: " + date() + " \\f00"
	else
		TextBox/W=$theLayout/C/N=$ksFileInfoBoxName/F=0/A=LT/X=0.5/Y=0.5 "\\F'" + font + "'\\Z09\\f02file: "  + IgorInfo(1) + "; printed: " + date() + " \\f00"	
	endif
	
End

#if IgorVersion() >= 7.00

Function AddPageAndDateInfo2Top()
	String topLayout = StringFromList(0, WinList("*", ";", "WIN:4"))
	if (strlen(topLayout) == 0)
		// no layout available
		return 0
	endif
	
	String font = ksDefFont, skipFirstPage = "No", bottomPage = "bottom", wDate="Yes"
	
	Prompt font, "Choose font", popup, FontList(";")
	Prompt skipFirstPage, "Skip first page?", popup, "Yes;No"
	Prompt bottomPage, "Location?", popup, "top;bottom"
	Prompt wDate, "Include date?", popup, "Yes;No"
	
	DoPrompt "Set page numbers", font, skipFirstPage, bottomPage, wDate
	if (V_Flag)
		// user clicked cancel, so stop here
		return 0
	endif
	
	Variable skipFirst = cmpstr(skipFirstPage, "Yes") == 0
	Variable bottom = cmpstr(bottomPage, "bottom") == 0
	Variable withDate = cmpstr(wDate, "Yes") == 0
	
	DoWindow /F $topLayout
	addPageNumbers2Layout(font=font, skipFirst=skipFirst, bottom=bottom, withDate=withDate)
	return 1
End

Function addPageNumbers2Layout([theLayout, font, skipFirst, bottom, withDate])
	String theLayout, font
	Variable skipFirst, bottom, withDate
	
	skipFirst = ParamIsDefault(skipFirst) || skipFirst < 1 ? 0 : 1
	bottom = ParamIsDefault(bottom) || bottom > 0 ? 1 : 0
	withDate = ParamIsDefault(withDate) || withDate > 0 ? 1 : 0 
	
	if (ParamIsDefault(font) || strlen(font) == 0 || WhichListItem(font, FontList(";")) < 0)
		font = ksDefFont
	endif
	
	if (ParamIsDefault(theLayout))
		theLayout = 	StringFromList(0, WinList("*", ";", "WIN:4"))
	endif

	String info = LayoutInfo(theLayout, "Layout")
	if (strlen(info) == 0)
		// no layout exists, so stop here
		print "** ERROR (addPageNumbers2Layout): could not find the given layout!"
		return 0
	endif
	
	Variable cpage = NumberByKey("CURRENTPAGENUM", info)
	Variable numpages = NumberByKey("NUMPAGES", info)

	Variable index = 0
	
	for (index = skipFirst ? 2 : 1; index <= numpages; index += 1)
		LayoutPageAction /W=$theLayout page=index
		if (withDate)
			Textbox /W=$theLayout/C/N=$ksDateInfoBoxName/F=0/A=RT/X=0.00/Y=0.5 "\\F'" + font + "'\\Z09\\JR\f02version: " + date()
		endif
		if (bottom)
			Textbox /W=$theLayout/C/N=$ksPageInfoBoxName/F=0/A=MB/X=0.00/Y=0.5 "\\F'" + font + "'\\Z09\\JCPage " + num2str(index) + "/" + num2str(numpages)
		else
			Textbox /W=$theLayout/C/N=$ksPageInfoBoxName/F=0/A=MT/X=0.00/Y=0.5 "\\F'" + font + "'\\Z09\\JCPage " + num2str(index) + "/" + num2str(numpages)	
		endif
	endfor
	LayoutPageAction /W=$theLayout page=cpage
	
	return 1
end


#endif
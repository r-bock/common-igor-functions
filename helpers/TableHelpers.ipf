#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.9
#pragma hide = 1
#include "ListHelpers"

// written by Roland Bock, September 2008
// -- version 1.9 (SEP 2019): added a hook to automatically load the help file when file is compiled
// -- version 1.8 (JUL 2016): added a function to place all waves of a graph into a table
// ** version 1.7 (Jan 2010)

#if Exists("OpenHelp") != 0	// only for Igor versions 7 and higher
static Function AfterCompiledHook()
	loadHelp()
	return 0
End

static Function loadHelp()
	String fPath = FunctionPath("")	
	
	if (cmpstr(fPath[0], ":") == 0)
		// either buildin procedure window or packed procedure or procedures not compiled
		return -1
	endif
	
	String fName = ParseFilePath(0, fPath, ":", 1, 0)
	String hName = ReplaceString(".ipf", fName, ".ihf")
	String wList = WinList(hName, ";", "WIN:512")

	if (ItemsInList(wList) > 0)
		// help file already open, nothing more to do
		return 0
	endif

	String path = ParseFilePath(1, fPath, ":", 1, 0)
	
	// create full path to help file, which should have the same file name, but a different
	// 	file ending (.ihf instead of .ipf)
	String help = path + hName
	OpenHelp /V=0/Z help
	return V_flag
End
#endif


Menu "Macros"
	"-"
	"Show top graph in table", TH_graph2table()
End

// A simple function to display a list of waves in a named table
//	theList		a semicolon separated list of wave names
//	theName	the name for the table
Function /S TH_showInTable(theList, [theName, labels])
	String theList, theName
	Variable labels
	
	String tableName = ""
	labels = ParamIsDefault(labels) || labels < 1 ? 0 : 1
	
	if (strlen(theList) == 0)
		print "** ERROR: need some waves to display in the table."
		return tableName
	endif

	if (ParamIsDefault(theName) || strlen(theName) == 0)
		Edit
		tableName = S_name
	else
		tableName = theName
	endif
	
	DoWindow /F $tableName
	if (V_flag)
		if (WinType(tableName) != 2)
			print "** ERROR (TH_showInTable): " + tableName + " is not a table!"
			return ""
		endif
		DoWindow /HIDE=0 $tableName
			
//		String existingWaves = WaveList("*", ";", "WIN:")
//		theList = excludeFromList(existingWaves, theList)
	else
		Edit /N=$theName
		tableName = S_name			// get the actual used name
	endif
	
	Variable index = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theWave = StringFromList(index, theList)

		if (labels)
			AppendToTable /W=$tableName $theWave.ld
		else
			AppendToTable /W=$theName $theWave.d
		endif
	endfor
	
	return tableName
End


Function /S TH_graph2table([graph])
	String graph
	
	String tableName = ""
	if (ParamIsDefault(graph) || strlen(graph) == 0)
		graph = StringFromList(0, WinList("*", ";", "WIN:1"))
		if (strlen(graph) == 0)
			return tableName
		endif
	endif
	
	String list = TraceNameList(graph, ";", 1)
	Variable numTraces = ItemsInList(list)
	Variable index, colNum = 1
	for (index = 0; index < numTraces; index += 1)
		String traceName = StringFromList(index, list)
		
		if (index == 0)
			Edit /K=1 as "Table from " + graph
			tableName = S_name
		endif
		
		Wave xTrace = XWaveRefFromTrace(graph, traceName)
		Variable hasX = WaveExists(xTrace) ? 1 : 0
		Wave trace = TraceNameToWaveRef(graph, traceName)
		
		if (hasX)
			AppendToTable /W=$tableName xTrace
			colNum += 1
		endif
		AppendToTable /W=$tableName trace
		colNum += 1
				
		String parent = GetWavesDatafolder(trace, 0)
		if (cmpstr(parent, "root") != 0)
			if (hasX)
				ModifyTable /W=$tableName title[colNum - 2] = parent + " (x)"
			endif
			ModifyTable /W=$tableName title[colNum - 1]=parent
		endif
	endfor
	
	return tableName
End

// This function assumes that a text wave in a table will contain wave names.
// All these wave names will be displayed in a graph. The name of the text wave will
// become the title of the graph window, while the name of the graph is the name of the
// text wave appended with a 0.
// theTable			name of the table, default is the top one
// theTxtWave		name of the text wave with the wave names
//  ask				if 1, presents a dialog with choices for the table and text wave name
//					default: take the top table and the first text wave name from the table.
Function plotFromTable([theTable, theTxtWave, ask])
	String theTable, theTxtWave
	Variable ask
	
	ask = ParamIsDefault(ask) || ask < 1 ? 0 : 1
	
	String theTables = ""
	
	if (ParamIsDefault(theTable) || strlen(theTable) == 0)
		// get the topmost table and check how many there are
		theTables = WinList("*", ";", "WIN:2")
		if (ItemsInList(theTables) > 1)
			if (ask)
				Prompt theTable, "Please choose a table", popup, theTables
				DoPrompt "Table Chooser", theTable
			else
				theTable = StringFromList(0, theTables)
			endif
		else
			theTable = StringFromList(0, theTables)
		endif
	endif

	if (strlen(theTable) == 0)
		// check if we have a name, if not, abort
		print "** No tables open, nothing to plot."
		return 0
	endif
	// bring the table to front
	DoWindow /F theTable
	
	String theWaves = ""
	String theOption = "TEXT:1,WIN:" + theTable
	
	if (ParamIsDefault(theTxtWave) || strlen(theTxtWave) == 0)
		theWaves = WaveList("*", ";", theOption)
		if (ItemsInList(theWaves) > 1)
			Prompt theTxtWave, "Please choose the text wave:", popup, theWaves
			DoPrompt "Textwave Chooser", theTxtWave
		else
			theTxtWave = StringFromList(0, theWaves)
		endif
	endif

	// check if we have a text wave, if not, stop here
	if (strlen(theTxtWave) == 0 || !WaveExists($theTxtWave))
		print "** No text wave found, nothing to plot."
		return 0
	endif

	Variable index = 0, first = 1
	String theGraph = ""
	Wave /T theTxt = $theTxtWave
	for (index = 0; index < numpnts($theTxtWave); index += 1)
		Wave theWave = $(theTxt[index])

		if (!WaveExists(theWave))
			print "** - can not find wave '" + theTxt[index] + "'."
			continue
		endif
		if (first)
			DoWindow $(nameOfWave(theTxt) + num2str(0))
			if (V_flag)
				DoWindow /K $(nameOfWave(theTxt) + num2str(0))
			endif
			Display /N=$(nameOfWave(theTxt) + num2str(0)) theWave as (nameOfWave(theTxt))
			theGraph = S_name
			first = 0
		else
			AppendToGraph /W=$theGraph theWave
		endif
	endfor
	return 1
End

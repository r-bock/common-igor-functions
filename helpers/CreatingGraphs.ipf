#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6
#pragma version = 1.5

// version 1.5 (August 2010), updated to use modern Igor features
// version 1.1 (October 2002)
// added graph display for manual peak analysis (added to PeakRelations.ipf).
//
// created by Roland Bock, August 2002
// collection of function to display waves in graphs, used after loading or other

// *******************************************
// This funtion takes a list of semicolon separated waves and displays it in one
// single graph named "Graph_1"

Function ShowSingleTracesInOne(List)
	String List
	
	SVAR loadingFile
	String theWave
	Variable index
	
	if (strlen(List) > 0)								// does the list have elements?
		for (index = 0; index < ItemsInList(List); index += 1)
			theWave = StringFromList(index, List)
			if (index == 0)		
				if (SVAR_exists(loadingFile))			// does the global string exists?
					display/K=1/M/W=(1,1,20,10) $theWave as loadingFile
				else
					display/K=1/M/W=(1,1,20,10) $theWave
				endif
			else
				AppendToGraph $theWave
			endif
		endfor
		DoWindow Graph_1
		if (V_flag)
			DoWindow/K Graph_1
		endif
		if (WinType("") == 1)
			DoWindow/C Graph_1
		endif
	else
		DoAlert 1, "No waves to display!"			// no waves in list
	endif
End


// *******************************************
// display a simple graph
Function ShowSimpleGraph(theWave, theName)
	String theWave, theName
	
	if (exists(theWave))
		Wave toShow = $theWave
		DoWindow $theName
		if (V_flag)
			DoWindow/K $theName
		endif
		Display/M/W=(2,2,15,12) $theWave
		ModifyGraph mode = 3, marker = 8
		ModifyGraph rgb = (0,0,0)
		DoWindow/C $theName
		DoWindow/T $theName, theName
	endif
End

// *******************************************
// display the manual peak analysis graph
Function ShowManualPeakAnalysis()
	
	if (exists("ManualPeakAnalysis"))
		Wave ManualPeakAnalysis
		DoWindow ManPeakAnal
		if (V_flag)
			DoWindow/K ManPeakAnal
		endif
		Display/W=(34,67,568,373) ManualPeakAnalysis
		ModifyGraph mode = 4, marker = 8
		ModifyGraph rgb = (0,0,0)
		SetAxis/A/N=1/E=1 left
		SetAxis/A/E=1 bottom
		DoWindow/C ManPeakAnal
		DoWindow/T ManPeakAnal, "ManPeakAnal"
	endif
End
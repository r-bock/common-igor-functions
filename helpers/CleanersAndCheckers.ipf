#pragma rtGlobals=1		// Use modern global access method.

// written by Roland Bock, September 2008
// Collection of functions to clean up the experiment files

Function deleteGobalWaveMetricsVars()

	String theList = VariableList("V_*", ";", 6)
	Variable index = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theVar = StringFromList(index, theList)
		KillVariables /Z $theVar
	endfor
	KillWaves /Z M_WaveStats
End

Function isIn(theWave, theString)
	String theWave
	String theString
	
	if (!WaveExists($theWave) && strlen(theWave) == 0)
		return 0
	endif
	
	if (WaveExists($theWave))
		Wave /T textWave = $theWave
		if (WaveType(textWave) != 0)
			// wave is not a text wave so do nothing (return false)
			return 0
		endif
		
		Variable index = 0
		for (index = 0; index < numpnts(textWave); index += 1)
			if (cmpstr(theString, textWave[index]) == 0)
				// found match, return true
				return 1
			endif
		endfor
	else
		// consider theWave as a string, not a wave name
		if (strsearch(theWave, theString, 0) > -1)
			return 1
		endif
	endif
	return 0
End // isIn

// Kills a list of waves
Function cleanUpUnwantedWaves(theWaveList)
	String theWaveList
	
	if (strlen(theWaveList) == 0)
		return 0
	endif
	
	Variable index = 0
	for (index = 0; index < ItemsInList(theWaveList); index += 1)
		String theWave = StringFromList(index, theWaveList)
		KillWaves /Z $theWave
	endfor
	return 1
End
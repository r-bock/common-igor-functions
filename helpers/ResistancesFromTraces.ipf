﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.01

//© by Roland Bock, February 2002
// FEB 2021 updated file text encoding to 'UTF-8', removed dependence on outdated wavemetrics
//           procedure <Strings as Lists> and updated comments to fit standardized style
//10.2002	added GetResistances function based on exponential curve fitting.
//02.2002	corrected the startpoint in the curve fitting function.

//! This function calculates the input and serial resistance from an current injection pulse.
// Required input variables are the timepoint when the pulse starts, the length of the pulse, 
// a semicolon separated list of the wavenames, the strength of the pulse and the scaling values
// for the output waves. The values for pulse start and length have to be in seconds.
// The values will be written in two waves named "InputResist" and SerialResist". Both waves will
// be displayed in a graph.
// @param StrListe: a semicolon separated string list of wave names
// @param pstart: start time of the test pulse
// @param plength: length of the test pulse
// @param pstrength: strength of the text pulse
// @param wscB: start time of the first trace (data point) in StrListe (often 0)
// @param wscD: interval between each trace in StrListe
// @return: True for successful processing
//-
function GetResistances(StrListe, pstart, plength, pstrength, wscB, wscD)
	string StrListe
	variable pstart, plength, pstrength
	variable wscB, wscD
	
	variable counter = 0
	variable startpoint
	variable amplitude, baseValue, steadyState, pdiff, Rs, Ri
	string InputResist = "InputResist"
	string SerResist = "SerialResist"
	string theWave
	
	make /O/N=0 $InputResist, $SerResist
	wave input = $InputResist
	wave serial = $SerResist
	do
		theWave = StringFromList(counter, StrListe, ";")
		if (strlen(theWave) == 0)
			break							//run out of waves
		endif
		wave temp = $theWave
		WaveStats/Q/R=(pstart, (pstart + 0.025)) temp
		pdiff = V_minloc - pstart + 0.001	//adjust pulselength for startpoint, find endpoint
		startpoint = V_minloc
		amplitude = V_min
//		display /K=1 temp
		WaveStats/Q/R=((startpoint - 0.02), (startpoint - 0.005)) temp
		baseValue = V_avg
		WaveStats/Q/R=((startpoint + plength - 0.005 - pdiff), (startpoint + plength - pdiff)) temp
		steadyState = V_avg - baseValue
		//calculate resistances
		amplitude -= baseValue
		Rs = pstrength/amplitude		
		Ri = (pstrength/steadyState) - Rs

		input[counter] = {Ri}
		serial[counter] = {Rs}
		
		counter += 1
	while(1)
	
	SetScale/P x wscB, wscD, "dat", input, serial
	
	dowindow/F Resistances					//try to bring right table to front
	if (V_Flag == 0)							//does not exist
		edit as "Resistances"
		dowindow/C Resistances
	endif
	appendtotable input, serial
	DoWindow/F ResistGraph
	if(V_Flag == 0)
		display input, serial as "ResistGraph"
		DoWindow/C ResistGraph
//		ModifyGraph height = 72*2.5, width = 72*2.5
		ModifyGraph dateInfo(bottom)={0,2,0}
		ModifyGraph mode=2, lsize=2
		ModifyGraph rgb($InputResist)=(0,0,0)
		ModifyGraph rgb($SerResist)=(34952,34952,34952)
		SetAxis/A/E=1 left
		SetAxis/A/E=1 bottom
		Label bottom, "time"
	
	endif
	return 1
end

//! This is a modification of the "GetResistances" function to use an exponential
// fit function to estimate the values.
// @param StrListe: a semicolon separated string list of wave names
// @param pstart: start time of the test pulse
// @param plength: length of the test pulse
// @param pstrength: strength of the text pulse
// @param wscB: start time of the first trace (data point) in StrListe (often 0)
// @param wscD: interval between each trace in StrListe
// @return: True for successful processing
//-
function GetResistances_FitHoon(StrListe, pstart, plength, pstrength, wscB, wscD)
	string StrListe
	variable pstart, plength, pstrength
	variable wscB, wscD
	
	variable counter = 0
	variable startpoint
	variable amplitude, baseValue, steadyState, pdiff, Rs, Ri
	string InputResist = "InputResist"
	string SerResist = "SerialResist"
	string theWave
	
	make /O/N=0 $InputResist, $SerResist
	wave input = $InputResist
	wave serial = $SerResist
	do
		theWave = StringFromList(counter, StrListe, ";")
		if (strlen(theWave) == 0)
			break							//run out of waves
		endif
		wave temp = $theWave
		WaveStats/Q/R=(pstart,pstart+0.01) temp
		pdiff = V_minloc - pstart + 0.001	//adjust pulselength for startpoint, find endpoint
		startpoint = V_minloc
		amplitude = V_min
//		display /K=1 temp
		WaveStats/Q/R=((startpoint - 0.01), (startpoint - 0.005)) temp
		baseValue = V_avg
		K0 = basevalue
		K1 = -500
		K2 = 0.01
		CurveFit /G/Q/W=0/N exp, temp (startpoint + 0.01, startpoint + 0.05)
		amplitude = K0 + K1 * exp(0.01/K2)
		print K0, K1, K2
		Rs=pstrength/(amplitude-baseValue)
		Ri=pstrength/(K0-basevalue)-Rs

		input[counter] = {Ri}					// add input resistance to wave
		serial[counter] = {Rs}					// add serial resistance to wave
		
		counter += 1
	while(1)
	
	SetScale/P x wscB, wscD, "dat", input, serial
	
	dowindow/F Resistances					//try to bring right table to front
	if (V_Flag == 0)							//does not exist
		edit as "Resistances"
		dowindow/C Resistances
	endif
	appendtotable input, serial
	DoWindow/F ResistGraph
	if(V_Flag == 0)
		display input, serial as "ResistGraph"
		DoWindow/C ResistGraph
//		ModifyGraph height = 72*2.5, width = 72*2.5
		ModifyGraph dateInfo(bottom)={0,2,0}
		ModifyGraph mode=2, lsize=2
		ModifyGraph rgb($InputResist)=(0,0,0)
		ModifyGraph rgb($SerResist)=(34952,34952,34952)
		SetAxis/A/E=1 left
		SetAxis/A/E=1 bottom
		Label bottom, "time"
	
	endif
	return 1
end

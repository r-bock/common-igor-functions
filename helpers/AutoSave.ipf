#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6
#pragma version = 2.01
#pragma hide = 1

// A set of procedures to automatically save the Igor experiment file in a given interval.
//
// written by Roland Bock
// version 2.01 (February 2021): improved documentation
// version 2 (October 2015): implemented automatic restart of autosave when experiment file
//                            is reopened. 
// version 1.8 (April 2013)
// version 1.75 (September 2011)
// ** version 1 (July 2011)


// ***********    C O N S T A N T    D E C L A R A T I O N S    *****************

static Strconstant ksPackageName = "AutoSave"		//< package name
static Constant kVersion = 2								//< version number

static Strconstant ksSaveInterval = "interval"


// ************   H O O K    F U N C T I O N S     *****************

static Function AfterCompiledHook()
	Variable hookResult = 0
	
	CtrlNamedBackground ExpAutoSave status
	String info = S_info
	if (NumberByKey("RUN", info))
		return hookResult
	endif
	
	if (getSaveStatus())
		// the current autosave process was interrupted, restart
		StartAutoSave(theTime=getSaveInterval())
	endif
	
	return hookResult
End

// ***************************    M E N U     **************************

Menu "Auto Save"
	"Start", StartAutoSave()
	"Stop", StopAutoSave()
End


// *********    S T A T I C    H E L P E R S     ****************


//! This static function returns a valid data folder reference to the package folder. If 
// the folder does not exists, it will be created.
// @return: a valid data folder reference to the package folder
//-
static Function /DF getPackage()
	DFREF cDF = GetDatafolderDFR()
	DFREF package = root:Packages:$ksPackageName
	if (DatafolderRefStatus(package) == 0)
		NewDatafolder /S/O root:Packages
		NewDatafolder /S $ksPackageName
		DFREF package = GetDatafolderDFR()
		SetDatafolder cDF
	endif
	return package
End


static Function getSaveStatus()
	DFREF package = getPackage()
	SVAR interval = package:$ksSaveInterval
	if (!SVAR_Exists(interval))
		String /G package:$ksSaveInterval = ""
		SVAR interval = package:$ksSaveInterval
	endif
	
	return strlen(interval) > 0
End

static Function setSaveInterval(interval)
	String interval
	
	DFREF package = getPackage()
	SVAR gInterval = package:$ksSaveInterval
	if (!SVAR_Exists(ginterval))
		String /G package:$ksSaveInterval = interval
	else
		gInterval = interval
	endif
	return 1
End

static Function /S getSaveInterval()
	DFREF package = getPackage()
	SVAR interval = package:$ksSaveInterval
	if (!SVAR_Exists(interval))
		return ""
	endif 
	return interval
End

// ****************     M A I N     F U N C T I O N S     *****************


//! Saves the experiment file if it has been modified. Uses a background process for it and 
// prints a save message into the history window.
//-
Function SaveTheExperiment(s)
	STRUCT WMBackgroundStruct &s
	
	ExperimentModified
	if (!V_flag)
		// experiment has not been modified, so no need to save it
		return 0
	endif

	DFREF cDF = GetDatafolderDFR() // remember where we are
	SaveExperiment
	printf "-- experiment saved on %s %s.\r", date(), time()
	// the next line is only for debugging
	// printf "Task %s running, experiment saved on %s %s.\r", s.name, date(), time()
	ExperimentModified 0

	SetDatafolder cDF	// with an experiment save, Igor returns to the root folder, so now
						//			return to where we started.
	return 0
End

//! Provides a dialog to enter the desired save interval and starts the background auto save
// process. The interval needs to be entered in a number and unit form with possible units 
// being d = days, h = hours, m = minutes or s = seconds, separated by a space.
// @param theTime: the interval as number <space> unit string
// @return: TRUE (1) for successful start or FALSE (0) for failure to start autosave
//-
Function StartAutoSave([theTime])
	String theTime
	Prompt theTime, "Enter the save interval (# space time unit as [d, h, m or s])"
	
	if (ParamIsDefault(theTime) || strlen(theTime) == 0)
		theTime = getSaveInterval()
		if (strlen(theTime) == 0)
			theTime = "5 m"
		endif
		DoPrompt "Interval Chooser", theTime
		if (V_flag)
			// cancel clicked
			return 0
		endif
	endif
	
	Variable timeNumber = str2num(StringFromList(0, theTime, " "))
	if (numtype(timeNumber) != 0)
		// the number conversion went wrong or the string did not contain a number
		//  so break here
		return 0
	endif
	
	setSaveInterval(theTime)
	
	String timeUnit = LowerStr((StringFromList(1, theTime, " "))[0,0])
	Variable unitFactor = 1
	
	strswitch (timeUnit)
		case "d":
			unitFactor *= 24
		case "h":
			unitFactor *= 60
		case "m":
		default:
			unitFactor *= 60
		case "s":
			unitFactor *= 1
	endswitch
	
	switch (unitFactor)
		case 1:
			timeUnit = "second"
			break
		case 60:
			timeUnit = "minute"
			break
		case 3600:
			timeUnit = "hour"
			break
		case 86400:
			timeUnit = "day"
			break
	endswitch
	if (timeNumber > 1)
		timeUnit += "s"
	endif
	
	Variable timeInSec = timeNumber * unitFactor
	
	Variable numTicks = timeInSec * 60 // 1 tick is 1/60th of a second
	
	CtrlNamedBackground ExpAutoSave, period=numTicks, proc=SaveTheExperiment
	CtrlNamedBackground ExpAutoSave, status
	if (NumberByKey("RUN", S_info) == 0)
		CtrlNamedBackground ExpAutoSave, start
		print "-- started auto save procedure every " + num2str(timeNumber) + " " + timeUnit + " on " + date() + " " + time()
	else
		print "-- changed auto save to " + num2str(timeNumber) + " " + timeUnit + " on ", date(), time()
	endif
	return 1
End

//! The stop function necessary to stop the auto save.
// @return: true for successful stopping
//-
Function StopAutoSave()
	CtrlNamedBackground ExpAutoSave, stop
	setSaveInterval("")
	print "-- stopped auto save procedure on " + date() + " " + time()
	return 1
End
#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 3
#pragma IgorVersion = 6.1
#pragma hide = 1

// by Roland Bock August 2013
// version 3.0 (MAR 2021): updated documentation - comment strings and modernized error feedback reporting.
// version 2.9 (SEP 2019): added an Igor > v7 function to automatically load the associated help file
//                         once this file is compiled.
// version 2.8 (JUL 2019): added a flag to the correctDate function to be able to flip the day
//             and month values independent of the date format
// version 2.7 (JUL 2019): needed a function to automatically determine the separator used in a 
//									date string
// version 2.6: (DEC 2018): added function to convert misinterpreted duration from Excel import
//                 into Igor seconds.
// version 2.5: added function to convert MMDDYYYY into date string YYMMDD
// version 2.3: added function to convert corrected time string into local date
// version 2.1: added function to convert time duration string into seconds
// version 2: converted the functions using sscanf function, which makes them much faster


static Strconstant ks_ErrorMsg = "** ERROR (%s) %s!\r"
static Strconstant ks_WarnMsg = "-- Warning (%s) %s.\r"

static Function /S getFeedbackStr(fName, str)
	String fName, str
	
	String fbs = ""
	sprintf fbs, str, fName, "%s"
	return fbs
End
static Function /S getErrorStr(fName)
	String fName
	
	return getFeedbackStr(fName, ks_ErrorMsg)
End
static Function /S getWarnStr(fName)
	String fName
	
	return getFeedbackStr(fName, ks_WarnMsg)
End

#if Exists("OpenHelp") != 0	// only for Igor versions 7 and higher
static Function AfterCompiledHook()
	loadHelp()
	return 0
End

static Function loadHelp()
	String fPath = FunctionPath("")	
	
	if (cmpstr(fPath[0], ":") == 0)
		// either buildin procedure window or packed procedure or procedures not compiled
		return -1
	endif
	
	String fName = ParseFilePath(0, fPath, ":", 1, 0)
	String hName = ReplaceString(".ipf", fName, ".ihf")
	String wList = WinList(hName, ";", "WIN:512")

	if (ItemsInList(wList) > 0)
		// help file already open, nothing more to do
		return 0
	endif

	String path = ParseFilePath(1, fPath, ":", 1, 0)
	
	// create full path to help file, which should have the same file name, but a different
	// 	file ending (.ihf instead of .ipf)
	String help = path + hName
	OpenHelp /V=0/Z help
	return V_flag
End
#endif


//! converts a string in the format of 00:00:00 to seconds.
//	@param theTimeString: 00 = only hours; 00:00 = HH:MM; 00:00:00 = HH:MM:SS. It may contain 'AM' or 'PM', separated with a space from the time.
// @return: the time in seconds.
//-
Function timeStr2Secs(theTimeString)
	String theTimeString
	
	Variable hours, minutes, seconds
	Variable isPM = 0
		
	Variable space = strsearch(theTimeString, " ", 0)
	if (space > -1)
		strswitch (theTimeString[space + 1, strlen(theTimeString) - 1])
			case "pm":
				isPM = 1
				break
		endswitch
		theTimeString = theTimeString[0, space - 1]
	endif
	
	sscanf theTimeString, "%d:%d:%d", hours, minutes, seconds
	
	hours = isPM && hours < 12 ? (hours + 12) * 60 * 60 : hours * 60 * 60
	minutes *= 60
		
	return (hours + minutes + seconds)
End // timeStr2Secs

//! Converts a timestamp of the igor format "data time" into seconds. The time part has to
// be formated as HH:MM:SS. The default date format is the english standard as MM/DD/YY.
// The date and time has to be separated by a single space.
// @param theTimeStamp: a string in the format 'MM/DD/YY HH:MM:SS'
// @param type: a number 1 (default) to 4, with 1 (MM/DD/YY), 2 (YYMMDD), 3 (YYYY-MM-DD), 4 (DD.MM.YYYY)
// @return: the calculated value of the time in seconds
//-
Function timeStamp2Secs(theTimeStamp [, type])
	String theTimeStamp
	Variable type
	
	String separatorList = "/;-;.;"
	String typeList = "1;3;4;"
	Variable seps = 0, position = 0
	String theDate = "", theTime = "", theSep = ""
	
	Variable space = strsearch(theTimeStamp, " ", 0)
	if (space > -1)
		// time stamp consists of date and time
		theDate = theTimeStamp[0, space - 1]
		theTime = theTimeStamp[space + 1, strlen(theTimeStamp)]
	else
		// time stamp is only date
		theDate = theTimeStamp
	endif
	
	if (ParamIsDefault(type))
		// try to figure out the format
		type = 2	// default to YYMMDD
		do
			position = strsearch(theDate, StringFromList(seps, separatorList), 0)
			if (position > 0)
				// if the date is in format YYMMDD this type is never set
				type = str2num(StringFromList(seps, typeList))
			endif 
			seps += 1
		while (seps < ItemsInList(separatorList) && type == 2)
	endif

	
	Variable theDateSec, theTimeSec
	String theYear
	Variable year, month, day
	
	switch(type)
		case 1:
			sscanf theDate, "%d/%d/%s", month, day, theYear
			year = correctYear(theYear)
			break
		case 2:
			day = str2num(theDate[strlen(theDate) - 2, strlen(theDate) - 1])
			month = str2num(theDate[strlen(theDate) - 4, strlen(theDate) - 3])
			year = correctYear(theDate[0, strlen(theDate) - 5])
			break
		case 3:
			sscanf theDate, "%s-%d-%d", theYear, month, day
			year = correctYear(theYear)
			break
		case 4:
			sscanf theDate, "%d.%d.%s", day, month, theYear
			year = correctYear(theYear)
			break
	endswitch
	
	theDateSec = date2secs(year, month, day)
	theTimeSec = timeStr2Secs(theTime)
	
	if (numtype(theDateSec) == 2)
		if (theTimeSec == 0)
			return NaN
		else
			return theTimeSec
		endif
	endif
	
	return (theDateSec + theTimeSec)
End


//! Converts a 2 digit year entry into a four digit year by considering every 2 digit year
// below 80 as in the 21st century, and everything above as being in the 20 century.
// @param theYear: a string of a 2 digit year value
// @return: a 4 digit integer of the corrected year
//-
Function correctYear(theYear)
	String theYear
	
	Variable year
	year = str2num(theYear)

	if (strlen(theYear) == 2)
		if (year < 80)
			year += 2000
		else
			year += 1900
		endif
	endif
	
	return year
End


//! Coverts a date string of the form "MM/DD/YYYY" or "DD.MM.YYYY" to the form "YYMMDD". 
// It automatically distinguishes between the default american and european style date format.
// @param theDate: the date string input to be corrected
// @param type: a number between 0 - 3 with 0 (default, american style [MM/DD/YYYY]), 1 (european style [DD.MM.YYYY]), 2 (timestamp style [YYYY-MM-DD]) or 3 (date style [MMDDYYY])
// @param separator: a single character string (default "/") defining the separator of the date groups.
// @param flip: 0 (default - False) or 1 (True) to flip the month and day value
// @return: a string with the corrected date
//-
Function/S correctDateString(theDate, [type, separator, flip])
	String theDate, separator
	Variable type, flip
	
	flip = ParamIsDefault(flip) || flip < 1 ? 0 : 1
	
	String errormsg = getErrorStr("correctDateString")
	String newDate = ""
	String sepList = "/;.;-;"
	Variable numberOfDateElements = 0, sepItem = 0
	
	if (strlen(theDate) == 6)
		if (strsearch(theDate, "/", 0) < 0 && strsearch(theDate, ".", 0) < 0 && strsearch(theDate, "-", 0) < 0)			
			if (numType(str2num(theDate)) == 0)
				return theDate
			else
				printf errormsg, "wrong date format: " + theDate
				return newDate
			endif
		endif
	endif

	Variable position = 0
	
	if (ParamIsDefault(type))
		// try to determine the date type by found separator
		type = -1
		if (ParamIsDefault(separator))
			do
				position = strsearch(theDate, StringFromList(sepItem, sepList), 0)
				if (position > 0)
					separator = StringFromList(sepItem, sepList)
					type = sepItem
				endif	
				sepItem += 1
			while (sepItem < ItemsInList(sepList) && type < 0)	
		else
			strswitch (separator)
				case "/":
					type = 0
					break
				case ".":
					type = 1
					break
				case "-":
					type = 2
					break
				default:
					type = -1
			endswitch
		endif
	endif
	
	if (type < 0)
		// something went wrong, so stop here
		printf errormsg, "can't determine the date type. STOPPED!"
		return newDate
	endif
	
	// now check if we have 3 elements: day, month and year
	if (type < 3)
		numberOfDateElements = ItemsInList(theDate, separator)
	else
		if (strlen(theDate) >= 8)
			theDate = theDate[0, 7]
			numberOfDateElements = 3
		else
			numberOfDateElements = 1
		endif
	endif
	if (numberOfDateElements != 3)
		printf errormsg, "can't determine day, month and year from ", theDate, "separated by", separator, ". STOPPED!"
		return newDate
	endif

	if (ParamIsDefault(separator))
		switch (type)
			case 0:
				separator = "/"
				break
			case 1:
				separator = "."
				break
			case 2:
				separator = "-"
				break
			case 3:
				separator = ""
				break
		endswitch
	endif
	
	String dateFormat = "%d" + separator + "%d" + separator + "%d"
	Variable year, month, day
	
	
	switch (type)
		case 0:
			sscanf theDate, dateFormat, month, day, year
			break
		case 1:
			sscanf theDate, dateFormat, day, month, year
			break
		case 2:
			sscanf theDate, dateFormat, year, month, day
			break
		case 3:
			day = str2num(theDate[2,3])
			month = str2num(theDate[0,1])
			year = str2num(theDate[4,7])
			break
	endswitch

	if (flip)
		// flip the month and day values
		Variable newDay = month
		month = day
		day = newDay
	endif

	newDate = correctDateDigits(num2str(correctYear(num2str(year)))) + correctDateDigits(num2str(month)) + correctDateDigits(num2str(day))
	
	return newDate
End

//! Makes sure that the number string is a double digit. If only a single digit number, appends
// a zero in the front, for all other length, returns NaN, except for 4 digit numbers. If a 
// number is four digits long, it returns only the last two (to deal with 4 digit years).
// @param theNumber: a string containing the number for correction
// @return: a string with the corrected number
//-
Function/S correctDateDigits(theNumber)
	String theNumber
	
	if (strlen(theNumber) == 1)
		theNumber = "0" + theNumber
		return theNumber
	elseif (strlen(theNumber) == 2)
		return theNumber
	elseif (strlen(theNumber) == 4)
		return theNumber[2, 3]
	endif
	
	
	theNumber = "NaN"
	return theNumber
End // function correctDateDigits

//! Converts microseconds to a human readable time string, useful for printouts in the history window
// after timing procedures.
// @param microseconds: the variable value to be converted.
// @return: a time string in human readable form
//-
Function /S microSecondsToTimeStr(microseconds)
	Variable microseconds
	
	String timeString = ""
	Variable days = 0, hours = 0, minutes = 0, seconds = 0, milliseconds = 0
	
	if (microseconds > 1000)
		milliseconds = microseconds / 1000
	endif
	if (milliseconds > 1000)
		seconds = floor(milliseconds / 1000)
		milliseconds = mod(milliseconds, 1000)
	endif
	if (seconds > 60)
		minutes = floor(seconds / 60)
		seconds = mod(seconds, 60)
	endif
	if (minutes > 60)
		hours = floor(minutes / 60)
		minutes = mod(minutes, 60)
	endif
	if (hours > 24)
		days = floor(hours / 24)
		hours = mod(hours, 24)
	endif
	
	
	if (days > 0)
		timeString = num2str(days) + " d " + num2str(hours) + " h " + num2str(minutes) + " m " + num2str(seconds) + " s " + num2str(milliseconds) + " ms"
	elseif (hours > 0)
		timeString = num2str(hours) + " h " + num2str(minutes) + " m " + num2str(seconds) + " s " + num2str(milliseconds) + " ms"
	elseif (minutes > 0)
		timeString = num2str(minutes) + " m " + num2str(seconds) + " s " + num2str(milliseconds) + " ms"
	else
		timeString = num2str(seconds) + " s " + num2str(milliseconds) + " ms"
	endif
	
	return timeString
end
	
//! Converts seconds into a human readable time string, useful for printout in the history window or graph labels.
// @param seconds: a variable value in seconds
// @return: a time string in human readable form
//-
Function /S seconds2TimeStr(seconds)
	Variable seconds
	
	String timeString = ""
	Variable days = 0, hours = 0, minutes = 0
	
	if (seconds > 60)
		minutes = floor(seconds / 60)
		seconds = mod(seconds, 60)
	endif
	if (minutes > 60)
		hours = floor(minutes / 60)
		minutes = mod(minutes, 60)
	endif
	if (hours > 24)
		days = floor(hours / 24)
		hours = mod(hours, 24)
	endif
	
	if (days > 0)
		timeString = num2str(days) + " d " + num2str(hours) + "h " + num2str(minutes) + " m " + num2str(seconds) + " s"
	elseif (hours > 0)
		timeString = num2str(hours) + "h " + num2str(minutes) + " m " + num2str(seconds) + " s"
	elseif (minutes > 0)
		timeString = num2str(minutes) + " m " + num2str(seconds) + " s"
	elseif (seconds > 0)
		timeString = num2str(seconds) + " s"
	else
		timeString = "Finished in no time (0 seconds)!"
	endif

	return timeString
End

//! Convert a string giving a duration into a seconds value.
// @param duration: a string with the duration. It requires a value separated by a space and a unit (d/day, h/hour, m/min).
// @return: a variable value in seconds
//-
Function durationToSecs(duration, [verbose])
	String duration
	Variable verbose
	
	verbose = ParamIsDefault(verbose) || verbose > 0 ? 1 : 0
	
	String errormsg = getErrorStr("durationToSecs")
	String warnmsg = getWarnStr("durationToSecs")
	Variable number = 0
	String unit = "s"
	Variable space = strsearch(duration, " ", 0)
	if (space < 0)
		if (numtype(str2num(duration)) == 2)
			if (verbose)
				printf errormsg, "no unit or numbers can be determined from '" + duration + "'."
			endif
			return NaN
		else
			if (verbose)
				printf warnmsg, "no unit could be determined from '" + duration + "'. Set to secs."
			endif
			number = str2num(duration)
		endif
	else
		number = str2num(duration[0, space-1])	
	endif
	
	if (space >= 1)
		unit = duration[space+1, strlen(duration)]
	endif
	
	strswitch (unit)
		case "d":
		case "day":
			number *= 24
		case "h":
		case "hour":
			number *= 60
		case "m":
		case "min":
			number *= 60
			break
		default:
			// could not determine unit, so set to negative number
			number = NaN
	endswitch
	
	return number
End

//! Converts/corrects a date string to a 'standard' time string based on a locale.
// @param theDate: a date string in the format YYMMDD or YYYYMMDD
// @param locale: a string for the locale (default "US") with the options "USDASH", "EUR", "DE", "US", "EN"
// @return: a date string in the desired output format
//-
Function /S correctedDateToLocale(theDate, [locale])
	String theDate, locale
	
	String errormsg = getErrorStr("correctDateToLocale")
	
	if (ParamIsDefault(locale) || strlen(locale) == 0)
		locale = "US"
	endif
	
	String day, month, year, newDate
	
	if (strlen(theDate) == 8)
		day = theDate[6, 7]
		month = theDate[4, 5]
		year = theDate[0, 3]
	elseif (strlen (theDate) == 6)
		day = theDate[4, 5]
		month = theDate[2, 3]
		year = theDate[0, 1]
	else
		printf errormsg, "can't figure out date format. Stopped conversion."
		return theDate
	endif
	
	strswitch( locale )
		case "USDASH":
			newDate = month + "-" + day + "-" + year
			break
		case "EUR":
		case "DE":
			newDate = day + "." + month + "." + year
			break
		case "US":
		case "EN":
		default:
			newDate = month + "/" + day + "/" + year
	endswitch
	
	return newDate
End

//! Convert a duration in the form of MM:SS that excel interpreted as HH:MM:SS AM/PM but has been imported
// into Igor as number, i.e. a duration of 1 min (01:00) becomes 1:00:00 AM in excel, as number 0.0416.
// @param duration: a wave with excel imported time values, the wave values will be converted in place 
// @return: 1 (True) for successful conversion
//-
Function convertExcelDuration2Secs(duration)
	Wave duration
	
	if (!WaveExists(duration))
		return 0
	endif
	duration = 24 * duration * 60
	return 1
End

//! This function attempts to determine the separator used in a date string. It looks for "/", ".", "-" 
// or spaces.
// @param datestr: the date string in a common form (i.e. "MM/DD/YY")
// @return: a single character string separating the date groups.
//-
Function /S determineDateSeparator(datestr)
	String datestr
	
	String separator = ""
	String separatorList = "/;.;-; "
	Variable index = 0
	
	do
		String tSeparator = StringFromList(index, separatorList)
		if (ItemsInList(datestr, tSeparator) > 1)
			separator = tSeparator
			break
		endif
		index += 1
	while (index < ItemsInList(separatorList))
	
	return separator
End
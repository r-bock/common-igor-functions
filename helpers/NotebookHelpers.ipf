#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.5

// A collection of functions to create and log information in notebooks
// written by Roland Bock, October 2008
//		for V. Alvarez, NIAAA / NIH
// version 1.5 (JAN 2018): added a function to break a list into nice chunks to print in 
//                         a notebook.
// version 1.3 (May 2012)
// version 0.1, October 2008

Function /S getNotebook(theName)
	String theName
	
	DoWindow /F $theName
	if (V_flag)
		if (WinType(theName) != 5)
			theName = UniqueName(theName, 10, 0)
			NewNotebook /F=0/K=1/N=$theName
		endif
	else
		NewNotebook /F=0/K=1/N=$theName
	endif
	return theName
End

// append some text to a notebook
Function /S TextToNotebook(theText, [theNotebook])
	String theText, theNotebook
	
	String theNotebookName = "", theNotebookTitle = ""
	
	if (ParamIsDefault(theNotebook) || strlen(theNotebook) == 0)
		theNotebookName = "ResultNotes"
		theNotebookTitle = "Result Notes"
	else
		theNotebookName = (ReplaceString(" ", theNotebook, ""))[0,31]
		theNotebookTitle = theNotebook
	endif
	
	DoWindow /F $theNotebookName
	
	if (!V_Flag)
		NewNotebook /F=1 /N=$theNotebookName as theNotebookTitle
	else
		if (WinType(theNotebookName) != 5)
			print "** ERROR: notebook '" + theNotebook + "' does not exists. STOPPED!"
			return ""
		endif
	endif
	
	Notebook $theNotebookName selection = {endOfFile, endOfFile}
	Notebook $theNotebookName text = theText + "\r"
	
	return theNotebookName
End


Function /S NB_makeListNice4Note(list, [listSep, prefix, interval])
	String list, prefix, listSep
	Variable interval

	String result = ""
	
	if (ParamIsDefault(prefix))
		prefix = ""
	endif
	if (ParamIsDefault(listSep) || strlen(listSep) == 0)
		listSep = ";"
	endif
	
	interval = ParamIsDefault(interval) || interval <= 0 ? 5 : round(interval)
	
	Variable numItems = ItemsInList(list, listSep)
	if (numItems <= 0)
		return result
	endif
	
	Variable index
	for (index = interval - 1; index < numItems; index += interval)
		String subject = StringFromList(index, list, listSep)
		Variable length = strlen(subject)
		Variable start = FindListItem(subject, list, listSep)
		list[start + length + 1] = "\r" + prefix
	endfor

	return prefix + list
End
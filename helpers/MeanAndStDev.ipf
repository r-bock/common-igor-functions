#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 7.2
#pragma IgorVersion = 6.2
#pragma ModuleName = SimpleStats
#pragma hide = 1
#include <Peak AutoFind>, menus = 0
#include "RBMinionsVersionControl", version >= 2.4
#include "GraphHelpers", version >= 3.76
#include "TableHelpers", version >= 1.8
#include "ListHelpers", version >= 4.1
#include "WindowHelper", version >= 2.7
#include "PlotHelpers", version >= 1

// written by Roland Bock
// ** version 7.2  (OCT 2021): added a z-score and robust z-score function operating on one-dimensional waves
// ** version 7.1  (MAR 2019): made the SumWaves function static to avoid it clashing with a simialr named 
//										 function in Neuromatic
// ** version 7.0  (OCT 2016): fixed a bug overwriting the "inGraph" option for the main mean calculation
// ** version 6.9  (JUN 2016): added small function to adjust graph and rescale mean waves
// ** version 6.8  (FEB 2016): added function to make an average of some matrix points over either rows
//										or columns
// ** version 6.71 (JUN 2015): made the descriptive stats function obey the cursor positions also to find the max
// ** version 6.70 (MAR 2015): added automatic display of PPR P1 and P2 amplitudes in table as individual waves
// ** version 6.65 (JAN 2015): added a "_none_" error option to the mean calculation
// ** version 6.62 (OCT 2014): fixed average & sum waves in graph to pay attention to cusors on graph for 
//                             range determination
// ** version 6.5  (JUL 2014): added function to make a data wave from the top graph
// ** version 6.37 (JUN 2014): added axis options to average panel
// ** version 6.31 (MAY 2014): added public getters for last mean and error wave names
// ** version 6.1  (APR 2014): added more flexibility to the decay fits to use minimum and provide waves
//                                               with start and end points
// ** version 5.9  (OCT 2013): made average function remember input names
// ** version 5.7  (MAY 2013)

// **********************************************************
// **********                                C O N S T A N T S
// **********************************************************
static Strconstant ksPackageName = "RB_SimpleStats"
static Constant kVersion = 7.2

static Strconstant ksErrorPrefix = "** ERROR (%s): %s!\r"
static Strconstant ksWarnPrefix = "-- Warning (%s): %s!\r"

static Strconstant ksErrDisplayFolder = "SiSt_errorDisplay"

static Strconstant ksDescriptiveTable = "SiSt_AmpAndArea"
static Strconstant ksDescriptiveTableTitle = "Simple Stats amplitude and area"
static Strconstant ksSimpleFitTable = "SiSt_SimpleFitValueTable"
static Strconstant ksSimpleFitTableTitle = "Simple Stats fit values"
static Strconstant ksCombinedFitTable = "SiSt_CombinedFitValueTable"
static Strconstant ksCombinedFitTableTitle = "Combined Simple Stats fit values"
static Strconstant ksSelPPRTable = "SiSt_PPRTableStats"
static Strconstant ksSelPPRTableTitle = "Simple Stats PPR Table"
static Strconstant ksPAmpValTable = "SiSt_PPR_AmpValues"
static Strconstant ksPAmpValTitle = "Simple Stats PPR Amplitudes"

static Strconstant ksDescMeanValues = "SiSt_amplitudeValues"
static Strconstant ksDescAreaValues = "SiSt_areaValues"
static Strconstant ksSimpleFitValues = "SiSt_SimpleFitValues"
static Strconstant ksCombinedFitValues = "SiSt_CombinedSimpleFitValues"
static Strconstant ksDecayFitGuesses = "SiSt_DecayFitMasterGuesses"
static Strconstant ksDerivFitStart = "SiSt_decayFitStartP"

static Strconstant ksSimpleTaus = "SiSt_currentTaus"
static Strconstant ksCombinedTaus = "SiSt_combinedTaus"

static Strconstant ksLastTauName = "SiSt_lastTauName"
static Strconstant ksLastTauLblName = "SiSt_lastTauLbls"

static Strconstant ksTracePeakStats = "SiSt_tracePeakStats"
static Strconstant ksSelPeakStats = "SiSt_selectedPeakStats"
static Strconstant ksAllPeakVals = "SiSt_allDetectedPeakVals"
static Strconstant ksAllPeakLocs = "SiSt_allDetectedPeakLocs"
static Strconstant ksSelPPRStats = "SiSt_selectedPPRStats"
static Strconstant ksP1Amps = "SiSt_P1AmpVals"
static Strconstant ksP2Amps = "SiSt_P2AmpVals"

static Strconstant ks_defSumName = "sumName"						// string name containing last sum wave name
static Strconstant ks_defAvgName = "avgName"						// same for average
static Strconstant ks_defSDName = "stdvName"						// same for standard deviation
static Strconstant ks_defSEMName = "semName"						// same for standard error of mean
static Strconstant ks_defHistName = "histName"						// string variable name containing the histogram name
static Strconstant ks_Sum2Name = "sum2Name"
static Strconstant ks_AvgWavesName = "avgWavesName"
static Strconstant ks_sum2StartXName = "sum2StartX"
static Strconstant ks_sum2EndXName = "sum2EndX"
static Strconstant ks_defType = "defErrType"
static Strconstant ks_definGraph = "inGraph"
static Strconstant ks_inWin = "inWin"								// string variable containing the window to show the trace in
static Strconstant ks_winType = "winType"							// either "graph" or "table"
static Strconstant ks_sum2SelectGraph = "sum2Graph"
static Strconstant ks_avgInGraphName = "avgInGraphName"
static Strconstant ks_defhAxis = "hAxis"
static Strconstant ks_defvAxis = "vAxis"
static Strconstant ks_defStartBin = "startBin"
static Strconstant ks_defBinSize = "binSize"
static Strconstant ks_defNumBins = "numBins"
static Strconstant ks_defDataPoint = "dataPoint"
static Strconstant ks_defDataName = "dataWaveName"
static Strconstant ks_defDataGraph = "dataGraph"

static Strconstant ks_defHistValue = "Def_histogram"

// key words
Strconstant ks_SIST_numWaves = "NUMWAVES"
Strconstant ks_SIST_waveNameList = "WAVELIST"
Strconstant ks_SIST_statsType = "TYPE"
Strconstant ks_SIST_valuePPnt = "VALPPNT"

#if defined(MACINTOSH)
	Strconstant ks_SIST_defFont = "Helvetica"
#else
	Strconstant ks_SIST_defFont = "Arial"
#endif

// **********************************************************
// **********                                M E N U 
// **********************************************************

Menu "Macros"
	SubMenu "Simple Stats"
		"Average waves per point ... /O1", GetMeanPerPoint(statsType="arithmetic mean", traces="no")
		"Average traces per point ... /O2", GetMeanPerPoint(statsType="arithmetic mean", traces="yes")
		"Average waves in graph ...", MeanOfWavesInGraph()
		"Sum waves per point ...", GetMeanPerPoint(statsType="sum")
		"Sum waves in graph ...", SiSt_SumWavesFromGraph()
		"z-scores from graph ...", calcZScoresFromTopGraph()
		"-"
		"adjust mean graph", adjustMeanGraph()
		"-"
		"percent calc of summaries", SiSt_normSummaries()
		"-"
		"make histogram from top graph", SiST_makeHistogramFromGraph()
		"-"
		"Get max-area from graph ...", SiSt_descripStatsFromGraph()
		"Get min-area from graph ...", SiSt_descripStatsFromGraph(minimum=1)
		"-"
		"\\M0Decay fit (top graph) ...", SiSt_decayFitToGraph()
		"\\M0Decay fit (derivative) ...", SiSt_decayFitToGraph(type="derivative")
		"-"
		"\\M0Make data wave (top graph)", SiSt_makeDataWaveFromGraph()
		"-"
		"\\M0Get paired pulse stats (top graph) ...", SiSt_getPPRFromTopGraph()
		"\\M0Get PPR stats (top graph & 3 pts base) ...", SiSt_getPPRFromTopGraph(baseline=3)
	End
	"Scale traces to sec ...", SiSt_rescaleAll2Sec(0, 5, ask = 1)
	"Subtract Traces ...", SiSt_SubtractionPanel()
End

// **********************    O N L Y    F O R   D E V E L O P M E N T   *********

static Function updateVersion()
	VCheck#setVersion(ksPackageName, kVersion)
End

// ******************************************************************************
// ****                   S T A T I C    F U N C T I O N S
// ******************************************************************************

static Function AfterCompiledHook()
	RBVC_checkVersion(ksPackageName, kVersion)
	return 0
End

static Function /DF getPackage([sub])
	String sub
	
	DFREF cDF = GetDatafolderDFR()
	DFREF packages = root:Packages
	if (DatafolderRefStatus(packages) == 0)
		NewDatafolder /S root:Packages
		NewDatafolder /S $ksPackageName
		DFREF package = GetDatafolderDFR()
		SetDatafolder cDF
	else
		DFREF package = packages:$ksPackageName
		if (DatafolderRefStatus(package) == 0)
			NewDatafolder /S packages:$ksPackageName
			DFREF package = GetDatafolderDFR()
			SetDatafolder cDF
		endif
	endif
	
	if (!ParamIsDefault(sub) && strlen(sub) > 0)
		DFREF subF = package:$sub
		if (DatafolderRefStatus(subF) == 0)
			NewDatafolder /S package:$sub
			DFREF package = GetDataFolderDFR()
		else
			DFREF package = subF
		endif
		SetDataFolder cDF
	endif
	
	return package
End

static Function /S getErrorStr(fName)
	String fName
	
	String errmsg = ""
	sprintf errmsg, ksErrorPrefix, fName, "%s"
	
	return errmsg
End

static Function /S getWarnStr(fName)
	String fName
	
	String warnmsg = ""
	sprintf warnmsg, ksWarnPrefix, fName, "%s"
	
	return warnmsg
End



static Function /DF getTmpWorkFolder([name])
	String name
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = "tmp"
	endif
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = getPackage()
	DFREF work = path:$name
	if( DatafolderRefStatus(work) == 0)
		NewDataFolder /S path:$name
		DFREF work = GetDatafolderDFR()
		SetDatafolder cDF
	endif
	return work
End

static Function /DF getErrDisplayFolder()
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	DFREF errDisp = package:$ksErrDisplayFolder
	if (DatafolderRefStatus(errDisp) == 0)
		SetDatafolder package
		NewDatafolder /S $ksErrDisplayFolder
		DFREF errDisp = GetDatafolderDFR()
		SetDatafolder cDF
	endif
	return errDisp
End

static Function /s getPackageStr(name, [defaultStr])
	String name, defaultStr
	
	if (ParamIsDefault(defaultStr) || strlen(defaultStr) == 0)
		defaultStr = ""
	endif
	DFREF package = getPackage()
	SVAR var = package:$name
	if (!SVAR_Exists(var))
		String /G package:$name = defaultStr
	endif
	return GetDatafolder(1, package) + name
End

static Function /S getPackageVar(name)
	String name
	
	DFREF package = getPackage()
	NVAR var = package:$name
	if (!NVAR_Exists(var))
		Variable /G package:$name = 0
	endif
	return GetDatafolder(1, package) + name
End

static Function /WAVE getPackageWave(name)
	String name
	
	DFREF package = getPackage()
	Wave values = package:$name
	return values
End

static Function /WAVE getSimpleFitValueWave()
	return getPackageWave(ksSimpleFitValues)
End

static Function /WAVE getCombinedFitValueWave()
	return getPackageWave(ksCombinedFitValues)
End

static Function /WAVE getMaxValueWave()
	return getPackageWave(ksDescMeanValues)
End
static Function /WAVE getAreaValueWave()
	return getPackageWave(ksDescAreaValues)
End

static Function /S SiST_getMatrixStatsTable(tableName, theWave, [title])
	String tableName, title, theWave
	
	if (strlen(tableName) == 0 || strlen(theWave) == 0)
		return ""
	endif
	if (ParamIsDefault(title) || strlen(title) == 0)
		title = tableName
	endif
	
	DoWindow /F $tableName
	if (V_flag)
		return tableName
	endif

	DFREF package = getPackage()
	Wave fitValues = package:$theWave
	if (!WaveExists(fitValues))
		DoAlert 0, "Default fit value wave does not exist. Fit first!"
		return ""
	endif
	Edit /N=$tableName fitValues.ld as title
	SetWindow $tableName, hook(plotTau)=plotTausFromTable
	
	return tableName
End

static Function plotTaus(fitValues, [type])
	Wave fitValues
	String type
	
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "line"
	endif
	Variable result = 0
	if (!WaveExists(fitValues))
		return result
	endif
	
	String fitLabels = makeLblListofWave(fitValues, dim=1)
	if (WhichListItem("tau", fitLabels) < 0)
		print "** ERROR (plotTaus): no 'tau' dimension label found in the fit value wave '", NameOfWave(fitValues), "'."
		return result
	endif
	
	SVAR lastTauName = $(getPackageStr(ksLastTauName, defaultStr=ksSimpleTaus))
	SVAR lastLblName = $(getPackageStr(ksLastTauLblName, defaultStr=(ksSimpleTaus + "_lbl")))
	
	String tauName = lastTauName
	String tauLblName = lastLblName	
	Prompt tauName, "Please choose a name for the tau values"
	Prompt tauLblName, "Please choose a name for categories of the taus"
	
	strswitch (type)
		case "line":
			DoPrompt "Wave Namme Chooser", tauName
			break
		case "box":
		case "category":
			DoPrompt "Wave Name Chooser", tauName, tauLblName
			break
		default:
			return result
	endswitch
	
	if (V_flag)
		// user clicked cancel
		return result
	endif
	
	String wavenames = ""
	strswitch (type)
		case "box":
		case "category":
			Make /N=(DimSize(fitValues, 0)) /O/T $tauLblName /WAVE=tauLbls
			tauLbls = GetDimLabel(fitValues, 0, p)
			wavenames = AddListItem(tauLblName, wavenames)
		case "line":
			Make /N=(DimSize(fitValues, 0)) /O $tauName /WAVE=taus
			taus = fitValues[p][%tau]
			wavenames = AddListItem(tauName, wavenames)
			result = 1
	endswitch
	
	String graphName = GH_getGraphFromWaveList(wavenames)
	if (strlen(graphName) == 0)
		Display
		graphName = S_name
	else
		graphName = StringFromList(0, graphName)
		DoWindow /F $graphName
		return result
	endif
	
	strswitch (type)
		case "box":
		case "category":
			AppendToGraph /W=$graphName /C=(0,0,0) taus vs tauLbls
			ModifyGraph /W=$graphName hbFill=5
			break
		case "line":
			AppendToGraph /W=$graphName /C=(0,0,0) taus
			ModifyGraph /W=$graphName mode=4, marker=19
			break
	endswitch	
	ModifyGraph /W=$graphName standoff=0
	Label left "\\[0\\F'Symbol'\\Z14t \\F]0\\Z]0(s)"
	SetAxis/A/N=1/E=1 left
	SetAxis/A/N=1/E=1 bottom
	
	return result
End

static Function SumWaves(theList, sumwave, [x1, x2])
	String theList, sumwave
	Variable x1, x2
	
	if (strlen(theList) == 0 || ItemsInList(theList) == 0)
		print "** ERROR (SumWaves): no waves in list."
		return 0
	endif
	Make /N=(ItemsInList(theList)) /O $sumWave /WAVE=theSum
	
	String dimLabel = ""
	Variable wnAreSame = checkWaveNamesSame(theList)
	Variable index
	for (index = 0; index < ItemsInList(theList); index += 1)
		String fullPath = StringFromList(index, theList)
		Wave data = $fullPath
		
		if (wnAreSame)
			dimLabel = ParseFilePath(1, fullPath, ":", 1, 0)
		else
			dimLabel = ParseFilePath(3, fullPath, ":", 0, 0)
		endif
		
		if (ParamIsDefault(x1) && ParamIsDefault(x2))
			theSum[index] = sum(data)
		elseif (ParamIsDefault(x1))
			theSum[index] = sum(data, leftx(data), x2)
		elseif (ParamIsDefault(x2))
			theSum[index] = sum(data, x1, rightx(data))
		else
			theSum[index] = sum(data, x1, x2)
		endif
		SetDimLabel 0, index, $dimLabel, theSum
	endfor
End

static Function /WAVE selectPeaks(result, [threshold, xAtZero, negative])
	Wave result
	Variable threshold, xAtZero, negative
	
	negative = ParamIsDefault(negative) || negative < 1 ? 0 : 1
	xAtZero = ParamIsDefault(xAtZero) || xAtZero > 0 ? 1 : 0
	
	Variable selectPeaks = 0
	if (!ParamIsDefault(threshold))
		selectPeaks = 1
	endif

	if (negative)
		result[][%peakVal, %peakAmp] *= -1
	endif

	Variable index = DimSize(result, 0)
	if (index == 0)
		return result
	endif
	
	do
		if (result[index][%peakAmp] < 0 || (selectPeaks && result[index][%peakAmp] < threshold))
			DeletePoints index, 1, result
		endif
		index -= 1
	while (index >= 0) 

	if (xAtZero)
		// this makes only sense when the stimulus comes at x = 0 so technically the first peak 
		//    can not have a negative start time
		if (result[0][%peakStart] < 0)
			result[0][%peakStart] = 0
		endif
	endif
	
	if (negative)
		result[][%peakVal, %peakAmp] *= -1
	endif

	return result
End

// ******************************************************************************
// ****                   P U B L I C    F U N C T I O N S
// ******************************************************************************

Function /S SiST_getLastStartBin()
	return getPackageStr(ks_defStartBin, defaultStr="auto")
End

Function /S SiST_getLastBinSize()
	return getPackageStr(ks_defBinSize, defaultStr="auto")
End

Function /S SiST_getLastNumBins()
	return getPackageStr(ks_defNumBins, defaultStr="auto")
End

Function /S SiST_getLastMeanName()
	return getPackageVar(ks_defAvgName)
End

Function /S SiST_getLastSDName()
	return getPackageVar(ks_defSDName)
End

Function /S SiST_getLastSEMName()
	return getPackageVar(ks_defSEMName)
End

Function /S SiST_getLastErrType()
	return getPackageVar(ks_defType)
End

Function /S SiST_getLastHistName()
	return getPackageStr(ks_defHistName, defaultStr=ks_defHistValue)
End

Function /S SiST_getSimpleFitTable([type])
	String type
	
	if (ParamIsDefault(type))
		type = ""
	endif
	strswitch(type)
		case "combined":
			return SiSt_getMatrixStatsTable(ksCombinedFitTable, ksCombinedFitValues, title=ksCombinedFitTableTitle)
			break
		case "current":
		default:
			return SiSt_getMatrixStatsTable(ksSimpleFitTable, ksSimpleFitValues, title=ksSimpleFitTableTitle)
	endswitch
End

Function /S SiSt_getMaxAreaTable()
	DoWindow /F $ksDescriptiveTable
	if (V_flag)
		return ksDescriptiveTable
	endif
	
	DFREF package = getPackage()
	Wave maxValues = package:$ksDescMeanValues
	Wave areaValues = package:$ksDescAreaValues
	if (!WaveExists(maxValues) || !WaveExists(areaValues))
		DoAlert 0, "One of the default value waves does not exist. Calculate the values first!"
		return ""
	endif
	Edit /N=$ksDescriptiveTable maxValues.ld, areaValues
	return ksDescriptiveTable
End

ThreadSafe Function CFit_SimplePrototype(summary, coefWave, yData, fitWave, startP, endP, threshold, repeats)
	Wave summary, coefWave, yData, fitWave
	Variable startP, endP, threshold, repeats
	
End

ThreadSafe Function CFit_ExpDecay_Driver(summary, coefWave, yData, fitWave, startP, endP, threshold, repeats)
		Wave summary, coefWave, yData, fitWave
		Variable startP, endP, threshold, repeats
		
		if (startP < 0)
			startP = 0
		endif
		if (endP < 0 || endP >= numpnts(yData))
			endP = numpnts(yData) - 1
		endif
		if (startP > endP)
			coefWave = 0
			return 0
		endif
		if (threshold < 0)
			threshold = 0
		endif
		repeats = repeats < 0 ? 0 : round(repeats)
		Variable steps = 0
		
		String waveLabel = NameOfWave(yData)
		summary[%$waveLabel][%threshold] = threshold
		summary[%$waveLabel][%endP] = endP
		
		do
			CurveFit /NTHR=0/N/Q/W=2 exp_XOffset, kwCWave=coefWave, yData[startP, endP] /D=fitWave
			startP += 1
			steps += 1
		while( V_chisq > threshold && steps < repeats + 1 && startP < endP)
		startP -= 1
		summary[%$waveLabel][%chisq] = V_chisq
		summary[%$waveLabel][%startP] = startP
		summary[%$waveLabel][%steps] = steps

		summary[%$waveLabel][%y0] = coefWave[0]
		summary[%$waveLabel][%A] = coefWave[1]
		summary[%$waveLabel][%tau] =coefWave[2]

		Wave W_sigma
		summary[%$waveLabel][%y0_sig] = W_sigma[0]
		summary[%$waveLabel][%A_sig] = W_sigma[1]
		summary[%$waveLabel][%tau_sig] = W_sigma[2]
		WaveClear W_sigma
		
		if (startP > 0)
			fitWave[0, startP-1] = NaN
		endif
		if (endP < numpnts(yData) - 1)
			fitWave[endP + 1, numpnts(fitWave)-1] = NaN
		endif
		
		WaveClear coefWave
		WaveClear fitWave
		WaveClear summary
		return 0
End

//The function calculates the average and the standard deviation of the points of several 
//waves. Two output waves are created to store the mean and the standard deviation. The waves 
//are provided through a list taken from the top graph or table. The macro just passes the
//parameters and calls the function.
Function /S GetMeanPerPoint([newwave, stdwave, semwave, type, direction, statsType, inGraph, hAxis, vAxis, traces, ask])
	String newwave, stdwave, semwave, type, direction, statsType, inGraph, hAxis, vAxis, traces
	Variable ask
	
	if (ParamIsDefault(statsType) || strlen(statsType) == 0)
		statsType = "arithmetic mean"
	endif
	if (ParamIsDefault(inGraph) || strlen(inGraph) == 0)
		inGraph = "_new_"
	endif

	ask = ParamIsDefault(ask) || ask < 1 ? 0 : 1
	
	DFREF package = getPackage()
	SVAR sumName = package:$ks_defSumName
	if (!SVAR_Exists(sumName))
		String /G package:$ks_defSumName = "SUM_"
		SVAR sumName = package:$ks_defSumName
	endif
	SVAR avgName = package:$ks_defAvgName
	if (!SVAR_Exists(avgName))
		String /G package:$ks_defAvgName = "MEAN_"
		SVAR avgName = package:$ks_defAvgName
	endif
	SVAR sdName = package:$ks_defSDName
	if (!SVAR_Exists(sdName))
		String /G package:$ks_defSDName = "STDV_"
		SVAR sdName = package:$ks_defSDName
	endif
	SVAR semName = package:$ks_defSEMName
	if (!SVAR_Exists(semName))
		String /G package:$ks_defSEMName = "SEM_"
		SVAR semName = package:$ks_defSEMName
	endif
	SVAR defType = package:$ks_defType
	if (!SVAR_Exists(defType))
		String /G package:$ks_defType = "SEM"
		SVAR defType = package:$ks_defType
	endif
	SVAR definGraph = package:$ks_definGraph
	if (!SVAR_Exists(definGraph))
		String /G package:$ks_definGraph = inGraph
		SVAR definGraph = package:$ks_definGraph
	endif
	SVAR defhAxis = package:$ks_defhAxis
	if (!SVAR_Exists(defhAxis))
		String /G package:$ks_defhAxis = "bottom"
		SVAR defhAxis = package:$ks_defhAxis
	endif
	SVAR defvAxis = package:$ks_defvAxis
	if (!SVAR_Exists(defvAxis))
		String /G package:$ks_defvAxis = "left"
		SVAR defvAxis = package:$ks_defvAxis
	endif
	
	
	String resultList = ""
	// Variable promptFlag
	
	strswitch (statsType)
		case "sum":
			if (ParamIsDefault(newwave) || strlen(newwave) == 0)
				newwave = sumName
				ask = 1
			endif
			if (ParamIsDefault(direction) || strlen(direction) == 0)
				direction = "normal"
			endif
			
			Prompt newwave, "enter wavename for means: "
			Prompt direction, "select the direction for sum:", popup, "normal;reverse"
			
			if (ask)
				DoPrompt "Get Sums Per Point", newwave, direction
				if (V_flag)
					print "-> user canceled operation."
					return resultList
				endif
				sumName = newwave
			endif
			break
		case "arithmetic mean":
		default:
			if(ParamIsDefault(newwave) || strlen(newwave) == 0)
				newwave = avgName
				ask = 1
			endif
			if (ParamIsDefault(stdwave) && ParamIsDefault(semwave))
				ask = 1
			endif
			if (ParamIsDefault(stdwave) || strlen(stdwave) == 0)
				stdwave = sdName
			endif
			if (ParamIsDefault(semwave) || strlen(semwave) == 0)
				semwave = semName
			endif
			if (ParamIsDefault(type) || strlen(type) == 0)
				type = defType
			endif
			if (ParamIsDefault(direction) || strlen(direction) == 0)
				direction = "normal"
			endif
			String flexString = "_none_;_new_;" + WinList("*", ";", "WIN:1")
			if (WhichListItem(definGraph, flexString) >= 0 && (ParamIsDefault(inGraph) || strlen(inGraph) == 0))
				inGraph = defInGraph
			endif
			if (ParamIsDefault(traces) || strlen(traces) == 0)
				traces = "no"
			endif
			if (ParamIsDefault(hAxis) || strlen(hAxis) == 0)
				hAxis = defhAxis
			endif
			if (ParamIsDefault(vAxis) || strlen(vAxis) == 0)
				vAxis = defvAxis
			endif
			
			Prompt newwave, "enter wavename for means: "
			Prompt stdwave, "enter wavename for standard deviation: "
			Prompt semwave, "enter wavename for standard error of mean: "
			Prompt type, "select error values: ", popup, "_none_;SD + SEM;SD;SEM"
			Prompt direction, "select the direction for average:", popup, "normal;reverse"
			Prompt inGraph, "select graph", popup, "_none_;_new_;" + WinList("*", ";", "WIN:1")
			Prompt hAxis, "select horiz. axis:"
			Prompt vAxis, "select vert. axis:"
			if (ParamIsDefault(traces))
				Prompt traces, "choose if you want to display as traces", popup, "yes;no"
			endif
			
			if (ask)
			
				if (ParamIsDefault(traces))
					DoPrompt "Get Means Per Point", newwave, stdwave, semwave, type, direction, inGraph, hAxis, vAxis, traces
				else
					DoPrompt "Get Means Per Point", newwave, stdwave, semwave, type, direction, inGraph, hAxis, vAxis
				endif
				if (V_flag)
					print "-> user canceled operation on", date(), time()
					return resultList
				endif
				avgName = newwave
				sdName = stdwave
				semName = semwave
				defType = type
				definGraph = inGraph
				defhAxis = hAxis
				defvAxis = vAxis
			endif
	endswitch
	
	Variable rev = cmpstr(direction, "reverse") == 0

	string Liste = TraceNameList("",";",5)
	string graphName = StringFromList(0, WinList("*", ";", "WIN:"))
	if (WinType(graphName) != 1)
		string theWindowName = graphName
		string msg = "The top window  '" + theWindowName + "'  is not a graph!"
		graphName = StringFromList(0, WinList("*", ";", "WIN:1"))
		if (strlen(graphName) > 0)
			DoAlert 1, msg + "\rDo you want to continue with the top graph  '" + graphName + "' ?"
			if (V_flag == 0)
				print "-> wrong top graph, user canceled."
				return resultList
			endif
		else
			DoAlert 0, msg + "\rPlease collect the waves in a graph window and try again."
			print "-> no graph available. Operation canceled on", date(), time()
			return resultList
		endif
	endif

	strswitch(statsType)
		case "sum":
			resultList = CalcSumPerPoint(Liste, newwave, theGraph=graphName, rev=rev)
			print "-> summed " + num2str(ItemsInList(Liste)) + " waves from graph " + graphName + " into wave " + newwave + "; rev = " + num2str(rev) + "."
			break
		case "arithmetic mean":
		default:
			resultList = CalcMeanPerPoint(Liste, newwave, stdwave=stdwave, semwave=semwave, type=type, theGraph=graphName, rev=rev)
			print "-> averaged " + num2str(ItemsInList(Liste)) + " waves from graph " + graphName + " into " + newwave + " (avg), " + stdwave + " (SD), " + semwave + " (SEM); rev = " + num2str(rev) + "."
			if (cmpstr(inGraph, "_none_") != 0)
				if (cmpstr(inGraph, "_new_") == 0)
					Display
					inGraph = S_name
				endif
				Wave theMean = $StringFromList(0, resultList)
				if (!WaveExists(theMean))
					print "** ERROR: can't find the mean wave '", StringFromList(0, resultList), "'. No display"
					return resultList
				endif
				DoWindow $inGraph
				if (V_flag)
					Wave error = $StringFromList(1, resultList)
					strswitch( LowerStr(traces) )
						case "no":
							if (cmpstr("left", LowerStr(vAxis)) == 0)
								if (cmpstr("bottom", LowerStr(hAxis)) == 0)	
									AppendToGraph /W=$inGraph theMean
								else
									AppendToGraph /W=$inGraph /B=$hAxis theMean
								endif
							elseif (cmpstr("right", LowerStr(vAxis)) == 0)
								if (cmpstr("bottom", LowerStr(hAxis)) == 0)	
									AppendToGraph /W=$inGraph /R theMean
								else
									AppendToGraph /W=$inGraph /R /B=$hAxis theMean
								endif
							else
								if (cmpstr("bottom", LowerStr(hAxis)) == 0)
									AppendToGraph /W=$inGraph /L=$vAxis theMean
								else
									AppendToGraph /W=$inGraph /L=$vAxis /B=$hAxis theMean
								endif
							endif

							if ( WaveExists(error) )
								ModifyGraph /W=$inGraph mode($NameOfWave(theMean))=4, marker($NameOfWave(theMean))=19
								ErrorBars /W=$inGraph $NameOfWave(theMean), Y wave=($NameOfWave(error), $NameOfWave(error))
							endif
							break
						case "yes":
							DFREF displayFolder = getErrDisplayFolder()
							if ( WaveExists(error) )
								SetScale /P x leftx(theMean), deltax(theMean), "", error
								Duplicate /O error, displayFolder:$(NameOfWave(error) + "_hi") /WAVE=hi, displayFolder:$(NameOfWave(error) + "_lo") /WAVE=lo
								hi = theMean + error
								lo = theMean - error
								
								if (cmpstr("left", LowerStr(vAxis)) == 0)
									if (cmpstr("bottom", LowerStr(hAxis)) == 0)	
										AppendToGraph /W=$inGraph hi, lo
									else
										AppendToGraph /W=$inGraph /B=$hAxis hi, lo
									endif
								else
									if (cmpstr("bottom", LowerStr(hAxis)) == 0)
										AppendToGraph /W=$inGraph /L=$vAxis hi, lo
									else
										AppendToGraph /W=$inGraph /L=$vAxis /B=$hAxis hi, lo
									endif
								endif
								ModifyGraph /W=$inGraph mode($NameOfWave(hi))=7, rgb($NameOfWave(hi))=(52428,52428,52428)
								ModifyGraph /W=$inGraph hbFill($NameOfWave(hi))=2, toMode($NameOfWave(hi))=1
								ModifyGraph /W=$inGraph rgb($NameOfWave(lo))=(52428,52428,52428)
							endif
							if (cmpstr("left", LowerStr(vAxis)) == 0)
								if (cmpstr("bottom", LowerStr(hAxis)) == 0)	
									AppendToGraph /W=$inGraph theMean
								else
									AppendToGraph /W=$inGraph /B=$hAxis theMean
								endif
							else
								if (cmpstr("bottom", LowerStr(hAxis)) == 0)
									AppendToGraph /W=$inGraph /L=$vAxis theMean
								else
									AppendToGraph /W=$inGraph /L=$vAxis /B=$hAxis theMean
								endif
							endif
							ModifyGraph /W=$inGraph rgb($NameOfWave(theMean))=(0,0,0)
							
							Variable index
							String udata = GetUserData(inGraph, "", "")
							String meanList = ""
							if (strlen(udata) > 0)
								meanList = StringByKey("MEANS", udata)
								if (strlen(meanList) > 0)
									for (index = 0; index < ItemsInList(meanList, ","); index += 1)
										String toSort = StringFromList(index, meanList, ",")
										String anchor = NameOfWave(theMean)
										ReorderTraces /W=$inGraph $NameOfWave(theMean), {$StringFromList(index, meanList, ",")}
									endfor
								endif
							endif
							
							meanList = AddListItem(NameOfWave(theMean), meanList, ",", Inf)
							udata = ReplaceStringByKey("MEANS", udata, meanList)
							SetWindow $inGraph, userdata=udata
					endswitch
				endif
			endif
	endswitch
	return resultList
end

// A function to calculate the mean, standard deviation and standard error of the 
// waves in the list.
// This mean is a mean of for each point of the waves
// type			""			DEFAULT = calculates SEM and SD
// type			"sd"		calculates only SD
// type 			"sem"		calculates only SEM
// type			"_none_"  do not calculate the error
// 
// rev         0        DEFAULT, uses the waves as is, 1 reverses them
function /S CalcMeanPerPoint(theList, meanwave, [stdwave, semwave, type, theGraph, rev])
	String theList, meanwave, stdwave, semwave, type, theGraph
	Variable rev
	
	String theWaveName, loggtext, resultList
	Variable maxpoints = 0
	Variable index = 0, points = 0
	String theNote = "", meanNote = ""
	
	if (ParamIsDefault(type))
		type = ""
	endif
	if (ParamIsDefault(stdwave))
		stdwave = meanwave[0,27] + "_std"
	endif
	if (ParamIsDefault(semwave))
		semwave = meanwave[0,27] + "_sem"
	endif
	if (ParamIsDefault(theGraph))
		theGraph = ""
	endif
	if (ParamIsDefault(rev))
		rev = 0
	else
		if (rev < 0)
			rev = 0
		endif
		if (rev > 1)
			rev = 1
		endif
		rev = floor(rev)
	endif
	
	String dataUnit = "", xunit = ""
	Variable xD = 0, scaleStart = 0, skipError = 0
	
	theNote = ReplaceNumberByKey(ks_SIST_numWaves, theNote, ItemsInList(theList))
	theNote = ReplaceStringByKey(ks_SIST_waveNameList, theNote, ReplaceString(";", theList, ","))
	theNote = ReplaceStringByKey(ks_SIST_statsType, theNote, "average")
	
	Make /N=0 /O $meanwave
	Wave theMeans = $meanwave

	// decide which waves to make
	strswitch (LowerStr(type))
		case "_none_":
			skipError = 1
			resultList = meanwave + ";"
			break
		case "sd":
			Make /N=0 /O $stdwave
			resultList = meanwave + ";" + stdwave + ";"
			break
		case "sem":
			Make /N=0 /O $semwave
			resultList = meanwave + ";" + semwave + ";"
			break
		default:
			Make /N=0 /O $stdwave
			Make /N=0 /O $semwave
			resultList = meanwave + ";" + stdwave + ";" + semwave + ";"
	endswitch

	// if they don't exist, they are just null
	Wave STD = $stdwave
	Wave SEM = $semwave
	
	do							//find out how many points are needed
			// in case we have waves with different lengths
		theWaveName = StringFromList(index, theList)
		if(strlen(theWaveName) == 0)
			break
		endif
		
		if (strlen(theGraph) == 0)
			Wave theWave = $theWaveName
		else
			Wave theWave = TraceNameToWaveRef(theGraph, theWaveName)
		endif
		
		if (rev)
			Reverse theWave
		endif
		
		if(maxpoints < numpnts(theWave))
			maxpoints = numpnts(theWave)
		endif
		index +=1
	while(1)
	
	index = 0
	
	do
		Make /O /N=0 temp

		for (points = 0; points < ItemsInList(theList); points += 1)
			theWaveName = StringFromList(points, theList)
			
			if (strlen(theGraph) == 0)
				Wave theWave = $theWaveName
			else
				Wave theWave = TraceNameToWaveRef(theGraph, theWaveName)
			endif
			
			if (points == 0)
				dataUnit = WaveUnits(theWave, 1)
				xUnit = WaveUnits(theWave, 0)
				xD = deltax(theWave)
				scaleStart = leftx(theWave)
			endif
			
			if (index >= numpnts(theWave))
				continue
			endif
			
			temp[numpnts(temp)] = {theWave[index]}
		endfor
		
		
		WaveStats /C=1 /W /Q temp
		Wave M_WaveStats
		
		theMeans[index] = {M_WaveStats[%avg]}
		meanNote = ReplaceNumberByKey(num2str(index), meanNote, M_WaveStats[%numPoints], "=", ",")
		
		strswitch (LowerStr(type))
			case "_none_":
				break
			case "sd":
				STD[index] = {M_WaveStats[%sdev]}
				break
			case "sem":
				SEM[index] = {numtype(M_WaveStats[%sdev]) == 2 ? NaN : M_WaveStats[%sdev] / sqrt(M_WaveStats[%numPoints])}
				break
			default:
				STD[index] = {M_WaveStats[%sdev]}
				SEM[index] = {numtype(M_WaveStats[%sdev]) == 2 ? NaN : M_WaveStats[%sdev] / sqrt(M_WaveStats[%numPoints])}
		endswitch
		index += 1
	while(index < maxpoints)
	
	SetScale d 0, 0, "mean " + dataUnit, theMeans
	SetScale /P x scaleStart, xD, xUnit, theMeans

	theNote = ReplaceStringByKey(ks_SIST_valuePPnt, theNote, meanNote)
	Note /K theMeans, theNote
	
	// check if any of the error calculations resulted in any NaNs. If so replace them by zeros
	if (!skipError)
		if (WaveExists(STD))
			STD = numtype(STD[p]) == 2 ? 0 : STD[p]
		endif
		if (WaveExists(SEM))
			SEM = numtype(SEM[p]) == 2 ? 0 : SEM[p]
		endif
	endif
		
	if (rev)
		index = 0
		do
			theWaveName = StringFromList(index, theList)
		
			if (strlen(theGraph) == 0)
				Wave theWave = $theWaveName
			else
				Wave theWave = TraceNameToWaveRef(theGraph, theWaveName)
			endif
		
			Reverse theWave
		
			index +=1
		while(index < ItemsInList(theList))
		Reverse theMeans
		if (!skipError)
			if (WaveExists(STD))
				Reverse /P STD
			endif
			if (WaveExists(SEM))
				Reverse /P SEM
			endif
		endif
	endif
	
	
	killwaves/Z M_WaveStats, temp
	return resultList
end

// calculates the sum of each point in a list of waves
function /S CalcSumPerPoint(theList, sumwave, [theGraph, rev])
	String theList, sumwave, theGraph
	Variable rev
	
	String theWaveName, loggtext, resultList, valuesPPoint = ""
	String theNote = ""
	Variable maxpoints = 0
	Variable index = 0, points = 0
	
	if (ParamIsDefault(theGraph))
		theGraph = ""
	endif
	if (ParamIsDefault(rev))
		rev = 0
	else
		if (rev < 0)
			rev = 0
		endif
		if (rev > 1)
			rev = 1
		endif
		rev = floor(rev)
	endif
	
	String dataUnit = "", xunit = ""
	Variable xD = 0, scaleStart = 0
	
	theNote = ReplaceNumberByKey(ks_SIST_numWaves, theNote, ItemsInList(theList))
	theNote = ReplaceStringByKey(ks_SIST_waveNameList, theNote, ReplaceString(";", theList, ","))
	theNote = ReplaceStringByKey(ks_SIST_statsType, theNote, "sum")
	
	Make /N=0 /O $sumwave
	Wave theSums = $sumwave

	resultList = sumwave
	
	do							//find out how many points are needed
			// in case we have waves with different lengths
		theWaveName = StringFromList(index, theList)
		if(strlen(theWaveName) == 0)
			break
		endif
		
		if (strlen(theGraph) == 0)
			Wave theWave = $theWaveName
		else
			Wave theWave = TraceNameToWaveRef(theGraph, theWaveName)
		endif
		
		if (rev)
			Reverse theWave
		endif
		
		if(maxpoints < numpnts(theWave))
			maxpoints = numpnts(theWave)
		endif
		index +=1
	while(1)
	
	index = 0
	
	do
		Make /O /N=0 temp

		for (points = 0; points < ItemsInList(theList); points += 1)
			theWaveName = StringFromList(points, theList)
			
			if (strlen(theGraph) == 0)
				Wave theWave = $theWaveName
			else
				Wave theWave = TraceNameToWaveRef(theGraph, theWaveName)
			endif
			
			if (points == 0)
				dataUnit = WaveUnits(theWave, 1)
				xUnit = WaveUnits(theWave, 0)
				xD = deltax(theWave)
				scaleStart = leftx(theWave)
			endif
			
			if (index >= numpnts(theWave))
				continue
			endif
			
			temp[numpnts(temp)] = {theWave[index]}
		endfor
		
		
		WaveStats /C=1 /W /Q temp
		Wave M_WaveStats
		
		theSums[index] = {M_WaveStats[%sum]}
		valuesPPoint = ReplaceNumberByKey(num2str(index), valuesPPoint, M_WaveStats[%numPoints], "=", ",")
		index += 1
	while(index < maxpoints)
	
//	SetScale d 0, 0, "mean " + dataUnit, theSums
	SetScale /P x scaleStart, xD, xUnit, theSums
	theNote = ReplaceStringByKey(ks_SiSt_valuePPnt, theNote, valuesPPoint)
	Note /K theSums, theNote
		
	if (rev)
		index = 0
		do
			theWaveName = StringFromList(index, theList)
		
			if (strlen(theGraph) == 0)
				Wave theWave = $theWaveName
			else
				Wave theWave = TraceNameToWaveRef(theGraph, theWaveName)
			endif
		
			Reverse theWave
		
			index +=1
		while(index < ItemsInList(theList))
		Reverse theSums
	endif
	
	
	killwaves/Z M_WaveStats, temp
	return resultList
end


Function SiSt_SumWavesFromGraph([theGraph, theWaveName, x1, x2, ask])
	String theGraph, theWaveName
	Variable x1, x2, ask
	
	if (ParamIsDefault(ask) || ask >= 1)
		ask = 1
	else
		ask = ask <= 0 ? 0 : 1
	endif
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	
	String aCsrTrace = "", bCsrTrace = ""
	Variable aCsrExists = 0, bCsrExists = 0
	Variable haveStart = 0, haveEnd = 0
	String theWindow = "", dispType = ""
	
	SVAR selectGraph = $(getPackageStr(ks_sum2SelectGraph, defaultStr = "_top_graph_"))
	SVAR sumName = $(getPackageStr(ks_Sum2Name, defaultStr = "SUM_of_Waves"))
	SVAR inWin = $(getPackageStr(ks_inWin, defaultStr = "_new_"))
	SVAR type = $(getPackageStr(ks_winType, defaultStr = "table"))
	NVAR startX = $(getPackageVar(ks_sum2StartXName))
	NVAR endX = $(getPackageVar(ks_sum2EndXName))

	String currentGraphList = WinList("*", ";", "WIN:1")
	String topGraph = StringFromList(0, currentGraphList)
	String currentWinList = WinList("*", ";", "WIN:3")
	
	theWindow = inWin
	dispType = type
	
	Prompt theGraph, "Choose a graph", popup, "_top_graph_;" + currentGraphList
	Prompt theWaveName, "Please enter the name of the result wave"
	Prompt x1, "Start x value"
	Prompt x2, "End x value"
	Prompt theWindow, "Choose a window to show result", popup, "_none_;_new_;_same_;" + currentWinList
	Prompt dispType, "Choose a display type", popup, "table;graph;"

	if (ask)
		if (ParamIsDefault(theGraph) || strlen(theGraph) == 0)
			theGraph = selectGraph
		endif
		if (ParamIsDefault(theWaveName) || strlen(theWaveName) == 0)
			theWaveName = sumName
		endif
		
		DoPrompt "Choose Values for Summation", theGraph, theWaveName, theWindow, dispType

		if (V_flag)
			// user clicked cancel
			print "-- user canceled summation."
			return 0
		endif
		
		selectGraph = theGraph
		inWin = theWindow
		type = dispType
		
		if (cmpstr(theGraph, "_top_graph_") == 0)
			theGraph = topGraph
		endif
		
		sumName = theWaveName
		
		aCsrTrace = StringByKey("TNAME", CsrInfo(A, theGraph))
		aCsrExists = strlen(aCsrTrace) > 0
		bCsrTrace = StringByKey("TNAME", CsrInfo(B, theGraph))
		bCsrExists = strlen(bCsrTrace) > 0
		
		if (ParamIsDefault(x1))
			if (aCsrExists)
				x1 = xcsr(A, theGraph)
				haveStart = 1
			endif
		else
			haveStart = 1
		endif
		
		if (ParamIsDefault(x2))
			if (bCsrExists)		
				x2 = xcsr(B, theGraph)
				haveEnd = 1
			endif
		else
			haveEnd = 1
		endif
		
	else
		if (ParamIsDefault(theGraph) || strlen(theGraph) == 0)
			theGraph = StringFromList(0, currentGraphList)
		endif	
		
		strswitch (theGraph)
			case "_top_graph_":
			case "topgraph":
			case "top":
				theGraph = StringFromList(0, currentGraphList)
				break
		endswitch
		
		if (strlen(theGraph) == 0)
			print "-- ERROR (SiSt_SumWavesFromGraph): need a graph to work on and can't find one, so stopped here!"
			return 0
		endif

		inWin = "_none_"
		
		aCsrExists = strlen(StringByKey("TNAME", CsrInfo(A, theGraph))) > 0
		bCsrExists = strlen(StringByKey("TNAME", CsrInfo(B, theGraph))) > 0
		
	
		if (ParamIsDefault(theWaveName) || strlen(theWaveName) == 0)
			theWaveName = sumName
		else
			sumName = theWaveName
		endif

		if (ParamIsDefault(x1) && ParamIsDefault(x2))
			if (aCsrExists)
				x1 = xcsr(A, theGraph)
				haveStart = 1
			endif
			if (bCsrExists)
				x2 = xcsr(B, theGraph)
				haveEnd = 1
			endif
		elseif (ParamIsDefault(x1) && aCsrExists)
			x1 = xcsr(A, theGraph)
			haveStart = 1
			haveEnd = 1
		elseif (ParamIsDefault(x2) && bCsrExists)
			x2 = xcsr(B, theGraph)
			haveEnd = 1
			haveStart = 1
		elseif (ParamIsDefault(x1))
			haveEnd = 1
		elseif (ParamIsDefault(x2))
			haveStart = 1
		endif
	endif
	
	String path, theName, response = ""
	String theList = FullTraceListFromGraph(theGraph)
	Variable numTraces = ItemsInList(theList)
	if (numTraces == 0)				
		return 0
	endif
	if (haveStart && haveEnd)
		SumWaves(theList, theWaveName, x1=x1, x2=x2)
		sprintf response, "--> summed %g traces from '%s' and put into %s\r--- range: %g to %g\r", numTraces, theGraph, theWaveName, x1, x2
	elseif (haveStart && !haveEnd)			
		SumWaves(theList, theWaveName, x1=x1)
		sprintf response, "--> summed %g traces from '%s' and put into %s\r--- range: %g to trace end\r", numTraces, theGraph, theWaveName, x1
	elseif (!haveStart && haveEnd)
		SumWaves(theList, theWaveName, x2=x2)
		sprintf response, "--> summed %g traces from '%s' and put into %s\r--- range: start of trace to %g\r", numTraces, theGraph, theWaveName, x2
	else
		SumWaves(theList, theWaveName)
		sprintf response, "--> summed %g traces from '%s' and put them into %s\r--- range: full trace length\r", numTraces, theGraph, theWaveName
	endif
	print response
	
	Wave summation = $theWaveName
	
	if (cmpstr(theWindow, "_none_") != 0)
		strswitch ( LowerStr(dispType) )
			case "graph":
				if (cmpstr(theWindow, "_new_") == 0)
					GH_plotWaveOnGraph(summation)
				elseif ( cmpstr(theWindow, "_same_") == 0 )
					GH_plotWaveOnGraph(summation, theWin=theGraph)
				else
					GH_plotWaveOnGraph(summation, theWin=inWin)
				endif
				break
			case "table":
			default:			
				if (cmpstr(theWindow, "_new_") == 0)
					TH_showInTable(theWaveName, labels=1)
				elseif ( cmpstr(theWindow, "_same_") == 0 )
					TH_showInTable(theWaveName, theName=theGraph, labels=1)
				else
					TH_showInTable(theWaveName, theName=inWin, labels=1)
				endif
		endswitch
	endif
	
	return 1
End

Function MeanOfWavesInGraph([theWaveName, zeros, error])
	String theWaveName, error
	Variable zeros
	
	Prompt theWaveName, "Please enter the name of the result wave"
	Prompt zeros, "Please enter 0 or 1 to consider or exclude zeros"
	Prompt error, "Please choose the error reporting", popup, "sem;sd"
	
	String graphList = WinList("*", ";", "WIN:1")
	if (ItemsInList(graphList) == 0)
		print "-- ERROR (MeanOfWavesInGraph): need a graph to work with, can't find one, so stopped here."
		return 0
	endif		
	
	String currentWinList = WinList("*", ";", "WIN:3")
	
	SVAR selectGraph = $(getPackageStr(ks_avgInGraphName, defaultStr = "_top_graph_"))
	SVAR summaryName = $(getPackageStr(ks_avgWavesName, defaultStr = "MEAN_of_Waves"))
	SVAR inWin = $(getPackageStr(ks_inWin, defaultStr = "_new_"))
	SVAR type = $(getPackageStr(ks_winType, defaultStr = "graph"))
	
	String theGraph = selectGraph
	Prompt theGraph, "Please choose a graph", popup, "_top_graph_;" + graphList
	
	String theWin = inWin
	Prompt theWin, "Please choose a display for the result", popup, "_none_;_new_;_same_;" + currentWinList
	String theType = type
	Prompt theType, "Please choose a display type", popup, "table;graph"
	
	if (ParamIsDefault(theWaveName) || strlen(theWaveName) == 0)
		theWaveName = summaryName
	endif
	
	DoPrompt "Choose Parameters for Mean Calculation", theGraph, theWaveName, zeros, error, theWin, theType
	if (V_flag)
		// user clicked cancel
		print "-- user aborted the mean calculation."
		return 0
	endif
	
	selectGraph = theGraph
	inWin = theWin
	type = theType
	
	if (cmpstr(theGraph, "_top_graph_") == 0)
		theGraph = StringFromList(0, graphList)
	endif
	summaryName = theWaveName
		
	Variable aCsrExists = strlen(StringByKey("TNAME", CsrInfo(A, theGraph))) > 0
	Variable bCsrExists = strlen(StringByKey("TNAME", CsrInfo(B, theGraph))) > 0
	
	Variable start = 0, numpoints = 0
	if (aCsrExists)
		start = pcsr(A, theGraph)
		if (bCsrExists)
			numPoints = pcsr(B, theGraph) - start + 1
		else
			Wave csrW = CsrWaveRef(A, theGraph)
			numPoints = numpnts(csrW) - start - 1
		endif
	elseif (bCsrExists)
		if (aCsrExists)
			numPoints = pcsr(B, theGraph) - pcsr(A, theGraph) + 1
		else
			numPoints = pcsr(B, theGraph) + 1
		endif
	endif
	
	if (aCsrExists || bCsrExists)
		CalcMeanOfWaves(theWaveName,excludeZeros=zeros,error=error, rangeStart=start, numPoints=numPoints)
	else		
		CalcMeanOfWaves(theWaveName,excludeZeros=zeros,error=error)
	endif
	
	Wave avg = $theWaveName
	Wave errWave = $(theWaveName + "_" + error)
	
	if (cmpstr(theWin, "_none_") != 0)
		strswitch ( LowerStr(theType) )
			case "graph":
				if (cmpstr(theWin, "_new_") == 0)
					GH_plotWaveOnGraph(avg, error=errWave)
				elseif ( cmpstr(theWin, "_same_") == 0 )
					GH_plotWaveOnGraph(avg, error=errWave, theWin=theGraph)
				else
					GH_plotWaveOnGraph(avg, error=errWave, theWin=theWin)
				endif
				break
			case "table":
			default:			
				if (cmpstr(theWin, "_new_") == 0)
					TH_showInTable(theWaveName, labels=1)
				elseif ( cmpstr(theWin, "_same_") == 0 )
					TH_showInTable(theWaveName, theName=theGraph, labels=1)
				else
					TH_showInTable(theWaveName, theName=inWin, labels=1)
				endif
		endswitch
	endif
	
End

Function CalcMeanOfWaves(theWaveName, [theList, theGraphName, excludeZeros, numPoints, last, rangeStart, error, verbose])
	String theWaveName, theList, theGraphName, error
	Variable excludeZeros, numPoints, last, rangeStart, verbose
	
	String feedbackText = "using "
	
	if (ParamIsDefault(theGraphName) || strlen(theGraphName) == 0)
		theGraphName = StringFromList(0, WinList("*", ";", "WIN:1"))
	endif
	if (WhichListItem(theGraphName, WinList("*", ";", "WIN:1")) < 0)
		print "** ERROR (CalcMeanOfWaves): a graph with the name '" + theGraphName + "' does not exist."
		return 0
	endif
	if (ParamIsDefault(excludeZeros) || excludeZeros < 0)
		excludeZeros = 0
	else
		excludeZeros = excludeZeros >= 1 ? 1 : 0
	endif
	if (ParamIsDefault(rangeStart) || rangeStart < 0)
		rangeStart = 0
	endif
	if (ParamIsDefault(numPoints) || numPoints < 0)
		numPoints = 0
		feedbackText += "all points "
	else
		if (ParamIsDefault(last) || last < 0)
			last = 0
		elseif (last > 1)
			last = 1
		else
			last = abs(round(last))
		endif
		if (last == 1)
			feedbackText += "last " + num2str(numPoints) + " points "
		else
			feedbackText += num2str(numPoints) + " points starting from point " + num2str(rangeStart)
		endif
	endif
	if (ParamIsDefault(error) || strlen(error) == 0)
		error = "sem"
	else
		error = LowerStr(error)
	endif
	if (ParamIsDefault(verbose) || verbose > 0)
		verbose = 1
	else
		verbose = verbose <= 0 ? 0 : 1
	endif

	String theErrorType = ""
	if (ParamIsDefault(theList) || strlen(theList) == 0)
		theList = TraceNameList(theGraphName,  ";", 1)
	endif
	Make /N=(ItemsInList(theList)) /O $(theWaveName[0,31]) /WAVE=means
	strswitch(error)
		case "sd":
		case "sdev":
			Make /N=(ItemsInList(theList)) /O $((theWaveName + "_sd")[0,31]) /WAVE=err
			theErrorType = "standard dev"
			break
		case "sem":
			Make /N=(ItemsInList(theList)) /O $((theWaveName + "_sem")[0,31]) /WAVE=err
			theErrorType = "standard error"
			break
	endswitch
	
	String dimLabel = ""
	Variable wnAreSame = checkWaveNamesSame(theList)
	
	Variable index, point = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		Wave theTrace = TraceNameToWaveRef(theGraphName, StringFromList(index, theList))
		
		if (wnAreSame)
			dimLabel = ParseFilePath(1, GetWavesDatafolder(theTrace, 2), ":", 1, 0)
		else
			dimLabel = NameOfWave(theTrace)
		endif
		
		if (excludeZeros)
			Duplicate /O theTrace, temp
			do
				if (temp[point] == 0)
					DeletePoints point, 1, $(NameOfWave(temp))
				else
					point += 1
				endif
			while (point < numpnts(temp))
			point = 0
			if (numPoints == 0)
				WaveStats /Q /W /C=0 temp
			else
				if (last)
					WaveStats /Q /W /C=0 /R=[(numpnts(theTrace) - numPoints), numpnts(theTrace) -1] temp
				else
					WaveStats /Q /W /C=0 /R=[rangeStart, rangeStart + numPoints - 1] temp
				endif
			endif
		else
			if (numPoints == 0)
				WaveStats /Q /W /C=0 theTrace
			else
				if (last)
					WaveStats /Q /W /C=0 /R=[(numpnts(theTrace) - numPoints), numpnts(theTrace) -1] theTrace
				else
					WaveStats /Q /W /C=0 /R=[rangeStart, rangeStart + numPoints - 1] theTrace
				endif
			endif
		endif
		Wave M_WaveStats
		means[index] = M_WaveStats[%avg]
		strswitch(error)
			case "sd":
			case "sdev":
				err[index] = M_WaveStats[%sdev]
			case "sem":
				err[index] = M_WaveStats[%sem]
				break
		endswitch
		SetDimLabel 0, index, $dimLabel, means
	endfor
	
	if (verbose)
//		print "-> averaged " + num2str(ItemsInList(theList)) + " waves from graph '" + theGraphName + "' into " + theWaveName + "."
		printf "-> averaged %g waves from graph '%s' into %s (%s: %s).\r", ItemsInList(theList), theGraphName, theWaveName, theErrorType, NameOfWave(err)
		print "--> " + feedbackText
		print "--> order: " + theList
	endif
	
	KillWaves /Z temp
	return 1
End



// Calculates the z-score for a list of wave. The waves should have the same length, if not they will be adjusted
// to the shortes wave. 
//
// input:		either one of the optional parameters
// theList:		a semicolon separated string of wave names
// theWindow:	the name of the graph window, which contains the wave
//
// returns
// a wave of wave references with 3 result waves
//				0: a matrix of z-scores calculated along the x axis
//				1: a matrix of z-scores calculated along the y axis
//				2: the x- and y- z-scores added together
Function /WAVE calcZScoreFromList([theList, theWindow, resBaseName, labelDim])
	String theList, theWindow, resBaseName
	Variable labelDim
	
	if (ParamIsDefault(resBaseName) || strlen(resBaseName) == 0)
		resBaseName = "zScores"
	elseif (strlen(resBaseName) > 27)
		resBaseName = resBaseName[0, 26]
	endif
	if (ParamIsDefault(labelDim) || labelDim < 0)
		labelDim = 0
	else
		labelDim = 1
	endif
	
	Wave /WAVE results				// make our return wave
	if (ParamIsDefault(theWindow))
		theWindow = ""
	elseif (WinType(theWindow) != 1)
		// only graph windows can be used
		return results
	endif
	if (ParamIsDefault(theList) || strlen(theList) == 0)
		if (strlen(theWindow) == 0)
			// if we don't get a list of waves, we need a graph window name to create one 
			return results
		else
			theList = TraceNameList(theWindow, ";", 1)
		endif
	endif
	
	Make /N=(0, ItemsInList(theList)) /O zScoreData	// make the data matrix
	 
	Variable lengthNotSame = 0
	Variable index = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		if (strlen(theWindow) == 0)
			Wave data = $(StringFromList(index, theList))
		else
			Wave data = TraceNameToWaveRef(theWindow, StringFromList(index, theList))
		endif
		if (index == 0)
			// take the first wave as measure for the number of rows
			Redimension /N=(numpnts(data), -1, -1) zScoreData
		elseif (numpnts(data) < DimSize(zScoreData, 0))
			// adjust the whole matrix to the smallest number of traces
			lengthNotSame = 1
			Redimension /N=(numpnts(data), -1, -1) zScoreData
		endif
		zScoreData[][index] = data[p]		// transfer values from data wave to data matrix
		if (labelDim)
			// use the data path to create a wave label
			String theName = GetWavesDataFolder(data, 1)
			theName = StringFromList(ItemsInList(theName, ":") - 1, theName, ":")
			SetDimLabel 1, index, $theName, zScoreData
			// create the label and location waves for the image plots
			if (index == 0)
				Make /N=(ItemsInList(theList)) /O /T $(resBaseName + "_lb") /WAVE=lbl
				Make /N=(ItemsInList(theList)) /O $(resBaseName + "_lc") /WAVE=loc
			endif
			lbl[index] = theName
		endif
	endfor
	
	if (labelDim)
		loc = p
	endif
	
	if (lengthNotSame)
		print "** WARNING (calcZScoreFromList): the waves do not have the same length. Adjusted to shortest wave with " + num2str(DimSize(zScoreData, 0)) + " points!"
	endif
	Variable theMax, theMin
	
	Make /N=3 /FREE /WAVE /O results
	String zSx = resBaseName + "_x"		// name for the z-scores calculated along the x axis
	String zSy = resBaseName + "_y"		// name for the z-scores calculated along the y axis
	String zSa = resBaseName + "_s"		// name for the sum of the both z-scores
	
	Duplicate /O zScoreData, $zSx /WAVE=zScores_x
	Note /K zScores_x, "DESCRIPTION:z-scores calculated along the x axis of the data waves"
	results[0] = zScores_x
	Make /N=(DimSize(zScoreData, 0)) /O tmp
	for (index = 0; index < DimSize(zScoreData, 1); index += 1)
		tmp = zScoreData[p][index]
		WaveStats /C=1 /W /Q tmp
		Wave M_WaveStats
		tmp = (tmp - M_WaveStats[%avg]) / M_WaveStats[%sdev]
		WaveStats /C=1 /W /Q tmp
		if (theMax < M_WaveStats[%max])
			theMax = M_WaveStats[%max]
		endif
		if (theMin > M_WaveStats[%min])
			theMin = M_WaveStats[%min]
		endif
		zScores_x[][index] = tmp[p]
	endfor
		
	MatrixTranspose zScoreData
	Duplicate /O zScoreData, $zSy /WAVE=zScores_y
	Note /K zScores_y, "DESCRIPTION:z-scores calculated along every point of the population (y axis) of the data waves"
	results[1] = zScores_y
	Redimension /N=(DimSize(zScoreData, 0)) tmp
	for (index = 0; index < DimSize(zScoreData, 1); index += 1)
		tmp = zScoreData[p][index]
		WaveStats /C=1 /W /Q tmp
		Wave M_WaveStats
		tmp = (tmp - M_WaveStats[%avg]) / M_WaveStats[%sdev]
		WaveStats /C=1 /W /Q tmp
		if (theMax < M_WaveStats[%max])
			theMax = M_WaveStats[%max]
		endif
		if (theMin > M_WaveStats[%min])
			theMin = M_WaveStats[%min]
		endif
		zScores_y[][index] = tmp[p]
	endfor
	MatrixTranspose zScores_y
	
	Variable scale = floor(min(abs(theMin), abs(theMax)))
	
	Variable widthAspect = DimSize(zScores_y, 0)/DimSize(zScores_y, 1)
	
	// now make images
	String xZName = "IMG_x" + resBaseName[0, 25]
	String yZName = "IMG_y" + resBaseName[0, 25]
	String addZName = "IMG_a" + resBaseName[0, 25]
	
	DoWindow /F $xZName
	if (!V_flag)
		NewImage /N=$xZName zScores_x
		DoWindow /T $xZName, xZName + ": along x"
		resizeWindowToScreen(xZName, screenFraction=0.5)
		moveWindowOnScreen(xZName, location="left")
	endif
	ModifyImage /W=$xZName $(NameOfWave(zScores_x)) ctab={-scale, scale, RedWhiteBlue, 0}
	ModifyGraph /W=$xZName width={Aspect, widthAspect}, gfont="Helvetica", margin(right)=57
	ColorScale /C/N=theLegend/F=0/A=RC/X=-35.00/Y=0.00 ctab={-scale, scale, RedWhiteBlue, 0}
	ColorScale /C/N=theLegend nticks=10
	if (labelDim)
		ModifyGraph /W=$xZName userticks(left)={loc, lbl}, tkLblRot=0, tlOffset(left)=0
		ModifyGraph /W=$xZName margin(left)=44
	endif
	
	DoWindow /F $yZName
	if (!V_flag)
		NewImage /N=$yZName zScores_y
		DoWindow /T $yZName, yZName + ": along y"
		resizeWindowToScreen(yZName, screenFraction=0.5)
		moveWindowOnScreen(yZName)
	endif
	ModifyImage /W=$yZName $(NameOfWave(zScores_y)) ctab={-scale, scale, RedWhiteBlue, 0}
	ModifyGraph /W=$yZName width={Aspect, widthAspect}, gfont="Helvetica", margin(right)=57
	ColorScale /C/N=theLegend/F=0/A=RC/X=-35.00/Y=0.00 ctab={-scale, scale, RedWhiteBlue, 0}
	ColorScale /C/N=theLegend nticks=10
	if (labelDim)
		ModifyGraph /W=$yZName userticks(left)={loc, lbl}, tkLblRot=0, tlOffset(left)=0
		ModifyGraph /W=$yZName margin(left)=44
	endif
	
	Duplicate /O zScores_x, $zSa /WAVE=zScores_add
	results[3] = zScores_add
	zScores_add += zScores_y
	
	DoWindow /F $addZName
	if (!V_flag)
		NewImage /N=$addZName zScores_add
		DoWindow /T $addZName, addZName + ": added x/y"
		resizeWindowToScreen(addZName, screenFraction=0.5)
		moveWindowOnScreen(addZName, location="right")
	endif
	ModifyImage /W=$addZName $(NameOfWave(zScores_add)) ctab={-(2*scale), (2*scale), RedWhiteBlue, 0}
	ModifyGraph /W=$addZName width={Aspect, widthAspect}, gfont="Helvetica", margin(right)=57
	ColorScale /C/N=theLegend/F=0/A=RC/X=-35.00/Y=0.00 ctab={-(2*scale), (2*scale), RedWhiteBlue, 0}
	ColorScale /C/N=theLegend nticks=10
	if (labelDim)
		ModifyGraph /W=$addZName userticks(left)={loc, lbl}, tkLblRot=0, tlOffset(left)=0
		ModifyGraph /W=$addZName margin(left)=44
	endif
	
	KillWaves /Z zScoreData, tmp
	
	return results
End


Function /S SiST_makeHistogramFromGraph()
	String result = ""
	String theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
	if (strlen(theGraph) == 0)
		return result
	endif
	SVAR name = $(SiST_getLastHistName())
	SVAR binStart = $(SiST_getLastStartBin())
	SVAR binSize = $(SiST_getLastBinSize())
	SVAR numBins = $(SiST_getLastNumBins())
	
	String traceList = TraceNameList(theGraph, ";", 1)
	if (ItemsInList(traceList) == 0)
		DoAlert 0, "No traces on the top graph '" + theGraph + "'!"
		return result
	endif
	
	String theName = name
	String theBinStart = binStart
	String theBinSize = binSize
	String theNumBins = numBins
	Prompt theName, "name of result wave"
	Prompt theBinStart, "start bin"
	Prompt theBinSize,  "bin size"
	Prompt theNumBins,  "number of bins"
	
	DoPrompt "Histogram Parameters", theName, theBinStart, theBinSize, theNumBins
	if (V_flag)
		printf "--> user canceled histogram making from '%s' in wave %s on %s %s.\r", theGraph, theName, date(), time() 
		return result
	endif		
	
	Variable autoFlag = 0
	autoFlag = numtype(str2num(theBinStart)) == 2 ? autoFlag | 2^2 : autoFlag & ~(2^2)
	autoFlag = numtype(str2num(theBinSize)) == 2 ? autoFlag | 2^1 : autoFlag & ~(2^1)
	autoFlag = numtype(str2num(theNumBins)) == 2 ? autoFlag | 2^0 : autoFlag & ~(2^0)
	
	name = theName
	result = theName
	
	switch (autoFlag)
		case 0:
			makeHistogramFromGraph(theName, theList=traceList, theGraphName=theGraph, binStart=str2num(theBinStart), binSize=str2num(theBinSize), bins=str2num(theNumBins))
			binStart = theBinStart
			binSize = theBinSize
			numBins = theNumBins
			break
		case 1:
			makeHistogramFromGraph(theName, theList=traceList, theGraphName=theGraph, binStart=str2num(theBinStart), bins=str2num(theNumBins))
			binStart = theBinStart
			binSize = theBinSize
			numBins = "auto"
			break
		case 2:
			makeHistogramFromGraph(theName, theList=traceList, theGraphName=theGraph, binStart=str2num(theBinStart), bins=str2num(theNumBins))
			binStart = theBinStart
			binSize = "auto"
			numBins = theNumBins
			break
		case 3:
			makeHistogramFromGraph(theName, theList=traceList, theGraphName=theGraph, binStart=str2num(theBinStart))
			binStart = theBinStart
			binSize = "auto"
			numBins = "auto"
			break
		case 4:
			makeHistogramFromGraph(theName, theList=traceList, theGraphName=theGraph, binSize=str2num(theBinSize),bins=str2num(theNumBins))
			binStart = "auto"
			binSize = theBinSize
			numBins = theNumBins
			break
		case 5:
			makeHistogramFromGraph(theName, theList=traceList, theGraphName=theGraph, binSize=str2num(theBinSize))
			binStart = "auto"
			binSize = theBinSize
			numBins = "auto"
			break
		case 6:
			makeHistogramFromGraph(theName, theList=traceList, theGraphName=theGraph, bins=str2num(theNumBins))
			binStart = "auto"
			binSize = "auto"
			numBins = theNumBins
			break
		case 7:
		default:
			makeHistogramFromGraph(theName, theList=traceList, theGraphName=theGraph)
			binStart = "auto"
			binSize = "auto"
			numBins = "auto" 
			break
	endswitch
	
	printf "--> made histogram from %g traces in '%s' into wave %s; using bin start: %s, bin size: %s, num bins: %s.\r", ItemsInList(traceList), theGraph, theName, theBinStart, theBinSize, theNumBins
	printf "----> data traces: %s\r\r", traceList
	return theName		
End


Function makeHistogramFromGraph(theWaveName, [theList, theGraphName, binStart, binSize, bins])
	String theWaveName, theList, theGraphName
	Variable binStart, binSize, bins
	
	Variable autoFlag = 0
	if (ParamIsDefault(theGraphName) || strlen(theGraphName) == 0)
		theGraphName = StringFromList(0, WinList("*", ";", "WIN:1"))
	endif
	if (WhichListItem(theGraphName, WinList("*", ";", "WIN:1")) < 0)
		print "** ERROR (CalcMeanOfWaves): a graph with the name '" + theGraphName + "' does not exist."
		return 0
	endif
	if (ParamIsDefault(theList) || strlen(theList) == 0)
		theList = TraceNameList(theGraphName,  ";", 1)
	endif
	if (ParamIsDefault(binStart))
		binStart = 0
		autoFlag = autoFlag | 2^2
	endif
	if (ParamIsDefault(binSize))
		binSize = 0
		autoFlag = autoFlag | 2^1
	endif
	if (ParamIsDefault(bins))
		bins = 0
		autoFlag = autoFlag | 2^0
	endif
	
	Make /N=0 /O $(theWaveName + "_data") /WAVE=histData
	Make /N=0 /O $theWaveName /WAVE=hist
	
	Variable index
	for (index = 0; index < ItemsInList(theList); index += 1)
		// first combine all waves to one 
		Wave tmp = TraceNameToWaveRef(theGraphName, StringFromList(index, theList))
		Concatenate /NP {tmp}, histData
	endfor
	
	WaveStats /Q/W/C=0 histData
	Wave M_WaveStats
	
	switch(autoFlag)
		case 0:
			// have everything
			break			
		case 3:
			// have bin start, need bin size and bins
			binSize = 3.49 * M_WaveStats[%sdev] * (numpnts(histData)^(-1/3))
		case 1:
			// have start and bin size, need bins
			bins = ceil((M_WaveStats[%max] - binStart) / binSize)
			break
		case 2:
			// have start time and bin number, need bin size
			binSize = round((M_WaveStats[%max] - binStart) / bins)
			break
		case 4:
			// have bin size and bins, need bin start
			binStart = M_WaveStats[%min]
			break
		case 5:
			// have bin size, need start and bins
			binStart = M_WaveStats[%min]
			bins = ceil((M_WaveStats[%max] - binStart) / binSize)
			break
		case 6:
			// have bins, need start and bin size
			binStart = M_WaveStats[%min]
			binSize = round((M_WaveStats[%max] - binStart) / bins)
			break
		case 7:
			// need everything
		default:
			binStart = M_WaveStats[%min]
			binSize = 3.49 * M_WaveStats[%sdev] * (numpnts(histData)^(-1/3))
			bins = ceil((M_WaveStats[%max] - binStart) / binSize)
	endswitch

	Histogram /B={binStart, binSize, bins}  histData, hist

	KillWaves /Z histData
End




// This procedure provides the input for the z-score calculating function and is called from the Macros Menu
//
// input:
// theGraphName:		the name of the graph which contains the waves (traces) to be analysed
// baseName:			a string with the commen part of the newly created matrixes and graphs
// labels:				"yes" or "no" 
Proc calcZScoresFromTopGraph(theGraphName, baseName, labels)
	String theGraphName
	String baseName = "zScores"
	String labels = "no"
	Prompt theGraphName, "choose graph name", popup, WinList("*", ";", "WIN:1")
	Prompt baseName, "enter the base name for the result wave"
	Prompt labels, "choose to label", popup, "yes;no;"
	
	Variable aLabel = 0
	if (cmpstr(labels, "yes") == 0)
		aLabel = 1
	else
		aLabel = 0
	endif
	
	if (strlen(theGraphName) == 0)
		DoAlert 0, "No graph window available. Make a graph first!"
		return 0
	endif
	calcZScoreFromList(theWindow=theGraphName, resBaseName=baseName, labelDim=aLabel)
	return 0
End

// requires both waves, theData and theCategory to have at least one matching row dimension label
Function /WAVE waveStatsIfCat(theData, theCategoryWave, theCategory, [quiet])
	Wave theData
	Wave /T theCategoryWave
	String theCategory
	Variable quiet
	
	String theDataSelectionName = "SiSt_cat_tmp"
	Wave tmp = $theDataSelectionName
	
	if (ParamIsDefault(quiet) || quiet < 0)
		quiet = 0
	endif
	if (!WaveExists(theData))
		print "-- ERROR: the data wave does not exist!"
		KillWaves /Z tmp
		return tmp
	endif
	if (!WaveExists(theCategoryWave))
		print "-- ERROR: the category wave does not exist!"
		KillWaves /Z tmp
		return tmp
	endif
	
	String dataLabels = makeLblListFromWave(theData)
	String categoryLabels = makeLblListFromStatus(theCategoryWave, text=theCategory)
	
	String combinedLabels = intersectLists(dataLabels, categoryLabels)
	if (ItemsInList(combinedLabels) == 0)
		// no matching labels in both waves, nothing more to do
		print "-- ERROR: none of the values matches the given category '" + theCategory + "'"
		KillWaves /Z tmp
		return tmp
	endif
	
	Make /N=(ItemsInList(combinedLabels)) /O $theDataSelectionName /WAVE=tmp
	
	Variable index
	for (index = 0; index < ItemsInList(combinedLabels); index += 1)
		tmp[index] = theData[%$(StringFromList(index, combinedLabels))]
		SetDimLabel 0, index, $(StringFromList(index, combinedLabels)), tmp
	endfor
	if (quiet)
		WaveStats /Q /C=1 /W tmp
	else
		print "Stats for category " + theCategory + ":"
		WaveStats /C=1 /W tmp
	endif
	
	return tmp
End
	
// requires both waves, theData and theCategory to have at least one matching row dimension label
Function /WAVE waveStatsIfVal(theData, theValueWave, [value, operation, quiet])
	Wave theData
	Wave theValueWave
	Variable value
	String operation
	Variable quiet
	
	String theDataSelectionName = "SiSt_val_tmp"
	Wave tmp = $theDataSelectionName
	String valFeedback = ""
	
	if (ParamIsDefault(quiet) || quiet < 0)
		quiet = 0
	endif
	if (!WaveExists(theData))
		print "-- ERROR: the data wave does not exist!"
		KillWaves /Z tmp
		return tmp
	endif
	if (!WaveExists(theValueWave))
		print "-- ERROR: the value wave does not exist!"
		KillWaves /Z tmp
		return tmp
	endif
	if (ParamIsDefault(value))
		value = NaN
		valFeedback = "all"
	else
		valFeedback = num2str(value)
	endif
	
	String dataLabels = makeLblListFromWave(theData)
	String valueLabels = makeLblListFromWave(theValueWave, value=value, operation=operation)
	
	String combinedLabels = intersectLists(dataLabels, valueLabels)
	if (ItemsInList(combinedLabels) == 0)
		// no matching labels in both waves, nothing more to do
		print "-- ERROR: none of the data values can be matched with '" + valFeedback + "'"
		KillWaves /Z tmp
		return tmp
	endif
	
	Make /N=(ItemsInList(combinedLabels)) /O $theDataSelectionName /WAVE=tmp
	
	Variable index
	for (index = 0; index < ItemsInList(combinedLabels); index += 1)
		tmp[index] = theData[%$(StringFromList(index, combinedLabels))]
		SetDimLabel 0, index, $(StringFromList(index, combinedLabels)), tmp
	endfor
	if (quiet)
		WaveStats /Q /C=1 /W tmp
	else
		print "Stats for grouping by  " + valFeedback + ":"
		WaveStats /C=1 /W tmp
	endif
	
	return tmp
End
	
Function SiSt_descripStatsFromGraph([graphName, minimum])
	String graphName
	Variable minimum

	minimum = ParamIsDefault(minimum) || minimum < 1 ? 0 : 1
	
	if (ParamIsDefault(graphName) || strlen(graphName) == 0)
		graphName = GH_SimpleGraphChooser()
	endif
	if (ItemsInList(graphName) == 0)
		// no graphs available
		DoAlert 0, "Please put all wave to be analysed in one graph!"
		return 0
	endif
	String fullTraceList = FullTraceListFromGraph(graphName)
	if (ItemsInList(fullTraceList) == 0)
		// no traces on graph
		DoAlert 0, "Can't find any traces on the graph '" + graphName + "'!"
		return 0
	endif
		
	String result = ""
	Variable aCsr = strlen(CsrInfo(A, graphName)) > 0
	Variable bCsr = strlen(CsrInfo(B, graphName)) > 0
	
	if (aCsr && bCsr)
		Variable theStart = xcsr(A, graphName)
		Variable theEnd = xcsr(B, graphName)
		result = descriptiveStatsFromList(fullTraceList, tStart=theStart, tEnd=theEnd, minimum=minimum)
	else	
		result = descriptiveStatsFromList(fullTraceList, minimum=minimum)
	endif
	
	String feedback = "- Simple stats (%s, area) calculation: results in"
	if (minimum)
		sprintf feedback, feedback, "minimum"
	else
		sprintf feedback, feedback, "maximum"
	endif
	
	print feedback, result, "on", date(), time()
End

Function /S descriptiveStatsFromList(theList, [ampName, areaName, tStart, tEnd, tableName, noShow,minimum])
	String theList, ampName, areaName, tableName
	Variable tStart, tEnd, noShow, minimum
	
	String resultList = ""
	Variable numItems = ItemsInList(theList)
	if (numItems == 0)
		// no waves in the list, so nothing more to do
		return resultList
	endif

	noShow = ParamIsDefault(noShow) || noShow < 1 ? 0 : 1
	minimum = ParamIsDefault(minimum) || minimum < 1 ? 0 : 1
	
	String tableTitle = ""
	if (ParamIsDefault(tableName) || strlen(tableName) == 0)
		tableName = ksDescriptiveTable
		tableTitle = ksDescriptiveTableTitle
	else
		tableTitle = ReplaceString("_", tableName, " ")
	endif
	if (ParamIsDefault(ampName) || strlen(ampName) == 0)
		ampName = ksDescMeanValues
	endif
	if (ParamIsDefault(areaName) || strlen(areaName) == 0)
		areaName = ksDescAreaValues
	endif
	if (ParamIsDefault(tStart))
		tStart = -Inf
	endif
	if (ParamIsDefault(tEnd))
		tEnd = +Inf
	endif
	
	DFREF package = getPackage()
	Make /N=(numItems) /O package:$ampName /WAVE=amplitudes
	resultList = AddListItem(GetWavesDatafolder(amplitudes, 2), resultList)
	Make /N=(numItems) /O package:$areaName /WAVE=areas
	resultList = AddListItem(GetWavesDatafolder(areas, 2), resultList, ";", Inf)
	Variable index = 0, unitSet = 0
	for (index = 0; index < numItems; index += 1)
		Wave data = $(StringFromList(index, theList))
		if (!WaveExists(data))
			continue
		endif
		amplitudes[index] = minimum ? WaveMin(data, tStart, tEnd) : WaveMax(data, tStart, tEnd)
		areas[index] = area(data, tStart, tEnd)
		SetDimLabel 0, index, $(NameOfWave(data)), amplitudes
		SetDimLabel 0, index, $(NameOfWave(data)), areas
		if (!unitSet)
			SetScale d 0,0, WaveUnits(data, 1), amplitudes
			SetScale d 0,0, (WaveUnits(data, 1) + "\\S2"), areas
			unitSet = 1
		endif
	endfor
	
	if (noShow)
		return resultList
	endif
	
	DoWindow /F $tableName
	switch( V_flag )
		case 0:
			// window does not exists
			Edit /N=$tableName as tableTitle
			tableName = S_name
			AppendToTable /W=$tableName amplitudes.ld
			AppendToTable /W=$tableName areas
			break
		case 2:
			DoWindow /HIDE=0 /F $tableName
	endswitch
	return resultList
End


Function SiSt_decayFitToGraph([type])
	String type

	if (ParamIsDefault(type))
		type = ""
	endif
	strswitch ( LowerStr(type) )
		case "derivative":
			type = "derivative"
			break
		case "normal":
		case "simple":	
		default:
			type = "normal"
	endswitch
	
	String graphName = GH_SimpleGraphChooser()
	if (ItemsInList(graphName) == 0)
		// no graphs available
		DoAlert 0, "Please put all wave to be analysed in one graph!"
		return 0
	endif
	String fullTraceList = FullTraceListFromGraph(graphName, excludeContaining="_fit")
	if (ItemsInList(fullTraceList) == 0)
		// no traces on graph
		DoAlert 0, "Can't find any traces on the graph '" + graphName + "'!"
		return 0
	endif
	
	Variable aCsr = strlen(CsrInfo(A, graphName)) > 0
	Variable bCsr = strlen(CsrInfo(B, graphName)) > 0
	
	String derivativeStartPTypes = "minimum;minimum + 1;maximum;maximum + 1;"
	
	NVAR keep = $(getPackageVar("keepFit"))
	String keepStr = ""
	if (keep)
		keepStr = "Yes"
	else
		keepStr = "No"
	endif
	
	Variable startP = -1
	String stPType = "minimum + 1"
	Variable endP = -1
	Variable threshold = 1e-18
	Variable repeats = 1
	Variable y0 = 0
	Variable A = 0.1
	Variable tau = 1
	Prompt startP, "enter start POINT (-1 for max value)"
	Prompt stPType, "choose start point selection", popup, derivativeStartPTypes
	Prompt endP, "enter end POINT (-1 for end of wave)"
	Prompt threshold, "enter chi square threshold"
	Prompt repeats, "enter num repeats"
	Prompt y0, "enter initial guess for y0"
	Prompt A, "enter initial guess for A"
	Prompt tau, "enter initial guess for tau"
	Prompt keepStr, "choose to keep all values", popup, "Yes;No"

	strswitch ( type )
		case "derivative":
			if (aCsr && bCsr)
				startP = pcsr(aCsr)
				stPType = "absolute"
				endP = pcsr(B, graphName)
				repeats = 0
				DoPrompt "Derivative Decay Fit Params with Cursors", y0, A, tau	, keepStr
			elseif (bCsr)
				endP = pcsr(B, graphName)
				repeats = 0
				DoPrompt "Derivative Decay Fit Params with End Cursor", stPType, y0, A, tau, keepStr
			else
				repeats = 0
				DoPrompt "Derivative Decay Fit Parameters", stPType, endP, threshold, repeats, y0, A, tau, keepStr
			endif
			strswitch ( stPType )
				case "absolute":
				case "maximum":
				case "maximum + 1":
				case "minimum":
				case "minimum + 1":
					break
				default:
					stPType = "min+1"				
			endswitch			
			break
		case "normal":
			if (aCsr && bCsr)
				startP = pcsr(A, graphName)
				endP = pcsr(B, graphName)
				repeats = 0
				DoPrompt "Decay Fit Params with Cursors", y0, A, tau
			else
				DoPrompt "Decay Fit Parameters", startP, endP, threshold, repeats, y0, A, tau
			endif
			break
	endswitch	
	if (V_flag)
		// user canceled
		print "- user canceled fit for traces on graph '", graphName, "on", date(), time()
		return 0
	endif

	keep = cmpstr(keepStr, "Yes") == 0
	if (keep)
		keepStr = "combined"
	else
		keepStr = ""
	endif
	
	DFREF package = getPackage()
	Make /N=3 /O package:$ksDecayFitGuesses /WAVE=guesses
	SetDimLabel 0, 0, y0, guesses
	SetDimLabel 0, 1, A, guesses
	SetDimLabel 0, 2, tau, guesses
	guesses[%y0] = y0
	guesses[%A] = A
	guesses[%tau] = tau
	
	strswitch ( type )
		case "derivative":
			fullTraceList = SiSt_differentiateTraces(fullTraceList, show=1, type=stPType, start = startP)
			graphName = StringFromList(0, GH_getGraphFromWaveList(fullTraceList))
			if (strlen(graphName) == 0)
				// error if not displayed
			endif
			ModifyGraph /W=$graphName rgb=(0,0,0)
			Wave startPoints = SiSt_getSimpleFitStartPoints()
			
			Wave result = fitExpDecayToSimpleList(CFit_ExpDecay_Driver, guesses, fullTraceList, startPWave=startPoints, endP=endP, threshold=threshold, repeats=repeats, noShow=1)
			if (keep)
				SiSt_combineFitValues()
			endif
			break
		case "normal":
			Wave result = fitExpDecayToSimpleList(CFit_ExpDecay_Driver, guesses, fullTraceList, startP=startP, endP=endP, threshold=threshold, repeats=repeats)
			break
	endswitch
	
	String traceList = TraceNameList(graphName, ";", 1)
	Variable index
	for (index = 0; index < ItemsInList(fullTraceList); index += 1)
		Wave data = $StringFromList(index, fullTraceList)
		if( WhichListItem(NameOfWave(data) + "_fit", traceList) < 0)
			AppendToGraph /W=$graphName /C=(0,0,65535) $(GetWavesDatafolder(data, 2) + "_fit")
		endif
	endfor

	SiSt_getSimpleFitTable(type=keepStr)

	return 1
End

Function /WAVE fitExpDecayToSimpleList(fitFunc, guesses, yDataList, [startP, endP, threshold, repeats, name, startPWave, endPWave, eRelative, tableName, noShow])
	FUNCREF CFit_SimplePrototype &fitFunc
	Wave guesses
	String yDataList, name, tableName
	Variable startP, endP, threshold, repeats, noShow, eRelative
	Wave startPWave, endPWave
	
	String tableTitle
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = ksSimpleFitValues
	endif
	if (ParamIsDefault(tableName) || strlen(tableName) == 0)
		tableName = ksSimpleFitTable
		tableTitle = ksSimpleFitTableTitle
	else
		tableTitle = ReplaceString("_", tableName, " ")
	endif
	if (ParamIsDefault(noShow) || noShow < 0)
		noShow = 0
	else
		noShow = noShow > 1 ? 1 : round(noShow)
	endif
	if (ParamIsDefault(eRelative) || eRelative < 0)
		eRelative = 0
	else
		eRelative = eRelative > 1 ? 1 : round(eRelative)
	endif
	
	Variable distance
	Wave summary
	
	Variable useMax = 0, useMin = 0, toEnd = 0, useSPWave = 0, useEPWave = 0
	if (ParamIsDefault(startPWave) || !WaveExists(startPWave))
		if (ParamIsDefault(startP))
			useMax = 1
		elseif (startP == -2)
			useMin = 1
		elseif (startP < 0)
			useMax = 1
		endif
	else
		if (numpnts(startPWave) != ItemsInList(yDataList))
			print "** ERROR (fitExpDecayToSimpleList): wave with start points has different size than the number of waves to be fitted; using maxium value as start point!"
			useMax = 1
		else
			useSPWave = 1		
		endif
	endif
	if (ParamIsDefault(endPWave) || !WaveExists(endPWave))
		if (ParamIsDefault(endP) || endP < 0)
			toEnd = 1
		else
			if (eRelative)
				distance = endP
			endif
		endif
	else
		if  (numpnts(endPWave) != ItemsInList(yDataList))
			print "** ERROR (fitExpDecayToSimpleList): wave with end points has different size than the number of waves to be fitted; using end of wave as end points!"
			toEnd = 1
		else
			useEPWave = 1
		endif
	endif
		
	DFREF package = getPackage()
	Variable numItems = ItemsInList(yDataList)
	
	Make /N=(0, (2 * numpnts(guesses)) + 5) /O package:$name /WAVE=summary
	SetDimLabel 1, 0, chisq, summary
	SetDimLabel 1, 1, threshold, summary
	SetDimLabel 1, 2, steps, summary
	SetDimLabel 1, 3, startP, summary
	SetDimLabel 1, 4, endP, summary
	Variable index
	for (index = 0; index < numpnts(guesses); index += 1)
		String factor = GetDimLabel(guesses, 0, index)
		String sigma = factor + "_sig"
		SetDimLabel 1, 2 * index + 5, $factor, summary
		SetDimLabel 1, 2 * index + 6, $sigma, summary
	endfor
		
	Make /N=0 /WAVE /FREE /O waveRefs
	Variable dataIndex = 0
	for (dataIndex = 0; dataIndex < numItems; dataIndex += 1)
		Wave yData = $StringFromList(dataIndex, yDataList)
		if (!WaveExists(yData))
			continue
		endif
		waveRefs[numpnts(waveRefs)] = {yData}
		InsertPoints /M=0 DimSize(summary, 0), 1, summary
		SetDimLabel 0, DimSize(summary, 0) - 1, $NameOfWave(yData), summary
	endfor		
	numItems = numpnts(summary)
	
	dataIndex = 0
	Variable  maximum, minimum
	String theWave = ""

	do
		Wave yData = waveRefs[dataIndex]
		if (useSPWave)
			startP = startPWave[dataIndex]
		elseif ( useMin )
			minimum  = WaveMin(yData)
			Extract /FREE /INDX /O yData, minloc, yData == minimum
			startP = minloc[0]
		elseif (useMax)
			maximum = WaveMax(yData)
			Extract /FREE /INDX /O yData, maxloc, yData == maximum
			startP = maxloc[0]
		endif
		if (useEPWave)
			if ( eRelative )
				endP = startP + endPWave[dataIndex]
			else
				endP = endPWave[dataIndex]
			endif
		elseif ( eRelative )
			endP = startP + distance
		elseif (toEnd)
			endP = numpnts(yData)
		endif

		DFREF wavePath = GetWavesDatafolderDFR(yData)
		DFREF tmp = getTmpWorkFolder()
		
		Duplicate /O guesses, tmp:$(NameOfWave(yData) + "_coef") /WAVE=cw
		Make /N=(numpnts(yData)) /O wavePath:$(NameOfWave(yData) + "_fit") /WAVE=fitWave
		SetScale /P x leftx(yData), deltax(yData), WaveUnits(yData, 0), fitWave
		fitFunc(summary, cw, yData, fitWave, startP, endP, threshold, repeats)
		dataIndex += 1
	while ( dataIndex < numItems )
	KillDatafolder tmp
	
	if (noShow)
		return summary
	endif
	
	DoWindow /F $tableName
	if( !V_flag )
		Edit /N=$tableName summary.ld as tableTitle
	endif
	return summary
End

Function SiSt_normSummaries([normType, statsType])
	String normType, statsType
	
	Prompt normType, "choose how to normalize", popup, "numWaves;numPoints;maxValue"
	Prompt statsType, "choose the summary type", popup, "sum;arithmetic mean"
	
	Variable ask = 0
	if (ParamIsDefault(normType))
		normType = ""
		ask = 1
	endif
	if (ParamIsDefault(statsType))
		statsType = ""
		ask = 1
	endif
	strswitch( normType )
		case "numWaves":
		case "numPoints":
		case "maxValue":
			ask = 0
			break
		default:
			ask = 1
	endswitch
	strswitch( statsType )
		case "arithmetic mean":
		case "sum":
			ask = 0
			break
		default:
			ask = 1
	endswitch
	
	if (ask)
		DoPrompt "Select Stats Type", normType, statsType
		if (V_flag)
			// user canceled
			print "-> user canceled normalization of summary waves", date(), time()
			return 0
		endif
	endif
	
	String theList = GetMeanPerPoint(statsType=LowerStr(statsType))
	Wave summary = $StringFromList(0, theList)

	strswitch( normType )
		case "numWaves":
			Variable numWaves = NumberByKey(ks_SIST_numWaves, note(summary))
			summary = (summary/numWaves) * 100
			break
		case "numPoints":
			Variable index
			String valueList = StringByKey(ks_SIST_valuePPnt, note(summary))
			if (ItemsInList(valueList, ",") > 0)
				summary = (summary[p] / NumberByKey(num2str(p), valueList, "=", ",")) * 100
//				for (index = 0; index < ItemsInList(valueList, ","); index += 1)
//				endfor
			endif
			break
		case "maxValue":
		default:
			Variable maxValue = WaveMax(summary)
			summary = (summary/maxValue) * 100
	endswitch
	
	SetScale /P x 1,1,"", summary
End

Function /WAVE SiSt_getSimpleFitStartPoints([name])
	String name
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = ksDerivFitStart
	endif
	DFREF cDF= GetDatafolderDFR()
	DFREF package = getPackage()
	Wave startP = package:$name
	if (!WaveExists(startP))
		Make /N=0 package:$name /WAVE=startP
	endif
	return startP
End

Function /S SiSt_differentiateTraces(theList, [show, type, start])
	String theList, type
	Variable show, start
	
	String resultList = ""
	if (ParamIsDefault(show) || show > 1)
		show = 1
	else
		show = show < 0 ? 0 : round(show)
	endif
	if (strlen(theList) == 0)
		return resultList
	endif
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "min"
	else
		strswitch ( LowerStr(type) )
			case "absolute":
			case "abs":
				type = "abs"
				if (ParamIsDefault(start) || start < 0)
					print "** ERROR (SiSt_differentiateTraces): requested absolute start point with invalid start point '", start, "! Set to default (minimum)."
					type = "min"
				endif
				break
			case "maximum":
			case "max":
				type = "max"
				break
			case "maximum + 1":
			case "max+1":
				type = "max+1"
				break
			case "minimum + 1":
			case "min+1":
				type = "min+1"
				break
			case "minimum":
			case "min":
			default:
				type = "min"
		endswitch
	endif

	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	String graphName = ""
	Variable index = 0
	Variable numItems = ItemsInList(theList)
	
	if (show)
		Display
		graphName = S_name
	endif
	
	Wave startP = SiSt_getSimpleFitStartPoints()
	if (numpnts(startP) != numItems)
		Redimension /N=(numItems) startP
	endif
	
	for (index = 0; index < numItems; index += 1)
		String theName = StringFromList(index, theList)
		String destName = ParseFilePath(0, theName, ":", 1, 0) + "_dif"
		DFREF destPath = $(ParseFilePath(1, theName, ":", 1, 0))
		Wave data = $theName
		Differentiate data /D=destPath:$destName
		if (show)
			AppendToGraph /W=$graphName destPath:$destName
		endif
		resultList = AddListItem(GetDatafolder(1, destPath) + destName, resultList, ";", Inf)
		
		if (cmpstr(type, "abs") == 0)
			startP[index] = start
		else
			WaveStats /Q /M=1 /C=1 /W destPath:$destName
			Wave M_WaveStats
			
			strswitch ( type )
				case "max":
					startP[index] = x2pnt(data, M_WaveStats[%maxLoc])
					break
				case "max+1":
					startP[index] = x2pnt(data, M_WaveStats[%maxLoc]) + 1
					break
				case "min":
					startP[index] = x2pnt(data, M_WaveStats[%minLoc])
					break
				case "min+1":
					startP[index] = x2pnt(data, M_WaveStats[%minLoc]) + 1
					break
			endswitch
		endif
		SetDimLabel 0, index, $destName, startP
	endfor
	
	return resultList
End


Function SiSt_combineFitValues()

	DFREF package = getPackage()
	String errormsg = getErrorStr("SiSt_combineFitValues")
	
	Wave simpleFit = getSimpleFitValueWave()
	if (!WaveExists(simpleFit))
		printf errormsg, "can't find the fit value wave. Fit first"
		return 0
	endif
	
	Wave combinedFit = getCombinedFitValueWave()
	if (!WaveExists(combinedFit))
		// doesn't yet exists, so just duplicate simple fit wave. 
		Duplicate /O simpleFit, package:$ksCombinedFitValues
		return 1
	endif
	
	if (DimSize(simpleFit, 1) != DimSize(combinedFit, 1))
		// column numbers are not identical, can't combine
		printf errormsg, "fit value waves contain different values, columns are not identical. Can't combine"
		return 0
	endif
	
	String simpleColumnHeaders = makeLblListofWave(simpleFit, dim=1)
	String combinedColumnHeaders = makeLblListofWave(combinedFit, dim=1)
	if (!sameItems(simpleColumnHeaders, combinedColumnHeaders))
		printf errormsg, "fit value waves contain different value types. Can't combine"
		return 0
	endif
	
	String waveLbls = makeLblListofWave(combinedFit)
	Variable i, j
	for (i = 0; i < DimSize(simpleFit, 0); i += 1)
		String waveLbl = GetDimLabel(simpleFit, 0, i)
		Variable item = WhichListItem(waveLbl, waveLbls)
		if (item < 0)
			InsertPoints DimSize(combinedFit, 0), 1, combinedFit
			SetDimLabel 0, DimSize(combinedFit, 0) - 1, $waveLbl, combinedFit
		endif
		
		combinedFit[%$waveLbl][] = simpleFit[%$waveLbl][%$(GetDimLabel(combinedFit, 1, q))]
		
	endfor
	
	return 1
End

Function /WAVE SiSt_createDataWaveFromList(theList, [point, resultName, add, verbose])
	String theList, resultName
	Variable point, add, verbose
	
	if (ParamIsDefault(verbose) || verbose != 0)
		verbose = 1
	endif
	if (ParamIsDefault(point) || point < -1)
		point = -1
	else
		point = round(point)
	endif
	if (ParamIsDefault(add) || add < 0)
		add = 0
	else
		add = add > 1 ? 1 : round(add)
	endif
	if (ParamIsDefault(resultName) || strlen(resultName) == 0)
		resultName = "combinedDataWave"
	endif
	
	Wave result = $resultName
	if (!WaveExists(result))
		Make /N=0 $resultName /WAVE=result
	endif
	if (strlen(theList) == 0)
		// no items in the list, so stop here	
		return result
	endif
	
	Variable index = 0, startPoint = 0
	Variable numWaves = ItemsInList(theList)
	
	if (point == -1)
		// concatenate whole data waves
		if (add)
			Concatenate /NP theList, $resultName
		else
			Concatenate /NP/O theList, $resultName
		endif
		
		if (verbose)
			if (add)
				printf "-> added all points of %g traces to wave %s.\r", numWaves, resultName
			else
				printf "-> combined all points of %g traces into wave %s.\r", numWaves, resultName
			endif
			print "--> used trace list: " + theList
		endif
		
		return result
	endif
	
	if (add)
		startPoint = numpnts(result)
		Redimension /N=(startPoint + numWaves) result
		result[startPoint,] = NaN
	else
		Redimension /N=(numWaves) result
		startPoint = 0
		result = NaN
	endif
	
	for (index = 0; index < numWaves; index += 1)
		Wave data = $(StringFromList(index, theList))
		if (!WaveExists(data))
			continue
		endif
		result[index + startPoint] = data[point]
	endfor
	
	if (verbose)
		if (add)
			printf "-> added point %g of %g traces to wave %s.\r", point, numWaves, resultName
		else
			printf "-> combined point %g of %g traces into wave %s.\r", point, numWaves, resultName
		endif
		print "--> used trace list: " + theList
	endif

	return result
End

Function /S SiSt_makeDataWaveFromGraph([theGraph, verbose])
	String theGraph
	Variable verbose
	
	String result = ""
	if (ParamIsDefault(verbose) || verbose != 0)
		verbose = 1
	endif
	if (ParamIsDefault(theGraph) || strlen(theGraph) == 0)
		theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
	endif
	if (strlen(theGraph) == 0 || WinType(theGraph) != 1)
		DoAlert 0, "I need a graph to work with"
		return result
	endif			
	
	String overwrite = "Yes"
	Prompt overwrite, "overwrite?", popup, "Yes;No"
	
	NVAR gPoint = $(getPackageVar(ks_defDataPoint))
	Variable point = gPoint
	SVAR gDataName = $(getPackageStr(ks_defDataName))
	String dataName = gDataName
	Prompt point, "Please enter data point"
	Prompt dataName, "Please choose a result wave name"
	
	SVAR gDispGraph = $(getPackageStr(ks_defDataGraph))
	if (strlen(gDispGraph) == 0)
		gDispGraph = "_new_"
	endif
	String dispName = gDispGraph
	Prompt dispName, "Please choose a graph name", popup, "_new_;_none_;" + WinList("*", ";", "WIN:1")
	
	DoPrompt "Make Data Waves", dataName, point, dispName, overwrite
	if (V_flag)
		// user hit cancel
		return result
	endif
	
	Variable lastUScr = strsearch(dataName, "_", Inf, 1)
	Variable hasPostFix = 0
	
	if (lastUScr != -1)
		hasPostFix = cmpstr(dataName[lastUScr, strlen(dataName)], "_data") == 0
	endif
	if (hasPostFix)
		gDataName = dataName
	else
		gDataName = dataName[0,28] + "_data"
	endif
	
	gPoint = point
	gDispGraph = dispName
	result = dispName
	
	Variable add = cmpstr(overwrite, "No") == 0
	
	String theList = ""
	String traceNames = TraceNameList(theGraph, ";", 1)
	Variable numTraces = ItemsInList(traceNames)
	Variable index 
	for (index = 0; index < numTraces; index += 1)
		theList = AddListItem(GetWavesDatafolder(TraceNameToWaveRef(theGraph, StringFromList(index, traceNames)), 2), theList, ";", Inf)
	endfor	
	
	if (verbose)
		printf "-> found %g traces on graph %s for data concatenation.\r", numTraces, theGraph
	endif
	
	Wave resultWave = SiSt_createDataWaveFromList(theList, point=gPoint, resultName=gDataName, add=add)
	
	strswitch (LowerStr(dispName))
		case "_none_":
		case "none":
			result = ""
			break
		case "_new_":
		case "new":
			Display resultWave
			result = S_name
			gDispGraph = result
			break
		default:
			DoWindow /F $dispName
			if (V_flag == 0)
				Display /N=$dispName resultWave
				result = S_name
				gDispGraph = result
			else
				AppendToGraph /W=$dispName resultWave
			endif
	endswitch

	if (verbose && strlen(result) != 0)
		printf "--> displayed result wave %s in graph %s.\r", gDataName, result
	endif
	
	return result
End
	
	
Function /WAVE SiSt_getPeakStatsFromGraph([selection])
	String selection
	
	Wave result
	String wList = WinList("*", ";", "WIN:3")
	String list = ""
	if (ParamIsDefault(selection) || strlen(selection) == 0 || cmpstr(selection, "_top_graph_") == 0)
		String theWin = StringFromList(0, WinList("*", ";", "WIN:1"))
		if (strlen(theWin) == 0)
			return result
		endif
	elseif (WhichListItem(selection, wList) < 0)
		// name provided with selection does not exist as a window name, so stop here
		return result
	else
		theWin = selection
	endif
		
	switch (WinType(theWin))
		case 1:
			list = TraceNameList("", ";", 1)
			if (strlen(list) == 0)
				return result
			endif
			break
		case 2:
			// implement getting waves from a table
			list = ""
			break
		default:
			// can't determine window type, so stop here. 
			return result
	endswitch
	
	Variable index = 0, numTraces = ItemsInList(list)
	String fullPathList = ""
	for (index = 0; index < numTraces; index += 1)
		String traceName = StringFromList(index, list)
		fullPathList = AddListItem(GetWavesDataFolder(TraceNameToWaveRef("", traceName), 2), fullPathList, ";", Inf)
	endfor
	
	return SiSt_getPeakStatsFromList(fullPathList)
End


Function /WAVE SiSt_getPeakStatsFromList(list, [traceNames, p4AmpBase])
	String list, traceNames
	Variable p4AmpBase

	Wave result
	if (strlen(list) == 0)
		return result
	endif

	Variable useTraceName = 0
	if (ParamIsDefault(traceNames) || strlen(traceNames) == 0)
		useTraceName = 1
	else
		if (ItemsInList(traceNames) != ItemsInList(list))
			useTraceName = 1
		endif
	endif
	
	p4AmpBase = ParamIsDefault(p4AmpBase) || p4AmpBase <= 1 ? 1 : round(p4AmpBase)

	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()

	Variable index = 0
	Variable numItems = ItemsInList(list)

	Make /O /N=(100, 8, numItems) package:$ksSelPeakStats /WAVE=collector
	SetDimLabel 1, 0, peakStart, collector
	SetDimLabel 1, 1, peakEnd, collector
	SetDimLabel 1, 2, peakLoc, collector
	SetDimLabel 1, 3, peakVal, collector
	SetDimLabel 1, 4, peakAmp, collector
	SetDimLabel 1, 5, peakDist, collector
	SetDimLabel 1, 6, adjPeakEnd, collector
	SetDimLabel 1, 7, peakRatio, collector

	collector = NaN

	Make /O /N=0 package:$ksAllPeakVals /WAVE=pVals
	Make /O /N=0 package:$ksAllPeakLocs /WAVE=pLocs

	Variable maxRows = 0, lastRow = 0
	String traceName = ""
	
	for (index = 0; index < numItems; index += 1)
		String tracePath = StringFromList(index, list)
		if (useTraceName)
			traceName = ParseFilePath(3, tracePath, ":", 0, 0)
		else
			traceName = StringFromList(index, traceNames)
		endif
		
		Wave data = $tracePath
		
		if (!WaveExists(data))
			continue
		endif
		
		Variable /C estimates = EstPeakNoiseAndSmFact(data, 0, numpnts(data))	
		Variable noiseLevel = real(estimates)
//		Variable smthFactor = imag(estimates)
		Wave traceStats = SiSt_getPeakStatsFromWave(data, threshold = 10 * noiseLevel, p4AmpBase = p4AmpBase)
		Variable numPeaks = DimSize(traceStats, 0)
		
		if (numPeaks == 0)
			continue
		endif
		
		Make /O /N=(numPeaks) /FREE peakVals, peakLocs
		peakVals = traceStats[p][%peakVal]
		peakLocs = traceStats[p][%peakLoc]		
		
		Concatenate /NP {peakVals}, pVals
		Concatenate /NP {peakLocs}, pLocs
		
		lastRow = DimSize(collector, 0)
		maxRows = max(maxRows, DimSize(traceStats, 0))
		if (maxRows > lastRow)
			Redimension /N=(lastRow + 100, -1, -1) collector
			collector[lastRow,][][] = NaN
		endif
		
		collector[0, numPeaks - 1][0,4][index] = traceStats[p][q]
		SetDimLabel 2, index, $traceName, collector
		
		switch (numPeaks)
			case 1:
				collector[0][%peakDist][index] = collector[0][%peakLoc][index]
				break
			case 2:
				collector[0][%peakDist][index] = collector[0][%peakLoc][index]
				collector[1][%peakDist][index] = collector[1][%peakLoc][index] - collector[0][%peakLoc][index]
				break
			default:
				Make /N=(numPeaks) /FREE /O diff, diffNew
				diff = traceStats[p][%peakLoc]
				diffNew = traceStats[p][%peakLoc]
				Differentiate /METH=2 diff
				diffNew[1,] = diff[p - 1]
				collector[][%peakDist][index] = diffNew[p]
		endswitch			
	endfor

	Redimension /N=(maxRows, -1, -1) collector
	Wave result = collector
	
	return result
End

Function /WAVE SiSt_getPeakStatsFromWave(data, [negative, threshold, p4AmpBase])
	Wave data
	Variable negative, threshold, p4AmpBase
	
	Wave result
	
	negative = ParamIsDefault(negative) || negative < 1 ? 0 : 1
	threshold = ParamIsDefault(threshold) ? 0 : threshold
	p4AmpBase = ParamIsDefault(p4AmpBase) || p4AmpBase <= 1 ? 1 : round(p4AmpBase)
	
	if (!WaveExists(data))
		return result
	endif
	
	return selectPeaks(detectPeaks(data, negative=negative, p4AmpBase=p4AmpBase), threshold=threshold, negative=negative)
End	


static Function /WAVE detectPeaks(data, [negative, p4AmpBase])
	Wave data
	Variable negative, p4AmpBase
	
	negative = ParamIsDefault(negative) || negative < 1 ? 0 : 1
	// the number of points to calculate the baseline for the amplitude calucation has to
	// be a positve integer larger or equal to 1
	p4AmpBase = ParamIsDefault(p4AmpBase) || p4AmpBase <= 1 ? 1 : round(p4AmpBase)
	
	Wave result
	Variable peakNum = 0
	DFREF package = getPackage()
	Make /N=(100, 5) /O package:$ksTracePeakStats /WAVE=result
	SetDimLabel 1, 0, peakStart, result
	SetDimLabel 1, 1, peakEnd, result
	SetDimLabel 1, 2, peakLoc, result
	SetDimLabel 1, 3, peakVal, result
	SetDimLabel 1, 4, peakAmp, result

	if (!WaveExists(data))
		return result
	endif
	
//	Variable /C estimates = EstPeakNoiseAndSmFact(data, 0, numpnts(data))	
//	Variable noiseLevel = real(estimates)
//	Variable smthFactor = imag(estimates)
	
	DFREF cDF = GetDatafolderDFR()
	NewDatafolder /O/S tmpDerivPeak
	
	Duplicate /O data, w1
//	Smooth /B=1 smthFactor, w1
	if (negative)
		w1 *= -1
	endif
	
	Duplicate /O w1, w2
	Differentiate w2
//	Smooth /E=2/B=1 2 * smthFactor, w2
	Differentiate w2
//	Smooth /E=2/B=1 2 * smthFactor, w2
	
	Variable peakBad = 0, noMax = 0, point = 0
	Variable startX = leftx(data), delta = deltax(data), endX = rightx(data)
	Variable startOfTrace = startX
	
	FindPeak /B=1/N/R=(startX,)/Q w2
	if (V_flag)
		// no minium found so just stop here
		Redimension /N=0 result
		return result
	endif
	result[peakNum][%peakLoc] = V_PeakLoc
	point = x2pnt(w2, V_PeakLoc)
	result[peakNum][%peakVal] = data[x2pnt(w2, V_PeakLoc)]
	FindPeak /B=1/R=(result[peakNum][%peakLoc], startX)/Q w2
	if (V_flag)
		peakBad = 1
	endif
	result[peakNum][%peakStart] = V_PeakLoc	
	FindPeak /B=1/R=(result[peakNum][%peakLoc],)/Q w2
	if (V_flag)
		Redimension /N=0 result
		return result
	endif
	if (peakBad)
		startX = result[peakNum][%peakEnd]
	endif
	
	do
		FindPeak /B=1/N/R=(startX,)/Q w2
		if (V_flag)
			break
		endif
		point = x2pnt(w2, V_PeakLoc)
		result[peakNum][%peakLoc] = V_PeakLoc
		result[peakNum][%peakVal] = data[point]
		FindPeak /B=1/R=(result[peakNum][%peakLoc], startOfTrace)/Q w2
		if (V_flag)
			break
		endif
		result[peakNum][%peakStart] = V_PeakLoc
		point = x2pnt(w2, V_PeakLoc)
		if (p4AmpBase == 1)
			result[peakNum][%peakAmp] = result[peakNum][%peakVal] - data[point]
		else
			Variable ampStartP = point - p4AmpBase - 1 < 0 ? 0 : ampStartP
			Variable ampStartX = pnt2x(w2, ampStartP)
			result[peakNum][%peakAmp] = result[peakNum][%peakVal] - mean(data, ampStartX, V_PeakLoc)
		endif
		FindPeak /B=1/R=(result[peakNum][%peakLoc],)/Q w2				
		if (V_flag)
			break
		endif
		result[peakNum][%peakEnd] = V_PeakLoc
		
//		startX = result[peakNum][%peakEnd] + delta
		startX = result[peakNum][%peakEnd]
		peakNum += 1
		if (peakNum >= DimSize(result, 0))
			Redimension /N=(peakNum + 100, -1) result
		endif
	while(1)
	
	Redimension /N=(peakNum, -1) result
	
	if (negative)
		result[][%peakVal, %peakAmp] *= -1
	endif
	
	KillDatafolder :
	SetDatafolder cDF
	return result
End

Function /WAVE SiSt_getPPRFromTopGraph([baseline])
	Variable baseline
	
	baseline = ParamIsDefault(baseline) || baseline <= 1 ? 1 : round(baseline)

	String errormsg = getErrorStr("SiSt_getPPRFromTopGraph")
	
	Wave result
	
	String theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
	if (strlen(theGraph) == 0)
		printf errormsg, " no graph windows open"
		return result
	endif
	
	DFREF package = getPackage()
	Wave p1a = package:$ksP1Amps
	Wave p2a = package:$ksP2Amps
	
	Wave result = SiSt_getPPRFromList(makeWaveListFromGraph(theGraph), p4AmpBase=baseline)
	
	DoWindow /F $ksPAmpValTable
	if (V_flag == 0)
		Edit /N=$ksPAmpValTable p1a.ld, p2a as ksPAmpValTitle
	endif
	moveWindowOnScreen(ksPAmpValTable, location="right")
	
	DoWindow /F $ksSelPPRTable
	if (V_flag == 0)
		Edit /N=$ksSelPPRTable result.ld as ksSelPPRTableTitle
	endif
	
	return result
End	
	

Function /WAVE SiSt_getPPRFromList(list, [traceNames, p4AmpBase])
	String list, traceNames
	Variable p4AmpBase

	Wave result
	if (strlen(list) == 0)
		return result
	endif

	Variable useTraceName = 0
	if (ParamIsDefault(traceNames) || strlen(traceNames) == 0)
		useTraceName = 1
		traceNames = ""
	else
		if (ItemsInList(traceNames) != ItemsInList(list))
			useTraceName = 1
		endif
	endif

	p4AmpBase = ParamIsDefault(p4AmpBase) || p4AmpBase <= 1 ? 1 : round(p4AmpBase)

	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()

	Variable index = 0
	Variable numItems = ItemsInList(list)

	Make /O /N=(numItems, 14) package:$ksSelPPRStats /WAVE=collector
	SetDimLabel 1, 0, P1_Start, collector
	SetDimLabel 1, 1, P1_kEnd, collector
	SetDimLabel 1, 2, P1_Loc, collector
	SetDimLabel 1, 3, P1_Val, collector
	SetDimLabel 1, 4, P1_Amp, collector
	SetDimLabel 1, 5, P1_adjPeakEnd, collector
	SetDimLabel 1, 6, P2_Start, collector
	SetDimLabel 1, 7, P2_kEnd, collector
	SetDimLabel 1, 8, P2_Loc, collector
	SetDimLabel 1, 9, P2_Val, collector
	SetDimLabel 1, 10, P2_Amp, collector
	SetDimLabel 1, 11, P2_adjPeakEnd, collector
	SetDimLabel 1, 12, peakDist, collector
	SetDimLabel 1, 13, peakRatio, collector

	collector = NaN

	Wave peakStats = SiSt_getPeakStatsFromList(list, traceNames = traceNames, p4AmpBase = p4AmpBase)
	Variable numTraces = DimSize(peakStats, 2)	
	
	String traceName = ""
	
	for (index = 0; index < numTraces; index += 1)
		collector[index][0,4] = peakStats[0][q][index]
		if (numtype(peakStats[1][0][index]) == 0)
			collector[index][6,10] = peakStats[1][q-6][index]
			collector[index][%peakDist] = collector[index][%P2_Loc] - collector[index][%P1_Loc]
			collector[index][%peakRatio] = collector[index][%P2_Amp] / collector[index][%P1_Amp]
		else
			collector[index][%peakDist] = collector[index][%peakLoc]
		endif
		traceName = GetDimLabel(peakStats, 2, index)
		SetDimLabel 0, index, $traceName, collector
	endfor
	
	Redimension /N=(numTraces, -1) collector
	Wave result = collector
	
	if (numTraces > 0)
		Make /N=(DimSize(result, 0)) /O package:$ksP1Amps /WAVE=p1a
		for (index = 0; index < numpnts(p1a); index += 1)
			SetDimLabel 0, index, $(GetDimLabel(result, 0, index)), p1a
		endfor
		Duplicate /O p1a, package:$ksP2Amps /WAVE=p2a
		
		p1a = result[p][%P1_Amp]
		p2a = result[p][%P2_Amp]
	else
		Make /N=0 /O package:$ksP1Amps /WAVE=p1a
		Make /N=0 /O package:$ksP2Amps /WAVE=p2a
	endif	
	
	return result
End


Function /WAVE SiSt_avgMatrixElements(matrix, pnt1, pnt2, [name, row])
	Wave matrix
	Variable pnt1, pnt2, row
	String name
	
	Wave result

	if (!WaveExists(matrix))
		return result
	endif
	
	row = ParamIsDefault(row) || row > 1 ? 1 : 0
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = "MatrixAverage"
	endif
	
	Variable numRows = DimSize(matrix, 0)
	Variable numCols = DimSize(matrix, 1)
	if (numCols <= 0 || numRows <= 0)
		return result
	endif
	
	DFREF waveFolder = GetWavesDatafolderDFR(matrix)

	if (row)
		if (pnt1 < 0 || pnt1 > numRows - 1)
			return result
		endif
		if (pnt2 < 0 || pnt2 > numRows - 1)
			return result
		endif
	else
		if (pnt1 < 0 || pnt1 > numCols - 1)
			return result
		endif
		if (pnt2 < 0 || pnt2 > numCols - 1)
			return result
		endif
	endif	
	
	Variable range = abs(pnt2 - pnt1)
	
	Duplicate /FREE matrix, mask
	mask = 0
	
	if (row)
		mask[pnt1,pnt2][] = 1
		Make /O/N=(numCols) waveFolder:$name /WAVE=result
		MatrixOp /O result = sumCols(matrix * mask) / range
	else
		mask[][pnt1,pnt2] = 1
		Make /O/N=(numRows) waveFolder:$name /WAVE=result
		MatrixOp /O result = sumRows(matrix * mask) / range
	endif
	
	return result
End


// This function rescales the x axis of all traces on a graph to seconds 
// type	0: inclusive scaling (start and end)
//			1: delta scaling (start and delta)
Function SiSt_rescaleAll2Sec(theStart, endOrDelta, [type, graphName, ask])
	Variable theStart, endOrDelta, type, ask
	String graphName

	Prompt theStart, "Set start time in seconds"
	Prompt endOrDelta, "Set end or delta in seconds" 
	
	String errormsg = getErrorStr("SiSt_rescaleAll2Sec")
	String warnmsg = getWarnStr("SiSt_rescaleAll2Sec")
	
	String scaleType = "inclusive;delta"
	
	type = ParamIsDefault(type) || type < 1 ? 0 : 1
	ask = ParamIsDefault(ask) || ask < 1 ? 0 : 1
	
	// some error checking
	if (endOrDelta < theStart)
		printf warnmsg, "the end value is less than the start value, reverse scaling"
	endif
	if (theStart == endOrDelta)
		printf errormsg, "the start and end or delta value need to be different. Stopped"
		return 0
	endif

	String displayType = StringFromList(type, scaleType)
	Prompt displayType, "Choose scale type", popup, scaleType

	String gList = WinList("*", ";", "WIN:1")
	if (ItemsInList(gList) == 0)
		printf errormsg, "no open graphs! Need a graph as input, so nothing more to do"
		return 0
	endif

	if (ParamIsDefault(graphName) || strlen(graphName) == 0)
		graphName = StringFromList(0, gList)
	endif
	
	Prompt graphName, "Choose a graph", popup, gList

	if (ask)
		DoPrompt "Choose Scale Variables", graphName, theStart, endOrDelta, displayType
		if (V_flag)
			// user clicked cancel
			print "---- user aborted scale function"
			return 0
		endif
	endif
	
	if (WhichListItem(graphName, glist) < 0)
		// graph not found in the list
		printf errormsg, "graph '" + graphName + "' not found. Nothing to do"
		return 0
	endif
		
	type = WhichListItem(displayType, scaleType)
	type = type < 0 ? 0 : type		
	
	String theList = TraceNameList(graphName, ";", 1)
	Variable numTraces = ItemsInList(theList)
	
	if (numTraces == 0)
		printf errormsg, "no traces on graph '" + graphName + "'. Nothing to do"
		return 0
	endif
	
	Variable index
	for (index = 0; index < numTraces; index += 1)
		Wave data = TraceNameToWaveRef(graphName, StringFromList(index, theList))
		if (!WaveExists(data))
			continue
		endif
		switch(type)
			case 1:
				SetScale /P x theStart, endOrDelta, "s", data 
				break
			case 0:
			default:
				SetScale /I x theStart, endOrDelta, "s", data
		endswitch
	endfor
	
	return 1
End


// This function calculates the median absolute deviations of a sample size. It is related to the standard
// deviation by a constant scale factor, but is considered a robust statistics, because it is less affected
// by outliers (see also https://en.wikipedia.org/wiki/Median_absolute_deviation).
// Changed the function to use the buildin StatsQuantiles function, that already delivers the MAD.
Function SiSt_MedianAbsoluteDev(data)
	Wave data
	
	if (!WaveExists(data))
		return NaN
	endif
	
	StatsQuantiles /Q data
	Wave W_StatsQuantiles 
	
	return W_StatsQuantiles[%MedianAbsoluteDeviation]
End


#if exists("median")  // Igor 6 doesn't have a median function

Function /WAVE SiSt_robustZScores(data, [med, mad])
	Wave data
	Variable med, mad
	
	String errormsg = getErrorStr("SiSt_robustZScores")
	String warnmsg = getWarnStr("SiSt_robustZScores")

	Wave result
	
	if (!WaveExists(data))
		return result
	endif
	
	DFREF df = GetWavesDataFolderDFR(data)
	String wName = NameOfWave(data)
	Variable rawLoc = strsearch(wName, "_raw", 0)
	String zScrName = ""
	if (rawLoc < 1)
		zScrName = wName + "_rz"
	else
		zScrName = wName[0, rawLoc] + "rz"
	endif
	
	Duplicate /O data, df:$zScrName /WAVE=scores

	if (ParamIsDefault(med))
		med = median(data)
		
		StatsQuantiles /Q data
		Wave W_StatsQuantiles
		
		scores = 0.6745 * (data - med) / W_StatsQuantiles[%MedianAbsoluteDeviation]
	else
		if (ParamIsDefault(mad))
			printf errormsg, "a median absolute deviation is needed when setting the median manually"
			return result
		endif
		
		scores = (data - med) / mad
	endif	
	
	return scores
End

#endif

Function /WAVE SiSt_calcZScores(data, [avg, sd])
	Wave data
	Variable avg, sd

	String errormsg = getErrorStr("SiSt_calcZScores")
	String warnmsg = getWarnStr("SiSt_calcZScores")

	Wave result
	
	if (!WaveExists(data))
		return result
	endif
	
	DFREF df = GetWavesDataFolderDFR(data)
	String wName = NameOfWave(data)
	Variable rawLoc = strsearch(wName, "_raw", 0)
	String zScrName = ""
	if (rawLoc < 1)
		zScrName = wName + "_z"
	else
		zScrName = wName[0, rawLoc] + "z"
	endif
	Duplicate /O data, df:$zScrName /WAVE=scores

	if (ParamIsDefault(avg))
		WaveStats /Q /ZSCR data
		Wave W_ZScores
		scores = W_ZScores[p]
	else
		if (ParamIsDefault(sd))
			printf errormsg, "when a mean is supplied, the standard deviation is also needed"
			return result
		endif
	endif
	
	if (numtype(avg) == 0)
		scores = (data - avg) / sd		
	endif
	
	return scores
End


Function SiSt_SubtractionPanel([name])
	String name
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = "SiStSubtractionPanel"
	endif
	DoWindow /F $name
	if (V_flag == 1)
		if (WinType(name) != 7)
			DoWindow /K $name
		endif
		return 1
	else
		NewPanel /W=(0, 0, 230, 210) /K=1/N=$name as "Subtraction Panel"
	endif
	
	Variable leftOffset = 42
	Variable topOffset = 2
	Variable cWidth = 140
	Variable bWidth = 140
	Variable cHeight = 20
	Variable stdDistance = 5
	
	TitleBox tb_subtractDesc, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, title="\\f02result = trace 1 - trace 2"
	TitleBox tb_subtractDesc, win=$name, fSize= 10, frame= 0, anchor= MC, fixedSize= 1
	
	leftOffset = 10
	topOffset += cHeight + stdDistance
	cWidth = 60 + bWidth
	PopupMenu pm_sourceSelector, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, bodyWidth=bWidth
	PopupMenu pm_sourceSelector, win=$name, title="trace source"
	PopupMenu pm_sourceSelector, win=$name, help={"select the source for the traces"}
	PopupMenu pm_sourceSelector, win=$name, fSize=10, mode=1, popvalue="root datafolder"
	PopupMenu pm_sourceSelector, win=$name, value= #"\"root datafolder;graphs\""
	PopupMenu pm_sourceSelector, win=$name, proc=pmFunc_setTraceSource
	
	topOffset += cHeight + stdDistance
	PopupMenu pm_graphSource, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, bodyWidth=bWidth
	PopupMenu pm_graphSource, win=$name, title="graph", help={"select the source for the traces"}
	PopupMenu pm_graphSource, win=$name, fSize=10, mode=1, value=SiSt_SUBPanelWinList(), disable=1
	
	topOffset += cHeight + (3 * stdDistance)
	PopupMenu pm_wave1Selector, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, bodyWidth=bWidth
	PopupMenu pm_wave1Selector, win=$name, title="trace 1"
	PopupMenu pm_wave1Selector, win=$name, help={"select trace 1 from which trace 2 will be subtracted"}
	PopupMenu pm_wave1Selector, win=$name, fSize=10, mode=1
	PopupMenu pm_wave1Selector, win=$name, userdata="root datafolder"
	PopupMenu pm_wave1Selector, win=$name, proc=pmFunc_selectTraces
	
	topOffset += cHeight + stdDistance
	Titlebox tb_minusLbl, win=$name, pos={leftOffSet, topOffset}, size={cWidth, cHeight}
	Titlebox tb_minusLbl, win=$name, fSize=10, frame=0, anchor= MC, title="\\f02minus"
	
	topOffset += cHeight + stdDistance
	PopupMenu pm_wave2Selector, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, bodyWidth=bWidth
	PopupMenu pm_wave2Selector, win=$name, title="trace 2"
	PopupMenu pm_wave2Selector, win=$name, help={"select trace 1 from which trace 2 will be subtracted"}
	PopupMenu pm_wave2Selector, win=$name, fSize=10, mode=1
	PopupMenu pm_wave2Selector, win=$name, userdata="root datafolder"
	PopupMenu pm_wave2Selector, win=$name, proc=pmFunc_selectTraces
	
	topOffset += cHeight + stdDistance
	Titlebox tb_eqLbl, win=$name, pos={leftOffSet, topOffset}, size={cWidth, cHeight}
	Titlebox tb_eqLbl, win=$name, fSize=10, frame=0, anchor= MC, title="\\f02equals"

	topOffset += cHeight + stdDistance
	SetVariable sv_resultName, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, bodyWidth=bWidth
	SetVariable sv_resultName, win=$name, fSize=10, limits={-inf,inf,0}, value= _STR:"default"
	SetVariable sv_resultName, win=$name, title="result"
	SetVariable sv_resultName, win=$name, proc=svFunc_setResultName
	
	topOffset += cHeight + stdDistance
	TitleBox tb_errorDisplay, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}
	TitleBox tb_errorDisplay, win=$name, fSize=10, frame=0, anchor= MC
	TitleBox tb_errorDisplay, win=$name, title=""	
		
	topOffset += cHeight + stdDistance
	Variable btWidth = 80
	cHeight = 30
	Button btn_subtractTraces, win=$name, pos={((cWidth + 2 * leftOffset)/2) - (btWidth/2), topOffset}, size={btWidth, cHeight}
	Button btn_subtractTraces, win=$name, title="subtract", fstyle=1
	Button btn_subtractTraces, win=$name, proc=btnFunc_subtractTraces
	topOffset += cHeight + stdDistance + 5
	
	adjustPanelSize(name, pixelWidth=cWidth + 2 * leftOffset, pixelHeight = topOffset)
	
	STRUCT WMPopupAction pu
	pu.ctrlName = "pm_sourceSelector"
	pu.win = name
	pu.popStr = "root datafolder"
	pu.eventCode = 2
	pmFunc_setTraceSource(pu)
	
	return 1
End

Function /S SiSt_SUBPanelWinList()
	String list = WinList("*", ";", "WIN:1")
	if (strlen(list) == 0)
		list = ";"
	endif
	return list
End

Function svFunc_setResultName(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch (sv.eventCode)
		case 2:			// enter key pressed
			Button btn_subtractTraces, win=$sv.win, userdata(resultName)= sv.sval
			break
	endswitch
	return 1
End

Function pmFunc_selectTraces(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu

	switch (pu.eventCode)
		case 2:
			String trace1Source = GetUserData(pu.win, "pm_wave1Selector", "")
			String trace2Source = GetUserData(pu.win, "pm_wave2Selector", "")
			String newName = "SUB_"
			
			strswitch (pu.ctrlName)
				case "pm_wave1Selector":
					if (cmpstr(trace1Source, "root datafolder") == 0)
						Button btn_subtractTraces, win=$pu.win, userdata(resultPath)= "root:"
						Button btn_subtractTraces, win=$pu.win, userdata(trace1)= "root:" + pu.popStr
						newName += (pu.popStr)[0,26]
						Button btn_subtractTraces, win=$pu.win, userdata(resultName)= newName
						SetVariable sv_resultName, win=$pu.win, value= _STR:newName
					else
						Wave trace1 = TraceNameToWaveRef(trace1Source, pu.popStr)
						if (!WaveExists(trace1))
							PopupMenu pm_wave1Selector, win=$pu.win, value= ";"
							Button btn_subtractTraces, win=$pu.win, userdata(trace1)= ""
							SetVariable sv_resultName, win=$pu.win, value= _STR:"default"
							Button btn_subtractTraces, win=$pu.win, userdata(resultPath)= "root:"
							Button btn_subtractTraces, win=$pu.win, userdata(trace1)= ""
						else
							Button btn_subtractTraces, win=$pu.win, userdata(resultPath)= GetWavesDatafolder(trace1, 1)
							Button btn_subtractTraces, win=$pu.win, userdata(trace1)= GetWavesDatafolder(trace1, 2)
						endif
						newName += (pu.popStr)[0,26]
						Button btn_subtractTraces, win=$pu.win, userdata(resultName)= newName
						SetVariable sv_resultName, win=$pu.win, value= _STR:newName
					endif
					break
				case "pm_wave2Selector":
					if (cmpstr(trace2Source, "root datafolder") == 0)
						Button btn_subtractTraces, win=$pu.win, userdata(resultPath)= "root:"
						Button btn_subtractTraces, win=$pu.win, userdata(trace2)= "root:" + pu.popStr
//						Button btn_subtractTraces, win=$pu.win, userdata(resultName) = ""
					else
						Wave trace2 = TraceNameToWaveRef(trace1Source, pu.popStr)
						if (!WaveExists(trace2))
							PopupMenu pm_wave2Selector, win=$pu.win, value= ";"
							Button btn_subtractTraces, win=$pu.win, userdata(trace2)= ""
							Button btn_subtractTraces, win=$pu.win, userdata(resultName) = ""
							Button btn_subtractTraces, win=$pu.win, userdata(trace2)= ""
						else
							Button btn_subtractTraces, win=$pu.win, userdata(trace2)= GetWavesDatafolder(trace2, 2)
						endif
					endif
					break
			endswitch
			break
	endswitch
	return 1
End
	
Function pmFunc_setTraceSource(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu
	
	switch (pu.eventCode)
		case 2:
			String quote = "\""
			String list = "", firstSelection = ""
			STRUCT WMPopupAction pua
			pua.ctrlName = "pm_wave1Selector"
			pua.win = pu.win
			pua.eventCode = pu.eventCode
			
			if (cmpstr(pu.popStr, "root datafolder") == 0)
				pua.popStr = StringFromList(0, WaveList("*", ";", ""))
				
				PopupMenu pm_wave1Selector, win=$(pu.win), userData=pu.popStr, value= WaveList("*", ";", "")
				PopupMenu pm_wave2Selector, win=$(pu.win), userData=pu.popStr, value= WaveList("*", ";", "")
				PopupMenu pm_graphSource, win=$(pu.win), disable=1
				
				Button btn_subtractTraces, win=$(pu.win), userdata(resultPath) = "root:"
			else
				PopupMenu pm_graphSource, win=$(pu.win), disable=0
				ControlUpdate /W=$(pu.win) pm_graphSource
				ControlInfo /W=$pu.win pm_graphSource
				if (strlen(S_Value) == 0)
					// no graphs open
					PopupMenu pm_wave1Selector, win=$(pu.win), userData=S_value, value= ";"
					PopupMenu pm_wave2Selector, win=$(pu.win), userData=S_value, value= ";"					
				else
					list =  TraceNameList(S_Value, ";", 1)
					pua.popStr = StringFromList(0, list)
					list = quote + list + quote
					PopupMenu pm_wave1Selector, win=$(pu.win), userData=S_value, value= #list
					PopupMenu pm_wave2Selector, win=$(pu.win), userData=S_value, value= #list
				endif
			endif
			pmFunc_selectTraces(pua)
			break
	endswitch
	return 1
End

Function btnFunc_subtractTraces(btn) : ButtonControl
	STRUCT WMButtonAction &btn
	
	switch (btn.eventCode)
		case 2:
			String msg = ""
			Titlebox tb_errorDisplay, win=$btn.win, title=msg, fColor=(0,0,0)
			
			String resultName = GetUserData(btn.win, btn.ctrlName, "resultName")
			String resultPath = GetUserData(btn.win, btn.ctrlName, "resultPath")
			String trace1Name = GetUserData(btn.win, btn.ctrlName, "trace1")
			String trace2Name = GetUserData(btn.win, btn.ctrlName, "trace2")
			
			DFREF path = $resultPath
			if (DatafolderRefStatus(path) == 0)
				DFREF path = root:
				msg = "WARNING: path invalid, set to root:"
				Titlebox tb_errorDisplay, title=msg
			endif
			Wave trace1 = $trace1Name
			if (!WaveExists(trace1))
				if (!WaveExists(trace2))
					msg = "Neither trace 1 nor trace 2 can be found!"
				else
					msg = "Trace 1 doesn't exist!"
				endif
				Titlebox tb_errorDisplay, win=$btn.win, title=msg, fColor=(65535, 0, 0)
				return 0
			endif
			Wave trace2 = $trace2Name
			if (!WaveExists(trace2))
				msg = "Trace 2 doesn't exist!"
				Titlebox tb_errorDisplay, win=$btn.win, title=msg, fColor=(65535, 0, 0)
				return 0
			endif
			if (strlen(resultName) == 0)
				msg = "Need a name for the result wave!"
				Titlebox tb_errorDisplay, win=$btn.win, title=msg, fColor=(65535, 0, 0)
				return 0
			endif			

			Duplicate /O trace1, path:$resultName /WAVE=result
			
			result = trace1 - trace2
			
			printf "- trace subtraction: %s = %s - %s, result located in %s\r", resultName, NameOfWave(trace1), NameOfWave(trace2), resultPath
			Display result
			ModifyGraph /W=$S_name mode($(NameOfWave(result)))=4, marker($(NameOfWave(result)))=19
			break
	endswitch
	
	return 1
End


Function adjustMeanGraph()

	String theWinList = WinList("*", ";", "WIN:1")
	if (strlen(theWinList) == 0)
		// no graph, nothing more to do
		return 0	
	endif
	
	String theGraph = StringFromList(0, theWinList)
	String traceList = TraceNameList(theGraph, ";", 1)
	
	Variable index = 0
	for (index = 0; index < ItemsInList(traceList); index += 1)
		Wave trace = TraceNameToWaveRef(theGraph, StringFromList(index, traceList))
		SetScale /P x 1, 1, "", trace
	endfor
	
	ModifyGraph gfont=$ks_SIST_defFont, standoff=0
	SetAxis/A/N=1/E=1 left
	SetAxis/A/N=1/E=1 bottom
	
End


Function /S SiSt_makeBarGraph4GroupList(keyList, dataName, [point, gName, resultPrefix, add, verbose])
	String keyList, dataName, resultPrefix, gName
	Variable point, add, verbose

	verbose = ParamIsDefault(verbose) || verbose < 0 ? 1 : verbose
	String errmsg = getErrorStr("SiSt_makeBarGraph4GroupList")
	String result = ""
	String drPattern = "%s_%s_%s_p%d"
	String mPattern = "%s_%s_mean_p%d"
	String ePattern = "%s_%s_err_p%d"
	String lPattern = "%s_%s_lbl"
		
	Variable numKeys = 0
	String keys = ""
	
	if (strlen(keyList) == 0)
		return result
	else
		keys = getKeysFromKeyList(keyList)
		numKeys = ItemsInList(keys)		
		if (numKeys == 0)
			if (verbose)
				printf errmsg, "missing group keys in the list, don't know how to group data"
			endif
			return result
		endif
	endif
	if (strlen(dataName) == 0)
		if (verbose)
			printf errmsg, "missing name of data wave"
		endif
		return result
	endif
	if (ParamIsDefault(resultPrefix))
		resultPrefix = "Extr"
	endif
	if (ParamIsDefault(gName))
		gName = "BarGraph"
	endif
	
	DFREF cDF = GetDataFolderDFR()
	DFREF bgF = getPackage(sub = "BarGraph")

	// ensure that we only have full positive integers for point selection
	point = ParamIsDefault(point) || point < 0 ? 0 : round(point)
	add = ParamIsDefault(add) || add < 1 ? 0 : 1

	Variable index = 0


	if (add)
		DoWindow /F $gName
		if (V_flag == 0)
			// graph does not exist
			Display /N=$gName
		endif
	else
		Display /N=$gName
		gName = S_name
	endif
	
	result = gName
	String drName = "", meanName = "", errName = "", lblName = ""
	
	sprintf meanName, mPattern, resultPrefix, dataName, point
	sprintf errName, ePattern, resultPrefix, dataName, point
	sprintf lblName, lPattern, resultPrefix, dataName
	
	Make /O/N=(numKeys) bgF:$meanName /WAVE=means, bgF:$errName /WAVE=errs
	Wave /T lbls = $makeTWaveFromList(keys, labelName = GetDataFolder(1, bgF) + lblName, unique = 1)	
	
	for (index = 0; index < numKeys; index += 1)
		String theKey = StringFromList(index, keys)
		sprintf drName, drPattern, resultPrefix, dataName, theKey, point
		
		String tList = ReplaceString(",", StringByKey(theKey, keyList), ":" + dataName + ";root:")
		tList = "root:" + tList + ":" + dataName
		Variable numTraces = ItemsInList(tList)
					
		Wave data = SiSt_createDataWaveFromList(tList, point = point, resultName = drName, verbose = 0)

		WaveStats /Q/C=1/W data
		Wave M_WaveStats
		
		means[index] = M_WaveStats[%avg]
		errs[index] = M_WaveStats[%sem]

	endfor
	
	if (add)
		AppendToGraph /W=$gName means
		ErrorBars $NameOfWave(means) Y,wave=(errs,)
		ModifyGraph hbFill($NameOfWave(means))=5
	else
		AppendToGraph /W=$gName means vs lbls
		ErrorBars $NameOfWave(means) Y,wave=(errs,)
		SetAxis/A/N=1/E=1 left
		ModifyGraph standoff=0
		ModifyGraph tick(bottom)=3,catGap(bottom)=0.2,barGap(bottom)=0.05
	endif
	
	return result
End


// *********************************************************************
// **********    W I N D O W    H O O K    F U N C T I O N
// *********************************************************************

Function plotTausFromTable(s)
	STRUCT WMWinHookStruct &s

	Variable hookResult = 0
	switch(s.eventCode)
		case 3:						// "Mousedown"
			if (s.eventMod & 0x10)	// right-clicked?
				String selection = StringByKey("SELECTION", TableInfo(s.winName, -2))
				if (strlen(selection) != 0)
					Variable selColumn = str2num(StringFromList(ItemsInList(selection, ",") - 1, selection, ","))
					String info = TableInfo(s.winName, selColumn)
					Wave fitValues = $(StringByKey("WAVE", info))
					String lbl = GetDimLabel(fitValues, 1, selColumn - 1)
					if (cmpstr(lbl, "tau") == 0)
						PopupContextualMenu "\\M0plot taus (line);\\M0plot taus (category)"
						switch (V_flag)
							case 1:
								hookResult = plotTaus(fitValues)						
								break
							case 2:
								hookResult = plotTaus(fitValues, type="box")
								break
							default:
								hookresult = 0
						endswitch
					endif
				endif
			endif
			break
	endswitch
	
	return hookResult
End

// *** For Igor 8 and higher only ***************************************
#if IgorVersion() >= 8.00

Function /WAVE SiSt_factorize(twave)
	Wave /T twave
	
	Wave resultWave
	
	if (!WaveExists(twave) || numpnts(twave) == 0)
		return resultWave
	endif
	
	String nameList = makeListFromWave(NameOfWave(twave), sorted = 1, unique = 1)
	Variable numItems = ItemsInList(nameList)
	Variable index = 0

	Make /N=(numpnts(twave)) /O $(NameOfWave(twave) + "_fkt") /WAVE=factors
	
	for (index = 0; index < numItems; index += 1)
		Extract /FREE /INDX /O twave, indices, cmpstr(twave, StringFromList(index, nameList)) == 0
		factors[indices] = index + 1		// - wave assignment using index wave since Igor 8
	endfor
	CopyDimLabels twave, factors			// - since Igor 8
	
	return resultWave
End

#endif
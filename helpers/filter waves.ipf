#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.1
#pragma Igorversion = 5
#include <Strings as Lists>
//by Roland Bock, 1998

//! This macro creates a list of the waves of the frontmost graph or table 
// and calls the filter function with this list and a suggested, adjustable 
// filter factor. This macro serves as the front end to the "Glaetten" function.
// @param smoothFactor: a variable setting the smoothing for the waves (default: 3)
//- 
macro Filter(smoothFactor)
	variable smoothFactor = 3
	Prompt smoothFactor, " please enter smooth factor: "
	
	string waveliste = WaveList("!*_2nd", ";","WIN:")
	
	Glaetten(waveliste, smoothFactor)
end
	
//******************************

//! This function takes a list of waves and a smooth factor to smooth every
// wave in this list with the smooth factor.
// @param waveliste: a semicolon separated string list with wave names in the current data folder
// @param smoothFactor: a variable to determine the smoothing with the build-in smooth Function
// @returns: nothing - the waves will be smoothed in place
//-
function Glaetten(waveliste, smoothFactor)
	string waveliste
	variable smoothFactor
	
	string theWave
	variable index = 0
	do
		theWave = GetStrFromList(waveliste, index, ";")
		if (strlen(theWave) == 0)
			break
		endif
		
		wave temporalwave = $theWave
		smooth smoothFactor, temporalwave
		index += 1
	while (1)
end

//******************************

﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.0
#include <Concatenate Waves>
#include "loadABF212"
#include "CloseAll"
#include "hoonsanalysis"

// © by Roland Bock 2000
// 
// FEB 2021: changed file text encoding to 'UTF-8' 


Menu "ABF"
	"Hoon's Area	/7", CalcArea() 
End

Proc CalcArea()
	
	silent 1
	string NameStorage = "AreaFile"
	string fileName, areaName, Liste, cmd
	
	if (!exists(NameStorage))
		Make/T/N=0/O $NameStorage
	endif
	LoadAxonBinaryFile()
	killwaves/Z wave0, ScaleArray, SampleSeq			//waves created by AxonLoader
	Liste = WaveList("w*", ";", "")
	fileName = GetABFFileName(GetBaseName(GetStrFromList(Liste, 0, ";")))
	sprintf cmd, "%s[%g] = {\"%s\"}", NameStorage, numpnts($NameStorage), fileName
	execute cmd				//nasty construct to append a text to a wave in proc
	CloseSpecificWindows("Graph*", 1)
	GetRidOfSecondWaves(Liste)
	Liste = WaveList("w*", ";", "")
	RescaleImpWaves(Liste)
	ConcatenateWavesInList(fileName, Liste)
	DisplayConcTrace(("G_" + fileName), fileName) 
	areaName = GetAreaOfBaseline(Liste, 0, 0.06, 0.18, 2)
	
	DisplayAreaTable("AreaTable", NameStorage, areaName)
	Killwaves/A/Z
End

//**********

//function to calculate the area between y=0 and trace. Summates all the areas of 
//the traces in the list. Needs as input a list of waves, the start and end values to caculate
//the baseline and the start and end values for the area calculation. It returns the name
//of the wave used to store the value as a string.

function/S GetAreaOfBaseline(list, baseXB, baseXE, AreaXB, AreaXE)
	string list
	variable baseXB, baseXE, AreaXB, AreaXE
	
	string theWave
	string storageWave = "Areas"
	variable index = 0, totalArea = 0
	variable baseline = 0 
	
	if (!exists(storageWave))
		make /O/N=0 $storageWave
	endif
	
	wave storage = $storageWave
	do
		theWave = GetStrFromList(list, index, ";")
		if (strlen(theWave) == 0)
			break
		endif
		wave temp = $theWave
		
		if (index == 0) 						//execute only for the first wave to get baseline
			WaveStats /Q /R=(baseXB, baseXE) temp
			baseline = V_avg
		endif
		
		temp -= baseline
		totalArea += area(temp, AreaXB, AreaXE)
		index += 1
	while(1)
	
	storage[numpnts(storage)] = {totalArea}
	printf "The total area is: %g\r", totalArea
	return storageWave
end

//**********

//Takes a single string of a wavename as input and determines the basename
//by finding the first underscore. The part until the first underscore will be returned
//as the basename

function/S GetBaseName(theWave)
	string theWave
	
	string baseName = ""
	if (strsearch(theWave, "_", 0) > -1)
		baseName = theWave[0, (strsearch(theWave, "_", 0) - 1)]
	endif
	return baseName
end

//**********

//The ABF file loader creates waves with the pattern "w...abf_#" while the "..." is the
//filename of the loaded file. The filename is returned as a string.

function/S GetABFFileName(baseName)
	string baseName
	
	string fileName = ""
	if (strlen(baseName) == 12)
		fileName = baseName[1,11]
	endif
	return fileName
end

//**********


Function DisplayAreaTable(GraphName, TextWave, ValueWave)
	string GraphName, TextWave, ValueWave
	
	string WindowList = ""
	
	wave AreaFile = $TextWave
	wave Areas = $ValueWave
	
	WindowList = WinList(GraphName, ";", "")
	if(strlen(WindowList) == 0)
		Edit/W=(623,44,944,407)/K=1 AreaFile,Areas as GraphName
		DoWindow /C $GraphName
	else
		DoWindow /F $GraphName
	endif
	
End


//**********
// use this function to display the concatenated traces and prevent them from being
// deleted.

Function DisplayConcTrace(GraphName, TraceName)
	string GraphName, TraceName
		
	string WindowList = ""
	wave Trace = $TraceName

	WindowList = WinList(GraphName, ";", "")
	if(strlen(WindowList) == 0)
		Display/K=1 Trace as GraphName
		DoWindow /C $GraphName
	else
		DoWindow /F $GraphName
	endif
	
End

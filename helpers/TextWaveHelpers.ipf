#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.1

// A collection of functions to process text waves
//
// Written by Roland Bock (December 2009)
// ** version 1.0 (Dec 2009)

// This function searches a text wave for the occurrences of the duplicate text
// entries. It will keep the first and delete all the rest of them. On request the 
// entries will be sorted and than checked.
// parameter:
//		textWave:			the text wave with duplicates
//		sorted:			0	(default) to keep the wave as is
//						1	text wave can be sorted, quicker
Function removeDuplicates(textWave, [sorted])
	Wave /T textWave
	Variable sorted
	
	if (!WaveExists(textWave))
		// nothing more to do just quit
		return 0
	endif
	
	if (ParamIsDefault(sorted))
		sorted = 0
	endif
	
	Variable index, elements
	String theItem = ""
	
	switch(sorted)
		case 1:
			Sort textWave, textWave
		default:
			for (elements = 0; elements < numpnts(textWave); elements += 1)
				theItem = textWave[elements]
				index = elements + 1
				if (index >= numpnts(textWave))
					continue
				endif
				do
					if (cmpstr(theItem, textWave[index]) == 0)
						DeletePoints index, 1, textWave
					elseif (sorted)
						break
					else
						index += 1
					endif
				while(index < numpnts(textWave))
			endfor
	endswitch
End
#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6
#pragma version = 2.7
#include "WindowHelper", version >= 2.7

// by Roland Bock
//
// This set of functions will average a section of a wave and subtract the average from the whole trace. 
// It is designed to set the baseline to zero to be able to compare the amplitudes of peaks. 
//
//	WARNING: the functions will operate on the original data waves and modify them. 
//
// version 2.7 (September 2019): added an Igor > v7 function to automatically load the help file with the 
//											compilation of this help file.
// version 2.6 (September 2019): added the straighten baseline function and integrated that into the panel
// version 2.5 (June 2015): made sure the panel closes without dialog
// version 2.4 (June 2015): changed input to use a panel and obeying cursor placement on selected graph
// version 2.3 (April 2015): added results into the wave note of the traces to keep track of them,
//                   added menu for easier access, handling all traces on a graph
//                   keeping count of how many times the function has been applied to the same trace
// version 2.2: January 2013
// version 2.1: June 2012
// version 2: August 2010
// version 1: modified August 2002

#if Exists("OpenHelp") != 0	// only for Igor versions 7 and higher
static Function AfterCompiledHook()
	loadHelp()
	return 0
End

static Function loadHelp()
	String fPath = FunctionPath("")	
	
	if (cmpstr(fPath[0], ":") == 0)
		// either buildin procedure window or packed procedure or procedures not compiled
		return -1
	endif
	
	String fName = ParseFilePath(0, fPath, ":", 1, 0)
	String hName = ReplaceString(".ipf", fName, ".ihf")
	String wList = WinList(hName, ";", "WIN:512")

	if (ItemsInList(wList) > 0)
		// help file already open, nothing more to do
		return 0
	endif

	String path = ParseFilePath(1, fPath, ":", 1, 0)
	
	// create full path to help file, which should have the same file name, but a different
	// 	file ending (.ihf instead of .ipf)
	String help = path + hName
	OpenHelp /V=0/Z help
	return V_flag
End
#endif


// **************************      S T A T I C     C O N S T A N T S     ***********************
static Strconstant ksPackageName = "SetToZero"

static Strconstant ks_ErrorMsg = "** ERROR (%s): %s.\r"

static Strconstant ks_baseKey = "BASELINE"
static Strconstant ks_stdKey = "BASESTD"
static Strconstant ks_semKey = "BASESEM"
static Strconstant ks_rangeSt = "BASESTART"
static Strconstant ks_rangeEnd = "BASEEND"
static Strconstant ks_applied = "ZEROED"

static Strconstant ks_baseWave = "BaseLine"
static Strconstant ks_stdWave = "BaseLineSTD"
static Strconstant ks_lastNameUsed = "lastBLName"

static Strconstant ks_baseGraph = "BaselineGraph"
static Strconstant ks_baseGraphTitle = "Baseline Graph"
static Strconstant ks_baseTable = "BaselineTable"
static Strconstant ks_baseTableTitle = "Baseline Table"

static Strconstant ks_ZeroPanelName = "SetToZeroPanel"
static Strconstant ks_PanelStartTime = "ZeroPanelStartTime"
static Strconstant ks_PanelEndTime = "ZeroPanelEndTime"
static Strconstant ks_PanelZeroMode = "ZeroMode"

static Constant k_PanelFSize = 12


// **************************      S T A T I C     F U N C T I O N S     ***********************
static Function /DF getPackage()
	DFREF cDF = GetDatafolderDFR()
	
	NewDatafolder /O/S root:Packages
	NewDatafolder /O/S $ksPackageName
	DFREF package = GetDatafolderDFR()
	SetDatafolder cDF
	return package
	
	
//	DFREF packages = root:Packages
//	if (DatafolderRefStatus(packages) == 0)
//		NewDatafolder /S root:Packages
//		NewDatafolder /S $ksPackageName
//		DFREF package = GetDatafolderDFR()
//		SetDatafolder cDF
//	else
//		DFREF package = packages:$ksPackageName
//		if (DatafolderRefStatus(package) == 0)
//			NewDatafolder /S packages:$ksPackageName
//			DFREF package = GetDatafolderDFR()
//			SetDatafolder cDF
//		endif
//	endif
	return package
End

static Function /S getPackageStr(name)
	String name
	
	DFREF package = getPackage()
	SVAR str = package:$name
	if (!SVAR_Exists(str))
		String /G package:$name = ""
	endif
	
	return (GetDatafolder(1, package) + name)
End

static Function /S getErrorStr(name)
	String name
	
	String errormsg = ""
	sprintf errormsg, ks_ErrorMsg, name, "%s"
	return errormsg
End

static Function /S getVarByName(name)
	String name
	DFREF package = getPackage()
	NVAR var = package:$name
	if (!NVAR_Exists(var))
		Variable /G package:$name = 0
	endif
	return (GetDatafolder(1, package) + name)
End

static Function /S getLastBLName()
	return getPackageStr(ks_lastNameUsed)
End

static Function /WAVE getBLWave([type])
	String type
	
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "avg"
	endif
	
	DFREF package = getPackage()
	Wave result
	
	strswitch (LowerStr(type))
		case "std":
			Wave result = package:$ks_stdWave
			break
		case "avg":
		default:
			Wave result = package:$ks_baseWave			
	endswitch
	return result
End

// **************************     M E N U    ***********************

Menu "Macros"
	Submenu "Baseline Adjustment"
		"\\M0Set traces 2 zero (top graph) ...", SetAllFGraph2ZeroInput()
		"Copy baseline values ...", CopyBaseLineToNew()
		"-"
		"Show baseline graph", ShowBaseLine()
		"Show baseline table", ShowBaseLine(type="table")
	End
End


// **************************     F U N C T I O N S     ***********************

// This function determines the base line value and substracts it from the wave, 
// setting its starting data point to zero. It returns a string with two key words
// "BASE:" with the base line value and "STDEV:" for the standard deviation
Function/S SetToZero(theWave, startTime, endTime)
	String theWave							// the wave name
	Variable startTime, endTime				// start/end point for baseline value determination
	
	String results
	if (exists(theWave))
		Wave temp = $theWave
		WaveStats/Q/R=(startTime, endTime) temp
		temp = temp - V_avg
	endif
	results = "BASE:" + num2str(V_avg) + ";" + "STDEV:" + num2str(V_sdev)
	return results								// return values
End

Function /S SetWaveToZero(theWave, sTime, eTime)
	Wave theWave
	Variable sTime, eTime
	
	String theNote = ""
	if (!WaveExists(theWave))
		return theNote
	endif
	theNote = note(theWave)
	Variable applied = NumberByKey(ks_applied, theNote)
	if (numtype(applied) == 2)
		applied = 1
	else
		applied += 1
	endif	

	WaveStats /C=0/W/Q/R=(sTime, eTime) theWave
	Wave M_WaveStats
	theWave -= M_WaveStats[%avg]
	
	theNote = ReplaceNumberByKey(ks_baseKey, theNote, M_WaveStats[%avg])
	theNote = ReplaceNumberByKey(ks_stdKey, theNote, M_WaveStats[%sdev])
	theNote = ReplaceNumberByKey(ks_semKey, theNote, M_WaveStats[%sem])
	theNote = ReplaceNumberByKey(ks_rangeSt, theNote, sTime)
	theNote = ReplaceNumberByKey(ks_rangeEnd, theNote, eTime)
	theNote = ReplaceNumberBykey(ks_applied, theNote, applied)
	Note /K theWave, theNote
	
	return theNote
End

// This function will straighten the baseline of recorded traces using in a line fit, assuming that the drift is
// constant. 
Function /S StraightenBaseline(theWave, startPoint, endPoint)
	Wave theWave
	Variable startPoint, endPoint
		
	String errormsg = getErrorStr("StraightenBaseline")
	String result = ""
	
	
	if (!WaveExists(theWave))
		printf errormsg, "Wave does not exist"
		return result
	endif
	if (startPoint < 0 || startPoint >= numpnts(theWave))
		printf errormsg, "start point is outside the wave point range - stopped"
		return result
	endif
	if (endPoint < 0 || endPoint >= numpnts(theWave))
		printf errormsg, "end point is outside the wave point range - stopped"
		return result
	endif
	
	DFREF wd = GetWavesDataFolderDFR(theWave)
	String newName = NameOfWave(theWave) + "_dat"
	Make /N=(endPoint - startPoint) /O wd:$newName /WAVE=dataWave
	SetScale /P x pnt2x(theWave, startPoint), deltax(theWave), "s", dataWave
	result = GetWavesDataFolder(dataWave, 2)

	dataWave = theWave[p + startPoint]
	
	CurveFit /Q/M=0/W=2 line, dataWave
	Wave W_coef
	
	Make /N=(endPoint - startPoint) /O $"values" /WAVE=sv
	SetScale /P x pnt2x(theWave, startPoint), deltax(theWave), "s", sv
	sv = W_coef[0] + W_coef[1] * x

	dataWave -= sv

	return result
End

Function /S StraightenAllBaselines(theList, startPoint, endPoint)
	String theList
	Variable startPoint, endPoint
	
	String errormsg = getErrorStr("StraightenAllBaselines")
	String theWave, result = "", sr = ""
	Variable index

	theList = SortList(theList, ";", 16)
	Variable numItems = ItemsInList(theList)
	if (numItems == 0)
		printf errormsg, "given wave list is empty - stopped"
		return result
	endif
	
	for (index = 0; index < numItems; index += 1)
		theWave = StringFromList(index, theList)		
		sr = StraightenBaseline($theWave, startPoint, endPoint)
		if (strlen(sr) > 0)
			result = AddListItem(sr, result, ";", Inf)
		endif
	endfor
	
	if (ItemsInList(result) < numItems)
		printf errormsg, "encountered errors, not all waves were processed"
	endif
	
	return result
End

Function /S StraightenBaselineFGraph(startPoint, endPoint, [graphName])
	String graphName
	Variable startPoint, endPoint
	
	String errormsg = getErrorStr("StraightenBaselineFGraph")
	String theList = "", result = ""
	Variable index 
	
	if (ParamIsDefault(graphName) || strlen(graphName) == 0)
		graphName = StringFromList(0, WinList("*", ";", "WIN:1"))
	endif
	if (WinType(graphName) != 1)
		// window is no graph -> stop here
		printf errormsg, "'" + graphName + "' is not a graph"
		return result
	endif
	
	String tList = TraceNameList(graphName, ";", 1)
	Variable numTraces = ItemsInList(tList)
	
	for (index = 0; index < numTraces; index += 1)
		Wave d = TraceNameToWaveRef(graphName, StringFromList(index, tList))
		String dn = GetWavesDataFolder(d, 2)
		theList = AddListItem(dn, theList, ";", index)
	endfor
	
	return StraightenAllBaselines(theList, startPoint, endPoint)
End


// Function to set the baseline of a list of waves to zero. Using the function
// "SetToZero". It creates two waves, a wave named "BaseLine" storing the 
// original baseline value and a wave named "BaseStDev" storing the corresponding
// standard deviation of the baseline. 
// assumptions:
//	a.	all waves have the same base name and different consecutive numbers
//		corresponding to their temporal order otherwise the function "rightorder"
//		fails.
Function SetAllToZero(List, startTime, endTime)
	String List									// semicolon separated list of waves
	Variable startTime, endTime
	
	String theWave = "", theWaveName = "", results = "", dataUnits = ""
	Variable index = 0, unitFlag = 0

	DFREF package = getPackage()

	List = SortList(List, ";", 16)						// ensure correct sorting
	Variable numItems = ItemsInList(List)
	Make /N=(numItems) /O package:$ks_baseWave /WAVE=BaseLine		// make wave for baseline values
	BaseLine = NaN
	Make /N=(numItems) /O package:$ks_stdWave /Wave=BaseStDev		// make wave for stdev of baseline
	BaseStDev = NaN
	
	if (numItems == 0)								// nothing more to do, stop here
		return 0
	endif

	for (index = 0; index < numItems; index += 1)
		theWave = StringFromList(index, List)
		theWaveName = ParseFilePath(0, theWave, ":", 1, 0)
		Wave dw = $theWave
		if (WaveExists(dw))
			results = SetWaveToZero(dw, startTime, endTime)
			if (!unitFlag)
				dataUnits = WaveUnits(dw, -1)
				unitFlag = 1
			endif
		else
			results = SetToZero(theWave, startTime, endTime)
			if (strlen(results) > 0 && !unitFlag)
				dataUnits = WaveUnits($theWave, -1)
				unitFlag = 1
			endif
		endif
						
		BaseLine[index] = NumberByKey(ks_baseKey, results)			// add point
		SetDimLabel 0, index, $theWaveName, BaseLine
		BaseStDev[index] = NumberByKey(ks_stdKey, results)			// add point
	endfor
	// get a wave and find the unit for the y scaling. Scale baseline wave appropiately
	SetScale d 0, 0, dataUnits BaseLine
	return 1
End

// This function creates the input list from a graph to feed to the SetAllToZero function
Function SetAllFGraph2Zero(startTime, endTime, [graphName])
	Variable startTime, endTime
	String graphName
	
	if (ParamIsDefault(graphName) || strlen(graphName) == 0)
		graphName = StringFromList(0, WinList("*", ";", "WIN:1"))	// top graph
	endif
	
	String theList = TraceNameList(graphName, ";", 1)
	String newList = ""
	Variable index
	for (index = 0; index < ItemsInList(theList); index += 1)
		Wave data = TraceNameToWaveRef(graphName, StringFromList(index, theList))
		newList = AddListItem(GetWavesDatafolder(data, 2), newList, ";", Inf)
	endfor
	SetAllToZero(newList, startTime, endTime)
	
	printf "--> adjusted baseline of traces in graph '%s'\r", graphName
	printf "--- calculating mean between %g and %g\r", startTime, endTime
	printf "---> processed traces %s\r", theList
	
	return 1
End

Function SetAllFGraph2ZeroInput()
	Variable startTime, endTime
	String graphName
	Prompt startTime, "Enter start time"
	Prompt endTime, "Enter end time"
	Prompt graphName, "Choose a graph", popup, WinList("*", ";", "WIN:1")
	
	Variable topOffSet, leftOffSet, cLeft, rightSize, bottomSize, height, width
	String graphList = WinList("*", ";", "WIN:1")
	Variable numGraphs = ItemsInList(graphList)
	if (numGraphs == 0)
		graphName = "_none_"
	else
		graphName = StringFromList(0, graphList)
	endif
	
	NVAR sT = $(getVarByName(ks_PanelStartTime))
	sT = 0
	NVAR eT = $(getVarByName(ks_PanelEndTime))
	eT = 0
	NVAR gSelectedMode = $(getVarByName(ks_PanelZeroMode))
	if (gSelectedMode < 1 || gSelectedMode > 2)
		gSelectedMode = 1
	endif
	
	NewPanel /K=1/W=(20,20,80,80) /N=$ks_ZeroPanelName as "Set Traces To Zero"

	topOffSet = 10
	leftOffSet = 10
	cLeft = leftOffSet
	width = 296
	height = 20
	
	PopupMenu pu_graphList, pos={leftOffset,topOffset}, size={width,height}, bodyWidth=220, title="Select graph"
	PopupMenu pu_graphList, fSize=k_PanelFSize, mode=1, value=WinList("*", ";", "WIN:1") + "_none_"
	PopupMenu pu_graphList, popvalue=graphName, proc=popFunc_selectGraph
		
	rightSize = 2 * leftOffSet + width
	topOffSet += height + 5

	width = 145
	SetVariable sv_setStartTime, pos={leftOffSet,topOffSet}, size={width,height}, title="start time"
	SetVariable sv_setStartTime, fSize=k_PanelFSize, limits={-inf,inf,0}, value=sT, disable=2
	
	SetVariable sv_setEndTime, pos={leftOffSet + width + 8, topOffSet}, size={width,height}, title="end time"
	SetVariable sv_setEndTime, fSize=k_PanelFSize, limits={-inf,inf,0}, value=eT, disable=2
	topOffSet += height + 5
	
	width = 70
	GroupBox gb_modeSelection, pos={leftOffSet, topOffset}, size={2 * width, 1.3 * height}
	
	cLeft += 4
	topOffSet += 4

	CheckBox cbx_modeBaseline, pos={cLeft, topOffSet}, size={width, height}, title="mean bsl"
	CheckBox cbx_modeBaseline, fSize=k_PanelFSize, mode=1, value=gSelectedMode==1
	CheckBox cbx_modeBaseline, proc=cbxFunc_chooseMode
	cLeft += width + 5
	
	CheckBox cbx_modelineFit, pos={cLeft, topOffSet}, size={width, height}, title="line fit"
	CheckBox cbx_modelineFit, fSize=k_PanelFSize, mode=1, value=gSelectedMode==2
	CheckBox cbx_modelineFit, proc=cbxFunc_chooseMode
	cLeft += width + 20
	
	CheckBox cbx_displayData, pos={cLeft, topOffSet}, size={width, height}, title="display data"
	CheckBox cbx_displayData, fSize=k_PanelFSize, value=0
	if (gSelectedMode == 1)
		CheckBox cbx_displayData, disable=1
	else
		CheckBox cbx_displayData, disable=0
	endif
	
	cLeft = leftOffSet
	topOffSet += height + 10
	
	height = 22
	width = 60
	
	leftOffSet = (rightSize / 2) - (width / 2)
	Button btn_Zero, pos={leftOffSet, topOffSet}, size={width,height}, title="\f01zero"
	Button btn_Zero, fSize=(k_PanelFSize + 4), userdata=graphName, proc=btnFunc_ZeroOrClose
	
	leftOffSet = (rightSize / 2) + (width / 2) + 5
	Button btn_Close, pos={leftOffSet, topOffset}, size={width, height}, title="close"
	Button btn_Close, fSize=k_PanelFSize, proc=btnFunc_ZeroOrClose
	
	bottomSize = topOffSet + height + 5
	
	adjustPanelSize(ks_ZeroPanelName, pixelWidth = rightSize, pixelHeight = bottomSize)
	
	STRUCT WMPopupAction pu
	pu.win = ks_ZeroPanelName
	pu.eventCode = 2
	pu.popStr = graphName
	popFunc_selectGraph(pu)
		
	return 1
End
	

// Make a graph to show the baseline values 
Function ShowBaseLine([type])
	String type
	
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "graph"
	else
		type = LowerStr(type)
	endif

	DFREF package = getPackage()
	
	Wave bl = package:$ks_baseWave
	Wave blstd = package:$ks_stdWave
	if (!WaveExists(bl))
		return 0
	endif
	
	String windowName = "", windowTitle = ""
	
	strswitch (type)
		case "table":
			windowName = ks_baseTable
			windowTitle = ks_baseTableTitle
			break			
		case "graph":
		default:
			windowName = ks_baseGraph
			windowTitle = ks_baseGraphTitle
	endswitch

	DoWindow /F $windowName
	if (V_flag)
		// window exists, and is up front, so just return here
		return 0
	endif
	
	strswitch (type)
		case "table":
			Edit /K=1/W=(10,44,370,280)/N=$windowName bl.ld, blstd as windowTitle
			break
		case "graph":
		default:
			Display /K=1/M/W=(1,11,15,18)/N=$windowName bl as windowTitle
			if (WaveExists(blstd))
				ErrorBars $(NameOfWave(bl)) Y,wave=(blstd,blstd)
			endif
			ModifyGraph mode=3,marker=8,rgb=(0,0,0)
			Label bottom "trace number"
	endswitch
	return 1
End

Function CopyBaseLineToNew([newName])
	String newName
	
	String errormsg = ""
	sprintf errormsg, ks_ErrorMsg, "CopyBaseLineToNew", "%s"
	
	Wave bl = getBLWave()
	if (!WaveExists(bl))
		DoAlert 0, "Baseline wave does not exist yet!"
		printf errormsg, "Baseline wave does not exist yet"
		return 0
	endif
	
	Wave blstd = getBLWave(type="std")
		
	SVAR lastName = $(getLastBLName())
	
	if (ParamIsDefault(newName) || strlen(newName) == 0)
		if (strlen(lastName) == 0)
			newName = "BaseLine_new"
			lastName = newName
		else
			newName = lastName
		endif
	endif
	
	Prompt newName, "Enter the new name for the baseline wave"
	DoPrompt "Copy Baseline Waves", newName
	if (V_flag)
		return 0
	endif
	
	Duplicate /O bl, $newName
	Duplicate /O blstd, $(newName[0, 26] + "_std")
	
	return 1
End



// **************************************************************************************
// *****************            P A N E L    F U N C T I O N S               ************
// **************************************************************************************


Function btnFunc_ZeroOrClose(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:
			strswitch(bn.ctrlName)
				case "btn_Zero":
					String graphName = GetUserData(bn.win, bn.ctrlName, "")
					if (cmpstr(graphName, "_none_") == 0)
						return 0
					endif
					NVAR sT = $(getVarByName(ks_PanelStartTime))
					NVAR eT = $(getVarByName(ks_PanelEndTime))					
					NVAR gMode = $(getVarByName(ks_PanelZeroMode))
					
					
					switch (gMode)
						case 1:
							SetAllFGraph2Zero(sT, eT, graphName=graphName)
							break
						case 2:
							String results = StraightenBaselineFGraph(sT, eT, graphName=graphName)
							ControlInfo /W=$bn.win cbx_displayData
							if (V_value && ItemsInList(results) > 0)
								Display
								String name = S_value
								Variable index
								for (index = 0; index < ItemsInList(results); index += 1)
									AppendToGraph /W=$name $(StringFromList(index, results))
								endfor
								SetAxis/W=$name/A/N=1 left
								SetAxis/W=$name/A/N=1 bottom
							endif
							break
					endswitch
					break
				case "btn_Close":
				default:
					KillWindow $ks_ZeroPanelName
			endswitch
			break
	endswitch
End


Function popFunc_selectGraph(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu
	
	switch (pu.eventCode)
		case 2:
			if (cmpstr(pu.popStr, "_none_") == 0)
				return 0
			endif
			
			Variable aCsr = strlen(CsrInfo(A, pu.popStr)) > 0
			Variable bCsr = strlen(CsrInfo(B, pu.popStr)) > 0
			
			NVAR gSelectedMode = $(getVarByName(ks_PanelZeroMode))
			
			if (!aCsr)
				SetVariable sv_setStartTime, win=$pu.win, title = "start time", disable = 0
			else
				NVAR sT = $(getVarByName(ks_PanelStartTime))
				switch(gSelectedMode)
					case 1:
						sT = xcsr(A, pu.popStr)
						break
					case 2:
						sT = pcsr(A, pu.popStr)
						break
				endswitch
				SetVariable sv_setStartTime, win=$pu.win, title = "start (A)", disable = 2
			endif
			if (!bCsr)
				SetVariable sv_setEndTime, win=$pu.win, title = "end time", disable = 0
			else
				NVAR eT = $(getVarByName(ks_PanelEndTime))
				switch(gSelectedMode)
					case 1:
						eT = xcsr(B, pu.popStr)
						break
					case 2:
						eT = pcsr(B, pu.popStr)
						break
				endswitch
				SetVariable sv_setEndTime, win=$pu.win, title = "end (B)", disable = 2
			endif
			
			if (aCsr && bCsr && sT > eT)
				SetVariable sv_setStartTime, win=$pu.win, title = "start (B)", disable = 2
				SetVariable sv_setEndTime, win=$pu.win, title = "end (A)", disable = 2
				
				switch(gSelectedMode)
					case 1:
						sT = xcsr(B, pu.popStr)
						eT = xcsr(A, pu.popStr)
						break
					case 2:
						sT = pcsr(A, pu.popStr)
						eT = pcsr(B, pu.popStr)
						break
				endswitch
			endif
			
			Button btn_Zero, win=$pu.win, userdata=pu.popStr
			
			break
	endswitch
End

Function cbxFunc_chooseMode(cb) : CheckBoxControl
	STRUCT WMCheckboxAction &cb
	
	switch (cb.eventCode)
		case 2:					// Mouse up
			NVAR gSelectedMode = $(getVarByName(ks_PanelZeroMode))
			ControlInfo /W=$cb.win pu_graphList
			String gName = S_Value
			
			Variable aCsr = strlen(CsrInfo(A, gName)) > 0
			Variable bCsr = strlen(CsrInfo(B, gName)) > 0
			
			
			strswitch(cb.ctrlName)
				case "cbx_modeBaseline":
					gSelectedMode = 1
					if (aCsr)
						NVAR sT = $(getVarByName(ks_PanelStartTime))
						sT = xcsr(A, gName)
						SetVariable sv_setStartTime, win=$cb.win, title="start (A)", disable = 2
					endif
					if (bCsr)
						NVAR eT = $(getVarByName(ks_PanelEndTime))
						eT = xcsr(B, gName)
						SetVariable sv_setEndTime, win=$cb.win, title="end (B)", disable = 2
					endif
					CheckBox cbx_displayData, win=$cb.win, disable=1
					break
				case "cbx_modelineFit":
					gSelectedMode = 2
					if (aCsr)
						NVAR sT = $(getVarByName(ks_PanelStartTime))
						sT = pcsr(A, gName)
						SetVariable sv_setStartTime, win=$cb.win, title="start (A)", disable = 2
					endif
					if (bCsr)
						NVAR eT = $(getVarByName(ks_PanelEndTime))
						eT = pcsr(B, gName)
						SetVariable sv_setEndTime, win=$cb.win, title="end (B)", disable = 2
					endif
					CheckBox cbx_displayData, win=$cb.win, disable=0
					break
			endswitch
		
			CheckBox cbx_modeBaseline, value = gSelectedMode==1	
			CheckBox cbx_modelineFit, value = gSelectedMode==2
			break
	endswitch
	
	return 0
End
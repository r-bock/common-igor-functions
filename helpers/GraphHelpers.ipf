#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6.3
#pragma version = 5.2
#pragma hide = 0
#include "ListHelpers"
#include "WindowHelper"

// Collection of functions to make graphing easier for my aesthetic sense
// written by Roland Bock 9/15/2008
// ** version 5.2 (April 2021): - add the function GH_convert2bargraph
// ** version 5.1 (March 2021): - added the GH_makeGraphNice function to combine some quick graph adjustments
//                                for a nice looking graph.
// ** version 5 (June 2019): - added a function to scatter overlapping raw datapoints for the display
//										with a bar graph
// ** version 4 (November 2017): - changed getRGBByName function to take a name so that 2 different 
//                               color waves can be created a the same time; added also colors
//											- added a function to rescale traces from graph from 1
// ** version 3.9  (January 2017): added a sub menu with functions to display the color names used in this set
// ** version 3.75 (August 2014) - added putOnGraph function to simplify display of traces
// ** version 3.5 (May 2014) - added scale bar drawing panel
// ** version 3.1 (April 2014)
// ** version 3 (October 2013) - added color 'midBlue' to color wave
// ** version 2.9 (MAR 2013)
// ** version 2.6 (MAR 2012)

Menu "Macros"
	SubMenu "Graph Modifications"
		"change trace color", colorGraphIndiviudally()
		"add slider control to graph", SLDR_addControlPanelToGraph()
		SubMenu "color names"
			"print color names", GH_printColorNames()
			"show color names in table", GH_showColorNamesTable()
			"show color names and values", GH_showColorNamesTable(full=1)
		End
		SubMenu "bar graphs"
			"Covert to bar graph", GH_convert2BarGraph()
		End		
		"-"
		"Make graph nice", GH_makeGraphNice()
		"Rescale Traces from 1", rescaleFromOne()                 //< rescale each trace on graph from 1
		"add dim labels to graph", GH_addDimLabelAnnotation("")
		"remove dim labels from graph", GH_removeLabelsFromGraph("")
		"-"
		"\\M0toggle trace display (top graph)", GH_toggleTraceDisplay()
		"Trace scalebar panel", GH_ScaleBarPanel()
		"-"
		"Scatter data points", GH_ScatterPoints()
	End
End

// ****************************************************************
// *******************    C O N S T A N T S                         *********************
// ****************************************************************
static StrConstant ksAlphabet = "A;B;C;D;E;F;G;H;I;J;K;L;M;N;O;P;Q;R;S;T;U;V;W;X;Y;Z;"
static StrConstant ksPackageName = "RB_GraphHelpersSupport"
static Constant kVersion = 5

static StrConstant ksColorValues = "RBGraphHelpersColorValues"
static StrConstant ksColorWave = "RBGraphHelpersColorRBGWave"

static StrConstant ksColorNameList = "black;red;green;blue;okker;orange;purple;lightRed;lightBlue;lightGreen;lightYellow;lightOrange;lightPurple;lightPink;lightGray;darkRed;darkBlue;darkGreen;darkYellow;darkOrange;midBlue;darkGray;gray;white;"

static StrConstant ksScalebarPanelName = "GH_ScalebarPanelName"

static Constant k_SBVersion = 2

static Strconstant ksQuote = "\""
static Strconstant ksSBUserdataName = "ScalebarCtrl"
static Strconstant ksUDbaseName = "scalebar_"
static Strconstant ksBarKey = "SCALEBARS"
static Strconstant ksBarNum = "BARNUM"

static StrConstant ksScatterPlotHelperName = "ScatterPlotHelper"
static StrConstant ksInputPanelName = "ScatterInputPanel"
static StrConstant ksInputPanelTitle = "Input Panel for Scatter Data"

Strconstant ksGH_ErrorMsg = "** ERROR (%s): %s!\r"
Strconstant ksGH_WarnMsg = "-- PROBLEM (%s): %s!\r"


#if defined(MACINTOSH)
static StrConstant ksPanelFont = "Verdana"
static Strconstant ks_defFont = "Helvetica"
#else
static StrConstant ksPanelFont = "Arial"
static Strconstant ks_defFont = "Arial"
#endif


// ******************              S T R U C T U R E S              *********************

Structure ScatterDataPrefs
	char wName[31]
	char graphName[31]
	double width
	double group
	int16 show
EndStructure

Structure GH_ScaleBar
	int16 version
	char name[32]
	int16 isDefault
	int16 labels
	char hAxis[32]
	char vAxis[32]
	double hStart
	double vStart
	double hDelta
	double vDelta
	char hUnit[15]
	char vUnit[15]
	int16 lineSize
	int16 fontSize
	char hLabel[32]
	char vLabel[32]
EndStructure

// ******************                    S T A T I C S                   *********************

static Function /S getFeedbackStr(name, pattern)
	String name, pattern
	
	String msg = ""
	sprintf msg, pattern, name, "%s"
	return msg
End

static Function /S getErrorStr(name)
	String name
	return getFeedbackStr(name, ksGH_ErrorMsg)
End	
	
static Function /S getWarnStr(name)
	String name
	return getFeedbackStr(name, ksGH_WarnMsg)
End	

static Function loadScatterPrefs(prefs)
	STRUCT ScatterDataPrefs &prefs

	DFREF cDF = GetDatafolderDFR()
	NewDataFolder /O/S root:Packages:$ksScatterPlotHelperName
	DFREF folder = GetDatafolderDFR()
	SetDatafolder cDF
	
	SVAR last = folder:$"lastSettings"
	if (!SVAR_Exists(last))
		String /G folder:$"lastSettings"
		SVAR last = folder:$"lastSettings"
		
		prefs.wName = ""
		prefs.graphName = UniqueName("ScatterPlot", 6, 1)
		prefs.width = 0.1
		prefs.group = 1
		prefs.show = 1
		
		StructPut /S prefs, last		
	endif
	
	StructGet /S prefs, last
	return 1
End

static Function saveScatterPrefs(prefs)
	STRUCT ScatterDataPrefs &prefs
	
	DFREF cDF = GetDatafolderDFR()
	NewDataFolder /O/S root:Packages:$ksScatterPlotHelperName
	DFREF folder = GetDatafolderDFR()
	SetDatafolder cDF
	
	SVAR last = folder:$"lastSettings"
	if (!SVAR_Exists(last))
		String /G folder:$"lastSettings"
		SVAR last = folder:$"lastSettings"
	endif

	StructPut /S prefs, last		
	return 1
End


static Function defaultScaleBar(bar, [num])
	STRUCT GH_ScaleBar &bar
	Variable num
	
	if (ParamIsDefault(num))
		num = 1
	else
		num = num <= 1 ? 1 : round(num)
	endif
		
	bar.version = k_SBVersion
	bar.name = ksUDbaseName + num2str(num)
	bar.isDefault = 1
	bar.labels = 1
	bar.hAxis = "bottom"
	bar.vAxis = "left"
	bar.hStart = 0
	bar.vStart = 0
	bar.hDelta = 1
	bar.vDelta = 1
	bar.hUnit = ""
	bar.vUnit = ""
	bar.lineSize = 2
	bar.fontSize = 12
	bar.hLabel = ""
	bar.vLabel = ""
End

static Function getScaleBar(bar, theName, [barName])
	STRUCT GH_ScaleBar &bar
	String theName, barName
	
	Variable success = -1
	if (ParamIsDefault(barName) || strlen(barName) == 0)
		barName = ksUDbaseName + "1"
	endif
	
	DoWindow $theName
	if (V_flag == 0)
		defaultScaleBar(bar)
		return success
	endif
	
	String scalebarList = getScalebarList(theName)
	Variable numBars = ItemsInList(scalebarList)
	String scalebar = ""
	
	if (numBars == 0)
		defaultScaleBar(bar)
		success = 0
	elseif (WhichListItem(barName, scalebarList) < 0)
		if (numBars > 0)
			scalebar = GetUserData(theName, "", StringFromList(0, scalebarList))
			StructGet /S bar, scaleBar
			success = 1
		else
			defaultScaleBar(bar)
			bar.name = barName
		endif
	elseif (ParamIsDefault(barName) || strlen(barName) == 0)
		scalebar = GetUserData(theName, "", "")
		if (strlen(scalebar) > 0)
			StructGet /S bar, scalebar
			success = 1
		else	
			scalebar = GetUserData(theName, "", barName)
			if (strlen(scalebar) > 0)
				StructGet /S bar, scalebar
				success = 1
			else
				defaultScaleBar(bar)
				bar.name = barName
			endif
		endif
	else
		scalebar = GetUserData(theName, "", barName)
		if (strlen(scalebar) > 0)
			StructGet /S bar, scalebar
			success = 1
		else
			defaultScaleBar(bar)
			bar.name = barName
		endif
	endif
	return success
End

static Function setScaleBar(bar, theName)
	STRUCT GH_ScaleBar &bar
	String theName
		
	DoWindow $theName
	if (V_flag == 0)
		defaultScaleBar(bar)
		return -1
	endif
	
	String scalebar = ""
	StructPut /S bar, scalebar
	
	String infoList = GetUserData(theName, "", ksSBUserdataName)
	String scalebarList = StringByKey(ksBarKey, infoList)
	Variable numBars = ItemsInList(scalebarList, ",")
	Variable maxNum = NumberByKey(ksBarNum, infoList)

	if (WhichListItem(bar.name, scalebarList, ",") < 0)
		scalebarList = AddListItem(bar.name, scalebarList, ",", Inf)
	endif
	
	Variable start = strlen(ksUDbaseName)
	Variable num = str2num((bar.name)[start, strlen(bar.name) - 1])
	
	infoList = ReplaceNumberByKey(ksBarNum, infoList, num)
	infoList = ReplaceStringByKey(ksBarKey, infoList, scalebarList)

	SetWindow $theName userdata($ksSBUserdataName)=infoList
	SetWindow $theName userdata($(bar.name))=scalebar
	SetWindow $theName userdata=scalebar
	return num
End

static Function removeScaleBar(theName, [barName])
	String theName, barName
	
	DoWindow $theName
	if (V_flag == 0)
		printf "** ERROR (removeScaleBar): graph window '%s' doesn't exist, nothing to remove.\r", theName
		return -1
	endif
	
	if (ParamIsDefault(barName) || strlen(barName) < 0)
		barName = ksUDbaseName + "1"
	endif
	
	String infoList = GetUserData(theName, "", ksSBUserdataName)
	String scalebarList = StringByKey(ksBarKey, infoList)
	Variable numBars = ItemsInList(scalebarList, ",")
	Variable barNum = WhichListItem(barName, scalebarList, ",")
	
	if (barNum < 0)
		return 0
	endif
	
	scalebarList = RemoveListItem(barNum, scalebarList, ",")
	numBars = ItemsInList(scalebarList, ",")
	infoList = ReplaceStringByKey(ksBarKey, infoList, scalebarList)

	Variable start = strlen(ksUDbaseName)
	String scalebar = StringFromList(numbars - 1, scalebarList, ",")
	Variable maxNum = str2num(scalebar[start, strlen(scalebar) - 1])
	
	infoList = ReplaceNumberByKey(ksBarNum, infoList, maxNum)
	
	SetWindow $theName userdata($ksSBUserdataName) = infoList
	SetWindow $theName userdata($barName) = ""
	return 1
End

static Function /S getScaleBarList(theGraph)
	String theGraph
	
	DoWindow $theGraph
	if (V_flag == 0)
		return ""
	endif
	
	String infoList = GetUserData(theGraph, "", ksSBUserdataName)
	String scaleBarList = ReplaceString(",", StringByKey(ksBarKey, infoList), ";")
	String updatedList = updateScalebarList(theGraph, scaleBarList)
	if ( cmpstr(scaleBarList, updatedList) != 0)
		setScalebarList(theGraph, updatedList)
	endif
	return updatedList 
End

static Function /S setScalebarList(theGraph, theList)
	String theGraph, theList
	
	DoWindow $theGraph
	if (V_flag == 0)
		return ""
	endif

	Variable numBars = ItemsInList(theList)
	String infoList = GetUserData(theGraph, "", ksSBUserdataName)

	if (numBars == 0)
		infoList = ReplaceStringByKey(ksBarKey, infoList, theList)
		infoList = ReplaceNumberByKey(ksBarNum, infoList, numBars)
	else
		String scalebar = StringFromList(numBars - 1, theList)
		Variable start = strlen(ksUDbaseName)
		Variable num = str2num(scalebar[start, strlen(scalebar) - 1])
		infoList = ReplaceStringByKey(ksBarKey, infoList, ReplaceString(";", theList, ","))
		infoList = ReplaceNumberByKey(ksBarNum, infoList, num)
	endif

	SetWindow $theGraph userdata($ksSBUserdataName)=infoList
	return theList
End	

static Function /S updateScalebarList(theGraph, theList)
	String theGraph, theList
	
	DoWindow $theGraph
	if (V_flag == 0)
		return ""
	endif
	
	Variable numItems = ItemsInList(theList)
	Variable index
	for (index = numItems - 1; index >= 0; index -= 1)
		String name = StringFromList(index, theList)
		DrawAction /W=$theGraph /L=UserFront getgroup=$name
		Variable groupExists = V_flag == 1
		if (!groupExists)
			theList = RemoveListItem(index, theList)
			SetWindow $theGraph userdata($name) = ""
		endif
	endfor
	return theList
End

static Function getMaxScaleBarNum(theGraph)
	String theGraph
	
	if (strlen(theGraph) == 0)
		// empty string, so top window
		theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
	else
		DoWindow $theGraph
		if (V_flag == 0)
			return -1
		endif
	endif
	
	String scalebarList = getScalebarList(theGraph)
	Variable numbars = ItemsInList(scalebarList)
	if (numbars == 0)
		return numbars
	endif
	Variable start = strlen(ksUDbaseName)
	String scalebar = StringFromList(numbars - 1, scalebarList)
	return str2num(scalebar[start, strlen(scalebar) - 1])
End

//static Function setMaxScalebarNum(theGraph, number)
//	String theGraph
//	Variable number
//	
//	if (strlen(theGraph) == 0)
//		// empty string, so top window
//		theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
//	else
//		DoWindow $theGraph
//		if (V_flag == 0)
//			return -1
//		endif
//	endif
//	
//	String dataList = GetUserData(theGraph, "", ksSBUserdataName)
//	dataList = ReplaceNumberByKey(ksBarNum, dataList, number)
//	SetWindow $theName userdata($ksSBUserdataName)=dataList
//	return 1
//End

static Function /DF getPackage()
	DFREF cDF = GetDatafolderDFR()
	DFREF loc = root:Packages
	DFREF package = loc:$ksPackageName
	
	if (DatafolderRefStatus(package) == 1)
		return package
	endif
	if (DatafolderRefStatus(loc) == 0)
		NewDataFolder /S root:Packages
	else
		SetDatafolder loc
	endif

	NewDatafolder /S $ksPackageName
	DFREF package = GetDatafolderDFR()
	SetDatafolder cDF
	return package
End

//static Function /S makeNumberList(number, [start])
//	Variable number, start
//	
//	String numList = ""
//	if (ParamIsDefault(start) || start < 0)
//		start = 0
//	else
//		start = round(start)
//	endif
//	
//	Variable index = 0
//	for (index = start; index <= number; index += 1)
//		numList += num2str(index) + ";"
//	endfor
//	
//	return ksQuote + numList + ksQuote	
//End

static Function /S getAxisList(theGraph, type)
	String theGraph, type
	
	String errormsg = ""
	sprintf errormsg, ksGH_ErrorMsg, "getAxisList", "%s"
	
	String theList = AxisList(theGraph)
	String axis = "", info = "", axType = ""
	Variable axes = ItemsInList(theList), item = 0
	if (axes == 0)
		printf errormsg, "can't find any axis on graph " + theGraph
		return ""
	endif 
	
	strswitch ( LowerStr(type) )
		case "horizontal":
		case "h":
		case "x":
			theList = RemoveListItem(WhichListItem("left", theList), theList)
			theList = RemoveListItem(WhichListItem("right", theList), theList)
			axes = ItemsInList(theList)
			if (axes > 1)
				for (item = 0; item < axes; item += 1)
					axis = StringFromList(item, theList)
					if (cmpstr(axis, "bottom") == 0 || cmpstr(axis, "top") == 0)
						continue
					endif
					axType = StringByKey("AXTYPE", AxisInfo(theGraph, axis))
					if (cmpstr(axType, "left") == 0 || cmpstr(axType, "right") == 0)
						theList = RemoveListItem(item, theList)
					endif
				endfor
			endif
			break
		case "vertical":
		case "v":
		case "y":
			theList = RemoveListItem(WhichListItem("bottom", theList), theList)
			theList = RemoveListItem(WhichListItem("top", theList), theList)
			axes = ItemsInList(theList)
			if (axes > 1)
				for (item = 0; item < axes; item += 1)
					axis = StringFromList(item, theList)
					if (cmpstr(axis, "left") == 0 || cmpstr(axis, "right") == 0)
						continue
					endif
					axType = StringByKey("AXTYPE", AxisInfo(theGraph, axis))
					if (cmpstr(axType, "bottom") == 0 || cmpstr(axType, "top") == 0)
						theList = RemoveListItem(item, theList)
					endif
				endfor
			endif
			break
		default:
			printf errormsg, "need to know the axis type (horizontal or vertical). STOPPED"
			return ""
	endswitch
	
	return theList
End


// return a marker number, default is open circle
Function getMarkerNumber(index)
	Variable index
	
	switch(index)
		case 0:
			return 8	// open circle
		case 1:
			return 5	// open square
		case 2:
			return 7	// open diamond
		case 3:
			return 6	// open triangle
		case 4:
			return 19	// closed circle
		case 5:
			return 16	// closed square
		case 6:
			return 18	// closed diamond
		case 7:
			return 17	// closed diamond
		case 8:
			return 48	// open right triangle
		case 9:
			return 49	// closed right triangle
		case 10:
			return 45	// open left triangle
		case 11:
			return 46	// closed left triangle
		default:
			return 0
	endswitch
end

// returns rgb colors as a 3 number wave with the values for (r, g, b) for color names
//	if the name is not recognized, it returns black as 0,0,0
// available color names:
//	red, blue, green, okker, lightBlue, lightYellow, orange, (lightGray, lightGrey)
Function/S getRGBByName(theColor, [name])
	String theColor, name
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = ksColorValues
	endif
	
	DFREF package = getPackage()
	Make /N=3 /O package:$name /WAVE=tmp
	strswitch(theColor)
		case "red":
			tmp = {65535, 0, 0}
			break
		case "blue":
			tmp = {0, 0, 65535}
			break
		case "green":
			tmp = {0, 65535, 0}
			break
		case "orange":
			tmp = {65535,21845,0}
			break
		case "okker":
			tmp = {65535, 43690, 0}
			break
		case "purple":
			tmp = {26411,1,52428}
			break
		case "lightRed":
			tmp = {65535,49151,49151}
			break
		case "lightBlue":
			tmp = {32768, 54615, 65535}
			break
		case "lightYellow":
			tmp = {65535,65534,49151}
			break
		case "lightOrange":
			tmp = {65535,43688,32768}
			break
		case "lightGreen":
			tmp = {49151,65535,49151}
			break
		case "lightPink":
			tmp = {65535, 49151, 62258}
			break
		case "lightPurple":
			tmp = {51664, 44236, 58982}
			break			
		case "lightGray":
		case "lightGrey":
			tmp = {48059,48059,48059}
			break
		case "darkBlue":
			tmp = {1,3,39321}
			break
		case "darkRed":
			tmp={39321,1,1}
			break
		case "darkGreen":
			tmp={2,39321,1}
			break
		case "darkYellow":
			tmp={39321,39319,1}
			break
		case "darkOrange":
			tmp={26214,8736,0}
			break
		case "midBlue":
			tmp = {32768, 32770, 65535}
			break
		case "grey":
		case "gray":
			tmp = {30583,30583,30583}
			break
		case "darkGray":
		case "darkGrey":
			tmp = {17476, 17476, 17476}
			break
		case "white":
			tmp = {65535, 65535, 65535}
			break
		default:
			tmp = {0, 0, 0}
	endswitch
	return GetWavesDatafolder(tmp, 2)
End

Function /WAVE getWaveRGBByName(theColor)
	String theColor
	
	Wave theWave = $(getRGBByName(theColor))
	return theWave
End

Function /WAVE getWaveRGBByIndex(index)
	Variable index
	
	Variable numColors = ItemsInList(ksColorNameList)
	index = index < 0 ? 0 : index > numColors ? numColors : index
	
	Wave theWave = $(getRGBByName(StringFromList(index, ksColorNameList)))
	return theWave
End

Function getColorNameIndex(name)
	String name
	
	return WhichListItem(name, ksColorNameList, ";", 0, 0)
End

// This function creates a simple 2D 3-column color wave with the values for the following
// colors are taken from the static color list
// It returns the name with the full path of the color wave
Function /S getColorValueWave([new])
	Variable new
	
	new = ParamIsDefault(new) || new < 1 ? 0 : 1
	
	String colorPortions = "red;green;blue;"
	Variable numColors = ItemsInList(ksColorNameList)
	DFREF package = getPackage()
	Wave theWave = package:$ksColorWave
	if (WaveExists(theWave) && !new)
		return GetWavesDatafolder(theWave, 2)
	elseif( !WaveExists(theWave) )
		Make /N=(numColors,3) package:$ksColorWave /WAVE=theWave
	else
		Make /O/N=(numColors,3) $ksColorWave /WAVE=theWave		
	endif
	theWave = 0
	Variable colorItem = 0

	for (colorItem = 0; colorItem < 3; colorItem += 1)
		SetDimLabel 1, colorItem, $(StringFromList(colorItem, colorPortions)), theWave
	endfor
	
	for (colorItem = 0; colorItem < numColors; colorItem += 1)
		Wave theColor = getWaveRGBByName(StringFromList(colorItem, ksColorNameList))
		theWave [colorItem][] = theColor[q]
		SetDimLabel 0, colorItem, $(StringFromList(colorItem, ksColorNameList)), theWave
	endfor

	return GetWavesDatafolder(theWave, 2)
End

Function GH_printColorNames()
	String colorNames = ReplaceString(";", ksColorNameList, "\r     ")
	printf "\r     %s\r", colorNames
End 

Function GH_showColorNamesTable([full])
	Variable full
	
	full = ParamIsDefault(full) || full < 1 ? 0 : 1
	
	String colorTable = "ColorNameTable"
	DoWindow /F $colorTable
	if (V_flag)
		return 0
	endif
	
	Wave colorWave = $(getColorValueWave())
	
	if (full)
		Edit /W=(66,63,460,498)/K=1/N=$colorTable colorWave.ld as "Color Name Table"
	else
		Edit /W=(66,63,258,525)/K=1/N=$colorTable colorWave.l as "Color Name Table"
	endif

	ModifyTable font="Times New Roman"
	ModifyTable format(Point) = 1,width(Point) = 35
	ModifyTable size(colorWave.l) = 14, width(colorWave.l) = 115

	if (full)
		ModifyTable size(colorWave.d) = 12, width(colorWave.d) = 75
	endif

	return 1
End
	
	

Function /S convertCMYK2RGB(cyan, magenta, yellow, black)
	Variable cyan, magenta, yellow, black
	
	String result = ""
	String errormsg = getErrorStr("convertCMYK2RGB")
	
	cyan = cyan < 0 || cyan > 1 ? -1 : cyan
	magenta = magenta < 0 || magenta > 1 ? -1 : magenta
	yellow = yellow < 0 || yellow > 1 ? -1 : yellow
	black = black < 0 || black > 1 ? -1 : black
	
	if (cyan < 0 || magenta < 0 || yellow < 0 || black < 0)
		printf errormsg, "all values must range from 0 to 1 - stopped"
		return result
	endif

	Variable red = 65535 * (1 - cyan) * (1 - black)
	Variable green = 65535 * (1 - magenta) * (1 - black)
	Variable blue = 65535 * (1 - yellow) * (1 - black)
	
	sprintf result, "(%d,%d,%d)", red, green, blue
	return result
End
	

// moves the graph window to the center of the screen and expands it to half
// the screen size
Function moveGraphWindowToCenter(theName, [minWidth, minHeight])
	String theName
	Variable minHeight, minWidth
	
	if (strlen(theName) == 0)
		return 0
	endif
	DoWindow /F $theName
	if (!V_flag)
		return 0
	endif
	if (ParamIsDefault( minWidth ) || minWidth < 0)
		minWidth = -1
	endif
	if (ParamIsDefault( minHeight ) || minHeight < 0)
		minHeight = -1
	endif
	
	moveWindowOnScreen(theName, location="center", minWidth=minWidth, minHeight=minHeight)
End

// removes all traces from a given graph name,
// compatibility function for older versions
Function removeAllTracesFromGraph(theGraphName, [verbose])
	String theGraphName
	Variable verbose
	
	if (ParamIsDefault(verbose))
		verbose = 1
	endif
	removeTracesFromGraph(theGraphName, verbose=verbose)
End

Function removeTracesFromGraph(theGraphName, [theTraceList, verbose])
	String theGraphName, theTraceList
	Variable verbose
	
	String errormsg = getErrorStr("removeTracesFromGraph")
	String warnmsg = getWarnStr("removeTracesFromGraph")

	if (strlen(theGraphName) == 0)
		printf errormsg, "need a graph name to operate"
		return 0
	endif
	
	verbose = ParamIsDefault(verbose) || verbose > 1 ? 1 : 0
	
	DoWindow /F $theGraphName
	if (!V_flag)
		printf errormsg, "no window with the name " + theGraphName + " exist."
		return 0
	endif
	
	if (WinType(theGraphName) != 1)
		printf errormsg, "graph name " + theGraphName + " is NOT a graph."
		return 0
	endif
	
	Variable all = 0	
	if (ParamIsDefault(theTraceList) || strlen(theTraceList) == 0)
		theTraceList = ""
		all = 1
	endif
	
	String theList = TraceNameList(theGraphName, ";", 1)
	if (strlen(theList) == 0)
		// is empty, nothing more to do, so just stop here
		if (verbose)
			printf warnmsg, "no trace on the graph " + theGraphName + ". Nothing to do"
		endif
		return 0
	endif
	
	Variable index = 0
	for (index = ItemsInList(theList) - 1; index >= 0; index -= 1)
		// go backwards for trace names
		String theTrace = StringFromList(index, theList)
		if (all || WhichListItem(theTrace, theTraceList) > -1)
			RemoveFromGraph /W=$theGraphName $theTrace
		endif
	endfor
End

// plots a waves from a key list, where the data folder is the key list.
Function /S GH_displayGraphFromList(theList, [multiple])
	String theList
	Variable multiple
	
	if(ParamIsDefault(multiple))
		multiple = 0
	endif
	multiple = ceil(multiple)
	if (multiple > 1)
		multiple = 1
	elseif (multiple < 0)
		multiple = 0
	endif
	
	Variable index = 0
	String theName = ""
	String theLegend = ""
	String theResult = "", GraphList = ""
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theWaveName = StringFromList(index, theList)
		String theSubject = StringFromList(0, theWaveName, ":")
		Wave theWave = $(":" + theWaveName)
		if (WaveExists(theWave))
			String theNote = note(theWave)
			theNote = ReplaceStringByKey("Subject", theNote, theSubject)
			Note /K theWave, theNote
			if (multiple)
				Display theWave
				GraphList = AddListItem(S_name, GraphList, ";", Inf)
				theLegend = "\\s(" + NameOfWave(theWave) + ") " + theSubject
				Legend /C/N=theLegend/J/F=0/A=MB/X=0.00/Y=0.00/E theLegend
			else
				if (strlen(theName) == 0)
					Display theWave
					theName = S_name
				else
					AppendToGraph /W=$theName theWave
				endif
			endif
		else
			continue
		endif
	endfor
	
	if (multiple)
		theResult = GraphList
	else
		theList = TraceNameList(theName, ";", 1)
		for (index = 0; index < ItemsInList(theList); index += 1)
			String theTrace =  StringFromList(index, theList)
			Wave theWave = TraceNameToWaveRef(theName, theTrace)
			theSubject = StringByKey("Subject", note(theWave))
			theLegend += "\\s(" + theTrace + ") " + theSubject + " "
		endfor
		DoWindow /F $theName
		Legend /C/N=theLegend/J/F=0/A=MB/X=0.00/Y=0.00/E
		AppendText /W=$theName/N=theLegend theLegend
		SetAxis /A/N=1/E=1 left
		SetAxis /A/N=1/E=1 bottom
		theResult = theName
	endif
	
	return theResult
End


// This function creates an simple plot of a wave and optional error bars in a 
// named graph window
Function /S GH_plotWaveOnGraph(data, [error, theWin])
	Wave data, error
	String theWin
	
	Variable new = 0
	if (ParamIsDefault(theWin) || strlen(theWin) == 0)
		Display
		theWin = S_name
		new = 1
	else
		DoWindow /F $theWin
		if (V_flag == 0)
			Display /N=$theWin
			theWin = S_name
			new = 1
		elseif (V_flag > 0)
			// unhide
			DoWindow /HIDE=0 $theWin
			if ( WinType(theWin) >= 2)
				DoAlert /T="Wrong Window Selection" 0, "The window " + theWin + " is not a graph"
				return ""
			endif
		endif
	endif
		
	String dataName = NameOfWave(data)
	String traceList = TraceNameList(theWin, ";", 1)

	if (WhichListItem(dataName, traceList) < 0)
		// trace is not yet displayed on the graph, avoid multiple display
		AppendToGraph /W=$theWin /Q data
	endif

	ModifyGraph /W=$theWin mode($dataName)=4, marker($dataName)=19, rgb($dataName)=(0,0,0)
	if (new)
		ModifyGraph /W=$theWin gfont="Arial", standoff=0
		SetAxis /A/E=1 left
		SetAxis /A/N=1 bottom
	endif
	
	if (!ParamIsDefault(error))
		if (WaveExists(error))
			ErrorBars $dataName Y, wave=(error,error)
		endif
	endif
	
	return theWin
End

// This function returns a list of graph (default) or table names in which the 
// wave is displayed. It returns an empty string if it is not found 
Function /S GH_getGraphFromWave(theWave, [type])
	Wave theWave
	String type
	
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "graph"
	endif
	Variable aType
	strswitch (LowerStr(type))
		case "table":
			aType = 2
			break
		case "graph":
		default:
			aType = 1
	endswitch
	
	
	String wn = NameOfWave(theWave)
	String windowList = WinList("*", ";", "WIN:" + num2str(aType))
	Variable numWins = ItemsInList(windowList)
	Variable win = 0
	String resultList = ""
	
	if (!WaveExists(theWave))
		return resultList
	endif
	
	for (win = 0; win < numWins; win += 1)
		String windowName = StringFromList(win, windowList)
		CheckDisplayed /W=$windowName theWave
		if (V_flag == 1)
			resultList = AddListItem(windowName, resultList, ";", Inf)
		endif
	endfor
	return resultList
End

// This function returns the name of the graph in which all waves are displayed.
// If not all waves are displayed in one graph, the returned string will be empty
Function /S GH_getGraphFromWaveList(theList, [type])
	String theList, type
	
	String resultList = ""
	if (strlen(theList) == 0)
		print "** ERROR (GH_getGraphFromWaveList): need a wave list to process. STOPPED!"
		return resultList
	endif
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "graph"
	endif
	Variable aType = 0
	strswitch(LowerStr(type))
		case "table":
			aType = 2
			break
		case "graph":
		default:
			aType = 1
	endswitch
	
	String tempList = ""
	Variable items = 0, numItems = ItemsInList(theList)
	for (items = 0; items < numItems; items += 1)
		if (items == 0)
			resultList = GH_getGraphFromWave($(StringFromList(items, theList)), type = type)
		else
			tempList = GH_getGraphFromWave($(StringFromList(items, theList)), type = type)
			resultList = intersectLists(resultList, tempList)
		endif
	endfor
	return resultList
End

//********************************************
// *********               GRAPHS

// A simple function to put all waves from a list into a graph
Function /S ListToGraph(theList, [theName])
	String theList, theName
	
	if (strlen(theList) == 0)
		print "** ERROR: nothing in the list!"
		return ""
	endif
	if (ParamIsDefault(theName))
		theName = ""
	endif
	
	Variable index = 0
	String theWave = ""
	
	do
		theWave = StringFromList(index, theList)
		
		if (WaveExists($theWave))
			if (strlen(theName) == 0)
				Display
				theName = S_name
			endif
			DoWindow $theName
			if (!V_flag)
				Display /N=$theName
				theName = S_name
			endif
			AppendToGraph /W=$theName $theWave
		endif
		index += 1
	while(index < ItemsInList(theList))
	return theName
End //

// Plots the wave with theWaveName that is in every data folder in the list dataFolderList in 
// a graph with the name theName.
Function /S DataFolderWavesToGraph(theWaveName, dataFolderList, [theName])
	String theWaveName, dataFolderList, theName
	
	if (strlen(dataFolderList) == 0)
		print "ERROR (DataFolderWavesToGraph): the data folder list is empty."
		return ""
	endif
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = "Wave_Overview"
	endif
	
	String theDF = "", theWaveList = ""
	Variable index = 0
	for (index = 0; index < ItemsInList(dataFolderList); index += 1)
		theDF = StringFromList(index, dataFolderList)
		if (WaveExists($(":" + theDF + ":" + theWaveName)))
			theWaveList = AddListItem(":" + theDF + ":" + theWaveName, theWaveList, ";", Inf)
		endif
	endfor
	
	return ListToGraph(theWaveList, theName=theName)
End // End 

Function colorGraphIndiviudally([theGraph, type])
	String theGraph, type
	
	if (ParamIsDefault(type))
		type = "pattern"
	endif

	Prompt type, "Please select the type:", popup, "color;pattern"

	if (ParamIsDefault(theGraph))
		Prompt theGraph, "Please select the graph:", popup, WinList("*", ";", "WIN:1")
		DoPrompt "Graph Chooser", theGraph, type
	endif
	
	DoWindow /F $theGraph
	
	if (!V_flag)
		print "No graph to work on! STOPPED!"
		return 0
	endif
	
	Variable index = 0, nextNum = 0
	String theTrace, theColorTable
	String theIndexList = WaveList("colorIndex_*", ";", "")
	if (ItemsInList(theIndexList) > 0)
		theIndexList = SortList(theIndexList, ";", 16)	// sort descending, so highest up front
		theTrace = StringFromList(ItemsInList(theIndexList) - 1, theIndexList)
		nextNum = str2num(theTrace[strsearch(theTrace, "_", Inf, 1) + 1, Inf])
	endif
	String theTraceList = TraceNameList(theGraph, ";", 1)
	
	if (ItemsInList(theTraceList) == 0)
		print "No traces are displayed on the graph! STOPPED!"
		return 0
	endif
	
	for (index = 0; index < ItemsInList(theTraceList); index += 1)
		theTrace = StringFromList(index, theTraceList)
		Wave tmp = TraceNameToWaveRef(theGraph, theTrace)
		nextNum += 1
		String theIndex = "colorIndex_" + num2str(nextNum)
		Make /N=(numpnts(tmp)) $(theIndex) = p + 1
		DoWindow /F GraphColorIndices
		if (V_flag)
			AppendToTable /W=GraphColorIndices $(theIndex)
		else
			Edit /K=1 /N=GraphColorIndices $(theIndex)
		endif
		strswitch(type)
			case "color":
				theColorTable = getColorValueWave()
				ModifyGraph /W=$theGraph zColor($theTrace)={$(theIndex),*,*,cindexRGB,0,$theColorTable}
				break
			case "pattern":
			default:
				ModifyGraph zpatNum($theTrace)={$(theIndex)}
		endswitch
	endfor
End

// This function duplicates a trace to map markers based on their data values. The duplicated
// wave is to be used to display different markers
Function mapPointers(theTrace, [limits, pointers, specialZero, zeroPointer])
	Wave theTrace
	String limits, pointers
	Variable specialZero, zeroPointer
	
	if (!WaveExists(theTrace))
		// if the wave doesn't exist, stop here
		print "(mapPointers) wave does not exist!"
		return 0
	endif
	Variable index, theLimit, thePointer, choose
	if (ParamIsDefault(limits) || strlen(limits) == 0)
		// if no limits are given, divide the data range into 3 equal parts
		WaveStats /C=1 /W /Q theTrace
		Wave M_WaveStats
		Variable step = (M_WaveStats[12] - M_WaveStats[10]) / 3
		theLimit = M_WaveStats[10] + step
		do
			limits = AddListItem(num2str(theLimit), limits, ";", Inf)
			theLimit += step
			index += 1
		while (index < 1)
	endif
	if (ParamIsDefault(pointers) || strlen(pointers) == 0)
		choose = 1
	endif
	if (ParamIsDefault(specialZero))
		specialZero = 0
	else
		specialZero = floor(specialZero)
		if (ParamIsDefault(zeroPointer))
			zeroPointer = 0
		elseif (zeroPointer < 0 || zeroPointer > 62)
			zeroPointer = 0
		endif
	endif
	
	// now figure out the name for the marker wave
	String theName = NameOfWave(theTrace)
	String newName
	if (strlen(theName) < 26)
		newName = "MrkW_" + theName
	else
		newName = "MrkW_" + theName[0, 25]
	endif
	// find out where the wave is located and duplicate it in the same place
	DFREF wDF = GetWavesDataFolderDFR(theTrace)
	Duplicate /O theTrace, wDF:$newName /WAVE=markerWave
	
	for (index = 0; index < ItemsInList(limits); index += 1)
		theLimit = str2num(StringFromList(index, limits))
		if (index == 0)
			if (choose)
				thePointer = getMarkerNumber(index)
			else
				thePointer = str2num(StringFromList(index, pointers))
			endif
			markerWave = theTrace[p] <= theLimit ? thePointer : NaN
		endif
		if (choose)
			thePointer = getMarkerNumber(index + 1)
		else
			thePointer = str2num(StringFromList(index + 1, pointers))
		endif
		markerWave = theTrace[p] > theLimit ? thePointer : markerWave[p]	
	endfor
	if (specialZero)
		markerWave = theTrace[p] == 0 ? zeroPointer : markerWave[p]
	endif
End

//! This function rescales all traces from the named graph from 1 for a nicer display.
// @param graphName: the graph name (default: top graph)
// @param start: first number to rescale from (default: 1)
// @param increment: scaling increment between numbers (default: 1)
// @param unit: x scale unit (default: <empty>)
// @return: TRUE (1) for success or FALSE (0) for no scaling
//-
Function rescaleFromOne([graphName, start, increment, unit])
	String graphName, unit
	Variable start, increment
	
	start = ParamIsDefault(start) ? 1 : start
	increment = ParamIsDefault(increment) ? 1 : increment
	Variable keepUnit = 0
	
	if (ParamIsDefault(graphName) || strlen(graphName) == 0)
		graphName = StringFromList(0, WinList("*", ";", "WIN:1"))
	endif
	if (ParamIsDefault(unit) || strlen(unit) == 0)
		keepUnit = 1
	endif
	
	DoWindow /F $graphName
	if (V_flag == 0)
		print "Graph does not exist."
		return 0
	endif
	
	String traceList = TraceNameList(graphName, ";", 1)
	Variable numTraces = ItemsInList(traceList)
	Variable index
	for (index = 0; index < numTraces; index += 1)
		Wave trace = TraceNameToWaveRef(graphName, StringFromList(index, traceList))
		if (keepUnit)
			SetScale /P x start, increment, WaveUnits(trace, 0), trace
		else
			SetScale /P x start, increment, unit, trace
		endif
	endfor
	
	return 1
End

//! A helper function to quickly make graphs look nice for myself. It removes the axis
// standoff and autoscales the axis to nice values and per default autoscales them from 
// zero.
// @param gName: graph name, default is the top graph if left empty
// @param fromZero: a flag, TRUE (1: default) to autoscale the axis from zero
// @param font: a font name, defaults to "Helvetica" on Mac and "Arial" on Windows PC
// @param fsize: a number for the font size, defaults to 16
// @param msize: a number for the marker size, defaults to 4
// @return: TRUE (1) for successful completion or FALSE (0).
//-
Function GH_makeGraphNice([gName, fromZero, font, fsize, msize])
	String gName, font
	Variable fromZero, fsize, msize
	
	if (ParamIsDefault(gName) || strlen(gName) == 0)
		gName = StringFromList(0, WinList("*", ";", "WIN:1"))
	endif
	if (strlen(gName) == 0)
		// no graphs available, nothing to do
		return 0
	endif
	if (ParamIsDefault(font) || strlen(font) == 0)
		font = ks_defFont
	endif
	
	fromZero = ParamIsDefault(fromZero) || fromZero > 0 ? 1 : 0
	fsize = ParamIsDefault(fsize) || fsize <= 0 ? 16 : ceil(fsize)
	msize = ParamIsDefault(msize) || msize <=0 ? 4 : ceil(msize)

	ModifyGraph /W=$gName standoff=0, gfont=font, gfsize=fsize, gmsize=msize
	rescaleFromOne(graphname = gName)
	
	String axisPattern = "SetAxis/W=%s/A/N=1/E=%d %s;"
	String cmd = "", sCmd = ""
	String al = AxisList(gName)
	Variable index = 0, numAxis = ItemsInList(al)	
	for (index = 0; index < numAxis; index += 1)
		sprintf sCmd, axisPattern, gName, fromZero, StringFromList(index, al)
		cmd += sCmd
	endfor
	Execute cmd
	
	return 1
End


// ********************************************
// *********               GUI
// ********************************************

//! Return the name of the top window when called from a subwindow, i.e. when
// the controls are in a panel inside a graph window.
// @param theName: the name of the current subwindow
// @return: the name of the parent window
//-
Function /S getTopWindowName(theName)
	String theName
	
	Variable pos = strsearch(theName, "#", 0)
	if (pos == -1)
		return theName
	else
		return theName[0, pos - 1]
	endif 
End

//! Get the name of the sub panel.
// @param theName: the name of the current panel.
// @return: the name of the sub panel.
// -
Function /S getSubPanelName(theName)
	String theName
	
	Variable pos = strsearch(theName, "#", Inf, 1)
	if (pos == -1)
		return theName
	else
		return theName[pos + 1, strlen(theName) - 1]
	endif
End

//! Creates a title from a graph name by replacing all underscores with spaces. If no 
// underscores are in the graph name, the title becomes the graph name.
// @param theName: the name of the graph
// @return: the new graph title
//-
Function /S getGraphTitleFromName(theName)
	String theName
	
	String theTitle = theName
	
	Variable underScore = strsearch(theName, "_", 0) >= 0
	if (underScore)
		theTitle = ReplaceString("_", theName, " ")
	elseif (strsearch(theName, " ", 0) >= 0)
		theTitle = theName
	else
		Variable index
		for (index = 0; index < ItemsInList(ksAlphabet); index += 1)
			if (strsearch(theTitle, StringFromList(index, ksAlphabet), 0) >= 0)
				theTitle = theTitle[0] + ReplaceString(StringFromList(index, ksAlphabet), theTitle[1, Inf], " " + StringFromList(index, ksAlphabet))
			endif
		endfor
	endif
	return theTitle
End

Proc SLDR_addControlPanelToGraph(theGraph)
	String theGraph
	Prompt theGraph, "Please choose the graph", popup, WinList("*", ";", "WIN:1")
	
	DoWindow /F $theGraph
	SLDR_makeCntrlPanel(theGraph)
End

//! Plots all data waves of a graph against an entered text wave to convert the graph into a
// bar graph.
// @param gName: the name of the graph to be converted (default: top graph)
// @param catWName: the name of the category wave (text wave with appropiate length) (default: CategoryLabels_1)
// @param newGName: name for the new graph
// @param overwrite: a flag (default 0: false) to overwrite existing graphs or create new name to avoid collision
// @param verbose: a flag (default 1: true) to provide user feedback for the function
//-
Function /S GH_convert2BarGraph([gName, catWName, newGName, overwrite, verbose])
	String gName, catWName, newGName
	Variable overwrite, verbose
	
	String result = ""
	String defCatName = "CategoryLabels_1"
	Wave /T catWave
	
	overwrite = ParamIsDefault(overwrite) || overwrite < 1 ? 0 : 1
	verbose = ParamIsDefault(verbose) || verbose > 0 ? 1 : 0
	
	if (ParamIsDefault(gName) || strlen(gName) == 0)
		gName = StringFromList(0, WinList("*", ";", "WIN:1"))
		if (strlen(gName) == 0)
			// no graphs are open
			return result
		endif
	else
		if (WinType(gName) != 1)
			// not a graph, so stop here
			return result
		endif
	endif
	
	String tList = TraceNameList(gName, ";", 1)
	Variable numTraces = ItemsInList(tList)
	Variable index = 0
	if (numTraces == 0)
		return result
	endif
	Variable maxCats = 0
	for (index = 0; index < numTraces; index += 1)
		Wave data = TraceNameToWaveRef(gName, StringFromList(index, tList))
		maxCats = max(maxCats, numpnts(data))
	endfor

	if (ParamIsDefault(newGName) || strlen(newGName) == 0)
		newGName = "BarGraph"
		Prompt newGName, "new graph name"
	endif
	
	if ((ParamIsDefault(catWName) || strlen(catWName) == 0))
		if (verbose)
			String options = ""
			String optPattern = "TEXT:1,MAXROWS:%g,MINROWS:%g"
			sprintf options, optPattern, maxCats, maxCats
			Prompt catWName, "Choose category wave", popup, WaveList("*", ";", options)
		else
			catWName = defCatName
		endif
	endif
	
	if (verbose)
		DoPrompt "Bar Graph Data", catWName, newGName
	endif 
	
	Wave /T catWave = $catWName
	if (!WaveExists(catWave))
		Make /N=(maxCats) /T $defCatName /WAVE=catWave
		catWave = num2str(p + 1)
	else
		if (numpnts(catWave) != maxCats)
			if (cmpstr(catWName[0, 13], defCatName[0,13]) == 0)
				String newDefCat = ""
				String wPattern = defCatName[0, 13] + "_%g"
				index = 1
				do
					sprintf newDefCat, wPattern, index
					Wave /T catWave = $newDefCat
					index += 1
				while(WaveExists(catWave) && numpnts(catWave) != maxCats)
			else
				// not same number of categories, so stop here back
				if (verbose)
					DoAlert /T="Category Mismatch" 0, "Category wave has not have the required number of categories."
				endif
				return result
			endif
		endif
	endif	
	
	Display /N=$newGName
	newGName = S_name
	result = newGName
	
	for (index = 0; index < numTraces; index += 1)
		String tName = StringFromList(index, tList)
		Wave data = TraceNameToWaveRef(gName, tName)
		
		AppendToGraph /W=$newGName data vs catWave
		
		String info = TraceInfo(gName, tName, 0)
		String ebars = StringByKey("ERRORBARS", info)
		Execute /Q ebars
		String mInfo = RemoveEnding(ListMatch(info, "rgb(x)*"))
		mInfo = ReplaceString("(x)", mInfo, "(" + tName + ")")
		Execute /Q "ModifyGraph " + mInfo
	
	endfor
	
	ModifyGraph catGap(bottom)=0.2,barGap(bottom)=0.05
	ModifyGraph standoff=0
	SetAxis /A/N=1/E=1 left
		
	return result
End
	
Function /S GH_transposeBarGraphData([gName, stackGName, add, verbose])
	String gName, stackGName
	Variable verbose, add
	
	String result = "", catName = ""
	String defCatPattern = "%s_catLbl"
	String defMatrixPattern = "%s_dm"
	String defErrMxPattern = "%s_errMx"
	String defColPattern = "%s_colIdx"
	String dataMatrixName = "", errMxName = "", traceName = "", colName = ""
	String info = ""
	Wave /T catWave
	Wave dataMatrix, errorMatrix
	
	Variable useError = 0
	Variable wStart = 0
	
	add = ParamIsDefault(add) || add < 1 ? 0 : 1
	verbose = ParamIsDefault(verbose) || verbose > 0 ? 1 : 0
	
	if (ParamIsDefault(gName) || strlen(gName) == 0)
		gName = StringFromList(0, WinList("*", ";", "WIN:1"))
		if (strlen(gName) == 0)
			// no graphs are open
			return result
		endif
	else
		if (WinType(gName) != 1)
			// not a graph, so stop here
			return result
		endif
	endif
	
	if (ParamIsDefault(stackGName) || strlen(stackGName) == 0)
		stackGName = "StackedBar"
	endif

	String tList = TraceNameList(gName, ";", 1)
	Variable numTraces = ItemsInList(tList)
	STRUCT Rect wS
	GetWindow $gName gsize
	wS.left = V_left
	wS.right = V_right
	wS.top = V_top
	wS.bottom = V_bottom
	
	// assuming category axis is bottom axis
	String ayInfo = AxisInfo(gName, "left")
	String abInfo = AxisInfo(gName, "bottom")

	Wave /T catWave = $(StringByKey("CATWAVEDF", abInfo) + StringByKey("CATWAVE", abInfo))
	if (!WaveExists(catWave))
		Wave /T catWave = $(StringByKey("CATWAVEDF", ayInfo) + StringByKey("CATWAVE", ayInfo))
		if (!WaveExists(catWave))
			// no category wave on left or bottom axis - return for now
			return result
		endif
	endif

	sprintf dataMatrixName, defMatrixPattern, gName
	sprintf errMxName, defErrMxPattern, gName

	Make /N=(numpnts(catWave), numTraces) /O $(dataMatrixName) /WAVE=dataMatrix, $(errMxName) /WAVE=errorMatrix
	if (add)
		DoWindow /F $stackGName
		if (V_flag == 0)
			// no window to add to, so stop here
			return result
		endif
		result = stackGName

		info = AxisInfo(stackGName, "bottom")
		Wave /T catWave = $(StringByKey("CATWAVEDF", info) + StringByKey("CATWAVE", info))
		info = TraceInfo(stackGName, StringFromList(0, TraceNameList(stackGName, ";", 1)), 0)
		String colCmd = ReplaceString(",", StringByKey("RECREATION", info), " , ")
		wStart = strsearch(colCmd, "{", 0)
		sscanf colCmd[wStart, Inf], "{%s", colName
		
	else
		sprintf catName, defCatPattern, gName
		sprintf colName, defColPattern, gName
	
		Make /N=(numTraces) /T/O $(catName) /WAVE=catWave
		catWave = num2str(p + 1)	
		Make /N=(numTraces) /O $(colName) /WAVE=colWave
		colWave = p
		
		Display /N=$stackGName
		stackGName = S_name
		result = stackGName
		MoveWindow /W=$stackGName wS.left, wS.top, wS.right, wS.bottom
	endif
	
	Variable index
	for (index = 0; index < numTraces; index += 1)
		String dataName = StringFromList(index, tList)
		Wave data = TraceNameToWaveRef(gName, dataName)
		dataMatrix[][index] = data[p]

		Wave errors
		info = TraceInfo(gName, dataName, 0)
		String errCMD = ReplaceString(",", StringByKey("ERRORBARS", info), " , ")
		wStart = strsearch(errCMD, "wave=(", 0)
		String errorNameP = "", errorNameN = ""
		sscanf errCMD[wStart, Inf], "wave=(%s , %s)", errorNameP, errorNameN
		if (strlen(errorNameP) > 0)
			Wave errors = $errorNameP
		elseif (strlen(errorNameN) > 0)
			Wave errors = $errorNameN
		endif
		
		if (WaveExists(errors))
			useError = 1
			errorMatrix[][index] = errors[p]
		endif
	endfor

	Variable instCount	
	traceName = NameOfWave(dataMatrix)
	for (index = DimSize(dataMatrix, 0) - 1; index >= 0; index -= 1)
		AppendToGraph /W=$stackGName dataMatrix[index][] vs catWave

		if (instCount > 0)
			sprintf traceName, "%s#%g", NameOfWave(dataMatrix), instCount
		endif
		instCount += 1
	
		if (index == 0)
			ModifyGraph toMode($traceName)=0
		else
			ModifyGraph toMode($traceName)=3
		endif
		ModifyGraph zColor($traceName)={$colName,*,*,cindexRGB,0,$(getColorValueWave())}
	
		if (useError)
			String errWPat = "%s[%g][*]"
			String errWStr = ""
			sprintf errWStr, errWPat, NameOfWave(errorMatrix), index
			String cmdPat = "ErrorBars %s Y, wave=(%s, %s)"
			String cmd = ""
			if (index == DimSize(dataMatrix, 0) - 1)
				sprintf cmd, cmdPat, traceName, errWStr, ""
			else
				sprintf cmd, cmdPat, traceName, "", errWStr
			endif
			Execute /Z cmd
		endif
	endfor

	if (!add)
		SetAxis/A/N=1/E=1 bottom
		SetAxis/A/N=1/E=1 left
	endif	
	ModifyGraph catGap(bottom)=0.2,barGap(bottom)=0.05,standoff=0
	
	return result
End


// **** ADD SLIDER FOR SCROLLING THROUGH GRAPH X *****
Function /S SLDR_makeCntrlPanel(theWindowName, [theCtrlPlName])
	String theWindowName, theCtrlPlName
	
	if (strlen(theWindowName) == 0 || (WinType(theWindowName) != 1))
		print "** ERROR: no graph with the name >" + theWindowName + "<"
		print "**        can not attach the panel. ABORTED!"
		return ""
	endif
	
	if (ParamIsDefault(theCtrlPlName))
		theCtrlPlName = "SLDR_CntrlPanel"
	endif

	WAVE /D SLDR_RANGECNTRL
	if (!WaveExists(SLDR_RANGECNTRL) || numpnts(SLDR_RANGECNTRL) == 0)
		Make /N=(0,5) /D/O SLDR_RANGECNTRL
		SetDimLabel 1, 0, range, SLDR_RANGECNTRL
		SetDimLabel 1, 1, minimum, SLDR_RANGECNTRL
		SetDimLabel 1, 2, maximum, SLDR_RANGECNTRL
		SetDimLabel 1, 3, traces, SLDR_RANGECNTRL
		SetDimLabel 1, 4, dateRange, SLDR_RANGECNTRL
	endif
	
	String varLabel = theWindowName + "#" + theCtrlPlName
	Variable doesExist = FindDimLabel(SLDR_RANGECNTRL, 0, varLabel)
	if (doesExist == -2)
		//-> the label does not exist
		InsertPoints numpnts(SLDR_RANGECNTRL), 1, SLDR_RANGECNTRL
		SetDimLabel 0, DimSize(SLDR_RANGECNTRL, 0) - 1, $varLabel, SLDR_RANGECNTRL
	endif
	
	Variable absMin = 0, absMax = 0, index = 0, xwaves = 0
	Variable changeVarSteps = 1, dateFlag = 0
	String guiData = ""
	String theTraces = TraceNameList(theWindowName, ";", 1)
	for (index = 0; index < ItemsInList(theTraces); index += 1)
		// get the smallest and largest x number to determine the absolute range
		Wave xTrace = XWaveRefFromTrace(theWindowName, StringFromList(index, theTraces))
		if (WaveExists(xTrace))
			xwaves = 1
			if (numpnts(xTrace) == 0)
				continue
			endif
			
			WaveStats /C=1/W/Q xTrace
			Wave M_WaveStats
			if (M_WaveStats[10] < absMin || index == 0)
				absMin = M_WaveStats[10]
			endif
			if (M_WaveStats[12] > absMax)
				absMax = M_WaveStats[12]
			endif
			if (cmpstr(WaveUnits(xTrace, -1), "dat") == 0)
				dateFlag = 1
				guiData = ReplaceStringByKey("dateFlag", guiData, num2str(dateFlag))
			endif
		else
			Wave theTrace = TraceNameToWaveRef(theWindowName, StringFromList(index, theTraces))
			if (pnt2x(theTrace, 0) < absMin)
				absMin = pnt2x(theTrace, 0)
			endif
			if (pnt2x(theTrace, numpnts(theTrace) - 1) > absMax)
				absMax = pnt2x(theTrace, numpnts(theTrace) - 1)
			endif
			if (cmpstr(WaveUnits(theTrace, 0), "dat") == 0)
				dateFlag = 1
				guiData = ReplaceStringByKey("dateFlag", guiData, num2str(dateFlag))
			endif
		endif
	endfor
	if (xwaves)
		changeVarSteps = (absMax - absMin) / 200
	else
		changeVarSteps = 1
	endif
	KillWaves /Z M_WaveStats
	
	SLDR_RANGECNTRL[%$varLabel][%traces] = ItemsInList(theTraces)
	SLDR_RANGECNTRL[%$varLabel][%minimum] = absMin
	SLDR_RANGECNTRL[%$varLabel][%maximum] = absMax
	SLDR_RANGECNTRL[%$varLabel][%range] = floor((absMax-absMin) / 10)
	
	String rangeUnit = calculateUnitFactor(SLDR_RANGECNTRL[%$varLabel][%range])
	Variable range = NumberByKey("range", rangeUnit)
	String unit = StringByKey("unit", rangeUnit)

	SLDR_RANGECNTRL[%$varLabel][%dateRange] = range
	SLDR_RANGECNTRL[%$varLabel][%range] = range * determineScaleFactor(unit)
	Variable lowerLimit = absMin + (SLDR_RANGECNTRL[%$varLabel][%range] / 2)
	Variable upperLimit = absMax - (SLDR_RANGECNTRL[%$varLabel][%range] / 2)

	SetWindow $theWindowName hook(ScrollSliderControl)=ChangeSliderLimits
	
	String theUnitList = "years;months;days;hours;minutes;seconds;"
	GetWindow $theWindowName, wsize
	Variable slExtent = floor((V_right - V_left) * 0.95) 
	
	ControlBar /W=$theWindowName /B 50
	NewPanel /FG=(GL,GB, FR, FB) /HOST=$theWindowName
	RenameWindow #, $theCtrlPlName
	ModifyPanel frameStyle=0
		
	Slider showVis, appearance={native}, pos={10, 10}, size={slExtent, 20}
	Slider showVis, vert=0, side=0, limits={absMin, absMax, 0}
	Slider showVis, value=(SLDR_RANGECNTRL[%$varLabel][%minimum] + floor(SLDR_RANGECNTRL[%$varLabel][%range] / 2))
	Slider showVis, proc=sl_moveRange
	Slider showVis, userdata = guiData
		
	SetVariable visibleRange, appearance={native}, title="range"
	SetVariable visibleRange, pos={10, 30}, size={90, 20}
	SetVariable visibleRange, limits={lowerLimit, upperLimit, changeVarSteps}, value=SLDR_RANGECNTRL[%$varLabel][%range]
	SetVariable visibleRange, proc=sv_changeRange
	SetVariable visibleRange, userdata = guiData
	
	CheckBox plotBase_normal, appearance={native}, title="normal"
	CheckBox plotBase_normal, pos={110, 30}, size={50,20}
	CheckBox plotBase_normal, mode=1, value=0
	CheckBox plotBase_normal, proc=ckbx_plotBase
	
	CheckBox plotBase_date, appearance={native}, title="date"
	CheckBox plotBase_date, pos={165, 30}, size={50, 20}
	CheckBox plotBase_date, mode=1, value=0
	CheckBox plotBase_date, proc=ckbx_plotBase
	
	SetVariable dateRange, appearance={native}, title="date range"
	SetVariable dateRange, pos={220, 30}, size={120, 20}
	SetVariable dateRange, value=SLDR_RANGECNTRL[%$varLabel][%dateRange]
	SetVariable dateRange, proc=sv_changeRange
	SetVariable dateRange, disable=1
	
	PopupMenu dateUnits, appearance={native}, title="date units"
	PopupMenu dateUnits, pos={350, 28}, size={120, 20}
	PopupMenu dateUnits, value="years;months;days;hours;minutes;seconds;", mode=(WhichListItem(unit, theUnitList) + 1)
	PopupMenu dateUnits, disable=1, proc=pop_dateUnitsChange
	
	Button removeBar, appearance={native}, title="remove slider"
	Button removeBar, pos={480,28}, size={110, 20}
	Button removeBar, proc=btn_removeSliderBar
	
	if (dateFlag)
		CheckBox plotBase_date, value=1
		SetVariable visibleRange, disable=2
		PopupMenu dateUnits, disable=0
		SetVariable dateRange, disable=0
	else
		CheckBox plotBase_normal, value =1
	endif
		
	SetActiveSubwindow ##
	SetAxis /W=$theWindowName bottom, SLDR_RANGECNTRL[%$varLabel][%minimum], SLDR_RANGECNTRL[%$varLabel][%maximum]
	return theWindowName + "#" + theCtrlPlName
End

// Slider function to change the displayed section of the graph while changing the slider value
Function sl_moveRange(SL_Struct)
	STRUCT WMSliderAction &SL_Struct
		
	if (SL_Struct.eventCode & 2^0)
		
		String theName = getTopWindowName(SL_Struct.win)
		WAVE SLDR_RANGECNTRL
		
		Variable low = SL_Struct.curval - (SLDR_RANGECNTRL[%$SL_Struct.win][%range] / 2)
		if (low < SLDR_RANGECNTRL[%$SL_Struct.win][%minimum])
			low = SLDR_RANGECNTRL[%$SL_Struct.win][%minimum]
		endif
		Variable high = SL_Struct.curval + (SLDR_RANGECNTRL[%$SL_Struct.win][%range] / 2)
		if (high > SLDR_RANGECNTRL[%$SL_Struct.win][%maximum])
			high = SLDR_RANGECNTRL[%$SL_Struct.win][%maximum]
		endif
		
		SetAxis /W=$theName bottom, low, high
	endif
End

// Set variable function to use determine the range of the displayed graph
Function sv_changeRange(SV_Struct) : SetVariableControl
	STRUCT WMSetVariableAction &SV_Struct
	
	String theName
	WAVE SLDR_RANGECNTRL
	if (SV_Struct.eventCode == 1 || SV_Struct.eventCode == 2)
		if (cmpstr(SV_Struct.ctrlName, "dateRange") == 0)
			ControlInfo /W=$SV_Struct.win dateUnits
			String theUnit = S_Value
			Variable theFactor = determineScaleFactor(theUnit)
			SLDR_RANGECNTRL[%$SV_Struct.win][%range] = SLDR_RANGECNTRL[%$SV_Struct.win][%dateRange] * theFactor
		endif
		theName = getTopWindowName(SV_Struct.win)
		String theTraces = TraceNameList(theName, ";", 1)
		Variable upperLimit
		Wave xTrace = XWaveRefFromTrace(theName, StringFromList(0, theTraces))
		if (WaveExists(xTrace))
			upperLimit = xTrace[numpnts(xTrace)]
		else
			Wave theTrace = TraceNameToWaveRef(theName, StringFromList(0, theTraces))
			 upperLimit = pnt2x(theTrace, numpnts(theTrace) - 1)
		endif
		
		ControlInfo /W=$SV_Struct.win showVis
		Variable theLocus = V_value
		Variable curVal = SV_Struct.dval / 2
		
		if (cmpstr(Sv_Struct.ctrlName, "dateRange") == 0)
			curVal *= theFactor
		endif
		Variable low = theLocus - curVal
		Variable up = theLocus + curVal
		if (low < 0)
			low = 0
		endif
		if (up > upperLimit)
			up = upperLimit
		endif
		
		Variable highLim = upperLimit - curVal
		if (highLim < 0)
			highLim = upperLimit
		endif
		SetAxis /W=$theName bottom, low, up
		Slider showVis, win=$SV_Struct.win, limits={(SV_Struct.dval / 2), highLim, 0}
	endif
	
	if (SV_Struct.eventCode == -1 && cmpstr(SV_Struct.ctrlName, "visibleRange") == 0)
		// the SetVariable gets killed, i. e. the panel removed from the graph, so 
		// clean up the variable from the wave
		WAVE SLDR_RANGECNTRL
		Variable labelPos = FindDimLabel(SLDR_RANGECNTRL, 0, SV_Struct.win)
		if (labelPos >= 0)
			DeletePoints labelPos, 1, SLDR_RANGECNTRL
		endif
		theName = getTopWindowName(SV_Struct.win)
		SetWindow $theName hook(ScrollSliderSize)=$""
	endif
	
	return 0
End

// Radio button control function to make sure that only one button is selected at a time
Function ckbx_plotBase(CB_Struct) : CheckBoxControl
	STRUCT WMCheckboxAction &CB_Struct
	
	strswitch(CB_Struct.ctrlName)
		case "plotBase_normal":
			CheckBox plotBase_date, win=$CB_Struct.win, value=0
			SetVariable visibleRange, disable = 0
			SetVariable dateRange, disable = 1
			PopupMenu dateUnits, disable = 1
			break
		case "plotBase_date":
			CheckBox plotBase_normal, win=$CB_Struct.win, value=0
			SetVariable visibleRange, disable = 2
			SetVariable dateRange, disable = 0
			PopupMenu dateUnits, disable = 0
			break
	endswitch
	
	return 0
End

// Function to change the date unit popup menu
Function pop_dateUnitsChange(PU_Struct) : PopupMenuControl
	STRUCT WMPopupAction &PU_Struct
	
	if (PU_Struct.eventCode == 2)
		Wave SLDR_RANGECNTRL
		Variable theFactor = determineScaleFactor(PU_Struct.popStr)
		SLDR_RANGECNTRL[%$PU_Struct.win][%range] = SLDR_RANGECNTRL[%$PU_Struct.win][%dateRange] * theFactor

		String theName = getTopWindowName(PU_Struct.win)
		String theTraces = TraceNameList(theName, ";", 1)
		Variable upperLimit, lowerLimit
		Wave xTrace = XWaveRefFromTrace(theName, StringFromList(0, theTraces))
		if (WaveExists(xTrace))
			lowerLimit = xTrace[0]
			upperLimit = xTrace[numpnts(xTrace)]
		else
			Wave theTrace = TraceNameToWaveRef(theName, StringFromList(0, theTraces))
			 lowerLimit = pnt2x(theTrace, 0)
			 upperLimit = pnt2x(theTrace, numpnts(theTrace) - 1)
		endif
		
		ControlInfo /W=$PU_Struct.win showVis
		Variable theLocus = V_value
		
		Variable low = theLocus - (SLDR_RANGECNTRL[%$PU_Struct.win][%dateRange] * theFactor / 2)
		Variable up = theLocus + (SLDR_RANGECNTRL[%$PU_Struct.win][%dateRange] * theFactor / 2)
		if (low < lowerLimit)
			low = lowerLimit
		endif
		if (up > upperLimit)
			up = upperLimit
		endif
		
		SetAxis /W=$theName bottom, low, up
		
	endif
End

Function btn_removeSliderBar(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch (bn.eventCode)
		case 2:
			String parent = getHostWinName(bn.win)
			SetWindow $parent hook(ScrollSliderControl)=$""
//			DoWindow /K $(bn.win)
			KillControl /W=$bn.win showVis
			KillControl /W=$bn.win visibleRange
			KillControl /W=$bn.win plotBase_normal
			KillControl /W=$bn.win plotBase_date
			KillControl /W=$bn.win dateRange
			KillControl /W=$bn.win dateUnits
			KillControl /W=$bn.win removeBar
			ControlBar /W=$parent /B 0
			SetAxis /A/N=1 bottom
			break
	endswitch
End

Function /S GH_simpleGraphChooser()

	String windows = WinList("*", ";", "WIN:1")
	String graphName = ""
	if (ItemsInList(windows) == 0)
		return graphName
	endif
	graphName = StringFromList(0, windows)
	Prompt graphName, "choose a graph", popup, windows
	DoPrompt "Graph Chooser", graphName
	if (V_flag)
		// user clicked on cancel
		return ""
	endif
	return graphName
End

// ************    Scaling Helper Functions ***********


//! Determine the scaling factor from a time unit to convert to seconds.
// @param theUnit: the unit ('years', 'months', 'days', 'hours' or 'minutes')
// @returns: a scaling factor to convert a variable into seconds
//-
Function determineScaleFactor(theUnit)
	String theUnit
	
	Variable theFactor = 1
	strswitch(theUnit)
		case "years":
			theFactor *= 12
		case "months":
			theFactor *= 30.5
		case "days":
			theFactor *= 24
		case "hours":
			theFactor *= 60
		case "minutes":
			theFactor *= 60
	endswitch
	return theFactor
End

Function /S calculateUnitFactor(theRange)
	Variable theRange
	
	Make /N=5 /O /FREE theFactors = {60, 60, 24, 30.5, 12}
	String unitList = "minutes;hours;days;months;years;"
	Variable index = 0
	String unit = "seconds"
	String returnKey = ""
	
	if (theRange < 10)
		returnKey = ReplaceNumberByKey("range", returnKey, theRange)
		returnKey = ReplaceStringByKey("unit", returnKey, unit)
		return returnKey
	else
		do
			theRange /= theFactors[index]
			unit = StringFromList(index, unitList)
			index += 1
		while(theRange > 10 && index < 5) 
		returnKey = ReplaceNumberByKey("range", returnKey, floor(theRange))
		returnKey = ReplaceStringByKey("unit", returnKey, unit)
	endif
	
	return returnKey
End
	

// A hook function that reacts on the modification and resizing of the graph of the attached graph
// window to modifiy the slider size and or its limits
Function ChangeSliderLimits(H_Struct)
	STRUCT WMWinHookStruct &H_Struct
				
	String ctrlPanels = ChildWindowList(H_Struct.winName)
	String ctrlPanel
	if (ItemsInList(ctrlPanels) > 1)
		ctrlPanel = "SLDR_CntrlPanel"
	else
		ctrlPanel =  StringFromList(0, ctrlPanels)
	endif
	String varLabel = H_Struct.winName + "#" + ctrlPanel
	
	strswitch (H_Struct.eventName)
		case "modified":
			Wave SLDR_RANGECNTRL
			if ((!WaveExists(SLDR_RANGECNTRL) || DimSize(SLDR_RANGECNTRL, 1) == 0) && H_Struct.eventCode != 2)
				print "** ERROR: can't find the wave SLDR_RANGECNTRL or it has the wrong size."
				return 0
			endif

			Variable absMin = 0, absMax = 0, index = 0, xwaves = 0
			Variable changeVarSteps = 1, dateFlag = 0
			String guiData = ""
			String theTraces = TraceNameList(H_Struct.winName, ";", 1)
			for (index = 0; index < ItemsInList(theTraces); index += 1)
				// get the smallest and largest x number to determine the absolute range
				Wave xTrace = XWaveRefFromTrace(H_Struct.winName, StringFromList(index, theTraces))
				if (WaveExists(xTrace))
					xwaves = 1
					if (numpnts(xTrace) == 0)
						continue
					endif
					
					WaveStats /C=1/W/Q xTrace
					Wave M_WaveStats
					if (M_WaveStats[10] < absMin || index == 0)
						absMin = M_WaveStats[10]
					endif
					if (M_WaveStats[12] > absMax)
						absMax = M_WaveStats[12]
					endif
					if (cmpstr(WaveUnits(xTrace, -1), "dat") == 0)
						dateFlag = 1
						guiData = ReplaceStringByKey("dateFlag", guiData, num2str(dateFlag))
					endif
				else
					Wave theTrace = TraceNameToWaveRef(H_Struct.winName, StringFromList(index, theTraces))
					if (pnt2x(theTrace, 0) < absMin)
						absMin = pnt2x(theTrace, 0)
					endif
					if (pnt2x(theTrace, numpnts(theTrace) - 1) > absMax)
						absMax = pnt2x(theTrace, numpnts(theTrace) - 1)
					endif
					if (cmpstr(WaveUnits(theTrace, 0), "dat") == 0)
						dateFlag = 1
						guiData = ReplaceStringByKey("dateFlag", guiData, num2str(dateFlag))
					endif
				endif
			endfor
			if (xwaves)
				changeVarSteps = (absMax - absMin) / 200
			else
				changeVarSteps = 1
			endif
			KillWaves /Z M_WaveStats
	
			SLDR_RANGECNTRL[%$varLabel][%traces] = ItemsInList(theTraces)
			SLDR_RANGECNTRL[%$varLabel][%minimum] = absMin
			SLDR_RANGECNTRL[%$varLabel][%maximum] = absMax
			Variable lowerLimit = absMin + (SLDR_RANGECNTRL[%$varLabel][%range] / 2)
			Variable upperLimit = absMax - (SLDR_RANGECNTRL[%$varLabel][%range] / 2)
				
			Slider showVis, win=$varLabel, limits={lowerLimit, upperLimit, 1}
			break
		case "resize":
			GetWindow $H_Struct.winName, wsize
			Variable slExtent = floor((V_right - V_left)  * 0.95)	
			Slider showVis, win=$varLabel, size={slExtent, 20}
			break
	endswitch
End

//! This function that adds the row dimension labels to each point of each trace of the 
// given graph.
// @param theGraphName: the name of the graph with the traces
// @param theTrace: the trace name for the dimension label
// @param prefix: prefix for the dimension label
// @param theLabel: a label
// @return: nothing- the labels are added to the graph
//-
Function GH_addDimLabelAnnotation(theGraphName, [theTrace, prefix, theLabel])
	String theGraphName, theTrace, prefix, theLabel
	
	String theTraces = ""
	Variable choose = 0
	
	if (strlen(theGraphName) == 0)
		theGraphName = GH_simpleGraphChooser()
		if (strlen(theGraphName) == 0)
			return 0
		endif
		choose = 1
	endif

	if (ParamIsDefault(theTrace))
		theTrace = ""
	endif
	if (ParamIsDefault(prefix))
		prefix = "dimLbl"
	endif
	if (ParamIsDefault(theLabel) || strlen(theLabel) == 0)
		theLabel = "all"
	endif
	
	if (WinType(theGraphName) != 1)
		// window is not a graph
		print "** ERROR: the graph " + theGraphName + " does not exist."
		return 0
	endif
	DoWindow /F $theGraphName
	
	theTraces = TraceNameList(theGraphName, ";", 1)
	
	if (strlen(theTraces) == 0 || ItemsInList(theTraces) == 0)
		print "** ERROR: can't find any traces on the graph " + theGraphName
		return 0
	endif

	if (choose)
		Prompt theTrace, "choose a trace:", popup, "all;" + theTraces
		DoPrompt "Trace Chooser", theTrace	
		if (V_flag)
			return 0
		endif
		String theLabels = makeLblListFromWave(TraceNameToWaveRef(theGraphName, theTrace))
		Prompt theLabel, "choose a label:", popup, "all;" + theLabels
		DoPrompt "Label Chooser", theLabel
		if (V_flag)
			return 0
		endif
	endif
		
	if (strlen(theTrace) > 0 && cmpstr(theTrace, "all") != 0)
		if (WhichListItem(theTrace, theTraces) == -1)
			print "** ERROR: trace >" + theTrace + "< is not on the graph " + theGraphName
			return 0
		endif
		theTraces = theTrace
	endif
	
	// remove labels first
	GH_removeLabelsFromGraph(theGraphName, theTrace=theTrace)
	String tagList = AnnotationList(theGraphName)
	
	Variable index = 0, points = 0
	String theTag
	for (index = 0; index < ItemsInList(theTraces); index += 1)
		theTrace = StringFromList(index, theTraces)
		for (points = 0; points < numpnts($theTrace); points += 1)
			theTag = GetDimLabel($theTrace, 0, points)
			if (strlen(theTag) == 0)
				continue
			endif
			if (cmpstr(theTag, theLabel) != 0 && cmpstr(theLabel, "all") != 0)
				continue
			endif
			Variable counter = 0
			String newName = ""
			do
				newName = prefix + num2str(index) + num2str(points) + num2str(counter)
				counter += 1
			while (WhichListItem(newName, tagList) > -1)
			Tag /C/N=$(newName)/F=0/L=1/TL=0 $theTrace, points, "\\Z09" + theTag
		endfor
	endfor
End

//! Removes the dimension label tags from a wave on a graph. If the trace is not given,
// a dialog will prompt for possible selections.
// @param theGraphName: the name of the graph
// @param theTrace: the name of the trace 
// @param prefix: the prefix for the tag labels
// @return: nothing- removes the tags from the graph
Function GH_removeLabelsFromGraph(theGraphName, [theTrace, prefix])
	String theGraphName, theTrace, prefix
	
	Variable select = 0, choose = 0
	
	if (strlen(theGraphName) == 0)
		theGraphName = GH_simpleGraphChooser()
		if (strlen(theGraphName) == 0)
			return 0
		endif
		choose = 1
	endif
	if (ParamIsDefault(theTrace))
		theTrace = ""
	endif
	if (ParamIsDefault(prefix))
		prefix = "dimLbl"
	endif
	if (choose)
		Prompt theTrace, "choose a trace:", popup, "all;" + TraceNameList(theGraphName, ";", 1)
		DoPrompt "Trace Chooser", theTrace
		if (V_flag)
			return 0
		endif
	endif
	
	if (strlen(theTrace) > 0 && cmpstr(theTrace, "all") != 0)
		if (WhichListItem(theTrace, TraceNameList(theGraphName, ";", 1)) == -1)
			print "** ERROR: the trace >" + theTrace + "< is not on the graph " + theGraphName
			return 0
		endif
		select = 1
	endif
	
	String AnnList = AnnotationList(theGraphName)
	Variable tags
	for (tags = 0; tags < ItemsInList(AnnList); tags += 1)
		String theTag = StringFromList(tags, AnnList)
		if (strsearch(theTag, prefix, 0) != 0)
			continue
		endif
		String theInfo = AnnotationInfo(theGraphName, theTag)
		if (cmpstr(StringByKey("TYPE", theInfo), "Tag") !=0)
			continue
		endif
		if (select && cmpstr(StringByKey("YWAVE", theInfo), theTrace) != 0)
			continue
		endif
		Tag /K/N=$theTag
	endfor
End

//! Toggle the trace display on the graph.
// @param theGraph: graph name (default: top graph)
// @returns: the graph name
//-
Function /S GH_toggleTraceDisplay([theGraph])
	String theGraph
	
	if (ParamIsDefault(theGraph) || strlen(theGraph) == 0)
		theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
		if (strlen(theGraph) == 0)
			// not graphs open 
			return ""
		endif
	endif
	
	DoWindow /F $theGraph
	if (V_flag == 0)
		return ""
	endif
	
	Variable on
	String uData = GetUserdata(theGraph, "", "")
	String status = StringByKey("TRACESTAT", uData)
	if (strlen(status) == 0)
		on = 0
	endif
	if (cmpstr(status, "on") == 0)
		on = 1
	endif
	if (cmpstr(status, "off") == 0)
		on = 0
	endif
	
	if (on)
		ModifyGraph /W=$theGraph noLabel=0,axThick=1,standoff=0, margin=0
		uData = ReplaceStringByKey("TRACESTAT", uData, "off")
	else
		ModifyGraph /W=$theGraph noLabel=2,axThick=0,standoff=0, margin=15
		uData = ReplaceStringByKey("TRACESTAT", uData, "on")
	endif
	SetWindow $theGraph userdata = uData
	
	return theGraph
End



Function GH_addScaleBar2Graph(theName, barName, hStart, hDelta, vStart, vDelta, hAxis, vAxis, [lSize, fSize, hUnit, vUnit, hLabel, vLabel, labels])
	String theName, barName, vAxis, hAxis, vUnit, hUnit, hLabel, vLabel
	Variable vStart, vDelta, hStart, hDelta, labels, lSize, fSize
	
	DoWindow $theName
	if (V_flag != 1)
		return 0
	endif
	
	Variable index
	String axis = "", axisTypeList = "", groupName = ""

	if (strlen(barName) == 0)
		groupName = "scalebar_1"
	else
		groupName = barName
	endif
	String graphAxis = AxisList(theName)
	if (WhichListItem(vAxis, graphAxis) < 0 || WhichListItem(hAxis, graphAxis) < 0)
		return 0
	endif
	if (ParamIsDefault(hUnit) || strlen(hUnit) == 0)
		hUnit = ""
	else
		hUnit = " " + hUnit
	endif
	if (ParamIsDefault(vUnit) || strlen(vUnit) == 0)
		vUnit = ""
	else
		vUnit = " " + vUnit
	endif
	if (ParamIsDefault(lSize) || lSize < 0)
		lSize = 2
	endif
	if (ParamIsDefault(fSize) || fSize < 0)
		fSize = 12
	endif
	if (ParamIsDefault(labels) || labels > 1)
		labels = 1
	else
		labels = labels < 0 ? 0 : round(labels)
	endif
	
	if (labels)
		if (ParamIsDefault(hLabel) || strlen(hLabel) == 0)
			hLabel = num2str(hDelta) + hUnit
		endif
		if (ParamIsDefault(vLabel) || strlen(vLabel) == 0)
			vLabel = num2str(vDelta) + vUnit
		endif
	endif
	
	Variable hOffset = hDelta * 0.1
	
	DrawAction /W=$theName /L=UserFront getgroup=$groupName
	Variable groupExists = V_flag == 1
	
	if (groupExists)
		DrawAction /W=$theName /L=UserFront getgroup=$groupName, delete, begininsert
	endif
	
	SetDrawLayer /W=$theName UserFront
	SetDrawEnv /W=$theName gstart, gname=$groupName
	SetDrawEnv /W=$theName linefgc=(0,0,0), linethick=lSize, xcoord=$hAxis, ycoord=$vAxis, save
	DrawLine /W=$theName hStart, vStart, hStart + hDelta, vStart
	DrawLine /W=$theName hStart, vStart, hStart, vStart + vDelta
	if (labels)
		SetDrawEnv /W=$theName fname="Arial", fsize=fSize, save
		SetDrawEnv /W=$theName textxjust=1, textyjust=2
		DrawText /W=$theName (hStart + (hDelta / 2)), vStart, hLabel
		SetDrawEnv /W=$theName textxjust=2, textyjust=1
		DrawText /W=$theName hStart - hOffset, (vStart + (vDelta / 2)), vLabel
	endif
	SetDrawEnv /W=$theName gstop
		
	if (groupExists)	
		DrawAction /W=$theName /L=UserFront endinsert
	endif
	
	return 1
End

Function GH_addSBStruct2Graph(theGraph, scalebar)
	String theGraph
	STRUCT GH_ScaleBar &scalebar
	
	return GH_addScalebar2Graph(theGraph, scalebar.name, scalebar.hStart, scalebar.hDelta, scalebar.vStart, scalebar.vDelta, scalebar.hAxis, scalebar.vAxis, hUnit=scalebar.hUnit, vUnit=scalebar.vUnit, hLabel=scalebar.hLabel, vLabel=scalebar.vLabel, labels=scalebar.labels, lSize=scalebar.lineSize, fSize=scalebar.fontSize)
End

static Function GH_removeSBStructFGraph(theGraph, name)
	String theGraph, name
	
	removeScaleBar(theGraph, barName=name)
		
	DrawAction /W=$theGraph /L=UserFront getgroup=$name
	Variable groupExists = V_flag == 1
	
	if (groupExists)
		DrawAction /W=$theGraph /L=UserFront getgroup=$name, delete
	endif
	return 1
End


Function GH_putWavesOnGraph(theList, [name, folder, add, title])
	String theList, name, title
	DFREF folder
	Variable add
	
	Variable newGraph = 0, dfr = 0
	if (!ParamIsDefault(folder))
		dfr = 1
		if (DatafolderRefStatus(folder) == 0)
			folder = GetDatafolderDFR()
		endif
	endif
	if (ParamIsDefault(add) || add < 0)
		add = 0
	else
		add = add >= 1 ? 1 : round(add)
	endif
	if (ItemsInList(theList) == 0)
		print "** ERROR (GH_putWavesOnGraph): need a list of waves to operate on."
		return 0
	endif
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = ""
		newGraph = 1
	endif
	if (ParamIsDefault(title) || strlen(title) == 0)
		title = ReplaceString("_", name, " ")
	endif
	
	if (newGraph)
		Display
		name = S_name	
	else
		DoWindow $name
		if (V_flag)
			if (WinType(name) != 1)
				KillWindow $name
				Display /N=$name as title
				name = S_name
			else
				if (!add)
					removeAllTracesFromGraph(name, verbose=0)
				endif
			endif
		else
			Display /N=$name as title
			name = S_name
		endif
	endif
	
	Variable index = 0
	Variable numItems = ItemsInList(theList)
	for (index = 0; index < numItems; index += 1)
		if (dfr)
			AppendToGraph /W=$name folder:$(StringFromList(index, theList))
		else
			AppendToGraph /W=$name $(StringFromList(index, theList))
		endif
	endfor
	
	return 1
End
	
//! This function provides the user interface to scatter the individual data points in a 
// bar graph (the ScatterDataPoints function).
// @return: the result from the ScatterDataPoints function
//-
Function GH_ScatterPoints()
	
	String wName = "", graphName = "", show = "yes"
	Variable group = 1, width = 0.1
	
	String feedback = "-- used wave %s, group: %g, width: %g with show = %s on %s\r"
	
	STRUCT ScatterDataPrefs prefs
	loadScatterPrefs(prefs)
	
	wName = prefs.wName
	graphName = prefs.graphName
	width = prefs.width
	group = prefs.group
	
	if ((prefs.show) == 0)
		show = "no"
	else
		show = "yes"
	endif
	
	Prompt wName, "Choose data wave", popup, WaveList("*", ";", "")
	Prompt graphName, "Choose graphName"
	Prompt group, "Enter group number"
	Prompt width, "Choose scatter width"
	Prompt show, "Choose to display data or not", popup, "yes;no"
		
	DoPrompt "Scattered Data Input Panel", graphName, wName, group, width, show 
	
	if (V_flag == 0)
		prefs.wName = wName
		prefs.graphName = graphName
		prefs.width = width
		prefs.group = group
		
		if (cmpstr(show, "no") == 0)
			prefs.show = 0
		else
			prefs.show = 1
		endif
		
		saveScatterPrefs(prefs)
		printf feedback, wName, group, width, show, graphName
	else
		print "-- User canceled input."
		return 0
	endif
	
	
	return ScatterDataPoints(prefs.wName, prefs.group, width = prefs.width, graphName = prefs.graphName, show = prefs.show)
End


//! This function adds some jitter to the data points to prevent overplotting.
// @param wave_name: wave name with the overplotting data
// @param group: a number identifying the data group - becomes the x scale
// @param width: jitter width (default: 0.1)
// @param graphName: name of the graph for the jittered data (default: Scatterplot)
// @param show: display the data on a graph or not
// @return: creates the jittered data in a new wave and a potential graph
//-
Function ScatterDataPoints(wave_name, group, [width, graphName, show])
	String wave_name, graphName
   Variable width, group, show

	if (ParamIsDefault(show))
		show = !ParamIsDefault(graphName) && strlen(graphName) > 0 ? 1 : 0
	else
		show = show <= 0 ? 0 : 1
	endif 
		
	if (ParamIsDefault(graphName) || strlen(graphName) == 0)
		graphName = UniqueName("ScatterPlot", 6, 1)
	endif
	
	width = ParamIsDefault(width) || width <= 0 ? 0.1 : width

   String wave_x = wave_name + "_x", wave_sorted = wave_name + "_sort"
        
   Wave orgData = $wave_name
   if (!WaveExists(orgData))
   	// original data wave does not exist, so stop here
   	print "** ERROR: original data " + wave_name + " wave does not exists."
   	return 0
   endif     
   
   DFREF cDF = GetDatafolderDFR()
   NewDataFolder /O /S root:Packages:$ksScatterPlotHelperName
   DFREF folder = GetDatafolderDFR()
   SetDatafolder cDF
   
   // make duplicates for x-distribution and sorting
   Duplicate /O orgData, folder:$wave_x /WAVE = xWave, folder:$wave_sorted /WAVE = sWave
   Sort /DIML sWave, sWave
	xWave = group
	
   Make /O/N=10 folder:w_hist /WAVE = wHist

#if (IgorVersion() >= 7.00)
   Histogram /B=3/DEST=wHist sWave
#else
	Histogram /B=3 sWave, wHist
#endif
  
   Variable no_div = numpnts(wHist), no_point = numpnts(orgData)
   Variable i = 0, j = 0, k = 0, offset = 0
         
   for (i = 0; i < no_div; i += 1)
   	if (mod(wHist[i], 2) == 1)
   		k += 1
   		offset = 1
   	else
   		offset = 0
   	endif
   	if (wHist[i] > 1)
			for (j = 1; j < wHist[i]; j += 2)
				xWave[k] += width * -(j + offset)
				xWave[k + 1] += width * (j + offset)
				k += 2
			endfor
		endif
	endfor
	
	if (show)
		DoWindow $graphName
		if (V_flag == 0)
			Display /N=$graphName /B=xScatter sWave vs xWave as graphName
			ModifyGraph /W=$graphName standoff=0
			ModifyGraph /W=$graphName freePos(xScatter)={0,left}
		else
			AppendToGraph /W=$graphName /B=xScatter sWave vs xWave
		endif
		ModifyGraph /W=$graphName mode=3, marker=19
		SetAxis /A/N=1/E=1 xScatter
		SetAxis /A/N=1/E=1 left
	endif
	
end
	

// *******************************************************************************************************
//                         S C A L E B A R    P A N E L    A N D    F U N C T I O N S
// *******************************************************************************************************

Function /S GH_ScaleBarPanel()

	DoWindow $ksScalebarPanelName
	if (V_flag == 1)
		switch (WinType(ksScalebarPanelName))
			case 7:
				DoWindow /F $ksScalebarPanelName
				return ksScalebarPanelName
				break
			default:
				KillWindow $ksScalebarPanelName
		endswitch
	endif
	
	Variable rightMax, leftOffset = 5, topOffset = 5, secLOffset, secTOffset
	Variable height, length, numBars, success
	String bars = "", selBar = "", hAxisList = "", vAxisList = ""
	String defNumFormat = "%8.4g"
	
	STRUCT GH_ScaleBar bar
	String theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
	if (strlen(theGraph) == 0)
		defaultScaleBar(bar)
		theGraph = "_none_"
		numBars = 0
		bars = "_none_"
		selBar = bars
	endif

	DoWindow /F $theGraph
	
	success = getScaleBar(bar, theGraph)
	bars = getScalebarList(theGraph)
	numBars = ItemsInList(bars)

	switch (numBars)
		case 0:
			bars = "_new_"
			selBar = bars
			break
		case 1:
		default:
			bars = getScaleBarList(theGraph)
			selBar = StringFromList(0, bars)
			bars = "_new_;" + bars
	endswitch
	
	bars = ksQuote + bars + ksQuote
	hAxisList = ksQuote + getAxisList(theGraph, "horizontal") + ksQuote
	vAxisList = ksQuote + getAxisList(theGraph, "vertical") + ksQuote
	
	NewPanel /K=1/N=$ksScalebarPanelName as "Scalebar Panel"
	
	height = 22
	length = 180
	PopupMenu pu_graphList win=$ksScalebarPanelName, pos={leftOffset, topOffset}, size={length, height}
	PopupMenu pu_graphList win=$ksScalebarPanelName, title="graph", font=$ksPanelFont, fsize=12, bodyWidth=140
	PopupMenu pu_graphList win=$ksScalebarPanelName, value=WinList("*", ";", "WIN:1"), popvalue=theGraph
	PopupMenu pu_graphList win=$ksScalebarPanelName, proc=SBpu_changeGraph
	rightMax = leftOffset + length
	secLOffset = rightMax + 5
	length = 150
	PopupMenu pu_scalebarNum win=$ksScalebarPanelName, pos={secLOffset, topOffset}, size={length, height}
	PopupMenu pu_scalebarNum win=$ksScalebarPanelName, title="bar", font=$ksPanelFont, fsize=12, bodywidth=120
	PopupMenu pu_scalebarNum win=$ksScalebarPanelName, value=#bars, userdata=theGraph, popvalue=selBar
	PopupMenu pu_scalebarNum win=$ksScalebarPanelName, proc=SBpu_changeScaleBar
	secLOffset += length + 10
	length = 30
	rightMax = secLOffset + length + 5
	CheckBox cb_label win=$ksScaleBarPanelName, pos={secLOffset, topOffset + 2}, size={length, height}
	CheckBox cb_label win=$ksScaleBarPanelName, title="labels", font=$ksPanelFont, fsize=12, value=bar.labels
	CheckBox cb_label win=$ksScalebarPanelName, proc=SBcb_labelCheck
	
	topOffset += height + 8
	length = 422
	GroupBox gb_axisSetting win=$ksScalebarPanelName, pos={leftOffset, topOffset}, size={length, 3.5 * height}
	GroupBox gb_axisSetting win=$ksScalebarPanelName, font=$ksPanelFont, fsize=10, fColor=(26214,26214,26214)
	GroupBox gb_axisSetting win=$ksScalebarPanelName, title="axis"
	rightMax = leftOffset + length + 5
	
	topOffset += 20
	
	length = 160
	PopupMenu pu_hAxis win=$ksScalebarPanelName, pos={leftOffset, topOffset}, size={length, height}
	PopupMenu pu_hAxis win=$ksScalebarPanelName, title="horizt. axis", font=$ksPanelFont, fsize=10, bodyWidth=95
	PopupMenu pu_hAxis win=$ksScalebarPanelName, value=#hAxisList, proc=SBpu_changeAxis
	secLOffset = leftOffset + length + 5
	length = 95
	SetVariable sv_hStart win=$ksScalebarPanelName, pos={secLOffset, topOffset+2}, size={length, height}
	SetVariable sv_hStart win=$ksScalebarPanelName, title="start", font=$ksPanelFont, fsize=10,format=defNumFormat
	SetVariable sv_hStart win=$ksScalebarPanelName, value=_NUM:bar.hStart, proc=SBsv_setStructValues
	secLOffset += length + 5
	SetVariable sv_hDelta win=$ksScalebarPanelName, pos={secLOffset, topOffset+2}, size={length, height}
	SetVariable sv_hDelta win=$ksScalebarPanelName, title="length", font=$ksPanelFont, fsize=10,format=defNumFormat
	SetVariable sv_hDelta win=$ksScalebarPanelName, value=_NUM:bar.hDelta, proc=SBsv_setStructValues
	secLOffset += length + 5
	length = 50
	SetVariable sv_hUnit win=$ksScalebarPanelName, pos={secLOffset, topOffset+2}, size={length, height}
	SetVariable sv_hUnit win=$ksScalebarPanelName, title="unit", font=$ksPanelFont, fsize=10
	SetVariable sv_hUnit win=$ksScalebarPanelName, value=_STR:bar.hUnit, proc=SBsv_setStructValues

	topOffset += height + 5
	length = 160
	PopupMenu pu_vAxis win=$ksScalebarPanelName, pos={leftOffset, topOffset}, size={length, height}
	PopupMenu pu_vAxis win=$ksScalebarPanelName, title="vert. axis", font=$ksPanelFont, fsize=10, bodyWidth=95
	PopupMenu pu_vAxis win=$ksScalebarPanelName, value=#vAxisList, proc=SBpu_changeAxis
	secLOffset = leftOffset + length + 5
	length = 95
	SetVariable sv_vStart win=$ksScalebarPanelName, pos={secLOffset, topOffset+2}, size={length, height}
	SetVariable sv_vStart win=$ksScalebarPanelName, title="start", font=$ksPanelFont, fsize=10, format=defNumFormat
	SetVariable sv_vStart win=$ksScalebarPanelName, value=_NUM:bar.vStart, proc=SBsv_setStructValues
	secLOffset += length + 5
	SetVariable sv_vDelta win=$ksScalebarPanelName, pos={secLOffset, topOffset+2}, size={length, height}
	SetVariable sv_vDelta win=$ksScalebarPanelName, title="length", font=$ksPanelFont, fsize=10,format=defNumFormat
	SetVariable sv_vDelta win=$ksScalebarPanelName, value=_NUM:bar.vDelta, proc=SBsv_setStructValues
	secLOffset += length + 5
	length = 50	
	SetVariable sv_vUnit win=$ksScalebarPanelName, pos={secLOffset, topOffset+2}, size={length, height}
	SetVariable sv_vUnit win=$ksScalebarPanelName, title="unit", font=$ksPanelFont, fsize=10
	SetVariable sv_vUnit win=$ksScalebarPanelName, value=_STR:bar.vUnit, proc=SBsv_setStructValues

	topOffset += height + 15
	leftOffset = 15
	length = 80
	SetVariable sv_lineSize win=$ksScalebarPanelName, pos={leftOffset, topOffSet}, size={length, height}
	SetVariable sv_lineSize win=$ksScalebarPanelName, title="line size", font=$ksPanelFont, fsize=10
	SetVariable sv_lineSize win=$ksScalebarPanelName, value=_NUM:bar.lineSize, limits={1, 30, 0}
	SetVariable sv_lineSize win=$ksScalebarPanelName, proc=SBsv_setStructValues
	secLOffset = leftOffset + length + 5
	SetVariable sv_fontSize win=$ksScalebarPanelName, pos={secLOffset, topOffSet}, size={length, height}
	SetVariable sv_fontSize win=$ksScalebarPanelName, title="font size", font=$ksPanelFont, fsize=10
	SetVariable sv_fontSize win=$ksScalebarPanelName, value=_NUM:bar.fontSize, limits={1, 144, 0}
	SetVariable sv_fontSize win=$ksScalebarPanelName, proc=SBsv_setStructValues

	length = 160
	secLOffset = rightMax - 10 - length
	SetVariable sv_hLabel win=$ksScalebarPanelName, pos={secLOffset, topOffSet}, size={length, height}
	SetVariable sv_hLabel win=$ksScalebarPanelName, title="horz. label", font=$ksPanelFont, fsize=10, bodywidth=100
	SetVariable sv_hLabel win=$ksScalebarPanelName, value=_STR:bar.hLabel, proc=SBsv_setStructValues
	topOffset += height
	SetVariable sv_vLabel win=$ksScalebarPanelName, pos={secLOffset, topOffSet}, size={length, height}
	SetVariable sv_vLabel win=$ksScalebarPanelName, title="vert. label", font=$ksPanelFont, fsize=10, bodywidth=100
	SetVariable sv_vLabel win=$ksScalebarPanelName, value=_STR:bar.vLabel, proc=SBsv_setStructValues
	

	topOffset += height
	length = 60
	Variable center = round(rightMax / 2)
	secLOffset = round(center - (1.5 * length)) - 10
	Button btn_removeBar win=$ksScalebarPanelName, pos={secLOffset, topOffset}, size={length, height}
	Button btn_removeBar win=$ksScalebarPanelName, title="remove", disable=2, proc=SBbn_scaleBarAction

	secLOffset = round(center - (length / 2))
	Button btn_drawBar win=$ksScalebarPanelName, pos={secLOffset, topOffset}, size={length, height}
	Button btn_drawBar win=$ksScalebarPanelName, title="draw", proc=SBbn_scaleBarAction
	
	adjustPanelSize(ksScalebarPanelName, pixelWidth=rightMax, pixelHeight=topOffset + height + 10)
	
	STRUCT WMPopupAction pu
	pu.ctrlName = "pu_graphList"
	pu.win = ksScalebarPanelName
	pu.eventCode = 2
	pu.popNum = 1
	pu.popStr = theGraph
	SBpu_changeGraph(pu)
	
	return ksScalebarPanelName
End

// *******************************************************************************************************
//                         F U N C T I O N S    F O R   S C A L E B A R P A N E L 

static Function updateControlValues(bar, theGraph, [changeHAxis, changeVAxis])
	STRUCT GH_ScaleBar &bar
	String theGraph
	Variable changeHAxis, changeVAxis
	
	if (ParamIsDefault(changeHAxis) || changeHAxis < 0)
		changeHAxis = 0
	else
		changeHAxis = changeHAxis >= 1 ? 1 : round(changeHAxis)
	endif
	if (ParamIsDefault(changeVAxis) || changeVAxis < 0)
		changeVAxis = 0
	else
		changeVAxis = changeVAxis >= 1 ? 1 : round(changeVAxis)
	endif
	
	String hAxisList = getAxisList(theGraph, "horizontal")
 	String vAxisList = getAxisList(theGraph, "vertical")
	
	if (!changeHAxis)
		GetAxis /Q/W=$theGraph $(StringFromList(0, hAxisList))
	else
		GetAxis /Q/W=$theGraph $(bar.hAxis)
	endif
	
	Variable delta = (V_max - V_min) / 7
	Variable start = V_max - delta
	if (!bar.isDefault)
		start = bar.hStart > V_max ? V_max : bar.hStart
		delta = bar.hDelta > (V_max - V_min) ? (V_max - V_min) : bar.hDelta
	else
		bar.hStart = start
		bar.hDelta = delta
	endif
	SetVariable sv_hStart win=$ksScalebarPanelName, value=_NUM:start, limits={V_min, V_max, 0}
	SetVariable sv_hDelta win=$ksScalebarPanelName, value=_NUM:delta, limits={0, V_max - V_min, 0}
	SetVariable sv_hUnit win=$ksScalebarPanelName, value=_STR:bar.hUnit

	if (!changeVAxis)
		GetAxis /Q/W=$theGraph $(StringFromList(0, vAxisList))
	else
		GetAxis /Q/W=$theGraph $(bar.vAxis)
	endif
	delta = (V_max - V_min) / 7
	start = V_max - delta
	if (!bar.isDefault)
		start = bar.vStart > V_max ? V_max : bar.vStart
		delta = bar.vDelta > (V_max - V_min) ? (V_max - V_min) : bar.vDelta
	else
		bar.vStart = start
		bar.vDelta = delta
	endif
	SetVariable sv_vStart win=$ksScalebarPanelName, value=_NUM:start, limits={V_min, V_max, 0}
	SetVariable sv_vDelta win=$ksScalebarPanelName, value=_NUM:delta, limits={0, V_max - V_min, 0}
	SetVariable sv_vUnit win=$ksScalebarPanelName, value=_STR:bar.vUnit

	CheckBox cb_label win=$ksScaleBarPanelName, value=bar.labels
	
	SetVariable sv_lineSize win=$ksScalebarPanelName, value=_NUM:bar.lineSize
	SetVariable sv_fontSize win=$ksScalebarPanelName, value=_NUM:bar.fontSize
	SetVariable sv_hLabel win=$ksScalebarPanelName, value=_STR:bar.hLabel
	SetVariable sv_vLabel win=$ksScalebarPanelName, value=_STR:bar.vLabel

	Variable hDispStr = WhichListItem(bar.hAxis, hAxisList) < 0 ? 1 : WhichListItem(bar.hAxis, hAxisList) + 1
	Variable vDispStr = WhichListItem(bar.hAxis, vAxisList) < 0 ? 1 : WhichListItem(bar.vAxis, vAxisList) + 1
	hAxisList = ksQuote + hAxisList + ksQuote
	PopupMenu pu_hAxis win=$ksScalebarPanelName, value=#hAxisList, popvalue=(bar.hAxis), mode=hDispStr
	vAxisList = ksQuote + vAxisList + ksQuote
	PopupMenu pu_vAxis win=$ksScalebarPanelName, value=#vAxisList, popvalue=(bar.vAxis), mode=vDispStr
	return 1
End

Function SBpu_changeGraph(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu
	
	switch ( pu.eventCode )
		case 2:
			DoWindow /F $pu.popStr
			DoWindow /F $pu.win
			
			STRUCT GH_ScaleBar bar
			getScaleBar(bar, pu.popStr)
			String barList = getScaleBarList(pu.popStr)
			barList = updateScalebarList(pu.popStr, barList)
			
			Variable numBars = ItemsInList(barList)
			Variable dispStrNum = WhichListItem(bar.name, barList)
			String bars = "", popVal = ""
			
			if (numBars == 0 || dispStrNum < 0)
				if (numBars == 0)
					Button btn_removeBar win=$pu.win, disable=2
				else
					Button btn_removeBar win=$pu.win, disable=0
				endif
				bars = "_new_"
				popVal = bars
				dispStrNum = 1
			else
				popVal = StringFromList(dispStrNum, barList)
				Button btn_removeBar win=$pu.win, disable=0
				bars = "_new_;" + barList
				dispStrNum += 1
			endif
			
			bars = ksQuote + bars + ksQuote
			
			PopupMenu pu_scalebarNum win=$pu.win, value=#bars, popvalue=popVal, userdata=pu.popStr, mode=dispStrNum
			updateControlValues(bar, pu.popStr)
			String storage = ""
			StructPut /S bar, storage
			SetWindow $pu.win userdata = storage
			SetWindow $pu.win userdata(curGraph) = pu.popStr
			break
	endswitch
	return 0
End

Function SBpu_changeScaleBar(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu
	
	switch ( pu.eventCode )
		case 2:
			String theGraph = pu.userdata
			if (strlen(theGraph) == 0)
				return 0
			endif
			
			if (cmpstr(pu.popStr, "_new_") != 0)
				STRUCT GH_ScaleBar bar
				getScalebar(bar, theGraph, barName = pu.popStr)
				updateControlValues(bar, theGraph)
				String storage = ""
				StructPut /S bar, storage
				SetWindow $pu.win userdata = storage
			endif
			break
	endswitch
	return 0
End

Function SBsv_setStructValues(sv) : SetVariableControl
	STRUCT WMSetVariableAction &sv
	
	switch ( sv.eventCode )
		case 1:
		case 2:
			STRUCT GH_ScaleBar bar
			String storage = GetUserData(sv.win, "", "")
			StructGet /S bar, storage
			strswitch ( sv.ctrlName )
				case "sv_hStart":
					bar.hStart = sv.dval
					break
				case "sv_hDelta":
					bar.hDelta = sv.dval
					break
				case "sv_hUnit":
					bar.hUnit = sv.sval
					break
				case "sv_vStart":
					bar.vStart = sv.dval
					break
				case "sv_vDelta":
					bar.vDelta = sv.dval
					break
				case "sv_vUnit":
					bar.vUnit = sv.sval
					break
				case "sv_lineSize":
					bar.lineSize = sv.dval
					break
				case "sv_fontSize":
					bar.fontSize = sv.dval
					break
				case "sv_hLabel":
					bar.hLabel = sv.sval
					break
				case "sv_vLabel":
					bar.vLabel = sv.sval
					break
			endswitch
			bar.isDefault = 0
			StructPut /S bar, storage
			SetWindow $sv.win userdata = storage
			break
	endswitch
	return 0
End

Function SBpu_changeAxis(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu
	
	switch ( pu.eventCode )
		case 2:
			STRUCT GH_ScaleBar bar
			String storage = GetUserData(pu.win, "", "")
			StructGet /S bar, storage
			ControlInfo /W=$pu.win pu_graphList
			String theGraph = S_Value
			
			strswitch (pu.ctrlName)
				case "pu_hAxis":
					bar.hAxis = pu.popStr
					updateControlValues(bar, theGraph, changeHAxis=1)
					break
				case "pu_vAxis":
					bar.vAxis = pu.popStr
					updateControlValues(bar, theGraph, changeVAxis=1)
					break
			endswitch
			StructPut /S bar, storage
			SetWindow $pu.win userdata=storage
	endswitch
	return 0
End

Function SBcb_labelCheck(cb) : CheckBoxControl
	STRUCT WMCheckboxAction &cb
	
	switch ( cb.eventCode )
		case 2:
			STRUCT GH_ScaleBar bar
			String storage = GetUserData(cb.win, "", "")
			StructGet /S bar, storage
			if (cb.checked)
				bar.labels = 1
			else
				bar.labels = 0
			endif
			StructPut /S bar, storage
			SetWindow $cb.win userdata=storage			
			break
	endswitch
	return 0
End

Function SBbn_scaleBarAction(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch(bn.eventCode)
		case 2:
			STRUCT GH_ScaleBar bar
			String storage = GetUserData(bn.win, "", "")
			StructGet /S bar, storage
			String theGraph = GetUserData(bn.win, "", "curGraph")
			Variable maxNum = getMaxScaleBarNum(theGraph)
			Variable dispStrNum = 1
			ControlInfo /W=$bn.win pu_scalebarNum
			String barName = S_Value
			
			STRUCT WMPopupAction pu
			pu.win = bn.win
			pu.eventCode = 2
			pu.popStr = theGraph
			
			strswitch(bn.ctrlName)
				case "btn_drawBar":
					if (cmpstr(barName, "_new_") == 0)
						if (numtype(maxNum) == 2)
							maxNum = 1
						else
							maxNum += 1
						endif
						bar.name = ksUDbaseName + num2str(maxNum)
					endif
					GH_addSBStruct2Graph(theGraph, bar)
					setScaleBar(bar, theGraph)
					SBpu_changeGraph(pu)
					break
				case "btn_removeBar":
					if (cmpstr(barName, "_new_") != 0)
						GH_removeSBStructFGraph(theGraph, barName)
						SBpu_changeGraph(pu)
					endif
					break
			endswitch
			break
	endswitch
	return 0
End
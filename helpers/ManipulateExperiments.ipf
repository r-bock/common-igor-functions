#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.1
#pragma version = 1

// by Roland Bock, September 2014
// a small function to merge experiments

Menu "Macros"
	"Merge Experiments ...", combineMultipleExperiments()
End

Function combineMultipleExperiments()

	Variable refNum
	String filterString = "Igor Experiments (*.pxp):.pxp;All Files:.*;"
	
	Open /D/R/F=filterString/MULT=1 refNum
	
	if (cmpstr(S_fileName, "") == 0)
		return 0
	endif
	
	String cmd = ""
	String files = ReplaceString("\r", S_fileName, ";")
	String fileNames = ""
	Variable numFiles = ItemsInList(files)
	Variable index
	
	for (index = 0; index < numFiles; index += 1)
		String file = StringFromList(index, files)
		sprintf cmd, "MERGEEXPERIMENT %s", file
		Execute/P cmd
		fileNames = AddListItem(ParseFilePath(3, file, ":", 0, 0), fileNames, ";", Inf)
	endfor	

	printf "** merged %s with this file on %s at %s **\r", fileNames, date(), time()
	return 1
End 

#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6.1
#pragma version = 2.4
#pragma ModuleName = ClusterAnalysisHelpers
#include "ListHelpers", version >= 4.7

// written by Roland Bock NIAAA / NIH
// version 2.4 (December 2011)
// version 1.0 (October 2010)

// *************************************************
// ********                       CONSTANTS
// *************************************************
static StrConstant ksParentFolder = "Summaries"
static StrConstant ksFolderName = "Clusters"

// keys for key-value list 
static StrConstant ksEventKey = "EVENTS"
static StrConstant ksEvTimesKey = "ETIMES"
static StrConstant ksClusterKey = "CLUSTERS"
static StrConstant ksClStTimesKey = "CSTIMES"
static StrConstant ksClEdTimesKey = "CETIMES"
static StrConstant ksClusterSizeKey = "CSIZE"
static StrConstant ksIRIClusterEvents = "IRICLE"
static StrConstant ksTotalEventsKey = "TOTALEV"

// *************************************************
// ********                   STATIC FUNCTIONS
// *************************************************
// Make sure that the Clusters data folder exists in the Summaries data folder.
static Function /DF getFolder()
	
	DFREF thePath = $("root:" + ksParentFolder + ":" + ksFolderName + ":")
	Variable status = DataFolderRefStatus(thePath)
	
	
	if (status == 0)
		DFREF cDF = GetDataFolderDFR()	
		SetDataFolder root:
		if (DataFolderExists(ksParentFolder))
			SetDataFolder ksParentFolder
		else
			NewDataFolder /S $ksParentFolder
		endif
		if (DataFolderExists(ksFolderName))
			SetDataFolder ksFolderName
		else
			NewDataFolder /S $ksFolderName
		endif
		DFREF thePath = GetDataFolderDFR()
		SetDataFolder cDF
	endif
	return thePath
End


// *************************************************
// ********                       FUNCTIONS
// *************************************************

// This file contains a collection of functions to analyse waves for single events and clusters of events. The data
// in the analysed waves have to be monotonously increasing or decreasing.

// 
// Input:
//		theWave			wave reference to the wave for analysis
//		clusterDistance		value that separates the single events from the clusters. Every event below the 
//								distance is added to a cluster, everything above treated as single event
//		minSize				minimum number of events to be in a cluster to be counted as a cluster. If it is less than
//								2 it defaults to 2 events per cluster
//		includeFirst		0 for FALSE (default), 1 for TRUE
//								assumes that there is a event at time zero that is not recorded in the traces
// Output:
//		Key list of 6 elements:
//			EVENTS:		number of single events after cluster analysis
//			CLUSTERS: 		number of clusters
//			ETIMES:		comma separated list of the single event timestamp in seconds from experiment start
//			CSTIMES:		comma separated list of cluster start times in seconds
//			CETIMES:		comma separated list of cluster end times in seconds
//			IRICLE:			comma separated list of inter-reward intervals of the events in each cluster
//			CSIZE:			comma separated list of number of events per cluster
//			TOTALEV:		number of total events in that wave
//
//			The time stamp lists are empty if no cluster or event is found.
Function /S CA_analyseForCluster(theWave, clusterDistance, minSize)
	Wave theWave
	Variable clusterDistance, minSize
	
	if (!WaveExists(theWave))
		print "** ERROR (CA_analyseForCluster): wave does not exists. NOT ANALYZED!"
		return ""
	endif
	if (minSize < 2)
		minSize = 2
	endif
	Variable rev = 0		// check if the values are decreasing by checking the first and last value
	if (theWave[0] > theWave[numpnts(theWave) - 1])
		Reverse theWave
		rev = 1
	endif
	
	String results = ""
	Variable index = 0, events = 0, clusters = 0
	String eventTimesList = "", clusterStTimesList = "", clusterEdTimesList = "", clEventIRIList = ""
	String eventFlag = "", strValue = ""
	
	events = numpnts(theWave)
	results = ReplaceNumberByKey(ksTotalEventsKey, results, events)
	
	if (events <= 1)
		if (events == 1)
			sprintf strValue, "%f", theWave[0]
			eventTimesList = AddListItem(strValue, eventTimesList, ",")
		endif
	else
		Differentiate /METH=1 /EP=1 theWave /D=difference
		WaveStats /Q /C=1 /W difference
		Wave M_WaveStats
		if (M_WaveStats[%min] > clusterDistance)
			// if the minimum difference is larger then the cluster distance, all events are single events,
			// nothing more to do analyse
			String cDF = GetDataFolder(1)
			String folder = GetWavesDataFolder(theWave, 1)
			SetDatafolder folder
			eventTimesList = makeListFromWave(NameOfWave(theWave))
			SetDatafolder cDF
		elseif (M_WaveStats[%max] < clusterDistance)
			// the maximum difference is smaller than the cluster distance, the whole wave is one cluster.
			sprintf strValue, "%f", theWave[0]
			clusterStTimesList = AddListItem(strValue, clusterStTimesList)
			sprintf strValue, "%f", theWave[numpnts(theWave) - 1]
			clusterEdTimesList = AddListItem(strValue, clusterEdTimesList)
		else
			for (index = 0; index < numpnts(difference); index += 1)
				if (difference[index] <= clusterDistance)
					// if difference is less than cluster distance its a cluster
					eventFlag = "cluster"
					Variable theStart = index
					do
						// now find end of cluster
						index += 1
					while (index < numpnts(difference) && difference[index] < clusterDistance)
					
					// now check if we have enough events for the mininum cluster size to call it a cluster
					if ((index - theStart) >= minSize - 1)
						sprintf strValue, "%f", theWave[theStart]
						clusterStTimesList = AddListItem(strValue, clusterStTimesList, ";", Inf)
						sprintf strValue, "%f", theWave[index]			
						clusterEdTimesList = AddListItem(strValue, clusterEdTimesList, ";", Inf)
						Variable i = theStart
						for (i = theStart; i < index; i += 1)
							clEventIRIList = AddListItem(num2str(difference[i]), clEventIRIList, ";", Inf)					
						endfor
					else
						// we did not find enough events to call it a cluster, so record them as single events
						do
							eventFlag = "event"
							sprintf strValue, "%f", theWave[theStart]
							eventTimesList = AddListItem(strValue, eventTimesList, ";", Inf)
							theStart += 1
						while(theStart <= index)
					endif
				elseif (difference[index] > clusterDistance)
					eventFlag = "event"
					sprintf strValue, "%f", theWave[index]
					eventTimesList = AddListItem(strValue, eventTimesList, ";", Inf)
					if (index == numpnts(difference) - 1)
						// make sure to record the last event. 
						sprintf strValue, "%f", theWave[index + 1]
						eventTimesList = AddListItem(strValue, eventTimesList, ";", Inf)
					endif
				endif
			endfor
		endif
	endif

	results = ReplaceNumberByKey(ksEventKey, results, ItemsInList(eventTimesList))
	results = ReplaceStringByKey(ksEvTimesKey, results, ReplaceString(";", eventTimesList, ","))
	results = ReplaceNumberByKey(ksClusterKey, results, ItemsInList(clusterStTimesList))
	results = ReplaceNumberByKey(ksClusterSizeKey, results, minSize)
	results = ReplaceStringByKey(ksClStTimesKey, results, ReplaceString(";", clusterStTimesList, ","))
	results = ReplaceStringByKey(ksClEdTimesKey, results, ReplaceString(";", clusterEdTimesList, ","))
	results = ReplaceStringByKey(ksIRIClusterEvents, results, ReplaceString(";", clEventIRIList, ","))
			
	KillWaves /Z difference
	if (rev)
		// if the wave was reversed, change it back
		Reverse theWave
	endif
	return results
End

// This function processes a list of waves and looks for clusters in each of them.
// Input:
//		theList				a semicolon separated list of waves
//		clusterDistance		minimum distance between two events to combine to a cluster
// Output:
//		List					a semicolon separated list of 4 wave names containing; each wave has the same length
//							as there are waves in the input list.
//			*_events:		the number of single events per wave in the list
//			*_clusters:		the number of clusters for the wave
//			*_avgEvPCl:	the mean number of events per cluster
//			*_percEvPTot:	the percentage of single events of the total events
//			*_allClLength:	length of each cluster in seconds
Function /S CL_clustersByList(theList, clusterDistance, [minSize, includeFirst])
	String theList
	Variable clusterDistance, minSize, includeFirst
	
	if (ParamIsDefault(includeFirst))
		includeFirst = 0
	endif
	if (includeFirst < 0)
		includeFirst = 0
	elseif (includeFirst > 1)
		includeFirst = 1
	else
		includeFirst = round(includeFirst)
	endif
	if (ParamIsDefault(minSize) || minSize < 2)
		minSize = 2
	endif
	
	String results = ""
	String resultList = ""
	String desc = "DESCRIPTION"
	
	String prefix = "CLAN"						// prefix for cluster analysis result waves
	String theNote = ""
	
	Variable elements = ItemsInList(theList)
	Make /O/N=(elements) $(prefix + "_events") /WAVE=clEvents
	resultList = AddListItem(prefix + "_events", resultList, ";", Inf)
	theNote = ReplaceStringByKey(desc, note(clEvents), "number of single events for " + num2str(elements) + " traces")
	Note /K clEvents, theNote
	
	Make /O/N=(elements) $(prefix + "_clusters") /WAVE=clClusters
	resultList = AddListItem(prefix + "_clusters", resultList, ";", Inf)
	theNote = ReplaceStringByKey(desc, note(clClusters), "number of clusters " + num2str(elements) + " traces")
	Note /K clClusters, theNote
	
	Make /O/N=(elements) $(prefix + "_avgEvPCl") /WAVE=clAvgEPC
	resultList = AddListItem(prefix + "_avgEvPCl", resultList, ";", Inf)
	theNote = ReplaceStringByKey(desc, note(clAvgEPC), "mean number of events per cluster " + num2str(elements) + " traces")
	Note /K clAvgEPC, theNote
	
	Make /O/N=0 $(prefix + "_allClIEIEvents") /WAVE=clAllIEIEvt
	SetScale d 0, 0, "s", clAllIEIEvt
	resultList = AddListItem(prefix + "_allClIEIEvents", resultList, ";", Inf)
	theNote = ReplaceStringByKey(desc, note(clAllIEIEvt), "all IEI of the events in each cluster for " + num2str(elements) + " traces")
	Note /K clAllIEIEvt, theNote

	Make /O/N=(elements) $(prefix + "_percEvPTot") /WAVE=clPercEPT
	resultList = AddListItem(prefix + "_percEvPTot", resultList, ";", Inf)
	theNote = ReplaceStringByKey(desc, note(clPercEPT), "percentage of single events per total events " + num2str(elements) + " traces")
	Note /K clPercEPT, theNote
	
	Make /O/N=0 $(prefix + "_allClLength") /WAVE=allClLength
	SetScale d 0, 0, "s", allClLength
	resultList = AddListItem(prefix + "_allClLength", resultList, ";", Inf)
	theNote = ReplaceStringByKey(desc, note(allClLength), "cluster length " + num2str(elements) + " traces")
	Note /K allClLength, theNote

	Make /O/N=0 $(prefix + "_allClStartTimes") /WAVE=allClStarts
	SetScale d 0, 0, "s", allClStarts
	resultList = AddListItem(prefix + "_allClStartTimes", resultList, ";", Inf)
	theNote = ReplaceStringByKey(desc, note(allClLength), "start times of clusters of " + num2str(elements) + " traces")
	Note /K allClStarts, theNote

	Variable index = 0
	for (index = 0; index < elements; index += 1)
		Wave data = $StringFromList(index, theList)
		if (includeFirst && numpnts(data) > 0)
			Duplicate /O data, tmp_data
			InsertPoints 0, 1, tmp_data
			tmp_data[0] = 0
			Wave data = tmp_data
		endif
		results = CA_analyseForCluster(data, clusterDistance, minSize)
		
		Variable totals = NumberByKey(ksTotalEventsKey, results)
		clEvents[index] = NumberByKey(ksEventKey, results)
		clClusters[index] = NumberByKey(ksClusterKey, results)
		clAvgEPC[index] = clClusters[index] == 0 ? 0 : (totals - clEvents[index]) / clClusters[index]
		clPercEPT[index] = totals == 0 ? 0 : clEvents[index] / totals * 100
		if (clClusters[index] > 0)
			String startTimes = StringByKey(ksClStTimesKey, results)
			String endTimes = StringByKey(ksClEdTimesKey, results)
			Variable events = 0
			for (events = 0; events < ItemsInList(startTimes, ","); events += 1)
				allClStarts[numpnts(allClStarts)] = {(str2num(StringFromList(events, startTimes, ",")))}
				allClLength[numpnts(allClLength)] = {(str2num(StringFromList(events, endTimes, ",")) - str2num(StringFromList(events, startTimes, ",")))}
			endfor
			String iriTimes = StringByKey(ksIRIClusterEvents, results)
			for (events = 0; events < ItemsInList(iriTimes); events += 1)
				clAllIEIEvt[numpnts(clAllIEIEvt)] = {str2num(StringFromList(events, iriTimes, ","))}
			endfor
		endif	
		if (includeFirst)
			KillWaves /Z tmp_data
		endif
	endfor
	return resultList
End // END CL_clustersByList

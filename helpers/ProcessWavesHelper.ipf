#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 2
#pragma IgorVersion = 6
#include "GraphHelpers"
#include "ListHelpers", version >= 5.0

// Utilities function to average points of a wave
// written by Roland Bock
//    * version 2 (October 2018)
//    - version 1.5 (August 2011)
// 	- version 1 (March 2009)

// Menu entry in the macros menu to provide a simple GUI interface as
// alternative to the command line usage
//    version 1.5 (August 2011)
Menu "Macros"
	"-"
	"process wave points", processWavePointsInput()
	"process wave points from graph...", processWPFromGraph()
	"-"
End

// Input procedure for processWavePoints. Only provides input to the command
// line function. Using the GUI will also display the waves in a graph. This graph is 
// only for short visual inspection and will be overwritten 
//    version 1.0 (March 2009)
Proc processWavePointsInput(theWave, NumOfPoints, theFunction)
	String theWave
	Variable NumOfPoints = 4
	String theFunction
	Prompt theWave, "select the wave", popup, WaveList("*", ";", "")
	Prompt NumOfPoints, "enter the number of points to combine:"
	Prompt theFunction, "enter the process function", popup, "sum;avg"
	
	if (cmpstr(theWave, "_none_") == 0)
		print "No waves available! Bye!"
		return 0
	endif
	
	String theList = processWavePoints($theWave, NumOfPoints, theFunction)
	showPWavesInGraph($(StringFromList(0, theList)), $(StringFromList(1, theList)), "ProcessResults")
	
End

// Process all the traces on a graph for the 
Proc processWPFromGraph(NumOfPoints, theFunction)
	Variable NumOfPoints = 4
	String theFunction
	Prompt NumOfPoints, "enter the number of points to combine:"
	Prompt theFunction, "enter the process function", popup, "sum;avg"

	String theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
	if (strlen(theGraph) == 0)
		DoAlert 0, "No graph window available. Please make a graph with the selected waves first."
		return 0
	endif
	
	String theList = TraceNameList(theGraph, ";", 1)
	
	processWPFromList(theList, NumOfPoints, theFunction, theGraph=theGraph)
End


// This function just serves as the input select function to make it easier to apply the 
// different functions.
//    version 1.0 (March 2009)
Function/S processWavePoints(theWave, NumOfPoints, theFunction)
	Wave theWave
	Variable NumOfPoints
	String theFunction
	
	String thePrefix
		
	if (cmpstr(theFunction, "sum") == 0)
		FUNCREF theProtoFunc theFunc = sumPoints
		thePrefix = "PSum"
	elseif (cmpstr(theFunction, "avg") == 0)
		FUNCREF theProtoFunc theFunc = averagePoints
		thePrefix = "PAvg_" 	
	else
		// default to average the points if nothing is recognized
		FUNCREF theProtoFunc theFunc = averagePoints
		thePrefix = "PAvg_"
	endif
	
	return processWavePointsBF(theWave, NumOfPoints, thePrefix, theFunc)
End

// Function to process the points of a wave, reducing the total number either 
// by summing or averaging. The new wave is scaled according to the input wave.
//    version 1.0 (March 2009)
Function/S processWavePointsBF(theWave, NumOfPoints, thePrefix, theFunc)
	Wave theWave
	Variable NumOfPoints
	String thePrefix
	FUNCREF theProtoFunc theFunc
	
	Variable index = 0		// index to loop through theWave
	Variable toIndex = 0
	Variable flag = 0
	
	String theName = NameOfWave(theWave)
	String thePWName = thePrefix + "_" + theName
		
	Make /N=0 /O $thePWName
	Wave theNewWave = $thePWName
	
	do
		if (toIndex < numpnts(theWave))
			toIndex = index + (NumOfPoints - 1)
		else
			toIndex = numpnts(theWave) - 1
		endif
		theNewWave[numpnts(theNewWave)] = {theFunc(theWave, index, toIndex, flag)}
		flag = 1
		index = toIndex + 1
	while(index < numpnts(theWave))
	
	SetScale /I x, leftx(theWave), rightx(theWave), WaveUnits(theWave, 0), theNewWave
	SetScale d, 0, 0, WaveUnits(theWave, 1), theNewWave
	return (theName + ";" + thePWName)
End

// Simple function to calculate the sum of the points. Returns only the sum of the points
//    version 1.0 (March 2009)
static Function sumPoints(theWave, StartPoint, EndPoint, Flag)
	Wave theWave
	Variable StartPoint, EndPoint, Flag

	return Sum(theWave, pnt2x(theWave, StartPoint), pnt2x(theWave, EndPoint))	
End

// Function to average the points, creates also a new wave to hold the standard deviation
//    version 1.0 (March 2009)
static Function averagePoints(theWave, StartPoint, EndPoint, Flag)
	Wave theWave
	Variable StartPoint, EndPoint, Flag
	
	if (!Flag)
		Make /N=0 /O $("PSdev_" + NameOfWave(theWave))
		Make /N=0 /O $("PSem_" + NameOfWave(theWave))
	endif
	Wave theSDWave = $("PSdev_" + NameOfWave(theWave))
	Wave theSEMWave = $("PSem_" + NameOfWave(theWave))
	
	WaveStats /Q /R=[StartPoint, EndPoint] theWave
	theSDWave[numpnts(theSDWave)] = {V_sdev}
	theSEMWave[numpnts(theSEMWave)] = {V_sdev / sqrt(V_npnts)}
	return V_avg
End

// This function processes a list of waves. If the graph name is given, the list is assumed
// to be a trace list from the graph
Function /S processWPFromList(theList, NumOfPoints, theFunction, [theGraph])
	String theList, theFunction, theGraph
	Variable NumOfPoints
	
	String resultList = "", resultName = ""
	String cDF = GetDatafolder(1)
	Variable index
	do
		if (ParamIsDefault(theGraph) || strlen(theGraph) == 0)
			resultName = processWavePoints($(StringFromList(index, theList)), NumOfPoints, theFunction)
			resultList = AddListItem(resultName, resultList, ";", Inf)
		else
			Wave theWave = TraceNameToWaveRef(theGraph, StringFromList(index, theList))
			SetDatafolder GetWavesDatafolder(theWave, 1)
			processWavePoints(theWave, NumOfPoints, theFunction)
			SetDatafolder cDF
		endif
		index += 1
	while (index < ItemsInList(theList))
	return resultList
End

// Just the prototype function for the processWavePoints function; does nothing, provides
// only the template
//    version 1.0 (March 2009)
Function theProtoFunc(theWave, StartPoint, EndPoint, Flag)
	Wave theWave
	Variable StartPoint, EndPoint
	Variable Flag		// boolean indicator 0 = false, 1 = true
	
End

// Just displays the original and the processed trace in one graph. It will always be overwritten
// when the function runs again.
//    version 1.0 (March 2009)
Function showPWavesInGraph(theWave, theOWave, theName)
	Wave theWave, theOWave
	String theName
	
	String theWName = NameOfWave(theWave)
	String theOWName = NameOfWave(theOWave)
	DoWindow /K $theName
	
	Display /K=1 theWave, theOWave as "Process Results"
	DoWindow /C $theName
	
	ModifyGraph rgb($theOWName)=(0,0,0)
	SetAxis/A/N=1 bottom
	SetAxis/A/N=1 left
	
	String thePrepend = theOWName[0, strsearch(theOWName, "_", 0) - 1]
	if (cmpstr(thePrepend, "PAvg") == 0)
		ModifyGraph mode($theOWName)=4, marker($theOWName)=8
		ErrorBars $theOWName Y, wave=($("PSem_" + theWName), $("PSem_" + theWName))
		AppendToGraph /W=$theName theOWave
		ModifyGraph rgb($(theOWName + "#1"))=(26214,26214,26214)
		ModifyGraph mode($(theOWName + "#1"))=4, marker($(theOWName + "#1"))=8
		ErrorBars $(theOWName + "#1") Y, wave=($("PSdev_" + theWName), $("PSdev_" + theWName))
		ReorderTraces $theOWName, {$(theOWName + "#1")}
	endif
	moveGraphWindowToCenter(theName)
End


// This wave processes a wave from the first to the last point and combines the data values using the same dimension
// labels. Depending on the mode, the values will be either averaged or summed and 
//
// required parameter
// data				data wave with dimension labels
//
// optional parameter
// mode		1		(default) use arithmetic mean
//				2		use sum
// new		0		(default) OVERWRITES the original data wave with the combined values
//				1		creates a new data wave with the addition of "_cmbd" to the data name
Function /WAVE PWH_combineDataByDimLabel(data, [mode, new])
	Wave data
	Variable mode, new
	
	Wave result
	if (!WaveExists(data))
		// no data wave, nothing to work with, so stop here.
		return result
	endif

	String lblList = makeLblListFromWave(data, unique = 1)
	Variable numLbls = ItemsInList(lblList)
	if (numLbls == 0)
		// no labels, nothing to work with, so just stop here.
		return result
	endif
	
	new = ParamIsDefault(new) || new < 1 ? 0 : 1
	
	if (ParamIsDefault(mode))
		mode = 3		// avg value in the M_WaveStats wave
	else
		switch (mode)
			case 2:
				// sum
				mode = 23			// point location of the sum value in the M_WaveStats wave
				break
			case 1:
			default:
				mode = 3			// point location of the avg value in the M_WaveStats wave
		endswitch
	endif
	
	if (new)
		DFREF dfr = GetWavesDataFolderDFR(data)
		Make /N=(numLbls)/O dfr:$(NameOfWave(data) + "_cmbd") /WAVE=combined
	else
		Make /FREE /N=(numLbls) /O combined	
	endif
		
	Variable index = 0
	for (index = 0; index < numLbls; index += 1)
		String lbl = StringFromList(index, lblList)
		Extract /FREE /O data, combineValues, cmpstr(GetDimLabel(data, 0, p), lbl) == 0
		if (numpnts(combineValues) > 0)
			WaveStats /Q /W/C=1 /M=1 /Z combineValues
			Wave M_WaveStats
			combined[index] = M_WaveStats[mode]
			SetDimLabel 0, index, $lbl, combined
		endif
	endfor

	if (!new)
		Duplicate /O combined, data
	endif

	return combined
End
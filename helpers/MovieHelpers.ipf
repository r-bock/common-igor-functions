#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6.12
#pragma version = 2.65
#pragma ModuleName = RBMovieHelpers
#include "TimeDateConversions"
#include "WindowHelper"

// written by Roland Bock
// ** version 2.65 (September 2012)
// ** version 0.1 (May 2011)

// ****************************************
// ********      MENU
// ****************************************
Menu "Movie", hideable
	"load movie ...", RBMovieHelpers#loadMovie()
	"-"
	"movie info", RBMovieHelpers#infoOnMovie()
	"show trace info", RBMovieHelpers#getInfoWave(type="trace")
	"-"
	"mark start", RBMovieHelpers#mark("start")
	"mark end", RBMovieHelpers#mark("end")
	"goto start mark", RBMovieHelpers#gotoMark("start")
	"goto end mark", RBMovieHelpers#gotoMark("end")
	"show first frame",RBMovieHelpers#showFrame(1)
	"get frame for ROI mask", RBMovieHelpers#getFrame4ROIMask()
//	"-"

//	"-"
//	"make movie", RBMovieHelpers#MakeMovie()

End

// ****************************************
// ********      CONSTANTS
// ****************************************
strConstant ksWin = "Windows"
strConstant ksMac = "Macintosh"
strConstant ksWaveDesc = "DESCRIPTION"

static StrConstant ksPackageName = "MovieHelpers"
static StrConstant ksMovieWaveName = "MovieInfo"
static StrConstant ksSelectedSequenceName = "SelectedSequence"
static StrConstant ksCurrentFrameName = "CurrentFrame"
static StrConstant ksDefaultMovieName = "Trace Movie"
static StrConstant ksTraceTimes = "TraceTimings"
static StrConstant ksConversionTime = "ConversionTime"
static StrConstant ksTotalMovieTime = "TotalMovieTime"
static StrConstant ksCurrentMovieName = "CurrentMovieName"

static StrConstant ksMovieFileName = "movieName"
static StrConstant ksMovieFilePath = "moviePath"
static StrConstant ksImage4ROI = "Image4ROI"
static StrConstant ksImage4ROIGraph = "Image4ROI_Graph"

static Constant kMaxWavePoints = 2147000000

// ****************************************
// ********     FUNCTIONS
// ****************************************

// create the data folder for the package, if it does not exists
static Function /DF getPackageFolder()
	
	DFREF cDF = GetDataFolderDFR()
	DFREF pDF = root:Packages:$ksPackageName
	
	if (DataFolderRefStatus(pDF) == 0)
		SetDataFolder root:
		if (DataFolderExists("Packages"))
			SetDataFolder Packages
		else
			NewDataFolder /S Packages
		endif
		NewDataFolder /O/S $ksPackageName
		DFREF pDF = GetDataFolderDFR()
	endif
	
	SetDataFolder cDF
	return pDF
End

static Function /S packagePathAsString()

	DFREF cDF = GetDatafolderDFR()
	DFREF pDF = getPackageFolder()
	
	SetDatafolder pDF
	String thePath = GetDatafolder(1)
	SetDatafolder cDF
	return thePath
End

//
// 		modes
//				0	default, return only the name
//				1	returns full file name
//				2 	returns only the path to the file, without the file name
static Function /S getMovieName([mode])
	Variable mode
	
	if (ParamIsDefault(mode))
		mode = 0
	endif
	
	String result = ""
	DFREF path = getPackageFolder()
	SVAR movieName = path:$ksMovieFileName
	SVAR moviePath = path:$ksMovieFilePath
		
	switch(mode)
		case 2:
			// return only path
			if (SVAR_Exists(moviePath))
				result = moviePath
			endif
			break
		case 1:
			if (SVAR_Exists(moviePath) && SVAR_Exists(movieName))
				result = moviePath + movieName
			endif
			break
		case 0:
		default:
			if (SVAR_Exists(movieName))
				result = movieName
			endif			
	endswitch
	
	return result
End
	
	
	
// return the current value for either the conversion time or total movie time
// created and caluclated by the pixelChange function
static Function getProcessTime(type)
	String type
	
	DFREF path = getPackageFolder()
	Variable aTime = 0
	
	strswitch(type)
		case "conversion":
			NVAR seconds = path:$ksConversionTime
			if (!NVAR_Exists(seconds))
				Variable /G path:$ksConversionTime
			else
				aTime = seconds
			endif
			break
		case "movie":
			NVAR seconds = path:$ksTotalMovieTime
			if (!NVAR_Exists(seconds))
				Variable /G path:$ksTotalMovieTime
			else
				aTime = seconds
			endif
			break
	endswitch
	return aTime
End

// get a wave reference to the info wave
static Function /WAVE getInfoWave([type])
	String type
	
	if (ParamIsDefault(type) || strlen(type) == 0)
		type = "movie"
	endif

	String theLabels, theInfoTableName, theInfoTableTitle, theName
	Variable index = 0
	DFREF thePackage = getPackageFolder()
	
	strswitch(type)
		case "trace":
			theLabels = "axisStart;axisMid;axisEnd;fixedTime;"
			theInfoTableName = "TraceInfoTable"
			theInfoTableTitle = "Trace Info"
			Wave info = thePackage:$ksTraceTimes
			theName = ksTraceTimes
			break
		case "movie":
		default:
			theLabels = "frames;framesPSec;totalTime;width;height;markStart;markEnd;selectedFrames;selTotalTime;maxFrames;maxTime;"
			theInfoTableName = "MovieInfoTable"
			theInfoTableTitle = "Movie Info"
			Wave info = thePackage:$ksMovieWaveName
			theName = ksMovieWaveName
	endswitch
	
	if (!WaveExists(info) || numpnts(info) != ItemsInList(theLabels))
		Make /N=(ItemsInList(theLabels)) /O thePackage:$theName /WAVE=info
		for (index = 0; index < ItemsInList(theLabels); index += 1)
			SetDimLabel 0, index, $(StringFromList(index, theLabels)), info
		endfor
		info = 0
	endif
	DoWindow /F $theInfoTableName
	if (V_flag == 0)
		// no window with this name exists, so make it
		Edit /W=(13,67,205,309)/N=$theInfoTableName /K=0 info.ld as theInfoTableTitle
		ModifyTable width(Point)=0
		ModifyTable width[1]=85, title[1]="info"
		ModifyTable width[2]=80, title[2]="value"
	elseif (V_flag == 2)
		DoWindow /HIDE=0 $theInfoTableName
	endif
	
	return info
End

static Function /S getCurrMovieName()
	
	DFREF thePackage = getPackageFolder()
	SVAR theName = thePackage:$ksCurrentMovieName
	if (!SVAR_Exists(theName))
		String /G thePackage:$ksCurrentMovieName = ""
	endif
	
	return (packagePathAsString() + ksCurrentMovieName)
End

static Function /S selectMovieCollectionFolder([new])
	Variable new
	
	if (ParamIsDefault(new) || new > 1)
		new = 1
	elseif (new < 0)
		new = 0
	else
		new = round(new)
	endif

	String msg = "Please choose the folder containing the movie folders"
	PathInfo rbMovieFolderPath
	if (!V_flag || new)
		NewPath /M=msg /O /Q rbMovieCollFolderPath
	else
		NewPath /M=msg /O/Q rbMovieCollFolderPath, S_path
	endif
	if (V_flag < 0)
		print "- User canceled macro."
		return ""
	endif
	
	return "rbMovieFolderPath"
End



// check videos for sizes
static Function checkVideoFolders4Sizes([useExisting])
	Variable useExisting

	if (ParamIsDefault(useExisting) || useExisting < 0)
		useExisting = 0
	elseif (useExisting > 1)
		useExisting = 1
	else
		useExisting = round(useExisting)
	endif

	String msg = "Please choose the folder with the movies"
	PathInfo rbMovieFolderPath
	if (!V_flag || !useExisting)
		NewPath /M=msg /O /Q rbMovieFolderPath
	else
		NewPath /M=msg /O/Q rbMovieFolderPath, S_path
	endif

	TextToNotebook("New video file check " + date(), theNotebook="Video Sizes")

	Variable folder = 0
	String moviePath = "", movieFileList = "", movie = "", parentFolder = ""
	
	do
		moviePath = IndexedDir(rbMovieFolderPath, folder, 1)
		if (strlen(moviePath) == 0)
			if (folder == 0)
				movieFileList = SortList(IndexedFile(rbMovieFolderPath, -1, ".mov"))
				if (ItemsInList(movieFileList) == 0)
					// neither folders nor video files in this path so something is wrong
					PathInfo rbMovieFolderPath
					print "** ERROR (checkVideoSizes): the selected folder does not contain video folder or video files. STOPPED!"
					break
				endif
				
				checkVideoFileSizes(movieFileList, "rbMovieFolderPath")
				break
			endif
			// reached last folder
			break
		endif
		
		NewPath /O/Q currentMoviePath, moviePath
		parentFolder = ParseFilePath(0, moviePath, ";", 1, 0)
		TextToNotebook("\r- processing: " + parentFolder, theNoteBook="Video Sizes")

		movieFileList = SortList(IndexedFile(currentMoviePath, -1, ".mov"))
		checkVideoFileSizes(movieFileList, "currentMoviePath")
	
		folder += 1
	while(1)
	
End

static Function checkVideoFileSizes(theList, thePath)
	String theList, thePath
	
	PathInfo $thePath
	if (V_flag)
		NewPath /O/Q videoFilesCheck, S_path
	else
		print "** ERROR (checkVideoFileSizes): path is invalid, don't know where to look for the files. STOPPED!"
		return 0
	endif
	if (ItemsInList(theList) == 0)
		print "** ERROR (checkVideoFileSizes): the file list is empty. STOPPED!"
		return 0
	endif
	
	Variable files = 0
	
	do
		String movie = StringFromList(files, theList)
		PlayMovie /P=videoFilesCheck as movie
		PlayMovieAction stop
		infoOnMovie(new=1)
		Wave info = getInfoWave()
		PlayMovieAction kill
		
		TextToNotebook("---> " + movie + ": " + num2str(info[%width]) + " x " + num2str(info[%height]), theNotebook="Video Sizes")
		files += 1
	while (files < ItemsInList(theList))
	
	return 1
End

// returns the Igor time for a time code based on YYYYMMDDHHMMSS
static Function getStartTimeFromCode(theCode)
	String theCode
	
	if (strlen(theCode) == 0)
		return 0
	endif
	
	Variable dotLoc = strsearch(theCode, ".", 0)
	if (dotLoc >= 14 || dotLoc == -1)
		theCode = theCode[0,13]
	else
		return 0
	endif
	
	Variable check = str2num(theCode)
	if (numtype(check) == 2)
		// conversion didn't work, so the we don't know how the numbers are distributed over the name
		return 0
	endif
	
	Variable year = str2num(theCode[0,3])
	Variable month = str2num(theCode[4,5])
	Variable day = str2num(theCode[6,7])
	Variable hour = str2num(theCode[8,9])
	Variable minutes = str2num(theCode[10,11])
	Variable seconds = str2num(theCode[12,13])

	return date2secs(year, month, day) + (hour*60*60) + (minutes*60) + seconds
End

// load movie
static Function loadMovie()
	
	DFREF package = getPackageFolder()	
	String videoFileFilters = "", pathSeparator = ""
	if (cmpstr(IgorInfo(2), ksMac) == 0)
		videoFileFilters = "Video File (*.mov):.mov;"
		pathSeparator = ":"
	else
		videoFileFilters = "Video File (*.avi, *.mov):.avi,.mov;"
		pathSeparator = "\\"
	endif
	SVAR movieName = package:$ksMovieFileName
	if (!SVAR_Exists(movieName))
		String /G package:$ksMovieFileName
		SVAR movieName = package:$ksMovieFileName
	endif
	SVAR moviePath = package:$ksMovieFilePath
	if (!SVAR_Exists(moviePath))
		String /G package:$ksMovieFilePath
		SVAR moviePath = package:$ksMovieFilePath
	endif
	
	String msg = "Please choose the movie file"
	PathInfo rbMovieFolderPath
	if (!V_flag)
		PathInfo home
		NewPath /O/Q rbMovieFolderPath, S_path
	endif
	
	Variable refNum
	Open /P=rbMovieFolderPath /D/R /F=videoFileFilters refNum
	movieName = ParseFilePath	(0, S_fileName, pathSeparator, 1, 0)
	moviePath = ParseFilePath(1, S_fileName, pathSeparator, 1, 0)
	NewPath /O/Q rbMovieFolderPath, moviePath
	
	PlayMovie /P=rbMovieFolderPath as movieName		// load movie
	infoOnMovie(new=1)
End

// extract information from the topmost movie window
static Function infoOnMovie([new])
	Variable new
	
	if (ParamIsDefault(new) || new < 0)
		new = 0
	elseif (new > 1)
		new = 1
	else
		new = round(new)
	endif
	
	
	Wave info = getInfoWave()
	
	PlayMovieAction /Z stop, gotoEnd, getTime		// get movie length in seconds
	if (V_flag > 0)
		DoAlert 0, "I don't seem to have a movie to process. Please load one first."
		return 0
	endif

	info[%totalTime] = V_value
	PlayMovieAction step = -1, getTime			// go to second to last frame and get time in seconds
	info[%frames] = info[%totalTime] / (info[%totalTime] - V_value)
	info[%framesPSec] = info[%frames] / info[%totalTime]	
	PlayMovieAction gotoBeginning, extract
	
	Wave M_MovieFrame
	info[%width] = DimSize(M_MovieFrame, 0)
	info[%height] = DimSize(M_MovieFrame, 1)
	
	// Igor 6.22A limits the maximum number of points for a wave to 2147 million. Calculate the 
	//     limits of number of frames and time that can be extracted
	if (new)
		info[%maxFrames] = floor(kMaxWavePoints / (info[%width] * info[%height] * 3))
		if (info[%frames] < info[%maxFrames])
			info[%maxFrames] = info[%frames]
			info[%maxTime] = info[%totalTime]
		else
			info[%maxTime] = floor(info[%maxFrames] / info[%framesPSec])
		endif
	
		info[%markStart] = 0
		info[%markEnd] = 0
		info[%selectedFrames] = 0
		info[%selTotalTime] = 0
	endif
	
	DFREF thePath = getPackageFolder()
	Wave currentFrame = thePath:$ksCurrentFrameName
	if (!WaveExists(currentFrame))
		Duplicate M_MovieFrame, thePath:$ksCurrentFrameName
	endif
End

// mark a selection 
static Function mark(type)
	String type
	
	DFREF thePath = getPackageFolder()
	Wave info = getInfoWave()
	PlayMovieAction getTime
	strswitch(type)
		case "END":
		case "end":
			info[%markEnd] = V_value
			break
		case "START":
		case "start":
		default:
			String theImageName = "CurrentImagePlot"
			String theImageTitle = "Current Image Plot"
			info[%markStart] = V_value

			PlayMovieAction extract
			Wave M_MovieFrame
			Duplicate /O M_MovieFrame, thePath:$ksCurrentFrameName /WAVE=frame
			DoWindow /F $theImageName
			if (V_flag == 0)
				NewImage /N=$theImageName frame
			endif
	endswitch

	if (info[%markStart] < info[%markEnd])
		info[%selTotalTime] = info[%markEnd] - info[%markStart]
		info[%selectedFrames] = info[%selTotalTime] * info[%framesPSec]
		
		if (info[%selectedFrames] > info[%maxFrames])
			strswitch (type)
				case "END":
				case "end":
					DoAlert /T="Max Frames Alert" 1, "You are trying to extract more frames\rthan Igor can handle. Do you want to adjust \rthe start point to match this end point?" 
					if (V_flag == 1)
						// Yes clicked -> adjust start point to match end point
						info[%selectedFrames] = info[%maxFrames]
						info[%markStart] = info[%markEnd] - info[%maxTime]
					else
						// No clicked -> move end point to matching start point
						info[%selectedFrames] = info[%maxFrames]
						info[%markEnd] = info[%markStart] + info[%maxTime]
					endif
					gotoMark("end")
					break
				case "START":
				case "start":
				default:
					DoAlert /T="Max Frames Alert" 1, "You are trying to extract more frames\rthan Igor can handle. Do you want to adjust \rthe end point to match this start point?" 
					if (V_flag == 1)
						// Yes clicked -> adjust end point to match start point
						info[%selectedFrames] = info[%maxFrames]
						info[%markEnd] = info[%markStart] + info[%maxTime]
					else
						// No clicked -> move start point to matching end point
						info[%selectedFrames] = info[%maxFrames]
						info[%markStart] = info[%markEnd] - info[%maxTime]
					endif
					gotoMark("start")
					break
			endswitch
		endif
		
		try
			// although we already determined the wave size limit, we still might run into a "out of memory"
			// runtime error
			strswitch (type)
				case "END":
				case "end":
					PlayMovieAction extract=(info[%selectedFrames]), step=-(info[%selectedFrames]); AbortOnRTE
					break
				case "START":
				case "start":
				default:
					PlayMovieAction extract=(info[%selectedFrames]); AbortOnRTE
			endswitch
			Wave M_MovieChunk
			Duplicate /O M_MovieChunk, thePath:$ksSelectedSequenceName
		catch
			if (V_AbortCode == -4)
				String message = GetRTErrMessage()
				if (cmpstr("out of memory", StringFromList(1, message)) == 0)
					DoAlert 0, "Igor ran out of memory, trying to adjust max number of frames it can handle.\rThis might take a while."
					Variable err = GetRTError(1)
					calcMaxFramesByRTE()
				endif
			endif
		endtry
	elseif (info[%markEnd] > 0)
		DoAlert 0, "The start time has to be smaller than the end time!\rPlease selecte either a new start or end time."
	endif
End

// go to the selected mark
static Function gotoMark(type)
	String type
	
	DFREF thePath = getPackageFolder()
	Wave info = getInfoWave()
	
	strswitch (type)
		case "END":
		case "end":
			PlayMovieAction frame=(info[%markEnd] * info[%framesPSec])
			break
		case "START":
		case "start":
		default:
			PlayMovieAction frame=(info[%markStart] * info[%framesPSec])
	endswitch
End

static Function /WAVE getSelectionWave()
	
	DFREF pDF = getPackageFolder()
	Wave info = getInfoWave()
	Wave theSelection = pDF:$ksSelectedSequenceName
	if (!WaveExists(theSelection))
		PlayMovieAction /Z getTime
		if (V_flag > 0)
			DoAlert 0, "I don't seem to have a movie to process. Please load one and select a sequence."
			return theSelection
		endif
		Variable theFrame = V_value * info[%framesPSec]
		PlayMovieAction frame=(info[%markStart] * info[%framesPSec])
		mark("start")
		if (info[%markEnd] == 0)
			PlayMovieAction gotoEnd
			mark("end")
		endif
		PlayMovieAction frame = theFrame
	endif
	return theSelection
End
	
static Function /WAVE getCurrentFrame()
	
	DFREF pDF = getPackageFolder()
	Wave info = getInfoWave()
	Wave currentFrame = pDF:$ksCurrentFrameName
	if (!WaveExists(currentFrame))
		PlayMovieAction getTime
		if (V_flag > 0)
			DoAlert 0, "I don't seem to have a movie to process. Please load one and select a sequence."
			return currentFrame
		endif		
		Variable theFrame = V_value * info[%framesPSec]
		PlayMovieAction frame=(info[%markStart] * info[%framesPSec])
		mark("start")
		PlayMovieAction frame = theFrame
	endif
	return currentFrame
End

// this function sets the current frame to the selected frame number
static Function showFrame(theFrame)
	Variable theFrame
	
	DFREF thePackage = RBMovieHelpers#getPackageFolder()
	Wave theImage = thePackage:CurrentFrame
	Wave theSelection = thePackage:SelectedSequence
	
	theImage = theSelection[p][q][r][theFrame]
End

Function displayChunk()

	String theWinName = "FrameSearch"
	DFREF thePackage = RBMovieHelpers#getPackageFolder()
	Wave theFrame = thePackage:CurrentFrame
	Wave theChunk = thePackage:SelectedSequence
	
	DoWindow $theWinName
	if (!V_flag)
		NewImage /N=$theWinName theFrame
	endif
	
	Variable chunks = DimSize(theChunk, 3)
	Variable chunk = 0

	do
		theFrame = theChunk[p][q][r][chunk]
		TextBox/C/N=currChunk/F=2/A=RB "\\K(65535,0,0)" + num2str(chunk)
		DoUpdate
		chunk += 1
	while (chunk < chunks)
		
End

static Function calcMaxFramesByRTE()

	Wave info = getInfoWave()
	PlayMovieAction gotoBeginning

	Variable error = 1
	
	do
		info[%maxTime] -= 1
		info[%maxFrames] = round(info[%maxTime] * info[%framesPSec])
		try
			PlayMovieAction extract=(info[%maxFrames]); AbortOnRTE
			PlayMovieAction frame=(info[%markStart] * info[%framesPSec])
			error = 0
		catch
			if (V_AbortCode == -4)
				String message = GetRTErrMessage()
				if (cmpstr("out of memory", StringFromList(1, message)) == 0)
					Variable err = GetRTError(1)
				endif
			endif
		endtry
	while(error == 1 && info[%maxTime] > 0)
	
	KillWaves /Z M_MovieChunks
	DoAlert 0, "Adjusted max number of frames to current memory state.\rMoved play position to original start."	
End

static Function getFrame4ROIMask()

	DFREF package = getPackageFolder()
	PlayMovieAction extract
	if (V_flag > 0)
		print "** ERROR (getFrame4ROIMask): no video seems to be open. Please load a video first!"
		return 0
	endif
	Wave M_MovieFrame
	ImageTransform rgb2gray M_MovieFrame
	Wave M_RGB2Gray
	Duplicate /O M_RGB2Gray, package:$ksImage4ROI /WAVE=img4roi
	DoWindow /F $ksImage4ROIGraph
	if (!V_flag)
		NewImage /N=$ksImage4ROIGraph img4roi
	endif
End





// make the actual movie
static Function MakeMovie()

	DFREF thePath = getPackageFolder()
	Wave info = getInfoWave()
	Wave theSequence = getSelectionWave()
	Wave currentFrame = getCurrentFrame()
	
	if (info[%framesPSec] == 0)
		// make sure we have all the necessary values
		infoOnMovie()
	endif
	
	// make new movie, initialize with the first frame
	NewMovie /Z/F=(info[%framesPSec]) /P=home /O as ksDefaultMovieName
//	
//	Wave M_MovieFrame
//	Wave M_MovieChunk
//	
//	NVAR timeStart, timeMid, timeEnd
//	Variable axisStart = timeStart, currStart = 0
//	Variable axisMid = timeMid, currMid = 0
//	Variable axisEnd = timeEnd, currEnd = 0
//	Variable chunks = DimSize(M_MovieChunk, 3)
//	Variable chunk = 0, drawn = 0
//	Variable advance = 1/framesPSec
//	do
//		M_MovieFrame = M_MovieChunk[p][q][r][chunk]
//		SetAxis time currStart, currMid
//		SetAxis time2 currMid, currEnd
//		if (chunk > framesPSec && !drawn)
//			SetDrawLayer UserFront
//			SetDrawEnv linethick= 2,linefgc= (52428,52425,1),fillpat= 0
//			DrawPoly 0.130208333333333,0.0815347721822542,1,1,{0.130208333333333,0.0815347721822542,0.317708333333333,0.163069544364508,0.3359375,0.163069544364508,0.348958333333333,0.155875299760192,0.3515625,0.143884892086331,0.359375,0.124700239808153,0.346354166666667,0.112709832134293,0.3203125,0.100719424460432,0.184895833333333,0.0407673860911271,0.166666666666667,0.0335731414868106}
//			SetDrawEnv xcoord= top,ycoord= left,fsize= 14,textrgb= (52428,52425,1)
//			DrawText 100,25,"sipper tube"
//			drawn = 1
//		endif
//		if (chunk/chunks > 0.38)
//			SetDrawLayer /K UserFront
//		endif
//		DoUpdate
//		AddMovieFrame
//		currStart = axisStart + (advance * chunk)
//		currMid = axisMid + (advance * chunk)
//		currEnd = axisEnd + (advance * chunk)
//		chunk += 1
//	while (chunk < chunks)
//	CloseMovie
//	
//	PlayMovie /P=home as "tester movie"
	return 0
End

//	required parameters:
//			numFrames			step width in number of frames (ie. with a 30 fps video enter 30 if every sec 
//									should be analyzed)
//	optional parameters:
//			add
//
// 			ROIMasks			a wave of wave references that determine the region of interests (ROI). Contrary
//								to the Igor ROI waves these masks have 1 in the interested region and 0 everywhere
//								else.
//   		mode			0	default: both
//							1	locomotion (difference between adjacent frames)
//							2	timing (dfference between current frame and initial background frame)
// returns:
//			Wave of wave references
Function /WAVE getChangedPixelProfile(numFrames, [add, highlight, quiet, updateRate, timeCode, ROIMasks, mode, avgFrames])
	Variable numFrames, add, highlight, quiet, updateRate, mode
	String timeCode, avgFrames
	Wave /WAVE ROIMasks
	
	DFREF thePackage = getPackageFolder()
	DFREF cDF = GetDatafolderDFR()
	SetDatafolder root:
	Variable masks = 0
	Variable avgStartFrame = 0, avgEndFrame = 0
	
	if (ParamIsDefault(add) || add < 0)
		add = 0
	elseif (add > 1)
		add = 1
	else
		add = round(add)
	endif
	if (ParamIsDefault(highlight) || highlight < 0)
		highlight = 5
	else
		highlight = round(highlight)
	endif
	if (ParamIsDefault(quiet) || quiet < 0)
		quiet = 0
	elseif (quiet > 1)
		quiet = 1
	else
		quiet = round(quiet)
	endif
	if (ParamIsDefault(updateRate))
		updateRate = 1
	elseif (updateRate < 0)
		updateRate = 0
	else
		updateRate = round(updateRate)
	endif
	if (ParamIsDefault(timeCode) || strlen(timeCode) == 0)
		timeCode = ""
	endif
	if (ParamIsDefault(mode) || mode < 0)
		mode = 0
	elseif (mode > 2)
		mode = 2
	else
		mode = round(mode)
	endif
	if (ParamIsDefault(avgFrames) || strlen(avgFrames) == 0)
		if (mode == 0 || mode == 2)
			avgStartFrame = 0
			avgEndFrame = 10
		endif
	else
		if (mode == 0 || mode == 2)
			Variable sep = strsearch(avgFrames, "-", 0)
			if (sep > 0)
				avgStartFrame = str2num(avgFrames[0, sep-1])
				avgEndFrame = str2num(avgFrames[sep+1, strlen(avgFrames)])
				if (numtype(avgStartFrame) == 2 || numtype(avgEndFrame) == 2)
					// the conversion failed, so either its not a number or other wise not recognizable
					print "** ERROR (getChangedPixelProfile): can't get a numeric value from avgFrames: '" + avgFrames + "', set to default range of first 10 frames."
					avgStartFrame = 0
					avgEndFrame = 10
				endif
			else
				// no range, so assume single number and try to convert
				avgStartFrame = str2num(avgFrames)
				if (numtype(avgStartFrame) == 2)
					// the conversion failed, so either its not a number or other wise not recognizable
					print "** ERROR (getChangedPixelProfile): can't get a numeric value from avgFrames: '" + avgFrames + "', set to default range of first 10 frames."
					avgStartFrame = 0
					avgEndFrame = 10
				else
					avgEndFrame = avgStartFrame
				endif
			endif
		endif
	endif
	

	// in the new version we are returning a wave of wave references. To be able to switch between the different
	//   options we have to create the storage waves here
	Make /N=1 /FREE /WAVE /O profileStorage
	Make /N=1 /FREE /WAVE /O bgStorage
	
	if (!ParamIsDefault(ROIMasks))
		if (WaveExists(ROIMasks) && numpnts(ROIMasks) > 0)
			masks = numpnts(ROIMasks)
			// make a wave as storage structure
			Redimension /N=(masks) profileStorage
			Redimension /N=(masks) bgStorage
			Make /N=(masks) /FREE /WAVE /O markerStorage
		endif
	endif

	Wave info = RBMovieHelpers#getInfoWave()
	NVAR cTime = thePackage:$ksConversionTime
	if (!NVAR_Exists(cTime))
		Variable /G thePackage:$ksConversionTime
		NVAR cTime = thePackage:$ksConversionTime
	endif	
	NVAR tTime = thePackage:$ksTotalMovieTime
	if (!NVAR_Exists(tTime))
		Variable /G thePackage:$ksTotalMovieTime
		NVAR tTime = thePackage:$ksTotalMovieTime
	endif
	
	String pixelGraph = "theChangedPixelGraph"
	String profileName = "changedPixelProfile"
	String bgName = "changedBackgroundPixels"
	String displayName = "changedPixProfileMarkerNum"
	String colorName = "changedPixProfileColIndex"
	
	Variable history = (highlight * info[%framesPSec]) / numFrames
	Variable theStart = DateTime				// get start time
	Variable frame = 0, newStartPoint = 0
	
	Wave profile
	Wave background
	
	PlayMovieAction gotoBeginning
	PlayMovieAction extract
	if (V_flag > 0)
		print "-- ERROR (getChangedPixelProfile): something went wrong accessing the movie. Is the movie loaded? Stopped here."
		return profile
	endif
	
	Wave M_MovieFrame
	ImageTransform rgb2gray M_MovieFrame
	Wave M_RGB2Gray
	Duplicate /O M_RGB2Gray, reference, difference
	difference = 0

	Variable maxPixels = DimSize(reference, 0) * DimSize(reference, 1)
	Variable startTime = getStartTimeFromCode(timeCode)
	
	Variable mask = 0
	
	if (!add)
		if (masks)
			do
				switch (mode)
					case 2:
						Make /N=(ceil(info[%frames]/numFrames)) /O $(bgName + "_ROI" + num2str(mask + 1)) /WAVE=background
						SetScale /P x startTime, (numFrames/info[%framesPSec]), "dat", background
						Note /K background, "TYPE:location;DESCRIPTION:profile of changed pixels between every " + num2str(numFrames) + " frame and the background"
						background = 0
						bgStorage[mask] = background
						Duplicate /O background, $(displayName + "_ROI" + num2str(mask + 1)) /WAVE=markers
						SetScale /P x 0, 1, "", markers
						Note /K markers, ""
						break
					case 1:
						Make /N=(ceil(info[%frames]/numFrames)) /O $(profileName + "_ROI" + num2str(mask + 1)) /WAVE=profile
						SetScale /P x startTime, (numFrames/info[%framesPSec]), "dat", profile
						Note /K profile, "TYPE:locomotion;DESCRIPTION:profile of changed pixels between every " + num2str(numFrames) + " frames"
						profile = 0
						profileStorage[mask] = profile
						Duplicate /O profile, $(displayName + "_ROI" + num2str(mask + 1)) /WAVE=markers
						SetScale /P x 0, 1, "", markers
						Note /K markers, ""
						break
					case 0:
					default:
						Make /N=(ceil(info[%frames]/numFrames)) /O $(profileName + "_ROI" + num2str(mask + 1)) /WAVE=profile
						Note /K profile, "TYPE:locomotion;DESCRIPTION:profile of changed pixels between every " + num2str(numFrames) + " frames"
						profile = 0
						profileStorage[mask] = profile
						Make /N=(ceil(info[%frames]/numFrames)) /O $(bgName + "_ROI" + num2str(mask + 1)) /WAVE=background
						SetScale /P x startTime, (numFrames/info[%framesPSec]), "dat", profile, background
						Note /K background, "TYPE:location;DESCRIPTION:profile of changed pixels between every " + num2str(numFrames) + " frame and the background"
						background = 0
						bgStorage[mask] = background
						Duplicate /O profile, $(displayName + "_ROI" + num2str(mask + 1)) /WAVE=markers
						SetScale /P x 0, 1, "", markers
						Note /K markers, ""
				endswitch
				markers = 255
				markerStorage[mask] = markers
				mask += 1
			while (mask < masks)
			if (WaveExists(profile))
				Duplicate /O profile, $colorName /WAVE=colors
			else
				Duplicate /O background, $colorName /WAVE=colors
			endif
			colors = (history + 10)
		else
			Make /N=(ceil(info[%frames]/numFrames)) /O $profileName /WAVE=profile
			Note /K profile, "TYPE:locomotion;DESCRIPTION:profile of changed pixels between every " + num2str(numFrames) + " frames"
			profileStorage[0] = profile
			Make /N=(ceil(info[%frames]/numFrames)) /O $bgName /WAVE=background
			Note /K background, "TYPE:location;DESCRIPTION:profile of changed pixels between every " + num2str(numFrames) + " frame and the background"
			Duplicate /O profile, $displayName /WAVE=markers
			Duplicate /O profile, $colorName /WAVE=colors
			SetScale /P x startTime, (numFrames/info[%framesPSec]), "dat", profile, background
			profile = 0
			background = 0
			markers = 255
			colors = (history + 10)
		endif
	else
		if (masks)
			do
				switch (mode)
					case 2:
						Wave background = $(bgName + "_ROI" + num2str(mask + 1))
						if (WaveExists(background))
							bgStorage[mask] = background
						else
							print "** ERROR (getChangedPixelProfile): wave " + bgName + "_ROI" + num2str(mask+1) + " does not exists.!"
						endif
						newStartPoint = numpnts(background)
						Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) background
						background[newStartPoint, numpnts(background)] = 0
						break
					case 1:
						Wave profile = $(profileName + "_ROI" + num2str(mask + 1))
						if (WaveExists(profile))
							profileStorage[mask] = profile
						else
							print "** ERROR (getChangedPixelProfile): wave " + profileName + "_ROI" + num2str(mask+1) + " does not exists.!"
						endif
						newStartPoint = numpnts(profile)
						Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) profile
						profile[newStartPoint, numpnts(profile)] = 0
						break
					case 0:
					default:
						Wave profile = $(profileName + "_ROI" + num2str(mask + 1))
						if (WaveExists(profile))
							profileStorage[mask] = profile
						else
							print "** ERROR (getChangedPixelProfile): wave " + profileName + "_ROI" + num2str(mask+1) + " does not exists.!"
						endif
						newStartPoint = numpnts(profile)
						Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) profile
						profile[newStartPoint, numpnts(profile)] = 0
						Wave background = $(bgName + "_ROI" + num2str(mask + 1))
						if (WaveExists(background))
							bgStorage[mask] = background
						else
							print "** ERROR (getChangedPixelProfile): wave " + bgName + "_ROI" + num2str(mask+1) + " does not exists.!"
						endif
						Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) background
						background[newStartPoint, numpnts(background)] = 0
				endswitch

				Wave markers = $(displayName + "_ROI" + num2str(mask + 1))
				if (WaveExists(markers))
					markerStorage[mask] = markers
				else
					print "** ERROR (getChangedPixelProfile): wave " + displayName + "_ROI" + num2str(mask+1) + " does not exists.!"
				endif
				mask += 1
				Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) markers
				markers[newStartPoint, numpnts(markers)] = 255
			while(mask < masks)
			Wave colors = $colorName
			Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) colors
			colors[newStartPoint, numpnts(colors)] = (history + 10)			
		else
			switch (mode)
				case 2:
					Wave background = $bgName
					bgStorage[0] = background
					newStartPoint = numpnts(background)
					Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) background
					background[newStartPoint, numpnts(background)] = 0
					break
				case 1:
					Wave profile = $profileName
					profileStorage[0] = profile
					newStartPoint = numpnts(profile)
					Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) profile
					profile[newStartPoint, numpnts(profile)] = 0
					break
				case 0:
				default:
					Wave profile = $profileName
					profileStorage[0] = profile
					Wave background = $bgName
					bgStorage[0] = background
					newStartPoint = numpnts(profile)
					Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) profile
					Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) background
					profile[newStartPoint, numpnts(profile)] = 0
					background[newStartPoint, numpnts(background)] = 0
			endswitch
			Wave markers = $displayName
			Wave colors = $colorName
			Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) markers
			markers[newStartPoint, numpnts(markers)] = 255
			Redimension /N=(newStartPoint + ceil(info[%frames]/numFrames)) colors
			colors[newStartPoint, numpnts(colors)] = (history + 10)
		endif
	endif
	
	if (mode == 0 || mode == 2)
		PlayMovieAction frame=avgStartFrame
		PlayMovieAction extract
		Wave M_MovieFrame
		ImageTransform rgb2gray M_MovieFrame
		Wave M_RGB2Gray
		Duplicate /O M_RGB2Gray, refBackground, difBackground
		difBackground = 0
		
		if (avgEndFrame != avgStartFrame)
			Redimension /D refBackground
			Variable frameStep = avgStartFrame
			do
				PlayMovieAction step=1
				PlayMovieAction extract
				ImageTransform rgb2gray M_MovieFrame
				refBackground += M_RGB2Gray
				frameStep += 1
			while (frameStep <= avgEndFrame)
			
			refBackground /= (avgEndFrame - avgStartFrame) + 1
			Redimension /B/U refBackground
		endif
		
		PlayMovieAction gotoBeginning
	endif
	
	do
		PlayMovieAction step=numFrames
		PlayMovieAction extract
		// starting to use a 5V sync signal which disrupts the frame (white out). This should only affect about 2 frames. 
		//  if you encounter this, just advance to the next available frame. 
		ImageStats M_MovieFrame
		if (V_avg > 230)
			do
				PlayMovieAction step=1
				PlayMovieAction extract
				ImageStats M_MovieFrame
			while (V_avg > 230)
		endif
		
		switch (mode)
			case 2:
				ImageTransform rgb2gray M_MovieFrame
				difBackground = refBackground - M_RGB2Gray
				difBackground = difBackground[p][q] < 30 ? 255 : difBackground[p][q]
				difBackground = difBackground[p][q] > 230 ? 255 : difBackground[p][q]
				break
			case 1:
				ImageTransform rgb2gray M_MovieFrame
				difference = reference - M_RGB2Gray
				difference = difference[p][q] < 30 ? 255 : difference[p][q]
				difference = difference[p][q] > 230 ? 255 : difference[p][q]
				reference = M_RGB2Gray
				break
			case 0:
			default:
				ImageTransform rgb2gray M_MovieFrame
				difference = reference - M_RGB2Gray
				difference = difference[p][q] < 30 ? 255 : difference[p][q]
				difference = difference[p][q] > 230 ? 255 : difference[p][q]
				reference = M_RGB2Gray
				ImageTransform rgb2gray M_MovieFrame
				difBackground = refBackground - M_RGB2Gray
				difBackground = difBackground[p][q] < 30 ? 255 : difBackground[p][q]
				difBackground = difBackground[p][q] > 230 ? 255 : difBackground[p][q]
		endswitch
				
		mask = 0
		
		if (masks)
			do
				Wave roiMask = ROIMasks[mask]
				Extract /O roiMask, changedPixels, roiMask == 1
				maxPixels = numpnts(changedPixels)
				
				switch (mode)
					case 2:
						Duplicate /FREE /O difBackground, difBackgroundROI
						difBackgroundROI *= roiMask
						difBackgroundROI = difBackgroundROI[p][q] == 0 ? 255 : difBackgroundROI[p][q]
						Extract /O difBackgroundROI, changedPixels, difBackgroundROI < 230
						
						Wave background = bgStorage[mask]
						Wave markers = markerStorage[mask]
					
						if (numpnts(changedPixels) <= maxPixels)
							background[newStartPoint + frame] = numpnts(changedPixels)
						else
							background[newStartPoint + frame] = NaN
						endif
						break
					case 1:
						Duplicate /FREE /O difference, differenceROI
						differenceROI *= roiMask
						differenceROI = differenceROI[p][q] == 0 ? 255 : differenceROI[p][q]
						Extract /O differenceROI, changedPixels, differenceROI < 230
						
						Wave profile = profileStorage[mask]
						Wave markers = markerStorage[mask]
					
						if (numpnts(changedPixels) <= maxPixels)
							profile[newStartPoint + frame] = numpnts(changedPixels)
						else
							profile[newStartPoint + frame] = NaN
						endif
						break
					case 0:
					default:
						Duplicate /FREE /O difference, differenceROI
						differenceROI *= roiMask
						differenceROI = differenceROI[p][q] == 0 ? 255 : differenceROI[p][q]
						Extract /O differenceROI, changedPixels, differenceROI < 230
						
						Wave profile = profileStorage[mask]
						Wave markers = markerStorage[mask]
					
						if (numpnts(changedPixels) <= maxPixels)
							profile[newStartPoint + frame] = numpnts(changedPixels)
						else
							profile[newStartPoint + frame] = NaN
						endif
						Duplicate /FREE /O difBackground, difBackgroundROI
						difBackgroundROI *= roiMask
						difBackgroundROI = difBackgroundROI[p][q] == 0 ? 255 : difBackgroundROI[p][q]
						Extract /O difBackgroundROI, changedPixels, difBackgroundROI < 230
						
						Wave background = bgStorage[mask]
					
						if (numpnts(changedPixels) <= maxPixels)
							background[newStartPoint + frame] = numpnts(changedPixels)
						else
							background[newStartPoint + frame] = NaN
						endif
				endswitch
				markers[newStartPoint + frame] = (8 - mask)
				mask += 1
			while (mask < masks)
		else
			switch (mode)
				case 2:
					Extract /O difBackground, changedPixels, difBackground < 230
					if (numpnts(changedPixels) <= maxPixels)
						background[newStartPoint + frame] = numpnts(changedPixels)
					else
						background[newStartPoint + frame] = NaN			// something went wrong, probably switched to color image with 3 planes
					endif
					break
				case 1:
					Extract /O difference, changedPixels, difference < 230
					if (numpnts(changedPixels) <= maxPixels)
						profile[newStartPoint + frame] = numpnts(changedPixels)
					else
						profile[newStartPoint + frame] = NaN			// something went wrong, probably switched to color image with 3 planes
					endif
					break
				case 0:
				default:
					Extract /O difference, changedPixels, difference < 230
					if (numpnts(changedPixels) <= maxPixels)
						profile[newStartPoint + frame] = numpnts(changedPixels)
					else
						profile[newStartPoint + frame] = NaN			// something went wrong, probably switched to color image with 3 planes
					endif
					Extract /O difBackground, changedPixels, difBackground < 230
					if (numpnts(changedPixels) <= maxPixels)
						background[newStartPoint + frame] = numpnts(changedPixels)
					else
						background[newStartPoint + frame] = NaN			// something went wrong, probably switched to color image with 3 planes
					endif
			endswitch
			markers[newStartPoint + frame] = 8
		endif

		colors[0, newStartPoint + (frame - history)] = (history + 10)
		colors[newStartPoint + (frame - history), newStartPoint + frame] = newStartPoint + (frame - p)

		String axisName = ""
		Variable axisPercent = 0
		if (frame == 0)
			DoWindow /F $pixelGraph
			if (V_flag == 0)
				Display /N=$pixelGraph
				mask = 0
				if (masks)
					if (mode != 1 && mode != 2)
						axisPercent = (floor(100 / (masks * 2)))/100
					else
						axisPercent = (floor(100 / masks))/100
					endif
					do
						switch (mode)
							case 2:
								axisName = "ROI" + num2str(mask)
								Wave background = bgStorage[mask]
								Wave markers = markerStorage[mask]
								AppendToGraph /W=$pixelGraph /L=$axisName background
								ModifyGraph zmrkNum($(NameOfWave(background)))={markers}
								ModifyGraph zColorMax($(NameOfWave(background)))=(0,0,0)
								ModifyGraph zColor($(NameOfWave(background)))={colors, 0,history,Rainbow,0}						
								ModifyGraph axisEnab($axisName)={mask * axisPercent, (mask + 1) * axisPercent} 
								ModifyGraph lblPos($axisName)=70,freePos($axisName)=0
								SetAxis /A/N=1/E=1 $axisName
								Label /W=$pixelGraph $axisName, "difference to background"
								break
							case 1:
								axisName = "ROI" + num2str(mask)
								Wave profile = profileStorage[mask]
								Wave markers = markerStorage[mask]
								AppendToGraph /W=$pixelGraph /L=$axisName profile
								ModifyGraph zmrkNum($(NameOfWave(profile)))={markers}
								ModifyGraph zColorMax($(NameOfWave(profile)))=(0,0,0)
								ModifyGraph zColor($(NameOfWave(profile)))={colors, 0,history,Rainbow,0}						
								ModifyGraph axisEnab($axisName)={mask * axisPercent, (mask + 1) * axisPercent} 
								ModifyGraph lblPos($axisName)=70, freePos($axisName)=0
								SetAxis /A/N=1/E=1 $axisName
								Label /W=$pixelGraph $axisName, "difference between frames"
								break
							case 0:
							default:
								axisName = "laROI" + num2str(mask)
								Wave profile = profileStorage[mask]
								Wave markers = markerStorage[mask]
								AppendToGraph /W=$pixelGraph /L=$axisName profile
								ModifyGraph zmrkNum($(NameOfWave(profile)))={markers}
								ModifyGraph zColorMax($(NameOfWave(profile)))=(0,0,0)
								ModifyGraph zColor($(NameOfWave(profile)))={colors, 0,history,Rainbow,0}						
								ModifyGraph axisEnab($axisName)={mask * 2 * axisPercent, ((mask * 2) + 1) * axisPercent} 
								ModifyGraph lblPos($axisName)=70, freePos($axisName)=0
								SetAxis /A/N=1/E=1 $axisName
								Label /W=$pixelGraph $axisName, "difference between frames"
								axisName = "locROI" + num2str(mask)
								Wave background = bgStorage[mask]
								Wave markers = markerStorage[mask]
								AppendToGraph /W=$pixelGraph /L=$axisName background
								ModifyGraph zmrkNum($(NameOfWave(background)))={markers}
								ModifyGraph zColorMax($(NameOfWave(background)))=(0,0,0)
								ModifyGraph zColor($(NameOfWave(background)))={colors, 0,history,Rainbow,0}						
								ModifyGraph axisEnab($axisName)={(mask * 2 * axisPercent) + axisPercent, (((mask * 2) + 1) * axisPercent) + axisPercent} 
								ModifyGraph lblPos($axisName)=70, freePos($axisName)=0
								SetAxis /A/N=1/E=1 $axisName
								Label /W=$pixelGraph $axisName, "difference to background"
						endswitch
						mask += 1
					while (mask < masks)
				else	
					switch (mode)
						case 2:
							AppendToGraph /W=$pixelGraph background
							ModifyGraph zmrkNum($bgName)={markers}
							ModifyGraph zColorMax($bgName)=(0,0,0)
							ModifyGraph zColor($bgName)={colors, 0,history,Rainbow,0}
							SetAxis /A/N=1/E=1 left
							Label /W=$pixelGraph left, "difference to background"
							break
						case 1:
							AppendToGraph /W=$pixelGraph profile
							ModifyGraph zmrkNum($profileName)={markers}
							ModifyGraph zColorMax($profileName)=(0,0,0)
							ModifyGraph zColor($profileName)={colors, 0,history,Rainbow,0}
							SetAxis /A/N=1/E=1 left
							Label /W=$pixelGraph left, "difference between frames"
							break
						case 0:
						default:
							AppendToGraph /W=$pixelGraph /L=la profile
							AppendToGraph /W=$pixelGraph /L=loc background
							ModifyGraph zmrkNum={markers}
							ModifyGraph zColorMax=(0,0,0)
							ModifyGraph zColor={colors, 0,history,Rainbow,0}
							ModifyGraph axisEnab(la)={0,0.48}, axisEnab(loc)={0.52,1}
							ModifyGraph lblPos(la)=70, lblPos(loc)=70
							SetAxis /A/N=1/E=1 la
							Label /W=$pixelGraph la, "difference between frames"
							SetAxis /A/N=1/E=1 loc
							Label /W=$pixelGraph loc, "differences to background"
					endswitch
				endif
				resizeWindowToScreen(pixelGraph, screenFraction=0.5)
				moveWindowOnScreen(pixelGraph, location="center")
				ModifyGraph mode=4,standoff=0,dateInfo(bottom)={1,1,0}
				ModifyGraph opaque=1, gfont="Helvetica"
			endif
			SetAxis /A/N=1/E=1 bottom
		endif

		if (updateRate > 0)
			// increasing the update rate, i. e. waiting longer for points to be displayed will considerably
			//    speed up the processing
			if (mod(frame, updateRate) == 0)
				DoUpdate
			endif
		endif

		frame += 1
	while (frame < (ceil(info[%frames]/numFrames)))
	
	Variable theEnd = DateTime
	SetAxis/A/N=1/E=1 bottom
	ModifyGraph rgb=(0,0,0)
	
	if (!quiet)
		print "- needed ", seconds2TimeStr(theEnd-theStart), " to process ", seconds2TimeStr(info[%totalTime]), " video time."
	endif
	if (add)
		cTime += (theEnd - theStart)
		tTime += info[%totalTime]
		if (!quiet)
			print "-- needed in total ", seconds2TimeStr(cTime), " to process ", seconds2TimeStr(tTime), " of total video time."
		endif
	else
		cTime = (theEnd - theStart)
		tTime = info[%totalTime]
	endif
	
	// now clean up
	KillWaves /Z reference, difference, refBackground, difBackground
	
	SetDatafolder cDF
	
	switch (mode)
		case 2:
			return bgStorage
			break
		case 1:
			return profileStorage
			break
		case 0:
		default:
			Make /N=(2 * numpnts(profile)) /FREE /WAVE /O resultWave
			resultWave[0, numpnts(profile)-1] = profileStorage[p]
			resultWave[numpnts(profile), ] = bgStorage[p - numpnts(profile)]
			return resultWave
	endswitch
End

#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma ModuleName = GUIHelper
#pragma IgorVersion = 6
#pragma version = 1.4
#include "GraphHelpers"
#include "WindowHelper"

// written by Roland Bock, January 2014
// ** version 1.4 (August 2019): added the PanelResolution function to make sure that the panels
//                                are correctly sized on Windows; which requires a minimum version 7
//                                using a static PanelResultion function to provide compatiblity with lesser
//                                Igor versions.
// ** version 1.3 (January 2017): added wave notes to keep track of associated 
//                      source file to the parameters.
// ** version 1.2 (January 2017): created a panel geography structure to make resizing
//                            more available and added a parameter dialog
//
// ******************    S T A T I C S    D E F I N I T I O N S        ****************
#if defined(MACINTOSH)
	Strconstant ksGUI_GraphDefFont = "Helvetica"
	Strconstant ksGUI_DefPanelFont = "Verdana"
#else
	Strconstant ksGUI_GraphDefFont = "Arial"
	Strconstant ksGUI_DefPanelFont = "Arial"
#endif

#if Exists("PanelResolution") != 3
static Function PanelResolution(wName)		// for compatibility with Igor versions lesser than 7
	String wName
	return 72
End
#endif

Constant kGUI_DefFontSize = 10

static Strconstant ksGUI_parameterPanelName = "ParameterInputPanel"
static Strconstant ksGUI_parameterValueName = "parameterValues"
static Strconstant ksGUI_parameterSelection = "parameterSel"


// ******************                 S T R U C T U R E S                *****************

Structure PanelGeo
	char name[31]					// panel name
	STRUCT Rect win				// panel location in points
	STRUCT Rect loc				// panel location in pixels
	STRUCT Point wSize			// panel size in points
	STRUCT Point size			// panel size in pixels
	STRUCT Point minSize		// panel minimum size in points
EndStructure

// ****************************************************************
// ************                F U N C T I O N S                          ***************
// ****************************************************************

// ****************           S T A T I C S    ********************

static Function setPStructWinSize(panel, left, top, right, bottom, [pixels])
	STRUCT PanelGeo &panel
	Variable left, top, right, bottom, pixels
	
	pixels = ParamIsDefault(pixels) || pixels < 1 ? 0 : 1
	
	if (pixels)
		panel.loc.left = left < 0 ? panel.loc.left : left
		panel.loc.top = top < 0 ? panel.loc.top : top
		panel.loc.right = right < 0 ? panel.loc.right : right
		panel.loc.bottom = bottom < 0 ? panel.loc.bottom : bottom

		panel.win.left = panel.loc.left
		panel.win.top = panel.loc.top
		panel.win.right = panel.loc.right
		panel.win.bottom = panel.loc.bottom	
	else
		panel.win.left = left < 0 ? panel.win.left : left
		panel.win.top = top < 0 ? panel.win.top : top
		panel.win.right = right < 0 ? panel.win.right : right
		panel.win.bottom = bottom < 0 ? panel.win.bottom : bottom

		panel.win.left = panel.loc.left * (ScreenResolution / PanelResolution(panel.name))
		panel.win.top = panel.loc.top * (ScreenResolution / PanelResolution(panel.name))
		panel.win.right = panel.loc.right * (ScreenResolution / PanelResolution(panel.name))
		panel.win.bottom = panel.loc.bottom * (ScreenResolution / PanelResolution(panel.name))	
	endif
	
	panel.wSize.v = panel.win.bottom - panel.win.top
	panel.wSize.h = panel.win.right - panel.win.left
	
	panel.size.v = panel.loc.bottom - panel.loc.top
	panel.size.h = panel.loc.right - panel.loc.left
	return 1
End



// ****************           S T A T I C S    ********************

static Function /DF getPIPFolder()

	DFREF cDF = GetDatafolderDFR()
	DFREF package = root:Packages:$ksGUI_parameterPanelName
	if (DatafolderRefStatus(package) == 1)
		return package
	endif
	
	NewDatafolder /S/O root:Packages
	NewDatafolder /S/O $ksGUI_parameterPanelName
	DFREF package = GetDatafolderDFR()
	SetDatafolder cDF

	return package
End

static Function /S getFeedbackStr(name, msg, type)
	String name, msg
	Variable type
	
	String errorStr = "** ERROR (%s): %s!\r"
	String warnStr = "-- WARNING (%s): %s.\r"
	
	type = type < 2 ? 1 : 2
	
	String result = ""
	
	switch (type)
		case 2:
			sprintf result, warnStr, name, msg
			break
		case 1:
		default:
			sprintf result, errorStr, name, msg
	endswitch
	
	return result
End

static Function /S getErrorStr(name)
	String name
	return getFeedbackStr(name, "%s", 1)
End

static Function /S getWarnStr(name)
	String name
	return getFeedbackStr(name, "%s", 2)
End
			





Function /S GUI_adjustText2Width(str, width, font, fontSize)
	String str, font
	Variable width, fontSize
	
	if (strlen(str) == 0)
		return ""
	endif
	if (width <= 0 || fontSize <= 0)
		return ""
	endif
	if (strlen(font) == 0)
		return ""
	endif
	
	Variable lineBreaks = strsearch(str, "\r", 0)
	if (lineBreaks > 0)
		// we already substituted line breaks into the text
		return str
	endif
	
	Variable fsPix = fontSize * (ScreenResolution / PanelResolution(""))  // use global panel resolution setting
	Variable widthPix = FontSizeStringWidth(font, fsPix, 0, str)
	
	if ( widthPix <= width )
		// already fits into the width, just return the text
		return str
	endif
	
	Variable tLength = strlen(str)
	Variable fraction = ceil( widthPix / width )
	Variable length = round(tLength / fraction)
	Variable index = 0, strStart = 0, strEnd = length, spaceloc = 0
	String newStr = ""
	
	do
//	for (index = 0; index < fraction; index += 1)
//		Variable strEnd = (index + 1) * length
		String part = str[strStart, strEnd]
		// first check if the new part will fit within length
		widthPix = FontSizeStringWidth(font, fsPix, 0, part)
		
		if ( widthPix < width ) 
			// if it does, check for the next space 
			if (strEnd < strlen(str) - 1)
				spaceloc = strsearch(str, " ", strEnd)
				if (spaceloc >= 0)
					// and check that length again
					part = str[strStart, spaceloc]
					widthPix = FontSizeStringWidth(font, fsPix, 0, part)
					if (widthPix <= width)
						// if it is still within length, replace the last space with linebreak
						length = strlen(part)
						part[length-1, length-1] = "\r"
						strStart += length
						newStr += part
					else
						// if it is too long
						Variable newSloc = strsearch(part, " ", strlen(part) - 2, 1)
						if (newSloc >= 0)
							part = part[0, newSloc]
							length = strlen(part)
							part[length-1, length-1] = "\r"
							strStart += newSloc + 1
							newStr += part
						else
							part += "\r"
							length = strlen(part) - 1
							newStr += part
							strStart += length
						endif
					endif
					strEnd = strStart + length		
				else
					// figure out what to do if there are no spaces
					spaceloc = strsearch(part, " ", Inf, 1)
					if (spaceloc >= 0)
						part = part[0, spaceloc]
						length = strlen(part)
						part[length-1, length-1] = "\r"
						strStart += length
					endif
				endif
				strEnd = strStart + length		
			else
				// we arrived at the end of the string, so just stick it at the back
				newStr += part
//				index = fraction 		// this should terminate the loop
				strStart = tLength
			endif
		else
			// initial pix length is longer than desired
			spaceloc = strsearch(part, " ", Inf, 1)
			if (spaceloc >= 0)
				// space exists
				do
					spaceloc = strsearch(part, " ", Inf, 1)
					part = part[0, spaceloc]
					if (strlen(part) <= 0)
						break
					endif
					widthPix = FontSizeStringWidth(font, fsPix, 0, part)
					if (widthPix <= width)
						break
					else
						part = part[0, spaceloc-1]
					endif
				while (1)
				if (strlen(part) > 0)
					Variable slength = strlen(part)
					part[slength-1, slength-1] = "\r"
					newStr += part
					strStart += slength
					strEnd = strStart + length
				endif
			else
				// figure out what to do if no space exists
				// how to shorten the text part
			endif
		
		endif
		
//		Variable spaceloc = strsearch(part, " ", Inf, 1)
//		if (spaceLoc >= 0)
//			part[spaceloc, spaceloc] = "\r"
//			strStart += spaceloc + 1
//			newStr += part[0, spaceloc] 
//		else
//			strStart += length + 1
//			newStr += part
//		endif
//	endfor
	while (strStart < tLength)
		
	return newStr
End


// Extract the top or parent window name from a window name string
Function /S GUI_getParentWindowName(fullWinName, [top])
	String fullWinName
	Variable top
	
	if (ParamIsDefault(top) || top <= 0)
		top = 0
	else
		top = top > 1 ? 1 : round(top)
	endif
	
	String parent = ""
	if (strlen(fullWinName) == 0)
		return parent
	endif
	
	Variable lHashLoc = 0
	if (top)
		lHashLoc = strsearch(fullWinName, "#", 0)
	else
		lHashLoc = strsearch(fullWinName, "#", strlen(fullWinName) - 1, 1)
	endif
	if (lHashLoc > 0)
		parent = fullWinName[0, lHashLoc - 1]
	else
		parent = fullWinName
	endif
	
	return parent
End


// returns the current screen coordinates in pixels
Function/S GUI_getScreenPxCoordinates()

	String theInfo = IgorInfo(0)
	String theScreen = ""
	
#if defined(MACINTOSH)
	theScreen = StringByKey("SCREEN1", theInfo)
	theScreen = theScreen[strsearch(theScreen, ",", 0) + 1, strlen(theScreen)]
	theScreen = theScreen[(strsearch(theScreen, "=",0) + 1), strlen(theScreen)]
	theScreen = ReplaceString(",", theScreen, ";")
#else
	GetWindow kwFrameInner  wsizeDC
	sprintf theScreen, "%g;%g;%g;%g;", V_left, V_top, V_right, V_bottom
#endif
	return theScreen
End




// ***********************************************************************************************
// ************            P A R A M E T E R    I N P U T    P A N E L     
// ***********************************************************************************************

Function /S MakeParameterInputPanel(params, [name, tWidth, new, check, extra, colCheckList, lbFunc, equalCols])
	Wave /T params
	Variable tWidth, check, new, equalCols
	String name, colCheckList, lbFunc, extra
	
	String errormsg = getErrorStr("MakeParameterInputPanel")
	String warnmsg = getWarnStr("MakeParameterInputPanel")
	String result = ""
	Variable index = 0
	
	if (ParamIsDefault(lbFunc))
		lbFunc = ""
	else
		String funcList = FunctionList("*", ";", "SUBTYPE:ListboxControl")
		if (WhichListItem(lbFunc, funcList) < 0)
			lbFunc = ""
		endif
	endif
	
	String uD = ""
	Variable interval = -1
	Variable length = -1
	if (!ParamIsDefault(extra) && strlen(extra) > 0)
		interval = str2num(StringFromList(0, extra))
		length = str2num(StringFromList(1,extra))
		
		if (numtype(interval) == 0)
			uD = AddListItem(num2str(interval), uD, ";", Inf)
		endif
		if (numtype(length) == 0)
			uD = AddListItem(num2str(length), uD, ";", Inf)
		endif
	endif
	
	if (!WaveExists(params) || DimSize(params, 1) == 0)
		printf errormsg, "the parameter wave either does not exist or has the wrong dimension"
		return result
	endif
	
	if (ParamIsDefault(colCheckList) || strlen(colCheckList) == 0)
		colCheckList = ""
	endif	
	Variable numCheckCols = ItemsInList(colCheckList)	

	tWidth = ParamIsDefault(tWidth) || tWidth < 300 ? 300 : tWidth
	new = ParamIsDefault(new) || new < 1 ? 0 : 1
	check = ParamIsDefault(check) || check < 1 ? 0 : 1
	equalCols = ParamIsDefault(equalCols) || equalCols < 1 ? 0 : 1

	String panelName = ksGUI_parameterPanelName
	if (!ParamIsDefault(name) && strlen(name) > 0)
		panelName = name
	endif

	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPIPFolder()
	
	STRUCT PanelGeo panel
	
	Variable borderSize = 15	
	Variable panelOffset = 50
	
	String structStr = ""
	
	Variable rows = DimSize(params, 0)
	Variable columns = DimSize(params, 1)

	DoWindow /F $panelName

	if (!V_flag)
		panel.name = panelName

		Make /N=(rows, columns, 2) /O package:$ksGUI_parameterSelection /WAVE=sel
		SetDimLabel 2, 1, backColors, sel
		sel = 0
		if (!check)
			sel[][][0] = (sel | 0x02)			// setting bit by or-ing with bit 2
		endif
		if (numCheckCols > 0)
			for (index = 0; index < numCheckCols; index += 1)
				Variable column = str2num(StringFromList(index, colCheckList))
				if (column < columns)
					sel[][column][0] = 0
				endif
			endfor
		endif
		sel[][0][%backColors] = 13
		
		setPStructWinSize(panel, panelOffset, panelOffset, panelOffset + tWidth + 15 + borderSize, panelOffset + 300, pixels = 1)
		
		NewPanel /W=(panel.loc.left, panel.loc.top, panel.loc.right, panel.loc.bottom) /K=2 /N=$panelName as "Parameter Input Panel"
		SetVariable templateName, win=$panelName, font=$ksGUI_DefPanelFont, fsize=kGUI_DefFontSize
		SetVariable templateName, win=$panelName, limits={0,0,0}, title="template", value=_STR:""
		
		PopupMenu templateOptions, win=$panelName, font=$ksGUI_DefPanelFont, fsize=kGUI_DefFontSize
		PopupMenu templateOptions, win=$panelName, mode=0, size={10,20}, value=theTemplateList()
		PopupMenu templateOptions, win=$panelName, proc=popFunc_setTemplate
		
		Button saveButton, win=$panelName, font=$ksGUI_DefPanelFont, fsize=kGUI_DefFontSize
		Button saveButton, win=$panelName, title="save", proc= btnFunc_saveButton
		
		ListBox parameters, win=$panelName, mode=5, editStyle=1
		ListBox parameters, win=$panelName, font=$ksGUI_DefPanelFont, fsize=kGUI_DefFontSize, frame=2
		ListBox parameters, win=$panelName, colorWave= $(getColorValueWave())
		if (equalCols)
			ListBox parameters, win=$panelName, widths={25, 25, 25, 25, 25, 25, 25}
		else
			ListBox parameters, win=$panelName, widths={20, 50, 20, 20, 20, 20, 30}
		endif
		ListBox parameters, win=$panelName, listwave=params, selWave=sel
		if (strlen(uD) > 0)
			ListBox parameters, win=$panelName, userdata=uD
		endif
		if (strlen(lbFunc) > 0)
			ListBox parameters, win=$panelName, proc=$lbFunc
		endif
		
		Button okButton, win=$panelName, font=$ksGUI_DefPanelFont, fsize=kGUI_DefFontSize + 2, fStyle = 1
		Button okButton, win=$panelName, title="Ok", proc=btnFunc_okButton

		StructPut /S panel, structStr
		SetWindow $panel.name userdata = structStr
		SetWindow $panel.name hook(resize) = resizePIP
	endif

	StructGet /S panel, GetUserData(panelName, "", "")
	
	Wave /T params = package:parameterValues	
	
	Variable fontSize = kGUI_DefFontSize * (ScreenResolution/PanelResolution(panelName))
	Variable rowHeight = FontSizeHeight(ksGUI_DefPanelFont, fontSize, 0)
	Variable tableHeight = (rows + 1) * (rowHeight + 4)
	Variable topOffSet = borderSize
	Variable objectWidth = 0, objectHeight = 0, buttonLeft = 0	
	
	objectHeight = 20
	objectWidth = tWidth - 35 - 40
	SetVariable templateName, win=$panelName, size={objectWidth, objectHeight}, pos={borderSize, topOffSet + 3}
	
	PopupMenu templateOptions, win=$panelName, pos={borderSize + objectWidth + 3, topOffSet}
	
	objectWidth = 40
	buttonLeft = tWidth + borderSize - objectWidth
	Button saveButton, win=$panelName, size={objectWidth, objectHeight}, pos={buttonLeft, topOffSet}
	topOffSet += objectHeight + 5
	
	ListBox parameters, win=$panelName, size={tWidth, tableHeight}, pos={borderSize, topOffSet}

	objectWidth = 80
	objectHeight = 20
	buttonLeft = ((tWidth + (2 *  borderSize)) / 2) - (objectWidth / 2)
	topOffSet += tableHeight + 15	
	Button okButton, win=$panelName, size={objectWidth, objectHeight}, pos={buttonLeft, topOffSet}
	topOffSet += objectHeight
	
	setPStructWinSize(panel, -1, -1, -1, panel.loc.top + topOffset + borderSize, pixels = 1)
	panel.minSize = panel.wSize

	StructPut /S panel, structStr
	SetWindow $panel.name userdata = structStr

	MoveWindow /W=$panel.name panel.win.left, panel.win.top, panel.win.right, panel.win.bottom
	moveWindowOnScreen(panel.name)
	GetWindow $panel.name wsize

	setPStructWinSize(panel, V_left, V_top, V_right, V_bottom)	
	StructPut /S panel, structStr
	SetWindow $panel.name userdata = structStr
	
	PauseForUser $panelName
	return panelName
End

// OK BUTTON action function
Function btnFunc_okButton(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch(bn.eventCode)
		case 2:
			KillWindow $bn.win
			break
	endswitch

End

// SAVE BUTTON action function
Function btnFunc_saveButton(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch(bn.eventCode)
		case 2:
			ControlInfo /W=$bn.win templateName
			String tN = ReplaceString(" ", S_Value, "_")
			if (CheckName(tN, 1) != 0)
				tN = CleanupName(tN, 1)		// ensure that it is valid
				tN = UniqueName(tN, 1, 1) 	// ensure that it is unique
			endif
			if (strlen(tN) > 0)
				Wave /T params = GH_getParameterWave()
				DFREF package = getPIPFolder()
				Duplicate /O params, package:$tN
			endif
			break
	endswitch
End

// POPUPMENU to choose and set the template
Function popFunc_setTemplate(pm) : PopupMenuControl
	STRUCT WMPopupAction &pm
	
	switch (pm.eventCode)
		case 2:
			DFREF pip = getPIPFolder()
			Wave params = pip:parameterValues
			String theNote = note(params)
			Wave /T template = pip:$(pm.popStr)
			Duplicate /O template, params
			Note /K template, theNote
			SetVariable templateName, win=$(pm.win), value=_STR:(pm.popStr)
			break
	endswitch

End

// LISTBOX action function, currently not used
Function lbFunc_episodeChange(lb) : ListboxControl
	STRUCT WMListboxAction &lb
	
	switch(lb.eventCode)
		case 7:
			Wave /T lw = lb.listWave
			if (lb.col == 2)		// episode_in column
				Variable num = str2num(lw[lb.row][lb.col])
				if (strlen(lw[lb.row][lb.col + 1]) == 0 || str2num(lw[lb.row][lb.col + 1]) < num)
					Variable interval = str2num(StringFromList(0, lb.userdata))
					Variable length = str2num(StringFromList(1, lb.userdata))
					if (numtype(interval) == 0)			
						lw[lb.row][lb.col + 1] = num2str(interval / 60 * (num - 1))
					endif
					if (numtype(length) == 0)
						lw[lb.row][lb.col + 2] = num2str(interval / 60 * (length - str2num(lw[lb.row][lb.col])))
					endif
				endif
			endif
			
			break
	endswitch
	
End

// custom function to create a list of templates for the popup menu
Function /S theTemplateList()
	String list = ""
	DFREF cDF = GetDatafolderDFR()
	DFREF pip = getPIPFolder()
	Wave /T params = pip:parameterValues
	String options = "MINROWS:%g,MAXROWS:%g,MINCOLS:%g,MAXCOLS:%g"
	sprintf options, options, DimSize(params, 0), DimSize(params, 0), DimSize(params, 1), DimSize(params, 1)
	SetDatafolder pip
	list = WaveList("*", ";", options)
	SetDatafolder cDF
	list = RemoveFromList("parameterValues", list)
	list = RemoveFromList("parameterSel", list)
	return list
End

// WINDOW HOOK function to monitor resizing of the panel.
Function resizePIP(ws)
	STRUCT WMWinHookStruct &ws
	
	Variable hookResult = 0
	
	switch (ws.eventCode)
		case 2:
			SetWindow $ws.winName hook(resize)=$""
			break
		case 6:		
			String structStr = ""
			STRUCT PanelGeo panel
			StructGet /S panel, GetUserData(ws.winName, "", "")
			
			if (ws.winRect.right > panel.minSize.h)				
				setPStructWinSize(panel, -1, -1, panel.win.left + ws.winRect.right, panel.win.top + panel.minSize.v)
			else
				setPStructWinSize(panel, -1, -1, panel.win.left + panel.minSize.h, panel.win.top + panel.minSize.v)
			endif
				
			Variable rightSide = 0
			Variable topOffSet = 0
			
			ControlInfo /W=$ws.winName saveButton
			Variable buttonWidth = V_Width
			Variable bTop = V_Top

			ControlInfo /W=$ws.winName parameters
			Variable borderSize = V_left
			Variable tWidth = panel.size.h - (2 * borderSize)
			Variable increase = tWidth - V_Width
			Listbox parameters, win=$ws.winName, pos={borderSize, V_top}, size={tWidth, V_Height}
			Button saveButton, win=$ws.winName, pos={tWidth - buttonWidth + borderSize, bTop}

			ControlInfo/W=$ws.winName templateOptions
			PopupMenu templateOptions, win=$ws.winName, pos={tWidth - buttonWidth - 10 - V_Width + borderSize, V_Top}, size={V_Width, V_Height}
			ControlInfo /W=$ws.winName templateName
			SetVariable templateName, win=$ws.winName, size={V_Width + increase, V_Height}, pos={V_Left, V_Top}

			ControlInfo /W=$ws.winName okButton
			Variable buttonLeft = ((tWidth + (2 * borderSize)) / 2 ) - (V_Width / 2)
			Button okButton, win=$ws.winName, pos={buttonLeft, V_top}, size={V_Width, V_Height}
			
			MoveWindow /W=$ws.winName panel.win.left, panel.win.top, panel.win.right, panel.win.bottom
			StructPut /S panel, structStr
			SetWindow $ws.winName userdata = structStr			
			break
	endswitch
	
	return hookResult
End

Function /WAVE GH_getParameterWave([rows, columns, columnTitles])
	Variable rows, columns
	String columnTitles
	
	Variable new = ParamIsDefault(rows) && ParamIsDefault(columns) ? 0 : 1
	rows = ParamIsDefault(rows) || rows < 1 ? 1 : rows
	columns = ParamIsDefault(columns) || columns < 1 ? 1 : columns
	
	if (ParamIsDefault(columnTitles))
		columnTitles = ""
	endif
	
	Variable index = 0
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPIPFolder()
	
	Wave /T params = package:$ksGUI_parameterValueName
	if (new)
		Make /N=(rows, columns) /T /O package:$ksGUI_parameterValueName /WAVE=params
		params = ""
		for (index = 0; index < columns; index += 1)
			SetDimLabel 1, index, $(StringFromList(index, columnTitles)), params	
		endfor
		
		if (cmpstr(LowerStr((GetDimLabel(params, 1, 0))[0,6]), "channel") == 0)
			params[][0] = num2str(p + 1)
		endif
	endif
	
	return params
End
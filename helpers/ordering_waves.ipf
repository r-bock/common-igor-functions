#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6
#pragma version = 1.5
// version 1.5:		August 2010; updated to use Igor 6 features
// version 1.2:		September 2002; included a function to determine the base part of a wave
//		name, until a certain separator like "-" or "_". If the separator is "-" the CorrectWaveNumbers
//		function will automatically call the ChangeMinusToUScore function and substitute the signs for
//		better wave name handling.
// version 1.1:		August 2002; updated CorrectWaveNumbers to take a freely definable separator
//		for the wave name to make it more flexible for other applications.
//by Roland Bock, 1999

//******************************

//This function is designed to sort a semicolon separated list of PULSE 
//wavenames "LLLL#_#_#" according to their name.  The sorted list is 
//returned. It requires an Igor version that can deal with text waves, 
//since a text is used to sort the names. 

function/S rightorder(waveliste)
	string waveliste
	
	string theWave, theWave2, debugglogtext
	string sortedlist = ""
	variable index
	make/O/N=0/T WaveNameStorage
	
	do
		theWave = StringFromList(index, waveliste)	
		if (strlen(theWave) == 0)
			break
		endif
		InsertPoints index, 1, WaveNameStorage		 
		WaveNameStorage[index] = theWave
		index += 1
	while(1)
	Sort WaveNameStorage WaveNameStorage
	index = 0
	do
		sortedlist += WaveNameStorage[index] + ";"
		index += 1
	while(index < numpnts(WaveNameStorage))

	killwaves WaveNameStorage
	return sortedlist	
end

//******************************
//This function returns the sweep number of a PULSE wave. The PULSE wavename
//matches the pattern "L_#_#_#", where "L" is the symbol for an abitratry 
//number of letters, "#" the symbol for a number which are separated by an 
//underscore.

function FindSweepNumber(theWave)
	string theWave
	
	variable ni1, ni2, swn
	ni2 = strlen(theWave)
	ni1 = strsearch(theWave, "_", 0)
	ni1 = strsearch(theWave, "_", ni1+1)
	swn = str2num(theWave[ni1+1,ni2-1])

	return swn
end

//******************************
//Does the same as the function above, only returns the series number
//of a PULSE wave. (see description of "FindSweepNumber".

function FindSeriesNumber(theWave)
	string theWave
	
	variable ni1, ni2, swn
	ni1 = strsearch(theWave, "_", 0)
	ni2 = strsearch(theWave, "_", ni1+1)
	swn = str2num(theWave[ni1+1,ni2])

	return swn
end

//******************************
// The following two functions correct wave names according to their number.
// They assume that the ordering number is at the end and a distinct separator 
// separates them from the base name of the wave (i.e. basename-1, basename-2, where
// basename would be the selector to create the list of waves, "-" the separator providing
// the distinct separation between the base name and the wave number. 
// The function will enter "00" for number smaller than 10 and "0" for numbers smaller than
// 100.
Function CorrectWaveNumbers(selector, separator)
	String selector, separator
	
	String theList = WaveList(selector, ";", "")		// generate semicolon separated list
	
	Variable /G NameFlag									// make sure it is not run twice
	if (NameFlag == 0)
		String theName, NewName
		Variable index = 0
		
		do
			theName = StringFromList(index, theList)
			if (strlen(theName) == 0)
				break
			endif
			if (strsearch (theName, separator, 0) > -1)
				NewName = AdjustWaveNameNumber(theName, separator)
				Rename $theName, $NewName					// rename wave with adjusted name
			endif
			index += 1
		while(1)	
		NameFlag += 1
	endif
	if (cmpstr(separator, "-") == 0)				// get rid of minus signs in wave names
		ChangeMinusToUScore(selector)
		SVAR identifier
		identifier[strlen(identifier) - 1, strlen(identifier)] = "_"
	endif
	return 1										//check if everything went alright
end

function/S AdjustWaveNameNumber(theName, separator)
	string theName, separator
	
	variable length = strlen(theName)
	variable finder, number, index = 0
	
	finder = length - 1
	do
		if (cmpstr(theName[finder], separator) == 0)
			break
		endif		
		finder -= 1	
	while(1)
		
	finder += 1
	number = str2num(theName[finder, length])
	if (number < 10)
		theName[length - 1] = "00"
	else
		if (number < 100)
			theName[length - 2] = "0"
		endif
	endif
	return theName
end

//******************************
// as the name suggest, convert the minus sign in wave names to an underscore
Function ChangeMinusToUScore(identifier)
	String identifier
	
	String List = WaveList(identifier, ";", "")
	String theWave, theNewWave
	Variable index = 0
	Variable index2 = 0
	Variable pos
	
	for (index = 0; index < ItemsInList(List); index += 1)
		theWave = StringFromList(index, List)
		pos = strsearch(theWave,"-",0)
		do
			if (pos > -1)
				theNewWave = theWave
				theNewWave[pos, pos] = "_"
				pos = strsearch(theNewWave, "-", pos)
				Rename $theWave, $theNewWave
			endif
		while (pos > -1)
	endfor
End

//******************************
// This function returns the base part of a wave. The base name is considered to be the 
// first part until the separator. The output is stored in a global string to use in other
// procedures.
Function/S DetermineBaseWaveName(theWave, separator)
	String theWave, separator
	
	Variable position
	String /G identifier
	position = strsearch(theWave, separator, 0)
	identifier = theWave[0, position]
	return theWave[0, position]
End	

#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.3
#pragma version = 1.1
#pragma ModuleName = PlotHelper
#include "ListHelpers", version >= 4.5

// by Roland Bock 
// * version 1.1 (Sep 2019): automatically open help file of same name.
// * version 1 (Jan 2017)

#if Exists("OpenHelp") != 0	// only for Igor versions 7 and higher
static Function AfterCompiledHook()
	loadHelp()
	return 0
End

static Function loadHelp()
	String fPath = FunctionPath("")	
	
	if (cmpstr(fPath[0], ":") == 0)
		// either buildin procedure window or packed procedure or procedures not compiled
		return -1
	endif
	
	String fName = ParseFilePath(0, fPath, ":", 1, 0)
	String hName = ReplaceString(".ipf", fName, ".ihf")
	String wList = WinList(hName, ";", "WIN:512")

	if (ItemsInList(wList) > 0)
		// help file already open, nothing more to do
		return 0
	endif

	String path = ParseFilePath(1, fPath, ":", 1, 0)
	
	// create full path to help file, which should have the same file name, but a different
	// 	file ending (.ihf instead of .ipf)
	String help = path + hName
	OpenHelp /V=0/Z help
	return V_flag
End
#endif


static Strconstant ksErrorPattern = "** ERROR (%s): %s.\r"
static Strconstant ksWarnPattern = "-- WARNING (%s): %s.\r"

static Strconstant ksAvailableSorts = "none;mean;min;max"

static Strconstant ksPlotName = "LasangePlot"
static Strconstant ksPlotTitle = "Lasange Plot"

static Strconstant ksPackageName = "PlotHelpers"
static Strconstant ksMeanName = "PH_lasange_mean"


Menu "Macros"
	Submenu "Plots"
		"Lasange Plot from top graph", GetWavesForLP(ask=1)
		"Lasange Plot with grouped subjects", SubjectWaves2LP(ask=1)
	End
End

// ********************************************
// *********         S T A T I C         H E L P E R S      *********    
// ********************************************

// get the feedback pattern for the error message (type = 1) or warning message (type = 2)
static Function /S getMsg(func, type)
	String func
	Variable type
	
	if (strlen(func) == 0)
		return ""
	endif
	String msg = ""
	switch (type)
		case 2:
			sprintf msg, ksWarnPattern, func, "%s"
			break
		case 1:
		default:
			sprintf msg, ksErrorPattern, func, "%s"
	endswitch
	return msg
End

static Function /DF getPackage()
	DFREF cDF = GetDatafolderDFR()
	NewDatafolder /O/S Packages
	NewDatafolder /O /S $ksPackageName
	DFREF package = GetDatafolderDFR()
	SetDatafolder cDF
	return package
End

static Function /DF getFolder()
	DFREF cDF = GetDatafolderDFR()
	NewDatafolder /O/S Summaries
	NewDatafolder/O/S $ksPackageName
	DFREF folder = GetDatafolderDFR()
	SetDatafolder cDF
	return folder
End

static Function /S getSummaryVar(name, type, [text, value, new])
	String name, text
	Variable type, new, value
	
	type = type < 2 ? 1 : 2
	value = ParamIsDefault(value) ? 0 : value
	new = ParamIsDefault(new) || new < 1 ? 0 : 1
	
	if (ParamIsDefault(text) || strlen(text) == 0)
		text = ""
	endif
	
	DFREF loc = getFolder()
	
	switch (type)
		case 2:
			SVAR str = loc:$name
			if (!SVAR_Exists(str))
				String /G loc:$name = text
			elseif (new)
				str = text
			endif
			break
		case 1:
		default:
			NVAR var = loc:$name
			if (!NVAR_Exists(var))
				Variable /G loc:$name = value
			elseif (new)
				var = value
			endif
			break
	endswitch		
	
	return GetDatafolder(1, loc) + name
End

static Function /S getMeanVar()

	return getSummaryVar(ksMeanName, 1)
End

// assumes same scaling and units of all waves
static Function /S LasangePlot(waves, [data, plot, title])
	Wave /Wave waves
	String data, plot, title
	
	if (ParamIsDefault(data) || strlen(data) == 0)
		data = "lasangeData"
	endif
	if (ParamIsDefault(plot) || strlen(plot) == 0)
		plot = ksPlotName
	endif
	if (ParamIsDefault(title) || strlen(title) == 0)
		if (cmpstr(plot, ksPlotName) == 0)
			title = ksPlotTitle
		else
			title = ReplaceString("_", plot, " ")		
		endif
	endif
	
	Variable numWaves = numpnts(waves)
	Variable theMin = 0, theMax = 0, maxPoints = 0, index = 0
	
	for (index = 0; index < numWaves; index += 1)
		Wave tmp = waves[index]
		maxPoints = max(maxPoints, numpnts(tmp))
		theMin = min(theMin, WaveMin(tmp))
		theMax = max(theMax, WaveMax(tmp))
	endfor
	
	DFREF loc = getFolder()
	
	Make /N=(maxPoints, numWaves) /O loc:$data /WAVE=lasange
	lasange = Nan
	SetScale /P x 1, 1, "session", lasange
	SetScale /P y 1, 1, "trace", lasange
	
	NVAR totMean = $(getMeanVar())
	Make /FREE /N=(numWaves) /O meanCollector
	meanCollector = mean(waves[p])
	totMean = mean(meanCollector)
	
	Variable rangeUnit = abs(theMax - theMin) / 256
	Variable offset = rangeUnit * 15
	
//	Variable maxVal = max(abs(theMax - totMean), abs(totMean - theMin)) + totMean
	
	for (index = 0; index < numWaves; index += 1)
		Wave tmp = waves[index]
		lasange[][index] = tmp[p]
	endfor
	
	DoWindow /F $plot
	if (V_flag == 0)
		Display /W=(40,44,750,318) /N=$plot
		AppendImage /W=$plot lasange
		ModifyGraph /W=$plot margin(right) = 108, mirror = 2, minor = 1, standoff = 0
		ModifyGraph /W=$plot minor(left) = 0
		ModifyImage /W=$plot $NameOfWave(lasange) ctab={*,theMax + offset,Gold,0}
		ColorScale/C/N=cScale/F=0/A=RC/X=2.00/Y=0.00/E=2 image=lasangeData
	endif

//	ModifyImage /W=$plot $NameOfWave(lasange) ctab={-maxVal,maxVal,Geo,0}

	return plot
End

Function /S GetWavesForLP([theList, sortKey, ask, name, title])
	String theList, sortKey, name, title
	Variable ask
	
	ask = ParamIsDefault(ask) || ask < 1 ? 0 : 1
	
	String errormsg = getMsg("GetWavesForLP", 1)
	String warnmsg = getMsg("GetWavesForLP", 2)
	String feedbackPattern1 = "--   choosing GetWavesForLP(sortKey=\"%s\") on %s %s\r"
	String feedbackPattern2 = "--   choosing GetWavesForLP(theList=\"%s\", sortKey=\"%s\") on %s %s\r"	
	String feedbackPattern3 = "--     using from %s traces\r%s\r"
	String feedbackPattern4 = "--     using waves %s\r"
	String result = ""
	
	Variable fromGraph = 0

	if (ParamIsDefault(name) || strlen(name) == 0)
		name = ""
	endif
	if (ParamIsDefault(title) || strlen(title) == 0)
		title = ""
	endif

	if (ParamIsDefault(theList) || strlen(theList) == 0)
		// we don't have a list, so look for the top graph
		String theGraph = StringFromList(0, RemoveFromList(ksPlotName, WinList("*", ";", "WIN:1")))
		if (strlen(theGraph) == 0) // no graphs open, so nothing more to do
			return result
		endif
		theList = TraceNameList(theGraph, ";", 5)
		fromGraph = 1
	endif
	if (ParamIsDefault(sortKey) || strlen(sortKey) == 0)
		sortKey = "none"
		if (ask)
			Prompt sortKey, "Choose sorting", popup, ksAvailableSorts
			DoPrompt "Choose a sort method", sortKey
			
			if (V_flag)
				// user clicked cancel, so stop here
				return result
			endif
		endif
		printf feedbackPattern1, sortKey, date(), time()
	endif
	
	Variable numWaves = ItemsInList(theList)
	if (numWaves == 0)
		return result
	endif
	
	Make /FREE /N=(numWaves) /O /WAVE waves
	
	Variable index
	
	// 1. create a wave of wave references from the list 
	if (fromGraph)
		DFREF loc = getFolder()
		waves = TraceNameToWaveRef(theGraph, StringFromList(p, theList))
		Make /T /N=(numWaves) /O loc:fullWaveNames = GetWavesDatafolder(waves[p], 2)
		printf feedbackPattern3, theGraph, ReplaceString(";", ReplaceString("root", makeListFromWave(GetDatafolder(1,loc) + "fullWaveNames"), "         "), "\r")
		
		KillWaves /Z loc:fullWaveNames
	else
		String missingWaves = ""
		Variable counter = -1
		for (index = 0; index < numWaves; index += 1)
			String wName = StringFromList(index, theList)
			Wave tmp = $wName
			if (!WaveExists(tmp))
				missingWaves += "     " + wName + "\r"
				continue
			else
				counter += 1
				waves[counter] = tmp
			endif
		endfor
		
		if (counter < 0)
			// none of the waves existed, so stop here
			printf errormsg, "NONE of the listed waves exist! Check names and path!"
			return result
		elseif (counter < numWaves - 1)
			printf warnmsg, "some waves did not exist! Check names and path of the following list"
			printf "--- missing waves list:\r%s\r", missingWaves
			
			DeletePoints counter + 1, numWaves, waves
		endif
		
	endif
	
	// 2. figure out the sorting and create the appropriate sort key wave
	if (cmpstr(sortKey, "none") != 0)
		numWaves = numpnts(waves)
		Make /FREE /N=(numWaves) sorter
		
		strswitch (LowerStr(sortKey))
			case "mean":
			case "average":
			case "avg":
				sorter = mean(waves[p])
				break
			case "max":
				sorter = WaveMax(waves[p])
				break
			case "min":
				sorter = WaveMin(waves[p])
				break
		endswitch
	endif

	if (WaveExists(sorter))
		Sort sorter, waves
	endif
	
	result = LasangePlot(waves, plot = name, title = title)
	return result
End

// uses the enclosing data folder as a label
Function /S SubjectWaves2LP([theList, sortWave, sortKey, ask])
	String theList, sortWave, sortKey
	Variable ask

	String feedbackPattern = "--> using SubjectWaves2LP(theList = \"%s\", sortWave = \"%s\", sortKey = \"%s\", ask = %d)\r"
	String errormsg = getMsg("SubjectWaves2LP", 1)
	String warnmsg = getMsg("SubjectWaves2LP", 2)

	String theGraph = ""
	Variable index = 0
	
	ask = ParamIsDefault(ask) || ask < 1 ? 0 : 1
	if (ParamIsDefault(theList))
		theList = ""
	endif
	if (ParamIsDefault(sortKey) || strlen(sortKey) == 0)
		sortKey = "none"
	endif
	if (ParamIsDefault(sortWave)) 
		sortWave = ""
	endif
	

	String catWaveList = WaveList("*", ";", "TEXT:1")
	Variable numTexts = ItemsInList(catWaveList)
	
	if (numTexts == 0)
		// no category waves -> nothing more to do
		printf errormsg, "no category waves available, nothing more to do. Create some category waves first!"
		return theGraph
	else
		catWaveList = getLabeledTextWaves(catWaveList)
	endif
	
	if (ask)
		Prompt sortWave, "Select category wave", popup, catWaveList
		Prompt sortKey, "Select sort method", popup, ksAvailableSorts
	
		DoPrompt "Choose Category Sort", sortWave, sortKey
		if (V_flag)	// user canceled
			print "-- user canceled procedure. "
			return theGraph
		endif
	endif

	Wave /T catSort = $sortWave
	if (!WaveExists(catSort))
		printf errormsg, "the given wave \"" + sortWave + "\" does not exist"
		return theGraph
	endif

	printf feedbackPattern, theList, sortWave, sortKey, ask

	if (strlen(theList) == 0)
		String graphSrc = StringFromList(0, RemoveFromList(ksPlotName, WinList("*", ";", "WIN:1")))
		if (strlen(graphSrc) == 0) // no graphs open, so nothing more to do
			return theGraph
		endif
		theList = TraceNameList(graphSrc, ";", 5)
	endif
	
	Variable numWaves = ItemsInList(theList)

	Make /N=(numWaves) /O /FREE /WAVE waves = TraceNameToWaveRef(graphSrc, StringFromList(p, theList))
	Make /T /N=(numWaves) /O /FREE subjectNames = GetWavesDatafolder(waves[p], 0)
			
	Make /N=(numWaves) /O /T /FREE groups = catSort[%$subjectNames[p]]
	String groupLabels = makeListFromWave(GetWavesDatafolder(catSort, 2), sorted = 1, unique = 1)
	Variable numGroups = ItemsInList(groupLabels)

	Make /N=(numGroups) /WAVE /O /FREE groupings
	Make /N=0 /O /FREE emptyGroups
	
	for (index = 0; index < numGroups; index += 1)
		Extract /FREE /O /INDX groups, tmp, cmpstr(StringFromList(index, groupLabels), groups[p]) == 0
		
		if (numpnts(tmp) == 0)
			emptyGroups[numpnts(emptyGroups)] = {index}
			continue
		endif
		
		if (cmpstr(sortKey, "none") != 0)
			Variable groupSize = numpnts(tmp)
			Make /FREE /N=(groupSize) sorter
			strswitch (LowerStr(sortKey))
				case "mean":
				case "average":
				case "avg":
					sorter = mean(waves[tmp[p]])
					break
				case "max":
					sorter = WaveMax(waves[tmp[p]])
					break
				case "min":
					sorter = WaveMin(waves[tmp[p]])
					break
			endswitch
		endif
		
		if (WaveExists(sorter))
			Sort sorter, tmp
		endif

		groupings[index] = tmp
	endfor
	
	if (numpnts(emptyGroups) > 0)					// now clean up the empty groups
		for (index = numpnts(emptyGroups) - 1; index >= 0; index -= 1)
			groupLabels = RemoveListItem(emptyGroups[index], groupLabels)
			DeletePoints emptyGroups[index], 1, groupings
		endfor
		
		numGroups = ItemsInList(groupLabels)
	endif
	
	Make /N=(numWaves) /WAVE /O /FREE groupedWaves
	Make /N=(numGroups) /O /FREE divisions
	Variable start = 0, toEnd = -1

	for (index = 0; index < numGroups; index += 1)
		Wave tmp = groupings[index]
		toEnd += numpnts(tmp)
		groupedWaves[start, toEnd] = waves[tmp[p - start]]
		start = toEnd + 1
		divisions[index] = start
	endfor
		
	theGraph = LasangePlot(groupedWaves)
	
	DFREF loc = getFolder()
	Make /N=(numWaves) /O loc:subjectLbls_Loc /WAVE=locs
	locs = p + 1
	Make /T /N=(numWaves) /O loc:subjectLbls /WAVE=lbls
	lbls = GetWavesDatafolder(groupedWaves[p], 0)
	lbls = (lbls[p])[strsearch(lbls[p], "_", strlen(lbls[p]), 1) + 1, strlen(lbls[p])]
	
	ModifyGraph /W=$theGraph userticks(left)={locs, lbls}
	ModifyGraph /W=$theGraph manTick(bottom)={0,2,0,0}, manMinor(bottom)={1,0}
	
	if (numGroups > 1)
		Redimension /N=(numpnts(divisions) - 1), divisions
		SetDrawLayer /K UserFront
		for (index = 0; index < numpnts(divisions); index += 1)
			SetDrawEnv ycoord=left, linethick= 2.00, linefgc= (2,39321,1)
			DrawLine 0, divisions[index] + 0.5, 1, divisions[index] + 0.5
		endfor
	endif

	return theGraph
end
#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6
#pragma version = 3.0
#pragma hide = 1


// Written by Roland Bock
// ** version 3.0 (September 2019): added a function for Igor > v7 to automatically open the 
//                                  corresponding help file, if this file compiles.
// ** version 2.9 (August 2019): added the PanelResolution function to ensure correct
//									display of the panels in higher Windows OS, but maintain
//									compatibility with Igor 6, also raised minimum Igor version 
//									to version 6 (nobody should use Igor 5 anymore)
// ** version 2.8 (September 2015): modernized slideWindow function
// ** version 2.6 (April 2014): added adjustPanel size function 
// ** version 2.5 (October 2013)
// ** version 2.4 (January 2013)
//

#if Exists("PanelResolution") != 3
static Function PanelResolution(wName)		// compatibility with Igor 7
	String wName
	return 72
End
#endif

#if Exists("OpenHelp") == 4	// only for Igor versions 7 and higher
static Function AfterCompiledHook()
	loadHelp()
	return 0
End

static Function loadHelp()
	String fPath = FunctionPath("")	
	
	if (cmpstr(fPath[0], ":") == 0)
		// either buildin procedure window or packed procedure or procedures not compiled
		return -1
	endif
	
	String fName = ParseFilePath(0, fPath, ":", 1, 0)
	String hName = ReplaceString(".ipf", fName, ".ihf")
	String wList = WinList(hName, ";", "WIN:512")

	if (ItemsInList(wList) > 0)
		// help file already open, nothing more to do
		return 0
	endif

	String path = ParseFilePath(1, fPath, ":", 1, 0)
	
	// create full path to help file, which should have the same file name, but a different
	// 	file ending (.ihf instead of .ipf)
	String help = path + hName
	OpenHelp /V=0/Z help
	return V_flag
End
#endif



// *******************************************************
// ****************        C O N S T A N T S          ********************
// *******************************************************
static strConstant kSysWin = "Windows"
static strConstant kSysMac = "Macintosh"

static Strconstant ksDefProgressBarName = "WH_ProgressBarPanel"
static Strconstant ksDefProgressBarTitle = "Progress Window"

#if cmpstr("Macintosh", IgorInfo(2)) == 0
     static Strconstant defFont = "Verdana"
#else
     static Strconstant defFont = "Arial"
#endif

static Constant defFSize = 10


// *******************************************************
// ****************          S T R U C T U R E S    ********************
// *******************************************************
Structure Screen
	STRUCT Rect size
	Variable width
	Variable height
EndStructure

//*****************************
// *****   MENUS
//*****************************
Menu "Macros"
	"-"
	"kill windows ...", SelectWindowsToKill()
	"kill all graph windows", killWinByList(WinList("*", ";", "WIN:1"))
End

//*****************************
// *****     INPUT FUNCTIONS
//*****************************

Function SelectWindowsToKill()
	
	String selector = "*"
	String theType = "Graph"
	Prompt selector, "Please enter a partial window name (with * ):"
	Prompt theType, "Please select the window type:", popup, "Graph;Table;Layout;Notebook;all"

	DoPrompt "Select Windows to Kill", selector, theType
	
	if (V_flag)
		// user clicked cancel
		return 0
	endif
	String type = ""
	
	strswitch (theType)
		case "Graph":
			type = "WIN:1"
			break
		case "Table":
			type = "WIN:2"
			break
		case "Layout":
			type = "WIN:4"
			break
		case "Notebook":
			type = "WIN:16"
			break
		case "all":
		default:
			type = ""
	endswitch
	
	killWinByList(WinList(selector, ";", type))
	return 1
End

//*****************************
//This funtion names a window and the title to to the given name
function/S RenameWin (Name)
	string Name
	
	DoWindow/C $Name									//renames target window
	DoWindow/T $Name, Name							//changes title of target window
	return Name										//returns name for further storage
end

//*****************************
//Returns 1 if the window exists, or 0 if it does not. 
function CheckWinExists(Name)
	string Name
	
	variable flag = 0
	string liste = WinList(Name, ";", "")
	string window = StringFromList(0, liste,";")
	if (strlen(window) > 0)
		flag = 1
	endif
	return flag
end

//*****************************
function/S CheckWindowExists(Name, ID)
	string Name
	variable ID
	
	string type = "WIN:" + num2str(ID)
	string Liste = ""
	Liste = WinList(Name, ";", type)
	return Liste
end

//*****************************
Function killWinByList(theList)
	String theList
	
	Variable index = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theWindow = StringFromList(index, theList)
		DoWindow /K $theWindow
	endfor
	return 1
End

//*****************************
Function /S getHostWinName(theName)
	String theName
	
	if (strlen(theName) == 0)
		return ""
	endif
	
	Variable poundLoc = strsearch(theName, "#", Inf, 1)
	if (poundLoc == -1)
		// no separator found, return full name
		return theName
	endif
	return theName[0, poundLoc - 1]
End

//******************************
function /C resizeWindowToScreen(Name, [screenFraction, ratio])
	string Name
	variable screenFraction, ratio
	
	if (ParamIsDefault(screenFraction) || screenFraction <= 0 || screenFraction > 1)
		screenFraction = 0.3
	endif
	
	Variable isWin = cmpstr(IgorInfo(2), kSysWin) == 0
	
	Variable screenWidth = 0, screenHeight = 0
	if (isWin)
		// on windows use the size of the Igor application frame instead of the actual screen size.
		GetWindow kwFrameInner wsize
		screenWidth = V_right - V_left
		screenHeight = V_bottom - V_top
	else
		STRUCT Screen theScreen
		getScreenSize(theScreen)
			
//			String theScreenInfo = StringByKey("SCREEN1", IgorInfo(0))
//			String theCoordinates = RemoveByKey("DEPTH", theScreenInfo, "=", ",")
//			theCoordinates = theCoordinates[strsearch(theCoordinates, "=", 0) + 1, strlen(theCoordinates)]
//			
//			screenWidth = str2num(StringFromList(2, theCoordinates, ","))	
//			screenHeight = str2num(StringFromList(3, theCoordinates, ","))	

		screenWidth = theScreen.width
		screenHeight = theScreen.height
	endif
	
	GetWindow $Name, wsize
	Variable theWidth = V_right - V_left
	Variable theHeight = V_bottom - V_top
	
	if (ParamIsDefault(ratio))
		ratio = theWidth / theHeight			// memorize the width to height ratio
	endif
	if (ratio > 1)
		// wider than heigh
		theWidth = screenWidth * screenFraction
		theHeight = theWidth * (1/ratio)
	else
		// heigher than or equal to width, since most screens are wider than heighter were are fairly safe not
		// scale past the screen
		theHeight = screenHeight * screenFraction
		theWidth = theHeight * ratio		
	endif

	// now check 
	if (theWidth > screenWidth)
		theWidth = screenWidth - 15
		theHeight = theWidth * ratio
	endif
	if (theHeight > screenWidth)
		theHeight = screenHeight - 15
		theWidth = theHeight * ratio
	endif
		
	MoveWindow /W=$Name V_left, V_top, (V_left + theWidth), (V_top + theHeight)
	return cmplx(theWidth, theHeight)
end


Function getScreenSize(screen, [number])
	STRUCT Screen &screen
	Variable number
	
	if (ParamIsDefault(number) || number < 1)
		number = 1
	else
		number = round(number)
	endif

	String theInfo = IgorInfo(0)
	String theScreen = StringByKey("SCREEN1", theInfo)
	theScreen = theScreen[strsearch(theScreen, ",", 0) + 1, strlen(theScreen)]
	theScreen = theScreen[(strsearch(theScreen, "=",0) + 1), strlen(theScreen)]
	theScreen = ReplaceString(",", theScreen, ";")
	
	screen.size.left = str2num(StringFromList(0, theScreen))
	screen.size.top = str2num(StringFromList(1, theScreen))
	screen.size.right = str2num(StringFromList(2, theScreen))
	screen.size.bottom = str2num(StringFromList(3, theScreen))
	
	screen.width = screen.size.right - screen.size.left
	screen.height = screen.size.bottom - screen.size.top
End

// moves the graph window to the center of the screen and expands it to half
// the screen size
Function /C moveWindowOnScreen(theName, [location, minWidth, minHeight])
	String theName, location
	Variable minWidth, minHeight
	
	if (strlen(theName) == 0)
		return 0
	endif
	DoWindow /F $theName
	if (!V_flag)
		return 0
	endif
	String legalLocations = "center;left;right"
	if (ParamIsDefault(location) || strlen(location) == 0)
		location = "center"
	elseif (WhichListItem(location, legalLocations) == -1)
		location = "center"
		print "** WARNING (moveWindowOnScreen): '" + location + "' is not a legal location entry. Moved window to center of screen."
	endif
	if (ParamIsDefault(minWidth) || minWidth < 0)
		minWidth = -1
	else
		minWidth = round(minWidth)
	endif
	if (ParamIsDefault(minHeight) || minHeight < 0)
		minHeight = -1
	else
		minHeight = round(minHeight)
	endif
		
	Variable screenWidth = 0, screenHeight = 0

	GetWindow $theName, wsize
	Variable theWidth = V_right - V_left			// width
	Variable theHeight = V_bottom - V_top 			// height

	strswitch (IgorInfo(2))
		case kSysWin:
			GetWindow kwFrameInner wsize
			screenWidth = V_right - V_left
			screenHeight = V_bottom - V_top
			break
		case kSysMac:
		default:
			String theScreenInfo = StringByKey("SCREEN1", IgorInfo(0))
			String theCoordinates = RemoveByKey("DEPTH", theScreenInfo, "=", ",")
			theCoordinates = theCoordinates[strsearch(theCoordinates, "=", 0) + 1, strlen(theCoordinates)]
			
			screenWidth = str2num(StringFromList(2, theCoordinates, ","))	
			screenHeight = str2num(StringFromList(3, theCoordinates, ","))	
	endswitch
	
//	Variable numScreens = NumberByKey("NSCREENS", IgorInfo(0))
	
	if (minWidth > 0 && theWidth < minWidth)
		theWidth = minWidth
	endif
	if (theWidth > screenWidth)
		theWidth = screenWidth
	endif
	if (minHeight > 0 && theHeight < minHeight)
		theHeight = minHeight
	endif
	if (theHeight > screenHeight)
		theHeight = screenHeight
	endif
	
	Variable newLeft, newRight, newTop, newBottom
	strswitch (location)
		case "left":
			newLeft = 5
			newTop = 15
			newRight = newLeft + theWidth
			newBottom = newTop + theHeight
			break
		case "right":
			newRight = screenWidth - 5
			newTop = 15
			newLeft = newRight - theWidth
			newBottom = newTop + theHeight
			break
		case "center":
		default:
			newLeft = floor((screenWidth - theWidth) / 2)
			newRight =  newLeft + theWidth
			newTop = floor((screenHeight - theHeight) / 2)
			newBottom = newTop + theHeight
	endswitch
	
	MoveWindow /W=$theName newLeft, newTop, newRight, newBottom
	return cmplx(theWidth, theHeight)
End

Function adjustPanelSize(name, [pixelWidth, pixelHeight])
	String name
	Variable pixelWidth,pixelHeight
	
	Variable success = 0
	if (strlen(name) == 0)
		return success
	else
		DoWindow $name
		if (V_flag == 0)
			// no panel with the given name exists
			return success
		endif
	endif
	if (ParamIsDefault(pixelWidth) && ParamIsDefault(pixelHeight))
		return success
	endif

	// wsize gets the window coordinates in points. Move window needs points to adjust the window size. The 
	// controls in panels are arranged in pixels, though, we need a conversion. On Macs a pixel is a point, usually 
	// runnnig at 72 dpi (-so the expression will evaluate to 1 and do nothing-), on PCs the resolution will be
	// running at 96 dpi, so to make sure that everything looks the same on both platforms we have to adjust.
	if (!ParamIsDefault(pixelWidth))
		GetWindow $name, wsize
		MoveWindow /W=$name V_left, V_top, (V_left + (pixelWidth * (PanelResolution(name)/ScreenResolution))), V_bottom
		success = 1
	endif
	if (!ParamIsDefault(pixelHeight))
		GetWindow $name, wsize
		MoveWindow /W=$name V_left, V_top, V_right,  (V_top + (pixelHeight * (PanelResolution(name)/ScreenResolution)))
		success = 1
	endif
	return success
End

Function /S getPixelsFromWindow(theName)
	String theName

	String result = ""
	DoWindow $theName
	if (V_flag == 0)
		// named window does not exists, so just stop here
		return result
	endif
	
	GetWindow $theName, wsize
	result = ReplaceNumberByKey("WIDTH", result, (V_right - V_left) * (Screenresolution/PanelResolution(theName)))
	result = ReplaceNumberbyKey("HEIGHT", result, (V_bottom - V_top) * (Screenresolution/PanelResolution(theName)))

	return result
End

//*****************************
// slide the named window
// newWidth and newHeight are the new values in pixels. If the add flag is set to 1, the values will be 
// added to the current width or height, otherwise the values are mend to be the total width or height
Function slideWindow(theName, [newWidth, newHeight, add])
	String theName
	Variable newWidth, newHeight, add
	
	newWidth = ParamIsDefault(newWidth) || newWidth < 0 ? 0 : newWidth
	newHeight = ParamIsDefault(newHeight) || newHeight < 0 ? 0 : newHeight
	// now convert pixels to points, since the movewindow command operates with points
	newWidth *= (72/ScreenResolution)
	newHeight *= (72/ScreenResolution)
	if (newWidth == 0 && newHeight == 0)
		return 0
	endif

	add = ParamIsDefault(add) || add < 1 ? 0 : 1
	
	GetWindow $theName, wsize
	Variable currLeft = V_left
	Variable currTop = V_top
	Variable currRight = V_right
	Variable currBottom = V_bottom

	Variable screenWidth, screenHeight
	strswitch (IgorInfo(2))
		case kSysWin:
			GetWindow kwFrameInner wsize
			screenWidth = V_right - V_left
			screenHeight = V_bottom - V_top
			break
		case kSysMac:
		default:
			String theScreenInfo = StringByKey("SCREEN1", IgorInfo(0))
			String theCoordinates = RemoveByKey("DEPTH", theScreenInfo, "=", ",")
			theCoordinates = theCoordinates[strsearch(theCoordinates, "=", 0) + 1, strlen(theCoordinates)]
			
			screenWidth = str2num(StringFromList(2, theCoordinates, ","))	
			screenHeight = str2num(StringFromList(3, theCoordinates, ","))	
	endswitch

	Variable widthDiff, heightDiff, newTop, newLeft, newBottom, newRight
	if (add)
		currRight = currRight + newWidth
		currBottom = currBottom + newHeight
	else
		currRight = newWidth == 0? currRight : currLeft + newWidth
		currBottom = newHeight == 0 ? currBottom : currTop + newHeight
	endif
	
	if (currRight > (screenWidth - 5))
		widthDiff = currRight - (screenWidth - 5)
		if (currLeft < widthDiff)
			 newLeft = 0
			 widthDiff -= currLeft
		endif
	endif
	if (currBottom > screenHeight)
		heightDiff = currBottom - screenHeight
		if (currTop < heightDiff)
			 newTop = 0
			 heightDiff -= currTop
		endif
	endif
	newRight = currRight - widthDiff
	newLeft = currLeft - widthDiff
	newBottom = currBottom - heightDiff
	newTop = currTop - heightDiff
	
	MoveWindow /W=$theName newLeft, newTop, newRight, newBottom 
End


Function /S WH_getProgressBar([theTitle, theName, theColor])
	String theTitle, theName, theColor
	
	if (ParamIsDefault(theTitle) || strlen(theTitle) == 0)
		theTitle = ksDefProgressBarTitle
	endif
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = ksDefProgressBarName
	endif
	if (ParamIsDefault(theColor) || strlen(theColor) == 0)
		theColor = "green"
	endif
	
	STRUCT RGBColor color
	strswitch(theColor)
		case "red":
			color.red = 65535
			color.green = 0
			color.blue = 0
			break
		case "blue":
			color.red = 0
			color.green = 0
			color.blue = 65535
			break
		case "green":
		default:
			color.red = 2
			color.green = 39321
			color.blue = 1
	endswitch

	NewPanel /W=(5,5,5,5) /K=1 /N=$theName as theTitle
	theName = S_name
	resizeWindowToScreen(theName, ratio=5)
	Variable width = NumberByKey("WIDTH", getPixelsFromWindow(theName))
	Variable height = NumberByKey("HEIGHT", getPixelsFromWindow(theName))
	Variable dispWidth = round(width * 0.9)
	Variable dispHeight = round(height * 0.25)
	Variable frameLDistance = round((width - dispWidth) / 2)
	Variable frameTDistance = round((height - dispHeight) * (2/3))
	Variable frameTForText = round(frameTDistance / 2)
	
	ValDisplay progressBar, win=$theName, pos={frameLDistance, frameTDistance}, size={dispWidth, dispHeight}
	ValDisplay progressBar, win=$theName, limits={0, 1, 0}, barmisc={0,0}
	ValDisplay progressBar, win=$theName, mode = 3, value=_NUM:0
	ValDisplay progressBar, win=$theName, highColor=(color.red,color.green,color.blue)
	
	TitleBox progressInfo, win=$theName, pos={frameLDistance, round(frameTDistance / 2)}, size={dispWidth, dispHeight}
	TitleBox progressInfo, win=$theName, font=$defFont, fSize=defFSize, frame=0
	Titlebox progressInfo, win=$theName, title = ""
	
	DoUpdate /W=$theName /E=1
	moveWindowOnScreen(theName)
	return theName
End

Function WH_updateProgressbar(progress, [info, theName])
	Variable progress
	String theName, info
	
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = ksDefProgressBarName
	endif
	
	ValDisplay progressBar, win=$theName, value=_NUM:progress
	
	if (!ParamIsDefault(info))
		Titlebox progressInfo, win=$theName, title=info
	endif
	DoWindow /F $theName
	DoUpdate /W=$theName
	return 0
End


Function killEmptyTable0()

	String info = TableInfo("Table0", -2)
	if (strlen(info) == 0)
		return 0			// no such table
	endif
	
	String lastCellInfo = StringByKey("LASTCELL", info)
	Variable fRow, fColumn
	sscanf lastCellInfo, "%d,%d", fRow, fColumn
	
	if (fRow == 0 && fColumn == -1)
		// table is empty, so kill it.
		KillWindow $"Table0"
		return 1
	endif
	// table is not empty, so don't kill it and return 0 as indication of not killing it.
	return 0
End
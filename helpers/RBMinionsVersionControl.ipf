#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.2
#pragma version = 2.4
#pragma ModuleName = VCheck	
#pragma hide = 1

// written by Roland Bock 2014

static StrConstant ksPackageName = "RB Minions Version Control"
static Constant kVersion = 2.4

static StrConstant ks_versionFileURL = "http://sites.google.com/site/igorhelpersversion/home/helper-version-file/"
static StrConstant ks_folderName = "RB Minions"

#if cmpstr(StringFromList(0, OperationList("easyHttp", ";", "external")), "easyHttp") == 0
	static Constant k_noInternet = 0
#else
	static Constant k_noInternet = 1
#endif

static Constant k_TypeLocal = 0
static Constant k_TypeInternet = 1

static Constant k_frequencyCheck = 7

static StrConstant ks_preferenceName = "PathPreferences.bin"
static Constant k_prefsVersion = 4
static StrConstant ks_versionFileName = "RBMinionsVersions.bin"
static Constant k_currentVersion = 3
static StrConstant ks_versionCheckName = "VersionChecks.bin"
static Constant k_versionCheckVersion = 1

static Constant k_IntOffset = 100

Structure RBVC_preferences
	int16 version
	int16 on						// turn checking on or off (1 or 0)
	int16 fileCheck				// turn checking of the file on or off (1 or 0)
	int16 type					// choose type of checking: 0 = local, 1 = internet
	char localPath[400]
	char winVolume[200]
	char folderName[200]
	char fileName[100]
	char uri[400]
	double fCheckDate			// date time of last file check
	int16 frequency				// frequency of checks in days
EndStructure

Structure RBVC_currentVersion
	int16 version
	char name[250]
	int16 packageVersion
EndStructure

Structure RBVC_versionChecked
	int16 version
	char name[250]
	double lastCheck
EndStructure

// ***********************    L O C A L   F U N C T I O N S 

static Function loadPrefs(prefs)
	STRUCT RBVC_preferences &prefs
	
	Variable recordID = 0	
	LoadPackagePreferences /MIS=1 ksPackageName, ks_preferenceName, recordID, prefs
	if (V_flag != 0 || V_bytesRead == 0)
		defaultPrefs(prefs)
	endif
	if (prefs.version != k_prefsVersion)
		savePrefs(prefs, reset=1)
		defaultPrefs(prefs)
		savePrefs(prefs)
	endif
	return 1		
End

static Function savePrefs(prefs, [reset, flush])
	STRUCT RBVC_preferences &prefs
	Variable reset, flush
	
	if (ParamIsDefault(reset) || reset < 0)
		reset = 0
	else
		reset = reset > 1 ? 1 : round(reset)	
	endif
	if (ParamIsDefault(flush) || flush < 0)
		flush = 0
	else
		flush = flush > 1 ? 1 : round(flush)
	endif
		
	Variable recordID = 0
	if (reset)
		SavePackagePreferences /KILL /FLSH=(flush) ksPackageName, ks_preferenceName, recordID, prefs	
	else
		SavePackagePreferences /FLSH=(flush) ksPackageName, ks_preferenceName, recordID, prefs
	endif
	return 1
End

static Function defaultPrefs(prefs)
	STRUCT RBVC_preferences &prefs
	
	prefs.version = k_prefsVersion
		
	prefs.on = 0
	prefs.fileCheck = 0
	prefs.type = k_TypeLocal
	prefs.folderName = ks_folderName
	prefs.localPath = SpecialDirPath("Igor Pro User Files", 0, 0, 0) + "User Procedures:"
	prefs.winVolume = ""
	prefs.fileName = ks_versionFileName
	prefs.uri = ks_versionFileURL
	prefs.fCheckDate = 0
	prefs.frequency = k_frequencyCheck
	return 1
End

static Function needCheck(name)
	String name
	
	Variable result = 0
	Variable recordID = getIDFromName(name)
	if (recordID < 0)
		return result
	endif
	
	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)
	if (!prefs.on)
		return result
	endif
		
	STRUCT RBVC_versionChecked vc
	LoadPackagePreferences /MIS=1 ksPackageName, ks_versionCheckName, recordID, vc
	if (V_flag != 0 || V_bytesRead == 0)
		result = 1
	else
		vc.version = k_versionCheckVersion
		vc.name = name
		String lastCheck = Secs2Date(vc.lastCheck, -2)
		String now = Secs2Date(DateTime, -2)
		if (cmpstr(lastCheck, now) != 0)
			result = 1
		endif
	endif
	return result
End

static Function setChecked(name)
	String name
	
	Variable recordID = getIDFromName(name)
	if (recordID < 0)
		return 0
	endif
	
	STRUCT RBVC_versionChecked vc
	vc.version = k_versionCheckVersion
	vc.name = name
	vc.lastCheck = DateTime
	SavePackagePreferences /FLSH=1 ksPackageName, ks_versionCheckName, recordID, vc
	return 1
End

static Function resetChecked(name)
	String name
	
	Variable recordID = getIDFromName(name)
	if (recordID < 0)
		return 0
	endif

	STRUCT RBVC_versionChecked vc
	vc.version = k_versionCheckVersion
	vc.name = name
	vc.lastCheck = 0
	SavePackagePreferences /FLSH=1 ksPackageName, ks_versionCheckName, recordID, vc
	return 1	
End

static Function getIDFromName(name)
	String name
	
	Variable recordID = 0
	strswitch(name)
		case ksPackageName:
			recordID = 0
			break
		case "RB_SimpleStats":
			recordID = 1
			break
		case "SNS_SA_Behavior":
			recordID = 2
			break
		case "SNSBehavior":
			recordID = 3
			break
		case "Spine Analysis":
			recordID = 4
			break
		case "Voltammetry":
			recordID = 5
			break
		case "Calcium Analysis":
			recordID = 6
			break
		case "Lickometer":
			recordID = 7
			break
		case "RB_TraceNavigator":
			recordID = 8
			break
		case "RM Template Loader":
			recordID = 9
			break
		default:
			recordID = -1
	endswitch
	return recordID
End

static Function updatePrefs(path, folder, name, uri, [reset])
	String path, folder, name, uri
	Variable reset
	
	if (ParamIsDefault(reset) || reset < 0)
		reset = 0
	else
		reset = reset > 1 ? 1 : round(reset)
	endif

	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)

	if (reset)
		savePrefs(prefs, reset=1, flush=1)
		defaultPrefs(prefs)
		savePrefs(prefs, flush=1)
		return 1
	endif	
	
	if (strlen(path) > 0)
		prefs.localPath = path
	endif
	if (strlen(folder) > 0)
		prefs.folderName = folder
	endif
	if (strlen(name) > 0)
		prefs.fileName = name
	endif
	if (strlen(uri) > 0)
		prefs.uri = uri
	endif
	savePrefs(prefs, flush=1)
End

static Function getVersion(name)
	String name
	
	Variable version = 0
	if (strlen(name) == 0)
		print "** ERROR: version check avoided, because no package name has been provided."
		return version
	endif
	
	Variable recordID = getIDFromName(name)
	if (recordID < 0)
		print "** ERROR: checking version for '", name, "'. Version number does not exists."
		return version
	endif
	
	STRUCT RBVC_currentVersion v
	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)
	String path = prefs.localPath + prefs.folderName + ":"
	NewPath /O/Q/Z rbVersion path
	
	PathInfo rbVersion
	
	if (!V_flag || cmpstr(S_path, path) != 0)
		NewPath /O/Q/Z rbVersion S_path
		prefs.localPath = ParseFilePath(1, S_path, ":", 1, 0)
		savePrefs(prefs)
		if (V_flag != 0)
			print "** NOTICE: version check for '", name, "' failed on", date(), time(), ", because I have no access to the version information file."
			return -1
		endif
	endif
	
	LoadPackagePreferences /MIS=1 /P=rbVersion prefs.folderName, prefs.fileName, recordID, v
	if (V_flag != 0 || V_bytesRead == 0)
		return version
	endif	
	version = v.packageVersion / k_IntOffset
	
	return version
End	
	
static Function setVersion(name, version)
	String name
	Variable version
	
	if (strlen(name) == 0)
		print "** ERROR: can't set a version number without a package name."
		return 0
	endif
	if (version <= 0)
		print "** ERROR: version number needs to be a positive number."
		return 0
	endif
	Variable recordID = getIDFromName(name)
	if (recordID < 0)
		print "** ERROR: the package '", name, "' has not been given a record number, yet."
		return 0
	endif
	
	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)
	String path = prefs.localPath + prefs.folderName + ":"
	NewPath /O/Q/Z rbVersionSave path
	
	if (V_flag != 0)
		print "** ERROR: can't set new version number for '", name, "', because I have no access to the version file."
		return 0
	endif
	
	version *= k_IntOffset
	
	STRUCT RBVC_currentVersion v
	LoadPackagePreferences /MIS=1 /P=rbVersionSave prefs.folderName, prefs.fileName, recordID, v

	v.version = k_currentVersion
	v.name = name
	if (v.packageVersion > version)
		DoAlert /T="Update Version" 1, "You are attempting to change from version " + num2str(v.packageVersion / k_IntOffset) + " to version " + num2str(version / k_IntOffset) + "!\r\rDo you want to proceed?"
		if (V_flag == 1)	
			v.packageVersion = version
		endif
	else
		v.packageVersion = version
	endif
	SavePackagePreferences /FLSH=1 /P=rbVersionSave prefs.folderName, prefs.fileName, recordID, v
	return 1
End

static Function downloadVersionFile()
	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)
	
	strswitch ( IgorInfo(2) )
		case "Macintosh":
			if (strlen(prefs.uri) == 0)
				// no location, so nothing more to do
				print "** ERROR (downloadVersionFile()): no file location has been set."
				return 0
			endif
			break
		case "Windows":
			if (strlen(prefs.winVolume) == 0)
				// no location, so nothing more to do
				print "** ERROR (downloadVersionFile()): no file location has been set."
				return 0
			endif
			break
	endswitch
					
	prefs.fCheckDate = datetime
	savePrefs(prefs, flush=1)

	String url = prefs.uri + prefs.fileName
	String store = prefs.localPath + prefs.folderName + ":" + prefs.fileName
	String file = ""
	Variable refNum
		
	switch ( prefs.type )
		case k_TypeLocal:
			strswitch ( IgorInfo(2) )
				case "Macintosh":
					file = FetchURL(url)
					Variable error = GetRTError(1)
					if (error != 0)
						return 0
					endif
					Open refNum as store
					FBinWrite refNum, file
					Close refNum
					break
				case "Windows":
					String path = prefs.winVolume + prefs.folderName + ":"
					NewPath /Q/O/Z rbWinVersion, path
					if (V_flag != 0)
						return 0
					endif
					Open /Z /R /P=rbWinVersion refNum as prefs.fileName
					if (V_flag != 0)
						return 0
					endif
					FStatus refNum
					file = PadString(file, V_logEOF, 0)
					FBinRead refNum, file
					Close refNum
					Open refNum as store
					FBinWrite refNum, file
					Close refNum
					break
			endswitch
			break
		case k_TypeInternet:
			#if cmpstr(StringFromList(0, OperationList("easyHttp", ";", "external")), "easyHttp") == 0			
				easyHttp /FILE=store url
				return V_flag == 0 ? 1 : 0
			#else
				return 0
			#endif
			break
	endswitch
	return 1
End

static Function needsFileCheck()
	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)
	if (!prefs.fileCheck)
		return 0
	endif
	
	Variable diffInDays = (datetime - prefs.fCheckDate) / (60 * 60 * 24)
	
	return (diffInDays > prefs.frequency)
End

static Function turnFileCheckOff()
	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)
	prefs.fileCheck = 0
	savePrefs(prefs, flush=1)
End

static Function updatePackageVersion()
	setVersion(ksPackageName, kVersion)
End

static Function AfterCompiledHook()
	if (needsFileCheck())
		downloadVersionFile()
	endif
	RBVC_checkVersion(ksPackageName, kVersion)
	return 0
End

// *********************** G L O B A L   F U N C T I O N S 

Function RBVC_checkVersion(name, version)
	String name
	Variable version
	
	Variable result = -1
	if (strlen(name) == 0)
		return result
	endif
	if (version <= 0)
		return result
	endif
	
	if (needCheck(name))
		Variable recVersion = getVersion(name)
		setChecked(name)
		if (recVersion > 0)
			result = 0
			if (recVersion > version)
				DoAlert 0, "A newer version of '" + name + "' is available. Please update!"
				result = 1
			endif
		else
			result = -1
		endif
	endif
	return result
End

Function RBVC_turnVersionCheckOff()
	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)
	prefs.on = 0
	prefs.fileCheck = 0
	prefs.fCheckDate = 0
	savePrefs(prefs, flush=1)
	return 1
End

Function RBVC_turnVersionCheckOn([type])
	String type
	
	if (ParamIsDefault(type) || strlen(type) == 0)
		if (k_noInternet)
			type = "local"
		else
			type = "internet"
		endif
	endif
	
	STRUCT RBVC_preferences prefs
	loadPrefs(prefs)
	prefs.on = 1
	prefs.fileCheck = 1
	prefs.fCheckDate = 0
	strswitch( LowerStr(type) )
		case "local":
			prefs.type = k_TypeLocal
			if (cmpstr(prefs.uri, ks_versionFileURL) == 0 || cmpstr((prefs.uri)[0,6], "file://") != 0)
				prefs.uri = ""
			endif
			break
		case "internet":
		default:
			if (k_noInternet)
				// tried to use the internet, but have no way to connect, so just reset and turn off
				prefs.type = k_TypeLocal
				prefs.on = 0
				prefs.fileCheck = 0
			else
				prefs.type = k_TypeInternet
			endif
	endswitch
	savePrefs(prefs, flush=1)
	return 1
End

// Function RBVC_tester()
//	
//	STRUCT RBVC_preferences p
//	savePrefs(p, reset=1)
//	defaultPrefs(p)
//	savePrefs(p)
//	RBVC_checkVersion(ksPackageName, kVersion)
//	Variable success = getVersion(ks_PackageName)
//	if (success < 0)
//		setVersion(ksPackageName, kVersion)
//	endif
// End	


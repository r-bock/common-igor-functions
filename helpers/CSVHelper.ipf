#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.1
#pragma ModuleName = CSVHelper
#include "TimeDateConversions"

// written by Roland Bock (August 2012)
// version 1.1 (October 2019): added simple csv loading function to import file into import folder

static StrConstant ksPackageFolder = "CSVData"
static Strconstant ksImportFolder = "Import"

static StrConstant ksDataName = "csvDataWave"
static StrConstant ksDateName = "csvDates"

static Strconstant ksLfName = "fileName"
static Strconstant ksNumWaves = "numberOfWaves"
static Strconstant ksWNameList = "loadedWaves"


StrConstant ksUnixLineFeed = "\n"
StrConstant ksMacLineFeed = "\r"
StrConstant ksWinLineFeed = "\r\n"

Menu "Macros"
	"Load CSV datafile ...", CSVHelper#CSVLoadData()
End

static Function /DF getPackageFolder()
	
	DFREF cDF = GetDatafolderDFR()
	DFREF folder = root:Packages:$ksPackageFolder
	Variable status = DatafolderRefStatus(folder)
	if (status != 1)
		if (status == 3)
			KillDatafolder /Z folder
		endif
		SetDatafolder root:
		if (DatafolderExists("Packages"))
			SetDatafolder Packages
		else
			NewDatafolder /S Packages
		endif
		NewDatafolder /S $ksPackageFolder
		DFREF folder = GetDatafolderDFR()	
	endif
	SetDatafolder cDF
	return folder
End

static Function /DF getImportFolder([init])
	Variable init
	
	init = ParamIsDefault(init) || init < 1 ? 0 : 1
	
	DFREF package = getPackageFolder()
	DFREF cDF = GetDataFolderDFR()
	SetDataFolder package
	NewDataFolder /O/S $ksImportFolder
	DFREF import = GetDataFolderDFR()
	SetDataFolder cDF	// return to where we started
	return import
End

static Function /S getImportStringValue(name)
	String name
	
	DFREF import = getImportFolder()
	SVAR str = import:$name
	if (!SVAR_Exists(str))
		return ""
	endif
	return GetDataFolder(1, import) + name
End

static Function /S getLastFile()
	return getImportStringValue(ksLfName)
End

static Function /S getLastWaveList()
	return getImportStringValue(ksWNameList)
End






static Function /DF CSVLoadData([columnSpec, verbose])
	String columnSpec
	Variable verbose
	
	verbose = ParamIsDefault(verbose) || verbose < 1 ? 0 : 1
	if (ParamIsDefault(columnSpec))
		columnSpec = ""
	endif
	DFREF cDF = GetDataFolderDFR()
	DFREF import = getImportFolder()
	SetDataFolder import
	KillWaves /A/Z				// clean up import folder before loading
	KillVariables /A/Z

	if (strlen(columnSpec) == 0)
		if (verbose)
			LoadWave /N/O/J/W
		else
			LoadWave /Q/N/J/W
		endif
	else
		if (verbose)
			LoadWave /B=columnSpec/N/O/J/W
		else
			LoadWave /Q/B=columnSpec/N/O/J/W
		endif		
	endif
	
	String /G $ksLfName = S_fileName
	String /G $ksWNameList = S_waveNames
	Variable /G $ksNumWaves = V_flag
	SetDataFolder cDF
	return import
End


Function /WAVE CSVLine2Numbers(theLine)
	String theLine
	
	if (strlen(theLine) == 0 || ItemsInList(theLine, ",") == 0)
		return numbersInLine
	endif
	Make /N=(ItemsInList(theLine, ",")) /FREE /O numbersInLine
	
	numbersInLine = str2num(StringFromList(p, theLine, ","))
	
	return numbersInLine
End

Function /S CSVNumbers(csv, [lineEnding, dateColumn, overwrite, labelsFirst])
	String csv, lineEnding
	Variable dateColumn, overwrite, labelsFirst
	
	String result = ""
	if (ParamIsDefault(lineEnding) || strlen(lineEnding) == 0)
		lineEnding = ksMacLineFeed
	endif
	if (ParamIsDefault(overwrite) || overwrite < 0)
		overwrite = 0
	elseif (overwrite > 1)
		overwrite = 1
	else
		overwrite = round(overwrite)
	endif		
	if (ParamIsDefault(dateColumn) || dateColumn < 0)
		dateColumn = -1
	endif
	if (ParamIsDefault(labelsFirst) || labelsFirst > 1)
		labelsFirst = 1
	elseif (labelsFirst < 0)
		labelsFirst = 0
	else
		labelsFirst = round(labelsFirst)
	endif
	
	DFREF cDF = GetDatafolderDFR()
	DFREF path = getPackageFolder()
	result = ReplaceStringByKey("PATH", result, GetDatafolder(1, path))
	
	Wave data = path:$ksDataName
	if (WaveExists(data))
		if (overwrite)
			Redimension /N=(0,0) data		
		endif
	else
		Make /N=(0,0) path:$ksDataName /WAVE=data
	endif
	result = ReplaceStringByKey("CSV", result, ksDataName)
	
	if (dateColumn > -1)
		Wave theDates = path:$ksDateName
		if (WaveExists(theDates))
			if (overwrite)
				Redimension /N=0 theDates
			endif
		else
			Make /N=0 path:$ksDateName /WAVE=theDates
		endif
		result = ReplaceStringByKey("DATES", result, ksDateName)
	endif
	
	Variable rows = ItemsInList(csv, lineEnding)
	if (rows == 0)
		return ""
	endif
	Variable columns = ItemsInList(StringFromList(0, csv, lineEnding), ",")
	if (columns == 0)
		return ""
	endif
	Variable oldRow = DimSize(data, 0)
	Variable oldColumn = DimSize(data, 1)
	if (oldColumn > columns)
		columns = oldColumn
	endif
	Redimension /N=(oldRow + rows, columns) data
	
	Variable lines = 0, labels = 0, counter = 0
	do
		String dataLine = StringFromList(lines, csv, lineEnding)

		if (labelsFirst && lines == 0)
			do 
				if (labels != dateColumn)
					SetDimLabel 1, labels, $(StringFromList(lines, csv, lineEnding)), data
				endif
				labels += 1
			while (labels < ItemsInList(dataLine, ","))
		else
			if (dateColumn  >= 0)
				String theDate = StringFromList(dateColumn, dataLine, ",")
				dataLine = RemoveListItem(0, dataLine, ",")
				theDates[oldRow + counter] = {timeStamp2Secs(theDate)}
			endif

			Wave aLine = CSVLine2Numbers(dataLine)
			data[oldRow + counter][] = aLine[q]
			counter += 1
		endif
		lines += 1
	while (lines < rows)
	
	return result
End

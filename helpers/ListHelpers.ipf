#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 5.4
#pragma IgorVersion = 6.2
#include "TimeDateConversions", version >= 2.4

// written by Roland Bock
// ** version 5.4 (OCT 2019) -> added a the removeEmptyItems function to clean lists with empty elements
// ** version 5.3 (SEP 2019) -> added an Igor 7 and later only function to automatically load the help file with this
//                 procedure file.
// ** version 5.2 (APR 2019) -> added a function to create a key-value list from a 2D wave
// ** version 5.1 (MAR 2019) -> added a recursive function to separate sublists indicated by braces
// ** version 5.0 (APR 2018) -> added a recursive function that produces a list of subject names from one or more category
//						 waves. It returns a key-value list with all category labels as key and a comma separated list of the 
//						 grouped subject names as value.
// ** version 4.9 (NOV 2017) -> created sortTWaveByCategories function to be able to sort a text wave by 
//                              text entries in other text waves.
// ** version 4.8 (OCT 2017) -> created convertTxt2Indx function to make numeric labels from a category wave
// ** version 4.7 (FEB 2017) -> created SortListByDate2 function to be able to sort waves with the date between 2 
//                 underscores.
// ** version 4.6 (JAN 2017) -> added a function to select only those text waves that contain row dimension labels; added
//                 a function that will combine 2 category labels of text waves based on their dimension label
// ** version 4.5 (DEC 2016) -> fixed a bug in the slice list part that would handle the range in a wrong manner
// ** version 4.4 (JUL 2016) -> fixed a bug in the getListItemWithPart function to correctly return the whole
//                                 item string
// ** version 4.3 (MAR 2016) -> added the sliceList function to select a range of the list based
//												on keywords FIRST, LAST and RANGE
// ** version 4.1 (JAN 2015) -> added makeWaveListFromGraph function to get a list of full path names
//                               from a graph
// ** version 4 (NOV 2014) -> finished getSubValueListByCategory function to slice a value wave into subLists
//                            based on its labels categories
// ** version 3.7 (APR 2014) -> changed makeLbl functions to use DimSize
// ** version 3.1 (OCT 2013) -> unified warnings, added same category function, enhanced makeLbl function
// ** version 2.8.5 (APR 2013) -> changed SortListByDate function to recognize multiple sessions per day
// ** version 2.8.1 (MAR 2013) -> added fullTraceListFromGraph function
//  ** version 2.7 (MAR 2012)
// ** version 0.1 (AUG 2008)


// **********************************************************************************************
// ***************    S T A T I C     S E C T I O N 
// **********************************************************************************************

static StrConstant ks_ErrorMsg = "** ERROR (%s): %s!\r"
static StrConstant ks_WarnMsg = "-- PROBLEM (%s): %s.\r"
static Strconstant ks_2ndLineMsg = "---- %s./r"


#if Exists("OpenHelp") != 0	// only for Igor versions 7 and higher
static Function AfterCompiledHook()
	loadHelp()
	return 0
End

static Function loadHelp()
	String fPath = FunctionPath("")	
	
	if (cmpstr(fPath[0], ":") == 0)
		// either buildin procedure window or packed procedure or procedures not compiled
		return -1
	endif
	
	String fName = ParseFilePath(0, fPath, ":", 1, 0)
	String hName = ReplaceString(".ipf", fName, ".ihf")
	String wList = WinList(hName, ";", "WIN:512")

	if (ItemsInList(wList) > 0)
		// help file already open, nothing more to do
		return 0
	endif

	String path = ParseFilePath(1, fPath, ":", 1, 0)
	
	// create full path to help file, which should have the same file name, but a different
	// 	file ending (.ihf instead of .ipf)
	String help = path + hName
	OpenHelp /V=0/Z help
	return V_flag
End
#endif


static Function /S getErrorStr(func)
	String func
	
	String errormsg = ""
	sprintf errormsg, ks_ErrorMsg, "[static] getErrorStr", "%s" 
	
	if (strlen(func) == 0)
		printf errormsg, "need a function name to generate error message"
		return ""
	endif
	
	String str = ""
	sprintf str, ks_ErrorMsg, func, "%s"
	return str
End

static Function /S getWarnStr(func)
	String func
	
	String errormsg = getErrorStr("[static] getWarnStr")
	
	if (strlen(func) == 0)
		printf errormsg, "need a function name to generate warning message"
		return ""
	endif
	
	String str = ""
	sprintf str, ks_WarnMsg, func, "%s"
	return str
End
	

// ************************   S T R U C T U R E S     ****************************************

//! A structure to make slicing lists of traces easier
//-
Structure LHSliceList
	String list					//< list to manipulate
	String selector			//< selector string (keyword-value list)
	String sep					//< separator of the list items
	String designator			
	Variable numTraces		//< number of traces in the list 
	Variable rangeStart		//< start of the sub list range
	Variable rangeEnd			//< end of the sub list range
EndStructure


//! Get the intersection from two sets (lists).
// @param theAList: a string list of the first set of values
// @param theBList: a string list of the second set of values
// @param sep: a single character string separating the values in both sets, defaults to ";"
// @return: a semicolon separated list of the intersecting values of both sets
//-
Function /S intersectLists(theAList, theBList, [sep])
	String theAList
	String theBList
	String sep
	
	String theNewList = ""
	if (ParamIsDefault(sep) || strlen(sep) != 1)
		sep = ";"
	endif
	if (strlen(theAList) == 0 && strlen(theBList) == 0)
		return theNewList
	endif
	if (strlen(theAList) == 0)
		return theNewList
	endif
	if (strlen(theBList) == 0)
		return theNewList
	endif
	
	Variable aNum = ItemsInList(theAList, sep)
	Variable bNum = ItemsInList(theBList, sep)
	Variable aCount = 0
	Variable bCount = 0
	
	for (aCount = 0; aCount < aNum; aCount += 1)
		String theAListItem = StringFromList(aCount, theAList, sep)
		if (WhichListItem(theAListItem, theBList, sep) < 0)
			continue
		endif
		theNewList = AddListItem(theAListItem, theNewList, ";", Inf)
	endfor
	return theNewList
End

//! Gets the intersecting values of 2 text waves.
// @param theAWave: the text wave for the first set
// @param theBWave: the text wave for the second set
// @return: the name of the result text wave in the current data folder
//-
Function /S intersectTextWaves(theAWave, theBWave)
	Wave /T theAWave
	Wave /T theBWave
	
	Make /T /O /N=0 RBLH_IntersectResult
	
	if (numpnts(theAWave) == 0)
		return nameOfWave(RBLH_IntersectResult)
	endif
	if (numpnts(theBWave) == 0)
		return nameOfWave(RBLH_IntersectResult)
	endif
	
	Variable aCount = 0
	Variable bCount = 0
	
	for (aCount = 0; aCount < numpnts(theAWave); aCount += 1)
		String theAListItem = theAWave[aCount]
		for (bCount = 0; bCount < numpnts(theBWave); bCount += 1)
			String theBListItem = theBWave[bCount]
			if (cmpstr(theAListItem, theBListItem) == 0)
				RBLH_IntersectResult[numpnts(RBLH_IntersectResult)] = {theAListItem}
			endif
		endfor
	endfor
	return nameOfWave(RBLH_IntersectResult)
End

//! Removes the items in the second list from the first set.
// @param theAList: a string list for the first set
// @param theBList: a string list of the values that should be removed from theAList
// @return: a semicolon separated string list with the values of the first set that are not in the second set.
//-
Function /S excludeFromList(theAList, theBList, [sep])
	String theAList, theBList
	String sep
	
	if (ParamIsDefault(sep) || strlen(sep) != 1)
		sep = ";"
	endif
	
	String theNewList = ""

	if (strlen(theAList) == 0)
		return theBList
	endif
	if (strlen(theBList) == 0)
		return theAList
	endif
	
	Variable aNum = ItemsInList(theAList, sep)
	Variable bNum = ItemsInList(theBList, sep)
	Variable aCount = 0
	Variable bCount = 0
	Variable inList = 0
	
	for (aCount = 0; aCount < aNum; aCount += 1)
		String theAListItem = StringFromList(aCount, theAList, sep)
		inList = 0
		for (bCount = 0; bCount < ItemsInList(theBList, sep); bCount += 1)
			String theBListItem = StringFromList(bCount, theBList, sep)
			if (cmpstr(theAListItem, theBListItem) == 0)
				inList = 1
				break
			endif
		endfor
		if (!inList)
			theNewList = AddListItem(theAListItem, theNewList, ";", ItemsInList(theNewList))
		endif
	endfor
	return theNewList
End

//! Checks if the all elements of the first list are in also in the second list. If the lists are not the same size,
// they are not the same.
// @param aList: a semicolon separated string list for the first set
// @param bList: a semicolon separated string list for the second set
// @return: 0 (False) if one element is different or 1 (True) if they are all the same 
//-
Function sameItems(aList, bList)
	String aList, bList
	
	Variable aNum = ItemsInList(aList)
	Variable bNum = ItemsInList(bList)
	
	if (aNum == 0 || bNum == 0 || aNum != bNum)
		return 0
	endif
	
	Variable index = 0, found = 0
	do
		found = WhichListItem(StringFromList(index, aList), bList)
		index += 1
	while(found >= 0 && index < aNum)
	return found < 0 ? 0 : 1
End		
	
//! Adds an item to a list, if it is not yet in it.
// @param theItem: a string with the item value
// @param theList: a string list to add the item to
// @param theSeparator: a single character string used as item separator in the list, defaults to ";"
// @return: the list with or without the added item, separated by the string in theSeparator
//-
Function /S AddUniqueToList(theItem, theList, [theSeparator])
	String theItem, theList, theSeparator
	
	if (strlen(theItem) == 0)
		return theList
	endif
	if (ParamIsDefault(theSeparator))
		theSeparator = ";"
	endif
	
	if (FindListItem(theItem, theList, theSeparator) == -1)
		theList = AddListItem(theItem, theList, theSeparator, Inf)
	endif
	return theList
End // AddUniqueToList

//! Filters a string list by removing all elements that match or do not match the filter.
// @param filter: a string giving the filter option
// @param theList: a string list of values to be filtered
// @param separator: a single character string separating the items in the list
// @param limitTo: a number (NOT zero based) giving the position of the single item to be returned
// @param range: a string in the form of "#-#" (i.e. 1-4 returns items 1 through 4) giving the start and end position of the filtered values to be returned
// @return: a semicolon separated string list of the filtered values or an empty list
//-
Function /S filterList(filter, theList, [separator, limitTo, range])
	String filter, theList, separator, range
	Variable limitTo
	
	if (ParamIsDefault(separator) || strlen(separator) != 1)
		separator = ";"
	endif
	if (strlen(filter) == 0 || strlen(theList) == 0)
		// no filter or list given, so just return the list
		return theList
	endif
	Variable not = 0
	if (cmpstr(filter[0], "!") == 0)
		filter = filter[1, strlen(filter)]
		not = 1
	endif
	
	Variable filterFound = strsearch(theList, filter, 0) == -1 ? 0 : 1
	if (!not && !filterFound)
		// filter not found, so return empty string, since nothing matches
		return ""
	endif
	if (not && !filterFound)
		// filter not found, but since we're looking for a NOT, return whole list.
		return theList
	endif		
	
	String newList = ""
	Variable index = 0
	for (index = 0; index < ItemsInList(theList, separator); index += 1)
		String theElement = StringFromList(index, theList, separator)
		filterFound = strsearch(theElement, filter, 0) == -1 ? 0 : 1
		
		if (!not && filterFound)
			newList = AddListItem(theElement, newList, separator, Inf)
		endif
		if (not && !filterFound)
			newList = AddListItem(theElement, newList, separator, Inf)
		endif
	endfor
	
	Variable numItems = ItemsInList(newList, separator)
	
	if (!ParamIsDefault(limitTo) && limitTo > 0)
		if (limitTo <= numItems)
			newList = StringFromList(limitTo - 1, newList, separator)
		else
			newList = ""
		endif
	elseif(!ParamIsDefault(range) || strlen(range) > 0)
		Variable r1 = str2num(StringFromList(0, range, "-"))
		Variable r2 = str2num(StringFromList(1, range, "-"))
				
		if (numtype(r1) == 2)
			r1 = 1
		endif
		if (numtype(r2) == 2)
			r2 = ItemsInList(newList)
		endif
		
		r1 = r1 <= 0 ? 0 : r1 - 1	// if start range is less than zero, use beginning of list
		r2 = r2 <= 0 ? numItems - 1 : r2 - 1		// if end range is less than zero, use end of list

		if (r1 >= numItems)
			// the start range is outside the number of items, so return an empty list, since
			// I don't want to look backward
			return ""
		else
			r2 = r2 >= numItems ? numItems - 1 : r2		// if only the end range is past the list,
																	// use the end of the list.				
		endif

		String tmpList = ""
		Variable item = 0
		for (item = r1; item <= r2; item += 1)
			tmpList = AddListItem(StringFromList(item, newList, separator), tmpList, separator, Inf)
		endfor
		newList = tmpList
	endif
	
	return newList
End

//! Restrict a given text list to items that are either longer, shorter or exactly a given length.
// @param theList: a semicolon separated string list of text values
// @param operation: one of either l or le (lesser or equal), g or ge (greater or equal) or e (equal)
// @param length: an integer value for the item length restriction
// @return: a new semicolon separated list with the restricted values or empty, if nothing matched
//-
Function /S restrictListByNameLength(theList, operation, length)
	String theList, operation
	Variable length
	
	String newList = ""
	
	if (strlen(theList) == 0)
		// nothing to do
		return newList
	endif
	
	// make sure length is an integer
	length = length <= 0 ? 0 : floor(length)
	
	Variable numItems = ItemsInList(theList)
	Variable index = 0
	for (index = 0; index < numItems; index += 1)
		String item = StringFromList(index, theList)
		strswitch (operation)
			case "le":
			case "<=":
				if (strlen(item) <= length)
					newList = AddListItem(item, newList, ";", Inf)
				endif
				break
			case "l":
			case "<":
				if (strlen(item) < length)
					newList = AddListItem(item, newList, ";", Inf)
				endif
				break
			case "ge":
			case ">=":
				if (strlen(item) >= length)
					newList = AddListItem(item, newList, ";", Inf)
				endif
				break
			case "g":
			case ">":
				if (strlen(item) > length)
					newList = AddListItem(item, newList, ";", Inf)
				endif
				break
			case "e":
			case "=":
			default:
				if (strlen(item) == length)
					newList = AddListItem(item, newList, ";", Inf)
				endif
		endswitch
	endfor
	
	return newList
End


// Old version that works, if there is only one session per day
//Function /S SortListByDate(theList)
//	String theList
//	
//	if (strlen(theList) == 0 || ItemsInList(theList) == 0)
//		print "** WARNING: no list to sort."
//		return ""
//	endif
//	
//	Make /N=(ItemsInList(theList)) /O /T SAWaveNamesToSort
//	Make /N=(ItemsInList(theList)) /O /T SADatesForSort
//	
//	Variable index = 0
//	for (index = 0; index < ItemsInList(theList); index += 1)
//		String theName = StringFromList(index, theList)
//		SAWaveNamesToSort[index] = theName
//		SADatesForSort[index] = theName[strsearch(theName, "_", Inf, 1) + 1, strlen(theName)]
//	endfor
//	Sort SADatesForSort, SAWaveNamesToSort
//	theList = ""
//	for (index = (numpnts(SAWaveNamesToSort) - 1); index >= 0; index -= 1)
//		theList = AddListItem(SAWaveNamesToSort[index], theList)
//	endfor
//	
//	KillWaves /Z SADatesForSort, SAWaveNamesToSort
//	return theList
//End



//! Sorts a list of wave names by their date. The wave name has to end in "_YYMMDD", i.e. SAT_M_090101, where
// 090101 is January 1st, 2009. This version does not recognize potential session information in the wave name, use 
// the version 2 (SortListByDate2) for it.
// @param theList: a semicolon separated string list of wave names, as generated by the build-in function WaveList
// @param verbose: a flag either 0 (False - default) or 1 (True) for user feedback in the history window
// @return: the sorted, semicolon separated string list of the wave names
//-
Function /S SortListByDate(theList, [verbose])
	String theList
	Variable verbose
	
	String warnmsg = getWarnStr("SortListByDate")
	
	if (ParamIsDefault(verbose) || verbose > 1)
		verbose = 1
	else
		verbose = verbose < 0 ? 0 : round(verbose)
	endif

	String returnList = ""
	if (strlen(theList) == 0 || ItemsInList(theList) == 0)
		if (verbose)
			printf warnmsg, "no list to sort"
		endif
		return returnList
	endif
	
	String sessionList = "", sKeyList = "", theDateList = "", theName = "", theSName = "", theDate = ""
	Variable index, sessions
	for (index = 0; index < ItemsInList(theList); index += 1)
		theName = StringFromList(index, theList)
		theDate = theName[strsearch(theName, "_", Inf, 1) + 1, strlen(theName)]
		theDateList = AddUniqueToList(theDate, theDateList)
		theSName = RemoveEnding(theName, "_" + theDate)
		sessionList = StringByKey(theDate, sKeyList, ":", ",")
		sessionList = SortList(AddListItem(theSName, sessionList, ";", Inf))
		sKeyList = ReplaceStringByKey(theDate, sKeyList, sessionList, ":", ",")
	endfor
	theDateList = SortList(theDateList)
	for (index = 0; index < ItemsInList(theDateList); index += 1)
		theDate = StringFromList(index, theDateList)
		sessionList = StringByKey(theDate, sKeyList, ":", ",")
		for (sessions = 0; sessions < ItemsInList(sessionList); sessions += 1)
			returnList = AddListItem(StringFromList(sessions, sessionList) + "_" + theDate, returnList, ";", Inf)
		endfor
	endfor
	return returnList
End

//! Sorts a list of wave names by their date. The wave name has to end in "_YYMMDD", i.e. SAT_M_090101, where
// 090101 is January 1st, 2009. This version does not recognize potential session information in the wave name, use 
// the version 2 (SortListByDate2) for it.
// @param theList: a semicolon separated string list of wave names, as generated by the build-in function WaveList
// @param verbose: a flag either 0 (False - default) or 1 (True) for user feedback in the history window
// @return: the sorted, semicolon separated string list of the wave names
//-
Function /S SortListByDate2(theList, [verbose])
	String theList
	Variable verbose
	
	String errormsg = getErrorStr("SortListByDate2")
	
	verbose = ParamIsDefault(verbose) || verbose > 0 ? 1 : 0
	
	String result = ""
	Variable numItems = ItemsInList(theList), index = 0, second = 0
		
	if (numItems == 0)	
		return result
	endif
	
	Make /N=(numItems) /T/O/FREE wList = StringFromList(p, theList)
	Make /N=(numItems) /O/FREE dIndex = strsearch(wList[p], "_", Inf, 1)
	Extract /FREE /INDX /O dIndex, tmp, dIndex == -1
	
	if (numpnts(tmp) > 0)
		// there are list items without underscore - can't determine date, so stop 
		if (verbose)
			if (numpnts(tmp) == numpnts(dIndex))
				printf errormsg, "no list items with date found, nothing to sort"
			else
				printf errormsg, num2str(numpnts(tmp)) + " items do not have an underscore separated date, STOPPED"
			endif
		endif
		return result
	endif
	
	Make /N=(numItems) /O /FREE iLength = dIndex[p] > 0 ? strlen(wList[p]) - dIndex[p] - 1 : dIndex[p] 
	Extract /FREE /INDX /O iLength, short, iLength < 6
	
	if (numpnts(short) > 0)
		Make /N=(numItems)/O/FREE sIndex = iLength[p] < 6 ? strsearch(wList[p], "_", dIndex[p] - 1, 1) : dIndex[p]
		Extract /FREE /O /INDX sIndex, tmp, sIndex < 1

		if (numpnts(tmp) > 0)
			if (verbose)
				printf errormsg, "not all list items have a valid date, not sorted"
			endif		
			return result
		endif

		Make /N=(numItems) /O/FREE selector = 0
		for (index = 0; index < numpnts(short); index += 1)
			selector[short[index]] = 1
		endfor

		iLength = selector[p] == 1 ? dIndex[short[index]] - sIndex[short[index]] - 1 : iLength[p]
		Extract /FREE /INDX /O iLength, tmp, iLength < 6
		if (numpnts(tmp) > 0)
			if (verbose)
				printf errormsg, "not all list items have a valid date, not sorted"
			endif		
			return result
		endif		
		
		dIndex = selector[p] == 1 ? sIndex[p] : dIndex[p]
	endif
	 
	Make /N=(numItems)/T/O/FREE sorter = (wList[p])[dIndex[p] + 1, dIndex[p] + 6]
	Sort sorter, wList
	
	for (index = 0; index < numItems; index += 1)
		result = AddListItem(wList[index], result, ";", Inf)
	endfor
	
	return result
End

//! This function creates sort keys for up to 3 categories (text waves) and sorts the initial text wave
// by category 1-3. The categories waves need to be as long as the inital text wave and must have a coresponding 
// for each entry in the original text wave. This function uses the 'convertTxt2Indx' function. The original text
// wave will be sorted in place.
// @param twave: a wave reference to a text wave to be sorted
// @param cat1: a wave reference to a text wave with categories
// @param cat1order: a semicolon separated string list with the sort order for the first set of categories
// @param cat2: like cat1 a wave reference to a textwave to a separate set of categories
// @param cat2order: similar to cat1order, the sort order for the second set of categories
// @param cat3: like cat1 and cat2 for the 3rd set of categories
// @param cat3order: sort order for the 3rd categories, like cat1order and cat2order
// @return: 0 (False) or 1 (True) on completion of the sort process, the original text wave will be sorted in place
//-
Function sortTWaveByCategories(twave, cat1, [cat1order, cat2, cat2order, cat3, cat3order])
	Wave /T twave, cat1, cat2, cat3
	String cat1order, cat2order, cat3order
	
	if (!WaveExists(twave))
		return 0
	endif
	if (!WaveExists(cat1))
		return 0
	endif
	
	Variable length = numpnts(twave)
	if (numpnts(cat1) != length)
		return 0
	endif
	
	DFREF wDF = GetWavesDatafolderDFR(twave)
	Sort /A/DIML twave, twave
	Make /N=(numpnts(twave)) /FREE /O orgIx
	convertTxt2Indx(twave, orgIx)
	
	Variable hasCat2 = 0, hasCat3 = 0
	
	if (!ParamIsDefault(cat2) && WaveExists(cat2) && numpnts(cat2) == length)
		hasCat2 = 1
		if (ParamIsDefault(cat2order))
			cat2order = ""
		endif
	endif
	if (!ParamIsDefault(cat3) && WaveExists(cat3) && numpnts(cat3) == length)
		hasCat3 = 1
		if (ParamIsDefault(cat3order))
			cat3order = ""
		endif
	endif
	
	Make /N=(numpnts(cat1)) /FREE /O cat1Ix
	if (ParamIsDefault(cat1order))
		cat1order = ""
	endif
	convertTxt2Indx(cat1, cat1Ix, order=cat1order)
	
	if (hasCat2)
		Make /N=(numpnts(cat2)) /FREE /O cat2Ix
		convertTxt2Indx(cat2, cat2Ix, order=cat2order)
	endif
	
	if (hasCat3)
		Make /N=(numpnts(cat3)) /FREE /O cat3Ix
		convertTxt2Indx(cat3, cat3Ix, order=cat3order)
	endif
	
	Make /FREE /T /N=(length) sortKeys

	sortKeys = num2str(cat1Ix)
	if (hasCat2)
		sortKeys = sortKeys[p] + num2str(cat2Ix)
	endif
	if (hasCat3)
		sortKeys = sortKeys[p] + num2str(cat3Ix)
	endif

	String sortNum = ""
	Variable index
	for (index = 0; index < length; index += 1)
		sprintf sortNum, "%03d", orgIx[index]
		sortKeys[index] = sortKeys[index] + sortNum
	endfor
	
	Sort /DIML sortKeys, twave
	
	return 1
End

//! This function returns a subset of items in the list by date. The list items have to have the dates as the 
// last part of their names, separated by an underscore (see SortListByDate function). The date strings are 
// processed by the correctDateString function which determines the available input formats.
// @param theList: the semicolon separated list of wave names with dates in their name
// @param startDate: the start date string in human readable form from where on to exclude the waves
// @param endDate: the end date to form a range with the start date, otherwise all waves after start date will be excluded
// @return: new subset of a wave names in a semicolon separated list string
//-
Function /S excludeListByDate(theList, startDate, [endDate])
	String theList, startDate, endDate
	
	String errormsg = getErrorStr("excludeListByDate")
	
	if (strlen(theList) == 0)
		printf errormsg, "no list submitted"
		return ""
	endif
	if (strlen(startDate) == 0)
		printf errormsg, "no start date submitted"
		return ""
	endif
	startDate = correctDateString(startDate)

	Variable hasEnd = 0
	if (ParamIsDefault(endDate) || strlen(endDate) == 0)
		hasEnd = 0
	else
		endDate = correctDateString(endDate)
		hasEnd = 1
	endif
	
	String excludeList = ""
	Variable index = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theItem = StringFromList(index, theList)
		String itemDate = theItem[strsearch(theItem, "_", Inf, 1) +1, strlen(theItem)]
		if (hasEnd)
			if (cmpstr(itemDate, startDate) == 1 && cmpstr(endDate, itemDate) == 1)
				excludeList = AddListItem(theItem, excludeList)
			endif
		else
			if (cmpstr(itemDate, startDate) == 1)
				excludeList = AddListItem(theItem, excludeList)
			endif
		endif
	endfor
	
	return excludeList
End


//! Separates a list of wave names (the waves have to exist) based on their grouping in the wave notes
// The function returns a key list, with the groups as keys and the waves in that group as a comma separated
// list.
//	@param theList: a semicolon separated list of wave names
// @param theGroups: a semicolon separated list of the group names in the wave notes
// @return: a semicolon separated key list, with the groups as keys and the wave names as comma separated values
//-
Function /S SeparateListByNote(theList, theGroups)
	String theList, theGroups
	
	String errormsg = getErrorStr("SeparateListByNote")
	String warnmsg = getWarnStr("SeparateListByNote")
	
	if (strlen(theList) == 0)
		printf errormsg, "nothing in the list. STOPPED!"
		return ""
	endif
	if (strlen(theGroups) == 0)
		printf warnmsg, "no grouping given, nothing to separate; returned list."
		return theList
	endif
	
	String theResultList = ""
	String theSepList = ""
	Variable groups = 0, index = 0
	for (groups = 0; groups < ItemsInList(theGroups); groups += 1)
		String theGroup = StringFromList(groups, theGroups)
		for (index = 0; index < ItemsInList(theList); index += 1)
			String theWaveName = StringFromList(index, theList)
			Wave theWave = $theWaveName
			if (!WaveExists(theWave))
				// skip over non existing waves
				continue
			endif
			if (cmpstr(note(theWave), theGroup) == 0)
				theSepList = AddListItem(theWaveName, theSepList, ",")
			endif
		endfor
		theResultList = ReplaceStringByKey(theGroup, theResultList, theSepList)
		theList = RemoveFromList(ReplaceString(",", theSepList, ";"), theList)		// waves only belong to one group, so remove
																											// the ones we found to make search faster
		theSepList = ""
	endfor
	
	return theResultList
End


//! Group the items in a list by the categories given in the text wave. The text wave must be as long as there are items in the list.
// @param theList: a semicolon separated list string, similar to the function SeparateListByNote.
// @param theTextWave: the name of the text wave with the groups, a string
// @return: a new semicolon separated key-value list with the groups as keys and the inital list elements a comma separated values
//-
Function /S SeparateListByWave(theList, theTextWave)
	String theList
	String theTextWave
	
	String errormsg = getErrorStr("SeparateListByWave")

	Wave /T theWave = $theTextWave

	// error checking still missing
	if (strlen(theList) == 0)
		printf errormsg, "nothing in the list. STOPPED!"
		return ""
	endif
	if (!WaveExists(theWave))
		printf errormsg, "wave >" + theTextWave + "< does not exist. STOPPED!"
		return ""
	endif
	if (ItemsInList(theList) != numpnts(theWave))
		printf errormsg, "the wave has to have as many points as there are items in the list!"
		printf ks_2ndLineMsg, "(" + num2str(ItemsInList(theList)) + " != " + num2str(numpnts(theWave)) + "!. STOPPED!"
		return ""
	endif
	String theResultList = ""
	String theGroupList = ""
	
	Variable index = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theItem = StringFromList(index, theList)
		theGroupList = StringByKey(theWave[index], theResultList)
		theGroupList = SortList(AddListItem(theItem, theGroupList, ","), ",")
		theResultList = ReplaceStringByKey(theWave[index], theResultList, theGroupList)
	endfor
	
	return theResultList
End

//! This function creates a semicolon separated string list of the values in the wave.
// @param theWaveName: a string value of the wave name for the list values
// @param sorted: 0 (default) or 1 for a flag to sort the values
// @param unique: 0 (default) or 1 for a flag to collect only unique values
// @param digits: a number how many after decimal digits of the values should be used, ignored when theWaveName is a text wave
// @return: a semicolon separated list of values.
//-
Function /S makeListFromWave(theWaveName, [sorted, unique, digits])
	String theWaveName
	Variable sorted, unique, digits
	
	String errormsg = getErrorStr("makeListFromWave")
	
	sorted = ParamIsDefault(sorted) || sorted < 1 ? 0 : 1
	unique = ParamIsDefault(unique) || unique < 1 ? 0 : 1
	digits = ParamIsDefault(digits) ? 0 : trunc(digits)
	
	String numberPattern = "%f"
	if (digits > 0)
		numberPattern = "%." + num2str(digits) + "f"
	endif
	
	String theList = ""
	if (!WaveExists($theWaveName))
		printf errormsg, "the wave >" + theWaveName + "< does not exist"
		return theList
	endif
	Variable isText, points = 0
	if (WaveType($theWaveName) == 0)
		Wave /T theWave = $theWaveName
		isText = 1
		points = numpnts(theWave)
	else
		Wave theNumWave = $theWaveName
		isText = 0
		points = numpnts(theNumWave)
	endif
	
	String strValue = ""
	Variable index = 0
	for (index = 0; index < points; index += 1)
		if (isText)
			if (unique)
				theList = AddUniqueToList(theWave[index], theList)
			else
				theList = AddListItem(theWave[index], theList, ";", Inf)
			endif
		else
			sprintf strValue, numberPattern, theNumWave[index]
			if (unique)
				theList = AddUniqueToList(strValue, theList)
			else
				theList = AddListItem(strValue, theList, ";", Inf)
			endif
		endif
	endfor
	
	if (sorted)
		theList = SortList(theList)
	endif
	return theList
End

//! Creates a text wave from a list.
// @param theList: a semicolon separated list string, which will end up in the text wave
// @param labelName: a name for the new text wave, defaults to 'textLabels'
// @param unique: a flag of either 0 (False - default) or 1 to only record unique labels, i.e every label in the list only once
// @returns: the name of the new text wave, which is created in the current data folder.
//- 
Function /S makeTWaveFromList(theList, [labelName, unique])
	String theList, labelName
	Variable unique
	
	Variable index = 0
	unique = ParamIsDefault(unique) || unique < 1 ? 0 : 1
	
	if (ParamIsDefault(labelName) || strlen(labelName) == 0)
		labelName = "textLabels"
	endif
	
	String newList = ""
	if (unique)
		for (index = 0; index < ItemsInList(theList); index += 1)
			newList = AddUniqueToList(StringFromList(index, theList), newList)
		endfor
	else
		newList = theList
	endif
	
	Make /N=(ItemsInList(newList)) /O /T $labelName /WAVE=labels
	
	labels = StringFromList(p, newList)
		
	return labelName
End

// This function creates a list from the index labels of a given dimension
Function /S makeLblListofWave(theWave, [dim, indexWave])
	Wave theWave, indexWave
	Variable dim

	String error = ""
	sprintf error, ks_ErrorMsg, "makeLblListofWave", "%s"
	
	String lblList = ""
	dim = ParamIsDefault(dim) || dim <= 0 ? 0 : round(dim)
	if (dim > 3)
		printf error, "Igor only handles 4 dimensional waves (dim 0-3)"
		return lblList
	endif
	if (!WaveExists(theWave))
		printf error, "the given wave does not exists"
		return lblList
	endif
	if (DimSize(theWave, dim) == 0)
		printf error, "dimension " + num2str(dim) + " does not exist in wave '" + NameOfWave(theWave) + "'"
		return lblList
	endif
	
	Variable numItems, useIndex = 0
	if (ParamIsDefault(indexWave) || !WaveExists(indexWave))
		numItems = DimSize(theWave, dim)
	else
		numItems = numpnts(indexWave)
		useIndex = 1
	endif
	
	Variable index = 0
	if (useIndex)
		for (index = 0; index < numItems; index += 1)
			lblList = AddListItem(GetDimLabel(theWave, dim, indexWave[index]), lblList, ";", Inf)
		endfor	
	else
		for (index = 0; index < numItems; index += 1)
			lblList = AddListItem(GetDimLabel(theWave, dim, index), lblList, ";", Inf)
		endfor
	endif
	return lblList
End

// This function takes the labels of a text wave and creates a list of the labels matching
// the entered text
// If the entered text is prepended with an "!" the function adds all labels NOT matching the text
Function /S makeLblListFromStatus(textWave, [text])
	Wave /T textWave
	String text
	
	String theResult = ""
	if (!WaveExists(textWave))
		return theResult
	endif
	
	Variable not = 0, isDefault = 0
	if (ParamIsDefault(text) || strlen(text) == 0)
		isDefault = 1
	else
		if (cmpstr(text[0], "!") == 0 && strlen(text) > 1)
			not = 1
		else
//			isDefault = 1
		endif
	endif
	
	Variable index
	for (index = 0; index < DimSize(textWave, 0); index += 1)
		if (isDefault)
			theResult = AddListItem(GetDimLabel(textWave, 0, index), theResult, ";", Inf)
		else
			if (not)
				if (cmpstr(text, textWave[index] )!= 0)
					theResult = AddListItem(GetDimLabel(textWave, 0, index), theResult, ";", Inf)
				endif
			else	
				if (cmpstr(text, textWave[index]) == 0)
					theResult = AddListItem(GetDimLabel(textWave, 0, index), theResult, ";", Inf)
				endif
			endif
		endif
	endfor
	return theResult
End

// This function creates a list of the dimension labels based on the entered value and the operation
// As operation enter
//		eq:		equal to
//		l:		lesser than
//		g:		greater than
//		le:		lesser than or equal to
//		ge:		greater than or equal to
//		a:		all
Function /S makeLblListFromWave(theWave, [value, operation, unique])
	Wave theWave
	Variable value, unique
	String operation
	
	String legalOperations = "eq;l;g;le;ge;a"
	String theResult = ""
	String theLabel = ""
	if (!WaveExists(theWave))
		return theResult
	endif
	if (ParamIsDefault(unique) || unique < 0)
		unique = 0
	else
		unique = unique >= 1 ? 1 : 0
	endif

	if (ParamIsDefault(value) || numtype(value) == 2)
		operation = "a"
	else
		if (ParamIsDefault(operation) || strlen(operation) == 0)
			operation = "a"
		endif
	endif
	if (WhichListItem(operation, legalOperations) < 0)
		print "-- WARNING (makeLblListFromWave): illegal operation '" + operation + "'. Operating on all values!"
	endif

	Variable index
	for (index = 0; index < DimSize(theWave, 0); index += 1)
		theLabel = GetDimLabel(theWave, 0, index)
		strswitch (operation)
			case "eq":
				if (theWave[index] == value)
					if (unique)
						theResult = AddUniqueToList(theLabel, theResult)
					else
						theResult = AddListItem(theLabel, theResult, ";", Inf)
					endif
				endif
				break
			case "l":
				if (theWave[index] < value)
					if (unique)
						theResult = AddUniqueToList(theLabel, theResult)
					else
						theResult = AddListItem(theLabel, theResult, ";", Inf)
					endif
				endif
				break
			case "g":
				if (theWave[index] > value)
					if (unique)
						theResult = AddUniqueToList(theLabel, theResult)
					else
						theResult = AddListItem(theLabel, theResult, ";", Inf)
					endif
				endif
				break
			case "le":
				if (theWave[index] <= value)
					if (unique)
						theResult = AddUniqueToList(theLabel, theResult)
					else
						theResult = AddListItem(theLabel, theResult, ";", Inf)
					endif
				endif
				break
			case "ge":
				if (theWave[index] >= value)
					if (unique)
						theResult = AddUniqueToList(theLabel, theResult)
					else
						theResult = AddListItem(theLabel, theResult, ";", Inf)
					endif
				endif
				break
			case "a":
			default:
				if (unique)
					theResult = AddUniqueToList(theLabel, theResult)
				else
					theResult = AddListItem(theLabel, theResult, ";", Inf)
				endif
		endswitch
	endfor
	return theResult
End

Function /S getLabeledTextWaves(theList)
	String theList
	
	String result = ""
	Variable numTexts = ItemsInList(theList), index = 0
	
	if (strlen(theList) == 0 || numTexts == 0)
		return result
	endif
		
	Make /FREE /WAVE /N=(numTexts) categories = $(StringFromList(p, theList))
//	Make /FREE /N=(numTexts) boolean = strlen(makeLblListofWave(categories[p]	)) == numpnts(categories[p])
	Extract /FREE /INDX /O categories, labeled, strlen(makeLblListofWave(categories[p])) != numpnts(categories[p])
	
	if (numpnts(labeled) < numTexts)
		for (index = 0; index < numpnts(labeled); index += 1)
			result = AddListItem(NameOfWave(categories[labeled[index]]), result, ";", Inf)
		endfor
	endif
	return result
End


// This function creates a list of data folders of the current data folder and returns it as a semicolon
// separated list string, excluding the data folders in theExcludeList.
Function /S getDatafolderList([ExcludeList, sorted])
	Variable sorted
	String ExcludeList
	
	String theList = DataFolderDir(1)
	theList = StringByKey("FOLDERS", theList)
	
	// change to default list separator
	theList = ReplaceString(",", theList, ";")
	
	// sort list if wished
	if (!ParamIsDefault(sorted) && sorted != 0)
		theList = SortList(theList, ";", 16)
	endif

	// remove certain folders from the list that are not subjects	
	if (!ParamIsDefault(ExcludeList))
		theList = RemoveFromList(ExcludeList, theList)
	endif
	return theList
End

// return the keys from a keylist, the default key separation string is ":", 
// the default list separation string is ";".
Function /S getKeysFromKeyList(theList, [keySepStr, listSepStr])
	String theList, keySepStr, listSepStr
	
	if (ParamIsDefault(keySepStr) || strlen(keySepStr) == 0)
		keySepStr = ":"
	endif
	if (ParamIsDefault(listSepStr) || strlen(listSepStr) == 0)
		listSepStr = ";"
	endif
	
	Variable numItems = ItemsInList(theList, listSepStr)
	String finalList = ""
	Variable index
	for (index = 0; index < numItems; index += 1)
		String theItem = StringFromList(index, theList, listSepStr)
		String theKey = StringFromList(0, theItem, keySepStr)
		finalList = AddUniqueToList(theKey, finalList)
	endfor
	
	return finalList
End

// This function 
Function /WAVE getSubValueListByCategory(theData, theCategoryWave, theCategory, [subListName, overwrite, verbose])
	Wave theData
	Wave /T theCategoryWave
	String theCategory, subListName
	Variable overwrite, verbose
	
	if (ParamIsDefault(overwrite) || overwrite < 0)
		overwrite = 0
	else
		overwrite = overwrite >= 1 ? 1 : 0
	endif
	if (ParamIsDefault(verbose) || verbose >= 1)
		verbose = 1
	else
		verbose = verbose <= 0 ? 0 : 1
	endif
	if (ParamIsDefault(subListName) || strlen(subListName) == 0)
		subListName = "SubListValues"
	endif
	if (strlen(theCategory) > 0)
		subListName = subListName[0, 32 - strlen(theCategory)] + "_" + theCategory
	endif
	
	Wave subListWave
	String errorMsg = "** ERROR (getSubValueListByCategory): %s\r"
		
	if (!WaveExists(theData) || numpnts(theData) == 0)
		printf errorMsg, "the data wave does not exist or has no points!"
		if (overwrite)
			KillWaves /Z subListWave
		endif
		return subListWave
	endif
	if (!WaveExists(theCategoryWave))
		printf errorMsg, "the category wave does not exist or has no points!"
		if (overwrite)
			KillWaves /Z subListWave
		endif
		return subListWave
	endif
	if (strlen(theCategory) == 0 || (cmpstr(theCategory[0], "!") == 0 && strlen(theCategory) == 1))
		printf errorMsg, "the category can NOT be empty!"
		if (overwrite)
			KillWaves /Z subListWave
		endif
		return subListWave
	endif
	
	String theDataList = makeLblListFromWave(theData)	
	Variable numItems = ItemsInList(theDataList)
	if (numItems == 0)
		printf errorMsg, "the data wave has no dimension labels!"
		return subListWave
	endif
	
	String theCategoryList = makeLblListFromStatus(theCategoryWave, text=theCategory)
	Variable numCategories = ItemsInList(theCategoryList)
	if (numCategories == 0)
		printf errorMsg, "no label matched the entered category '" + theCategory + "'!"
		return subListWave
	endif	
	
	String categoryLabels = intersectLists(theDataList, theCategoryList)
	Variable numCatLabels = ItemsInList(categoryLabels)
	if (numCatLabels == 0)
		printf errorMsg, "no data label matched the category '" + theCategory + "'!"
		return subListWave
	endif
	
	Make /N=(numCatLabels) /O $subListName /WAVE=subListWave
	
	Variable index = 0
	for (index = 0; index < numCatLabels; index += 1)
		String lbl = StringFromList(index, categoryLabels)
		subListWave[index] = theData[%$lbl]
		SetDimLabel 0, index, $lbl, subListWave
	endfor
	
	if (verbose)
		print "-- extracted " + num2str(numCatLabels) + " values from " + NameOfWave(theData) + " into " + subListName
	endif
		
	return subListWave
End

Function /S FullTraceListFromGraph(graphName, [excludeContaining])
	String graphName, excludeContaining
	
	String result = ""
	DoWindow $graphName
	if (V_flag == 0)
		return result
	endif
	
	String traceList = TraceNameList(graphName, ";", 1)
	if (ItemsInList(traceList) == 0)
		return result
	endif
	
	Variable all = 0
	if (ParamIsDefault(excludeContaining) || strlen(excludeContaining) == 0)
		all = 1
	endif
	
	Variable traces
	for (traces = 0; traces < ItemsInList(traceList); traces += 1)
		String traceName = StringFromList(traces, traceList)
		if (!all)
			if (strsearch(traceName, excludeContaining, 0) >= 0)
				continue
			endif
		endif
		result = AddListItem(GetWavesDatafolder(TraceNameToWaveRef(graphName, traceName), 2), result, ";", Inf)
	endfor
	return result
End

// This function checks if the all the names contain the same selector and returns 1 if they do, 0 otherwise
Function itemsAreSameCat(theList, category)
	String theList, category
	
	if (strlen(theList) == 0)
		return 0
	endif
	if (strlen(category) == 0)
		return 0
	endif
	Variable index = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theItem = StringFromList(index, theList)
		if (strsearch(theItem, category, 0) < 0)
			// selector not found, so at least one item is of a different category
			return 0
		endif
	endfor
	return 1
End

// This function searches a list string for the first occurrence of part. If it finds it, it extracts and 
// returns the list item between the given separators.
// If the search fails, an empty string is returned.
// list:		the list to be searched
// part:		the string to be in the list
// 
//	sep:		separator of the list items, defaults to ";"
Function /S getListItemWithPart(list, part, [sep])
	String list, part, sep
	
	String result = ""
	String errormsg = getErrorStr("getListItemWithPart")
	String warnmsg = getWarnStr("getListItemWithPart")
	
	if (strlen(list) == 0)
		printf errormsg, "need a list"
		return result
	endif
	if (strlen(part) == 0)
		printf errormsg, "need a part to search for"
		return result
	endif
	if (ParamIsDefault(sep) || strlen(sep) == 0)
		sep = ";"
	else
		sep = sep[0]
	endif
	
	Variable offset = strsearch(list, part, 0)
	if (offset < 0)
		// haven't found the part of the wave name so just return empty string
		return result
	endif
	Variable start = strsearch(list, sep, offset, 1)
	start = start < 0 ? 0 : start + 1 
	Variable theEnd = strsearch(list, sep, offset)
	theEnd = theEnd < 0 ? strlen(list) - 1 : theEnd - 1
	
	result = list[start, theEnd]
	return result
End

Function /S getListItemsContaining(list, part, [sep])
	String list, part, sep
	
	String result = ""
	String errormsg = getErrorStr("[getListItemsContaining]")
	
	if (strlen(list) == 0)
		printf errormsg,  "need a list!"
		return result
	endif
	if (strlen(part) == 0)
		printf errormsg, "need a part to search for!"
		return result
	endif
	if (ParamIsDefault(sep) || strlen(sep) == 0)
		sep = ";"
	else
		sep = sep[0]
	endif
	
	Variable offset = strsearch(list, part, 0)
	if (offset < 0)
		// haven't found the part of the wave name so just return empty string
		return result
	endif
	Variable firstSep = strsearch(list, sep, 0)
	if (firstSep < 0)
		// no separator in the list, so just one item, offset is larger than 0, so return
		// the complete string
		return list
	endif
	Variable done = 0
	do
		Variable start = strsearch(list, sep, offset, 1)
		if (start < 0)
			start = 0
		else
			start += 1
		endif

		Variable theEnd = strsearch(list, sep, start)
		if (theEnd < 0)
			theEnd = strlen(list)
			done = 1
		else 
			theEnd -= 1
		endif
		result = AddListItem(list[start, theEnd], result, sep, Inf)
		start = theEnd
		offset = strsearch(list, part, theEnd)
		if (offset < 0)
			done = 1
		endif
	while (done == 0)

	return result
End

Function checkTraceNamesSame(theList)
	String theList
	
	Variable counter = 0
	Variable index, numItems = ItemsInList(theList)
	for (index = 0; index < numItems; index += 1)
		String theName = StringFromList(index, theList)
//		Variable pound = strsearch(theName, "#", 0)
//		if (pound >= 0)
			counter += strsearch(theName, "#", 0) >= 0 ? 1 : 0
//		endif
			
	endfor
	
	return counter == numItems - 1
End

Function checkWaveNamesSame(theList)
	String theList
	
	String newList = ""
	Variable index, numItems = ItemsInList(theList)
	for (index = 0; index < numItems; index += 1)
		newList = AddUniqueToList(ParseFilePath(3, StringFromList(index, theList), ":", 0,0), newList)
	endfor
	
	return ItemsInList(newList) == 1
End

Function /S makeWaveListFromGraph(name)
	String name
	
	String result = ""
	if (strlen(name) == 0)
		name = StringFromList(0, WinList("*", ";", "WIN:1"))
		if (strlen(name) == 0)
			// no graphs
			return result
		endif
	else
		if (WinType(name) != 1)
			return result
		endif
	endif
	
	String traceList = TraceNameList(name, ";", 1)
	Variable numTraces = ItemsInList(traceList)
	if (numTraces == 0)
		return result
	endif
	
	Variable index
	for (index = 0; index < numTraces; index += 1)
		result = AddListItem(GetWavesDatafolder(TraceNameToWaveRef(name, StringFromList(index, traceList)), 2), result, ";", Inf)
	endfor
	
	return result
End

//! This function combines 2 lists and returns a list with the unique values in both of them.
// @param listA: a string list for set A
// @param listB: a string list for set B
// @param separator: a single character string separating the values of each list, defaults to ";"
// @return: a new string list with the unique values of both lists separated by the character given in separator
//-
Function /S combineListsUnique(listA, listB, [separator])
	String listA, listB, separator
	
	String result = ""
	
	if (strlen(listA) == 0 && strlen(listB) == 0)
		return result
	endif
	
	if (strlen(listA) == 0)
		return listB
	endif

	if (strlen(listB) == 0)
		return listA
	endif

	if (ParamIsDefault(separator) || strlen(separator) == 0 || strlen(separator) > 2)
		separator = ";"
	endif

	Variable numAs = ItemsInList(listA, separator)
	Variable numBs = ItemsInList(listB, separator)
	Variable index = 0

	for (index = 0; index < numAs; index += 1)
		result = AddUniqueToList(StringFromList(index, listA, separator), result, theSeparator=separator)
	endfor
	for (index = 0; index < numBs; index += 1)
		result = AddUniqueToList(StringFromList(index, listB, separator), result, theSeparator=separator)
	endfor
	
	return result
End

Function /WAVE combineCatWaves(waveA, waveB, [df, name])
	Wave /T waveA, waveB
	DFREF df
	String name
	
	String errormsg = getErrorStr("combineCatWaves")
	Wave /T result
	
	if (ParamIsDefault(df) || DatafolderRefStatus(df) != 1)
		DFREF df = root:
	endif
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = NameOfWave(waveA) + "_comb"
		name = name[0,31]
	endif
	
	if (!WaveExists(waveA) || !WaveExists(waveB))
		printf errormsg, "one of the input category waves does not exist."
		return result
	endif
	
	String labels = intersectLists(makeLblListofWave(waveA), makeLblListofWave(waveB))
	Variable numLabels = ItemsInList(labels)
	
	Make /N=(numLabels) /O /T df:$name /WAVE=comb
	Variable index = 0
	for (index = 0; index < numLabels; index += 1)
		String lbl = StringFromList(index, labels)
		SetDimLabel 0, index, $lbl, comb
		comb[index] = waveA[%$lbl] + waveB[%$lbl]
	endfor
	
	return comb
End


// This function creates a labeled text wave from two other category waves
Function /WAVE selectCatFromWaves(waveA, catA, waveB, catB)
	Wave /T waveA, waveB
	String catA, catB
	
	String errormsg = ""
	sprintf errormsg, ks_ErrorMsg, "selectCatFromWaves", "%s"
	
	Wave result
	
	if (WaveExists(waveA))
		Extract /FREE /INDX waveA, waveAElements, cmpstr(waveA, catA) == 0
	else
		printf errormsg, "the given category wave '" + NameOfWave(waveA) + "' does not exists - STOPPED"
		return result
	endif
	
	if (WaveExists(waveB))
		Extract /FREE /INDX waveB, waveBElements, cmpstr(waveB, catB) == 0
	else
		printf errormsg, "the given category wave '" + NameOfWave(waveA) + "' does not exists - STOPPED"
		return result
	endif

	Variable justOne = 0
	if (numpnts(waveAElements) == 0 && numpnts(waveBElements) == 0)
		printf errormsg, "the given categories '" + catA + "' and '" + catB + "' do not appear in the category waves - STOPPED"
		return result
	elseif (numpnts(waveAElements) == 0)
	
	elseif (numpnts(waveBElements) == 0)
	
	endif
	
	String listA = makeLblListofWave(waveA, indexWave=waveAElements)
	String listB = makeLblListofWave(waveB, indexWave=waveBElements)
	
	
	return result
End

// This function selects a range of the list items based on the keywords 
//
// FIRST		value of first selects the first list items
// LAST		value of last selects the last list items
// RANGE		value of range selects that continuous range of items. The 
//				range needs to be 2 numbers separated by a dash ('-')
//
// The value of the keywords are NOT index based

Function /S sliceList(list, selector, [sep, verbose])
	String list, selector, sep
	Variable verbose
	
	verbose = ParamIsDefault(verbose) || verbose < 1 ? 1 : 0
	
	String errormsg = getErrorStr("sliceList")
	String warnmsg = getWarnStr("sliceList")
	
	if (strlen(list) == 0)
		if (verbose)
			printf errormsg, "the list is empty"
		endif
		return ""
	endif
	if (strlen(selector) == 0)
		// no selectors given, so nothing to limit
		if (verbose)
			printf warnmsg, "the selector is emtpy, so returning the original list"
		endif
		return list
	endif
	
	if (ParamIsDefault(sep) || strlen(sep) == 0)
		sep = ";"
	endif
	
	Variable numItems = ItemsInList(list)
	
	String anItem = ""
	Variable aLimit = NumberByKey("FIRST", selector)
	if (numtype(aLimit) != 2)
		// we have a valid number, so process it
		if (aLimit < numItems)
			anItem = StringFromList(aLimit, list)
			list = list[0, FindListItem(anItem, list) - 1]
		endif
		
		// we have a list, so ignore any potential LAST or RANGE selector
		return list
	endif
	
	aLimit = NumberByKey("LAST", selector)
	if (numtype(aLimit) != 2)
		// a valid number, so process it
		if (aLimit < numItems)
			anItem = StringFromList(numItems - aLimit, list)
			list = list[FindListItem(anItem, list), strlen(list) - 1]
		endif
		
		// ignore any potential RANGE selector
		return list
	endif
	
	String theRange = StringByKey("RANGE", selector)
	if (strlen(theRange) > 0)
		Variable dashExists = strsearch(theRange, "-", 0) > 0
		if (dashExists)
			aLimit = str2num(StringFromList(0, theRange, "-"))
			if (numtype(aLimit) != 2)
				anItem = StringFromList(aLimit, list)
				list = list[FindListItem(anItem, list), strlen(list) - 1]
			else
				if (verbose)
					printf errormsg, "need 2 numbers separated by a dash ('-') when using the keyword 'RANGE'"
				endif
			endif
			aLimit = str2num(StringFromList(1, theRange, "-"))
			if (numtype(aLimit) != 0)
				anItem = StringFromList(aLimit, list)
				list = list[0, FindListItem(anItem, list) - 1]
			else
				if (verbose)
					printf errormsg, "need 2 numbers separated by a dash ('-') when using the keyword 'RANGE'"
				endif
			endif
		else
			if (verbose)
				printf errormsg, "the 'RANGE' keyword needs 2 numbers separated by a dash ('-'). Can not find the dash"
			endif
		endif
		
		return list
	endif
	
	// if we reach this, we did not find any valid keyword in the selector, so 
	//    just return the full list
	if (verbose)
		printf warnmsg, "no valid keyword (FIRST, LAST or RANGE) found, returning unaltered list"
	endif
	return list
End


Function /S sliceListStruct(ls, [verbose])
	STRUCT LHSliceList &ls
	Variable verbose
	
	verbose = ParamIsDefault(verbose) || verbose < 1 ? 1 : 0
	
	String errormsg = getErrorStr("sliceListStruct")
	String warnmsg = getWarnStr("sliceListStruct")
	
	if (strlen(ls.list) == 0)
		if (verbose)
			printf errormsg, "the list is empty"
		endif
		return ""
	endif
	if (strlen(ls.selector) == 0)
		// no selectors given, so nothing to limit
		if (verbose)
			printf warnmsg, "the selector is emtpy, so returning the original list"
		endif
		return ls.list
	endif
	
	if (strlen(ls.sep) == 0)
		ls.sep = ";"
	endif
	
	Variable numItems = ItemsInList(ls.list)
	
	String anItem = ""
	Variable aLimit = NumberByKey("FIRST", ls.selector)
	if (numtype(aLimit) == 0)
		// we have a valid number, so process it
		if (aLimit < numItems && aLimit >= 0)
			anItem = StringFromList(aLimit, ls.list, ls.sep)
			ls.list = ls.list[0, FindListItem(anItem, ls.list, ls.sep) - 1]
		endif
		ls.designator = "first"
		
		// we have a list, so ignore any potential LAST or RANGE selector
		ls.numTraces = ItemsInList(ls.list, ls.sep)
		return ls.list
	endif
	
	aLimit = NumberByKey("LAST", ls.selector)
	if (numtype(aLimit) == 0)
		// a valid number, so process it
		if (aLimit < numItems)
			anItem = StringFromList(numItems - aLimit, ls.list)
			ls.list = ls.list[FindListItem(anItem, ls.list), strlen(ls.list) - 1]
		endif
		ls.designator = "last"
		
		// ignore any potential RANGE selector
		ls.numTraces = ItemsInList(ls.list, ls.sep)
		return ls.list
	endif
	
	String theRange = StringByKey("RANGE", ls.selector)
	if (strlen(theRange) > 0)
		Variable dashExists = strsearch(theRange, "-", 0) > 0
		ls.designator = "range"
		if (dashExists)
			aLimit = str2num(StringFromList(0, theRange, "-"))
			if (numtype(aLimit) == 0)
				if (aLimit < numItems)
					anItem = StringFromList(aLimit, ls.list)
					ls.list = ls.list[FindListItem(anItem, ls.list), strlen(ls.list) - 1]
					ls.rangeStart = aLimit
				else
					ls.list = ""
					return ls.list
				endif
			else
				if (verbose)
					printf errormsg, "need 2 numbers separated by a dash ('-') when using the keyword 'RANGE'"
				endif
			endif
			Variable bLimit = str2num(StringFromList(1, theRange, "-"))
			if (numtype(bLimit) == 0)
				if (bLimit < numItems)
					anItem = StringFromList(bLimit - aLimit, ls.list)
					ls.list = ls.list[0, FindListItem(anItem, ls.list) + strlen(anItem)]
					ls.rangeEnd = bLimit
				else
					ls.rangeEnd = numItems - 1
				endif
			else
				if (verbose)
					printf errormsg, "need 2 numbers separated by a dash ('-') when using the keyword 'RANGE'"
				endif
			endif
		else
			aLimit = str2num(theRange)
			if (numtype(aLimit) == 0)
				// we have a single number with the range key word, so use as both start and end
				if (aLimit < numItems)
					anItem = StringFromList(aLimit, ls.list)
					ls.list = anItem + ";"
					ls.rangeStart = aLimit
					ls.rangeEnd = aLimit
				else
					ls.list = ""
					if (verbose)
						printf errormsg, "the 'RANGE' keyword needs 2 numbers separated by a dash ('-'). Can not find the dash"
					endif
				endif
			else
				ls.list = ""
				if (verbose)
					printf errormsg, "the 'RANGE' keyword needs 2 numbers separated by a dash ('-'). Can not find the dash"
				endif
			endif
		endif
		
		ls.numTraces = ItemsInList(ls.list, ls.sep)
		return ls.list
	endif
	
	// if we reach this, we did not find any valid keyword in the selector, so 
	//    just return the full list
	if (verbose)
		printf warnmsg, "no valid keyword (FIRST, LAST or RANGE) found, returning unaltered list"
	endif
	ls.numTraces = numItems
	return ls.list
End

// Sets the text category wave to category at the provided labels in the list. Requires the category wave to 
// have row dimension labels (i.e. subjects)
Function setCategoryByCat(catWave, category, list)
	Wave /T catWave
	String category, list
	
	Variable index = 0, length = ItemsInList(list)
	
	if (length == 0)
		return 0
	endif
	
	for (index = 0; index < length; index += 1)
		String lbl = StringFromList(index, list)
		if (FindDimLabel(catWave, 0, lbl) < 0)
			continue
		endif
		catWave[%$lbl] = category
	endfor
	
	return 1
End

Function setCategoryByValue(catWave, dataWave, category, value, operation)
	Wave /T catWave
	Wave dataWave
	Variable value
	String operation, category
	
	String errormsg = getErrorStr("setCategoryByValue")
	String warnmsg = getWarnStr("setCategoryByValue")
	
	if (strlen(category) == 0)
		// need a category, empty one is not really usefull, stop here
		printf errormsg, "need a category to assign, can not be left empty!"
		return 0
	endif
	if (!WaveExists(catWave))
		printf errormsg, "category wave must exist! - Nothing to do"
		return 0
	endif
	if (!WaveExists(dataWave))
		printf errormsg, "data value wave must exist! - Nothing to do."
		return 0
	endif
	if (strlen(operation) == 0)
		operation = "a"
		printf warnmsg, "you must enter an operation - set to 'a'.	"
	endif
	
	Variable sameLength = numpnts(catWave) == numpnts(dataWave)
	Variable useLabel = 1
	Variable index = 0
	
	String existCatLabels = makeLblListofWave(catWave)
	String existDataLabels = makeLblLIstofWave(dataWave)

	if (strlen(existDataLabels) == 0)
		// no dim labels, check if the waves have the same length so we could use positional information
		if (!sameLength)
			// both wave are of different length, so we don't have enough information to proceed
			printf errormsg, "no dimension labels on the data wave and different lengths of category wave -> can proceed without more information"
			return 0
		else
			// both waves have the same length, assumme identity and use positional information
			printf warnmsg, "using positional information to set the status"
			useLabel = 0
		endif
	endif
	
	if (strlen(existCatLabels) == 0 && useLabel == 1)
		// category wave is not labeled, so check if category wave and data wave are the same length
		if (!sameLength)
			// not the same length - break here and let the user make the category wave
			printf errormsg, "the labels of the category wave don't exist and both wave have different lengths. Please create the correct category wave."
			return 0
		else
			printf warnmsg, "the category labels don't exist, assuming they are identical to the data wave, fixing it."
			for (index = 0; index < numpnts(dataWave); index += 1)
				SetDimLabel 0, index, $(GetDimLabel(dataWave, 0, index)), catWave
			endfor
		endif
	endif
	
	String resultList = makeLblListFromWave(dataWave, value=value, operation=operation)
	Variable numSubj = ItemsInList(resultList)
	
	for (index = 0; index < numSubj; index += 1)
		String dimLabel = StringFromList(index, resultList)
		if (WhichListItem(dimLabel, existCatLabels) >= 0)
			catWave[%$dimLabel] = category
		else
			printf warnmsg, "the subject " + dimLabel + " is not in the category wave, skipped"
		endif
	endfor
	
	return 1
End

//! Converts a text wave with category labels/factors into numeric indexes, useful for transforming
// statistical factors into numbers to be analyzed with other statistical software.
// @param txt: a text wave with the categories or factors
// @param indx: a numberic wave of the same length as the text wave. If it doesn't exist, defaults to "CategoryIndexWave"
// @param order: a semicolon separated string list giving the order of the categories/factors
// @return: True or False after completion. The results are stored in the given index wave. 
//-
Function convertTxt2Indx(txt, indx, [order])
	Wave /T txt
	Wave indx
	String order
	
	if (!WaveExists(txt) || numpnts(txt) == 0)
		return 0
	endif
	if (!WaveExists(indx))
		DFREF folder = GetWavesDatafolderDFR(txt)
		Make /O/N=(numpnts(txt)) folder:$"CategoryIndexWave" /WAVE=indx
	else
		Redimension /N=(numpnts(txt)) indx
		indx = NaN
	endif	
	
	String txtList = "", theItem = ""
	Variable index, catIx
	for (index = 0; index < numpnts(txt); index += 1)
		txtList = AddUniqueToList(txt[index], txtList)
	endfor
	
	if (!ParamIsDefault(order) && ItemsInList(order) > 0)
		String diffList = excludeFromList(txtList, order)
		if (ItemsInList(diffList) > 0)
			if (cmpstr(order[strlen(order)-1], ";") == 0)
				txtList = order + diffList
			else
				txtList = order + ";" + diffList
			endif
		else
			txtList = order
		endif
	endif
	
	Variable numItems = ItemsInList(txtList)
	for (index = 0; index < numItems; index += 1)
		theItem = StringFromList(index, txtList)
		
		Extract /INDX /FREE txt, cat, cmpstr(txt, theItem) == 0
		if (numpnts(cat) > 0)
			for (catIx = 0; catIx < numpnts(cat); catIx += 1)
				indx[cat[catIx]] = index
			endfor
		endif
	endfor	
	
	return 1
end

//! A recursive function that categorizes subjects. It needs one or more category (text) waves where the data column is the 
// category label and the row dimension label contains the corresponding subject label. 
// @param catWaves: a wave of wave references pointing to text waves with row dimension labels
// @param level: a number 
// The input is a wave of wave references of these category waves. 
Function /S categorizeList(catWaves, level)
	Wave /WAVE catWaves
	Variable level
	
	String result = "", newList = "", category = ""
	Variable index = 0, ci = 0
	
	Wave /T subjects = catWaves[level]	
	String categories = makeListFromWave(GetWavesDatafolder(subjects, 2), unique=1)
	Variable numCats = ItemsInList(categories)

	if (numpnts(catWaves) - 1 == level)
		for (index = 0; index < numCats; index += 1)
			category = StringFromList(index, categories)
			result = ReplaceStringByKey(category, result, ReplaceString(";", makeLblListFromStatus(subjects, text=category), ","))
		endfor
		return result
	endif
	
	result = categorizeList(catWaves, level + 1)

	Variable numKeys = ItemsInList(result)
	String keyList = getKeysFromKeyList(result)
	
	for (index = 0; index < numCats; index += 1)
		category = StringFromList(index, categories)
		String sList = makeLblListFromStatus(subjects, text=category)
		
		for (ci = 0; ci < numKeys; ci += 1)
			String key = StringFromList(ci, keyList)
			String nList = intersectLists(ReplaceString(",", StringByKey(key, result), ";"), sList)
			newList = ReplaceStringByKey(category + "_" + key, newList, ReplaceString(";", nList, ","))
		endfor
	endfor
	
	return newList
End

//! Extracts elements within braces and puts in an igor key-value list format.
// @param str: a string with sublists in braces
// @param item: an integer value for the iteration of the function
// @return: a key-value list with the element in braces listed with the keys "SUBLIST#" where # is the iteration.
//-
Function /S switchSublists2KeyList(str, [item])
	String str
	Variable item
	
	item = ParamIsDefault(item) ? 0 : trunc(item)		// make sure we have only integers
	str = ReplaceString(" ", str, "")						// remove spaces
	
	String result = "", sub = ""
	String lstKey = "SUBLIST" + num2str(item)

	Variable oBL = 0, cBL = 0, iterator = 1
	
	do
		oBL = strsearch(str, "{", 0)
		
		if (oBL < 0 && iterator == 1)
			// nothing more to do, just return original list
			result = ReplaceStringByKey(lstKey, result, str)
			return result
		elseif (oBL < 0)
			// found the end of the string, so just process the leftover items, make sure
			// to remove the empty items
			sub = ""
			Variable index
			for (index = 0; index < ItemsInList(str); index += 1)
				String anItem = StringFromList(index, str)
				if (strlen(anItem) == 0)
					continue
				endif
				sub = AddListItem(anItem, sub, ";", Inf)
			endfor
			result = ReplaceStringByKey(lstKey, result, sub, ":", ",")
			return result
		endif
		
		cBL = strsearch(str, "}", oBL + 1)
		
		if (oBL == 0 && cBL == strlen(str) - 1)
			result = ReplaceStringByKey(lstKey, result, str[oBL + 1, cBL - 1])		// remove braces and return
			return result
		endif
		
		if (cBL > 0)
			// we started the search on the second character of the string, so it should always be 
			// larger than zero
			sub = str[oBL, cBL]
			str[oBL, cBL] = ""
			result = AddListItem(switchSublists2KeyList(sub, item = (item * 10) + iterator), result, ",", Inf)
		endif
		
		iterator += 1
	while (oBL > 0)	
	
	return result
End

//! This function takes a 2 dimensional wave of values and converts it into a key-value list. The values of the column given by key 
// become the keys in the list, the values of the value column the value part of the list. 
// @param params: a 2D wave of values
// @param key: column number of the key values (default: 0 - first column)
// @param value: column number of the values (default: 1 - second column)
// @return: a semicolon separated key - value list with a colon separating the key and the value
//- 
Function /S waveToKeyValueList(params, [key, value])
	Wave params
	Variable key, value

	String result = ""
	String error = getErrorStr("waveToKeyValueList")
	
	Variable type = WaveType(params, 1)
	switch (type)
		case 1:
			break
		case 2:
			// is text wave, so redefine wave
			Wave /T paramText = params
			break
		default:
			// either doesn't exist or can't work with the wave type so stop here
			return result
	endswitch
	
	if (WaveDims(params) < 2)
		printf error, "need at least 2 dimensions to create list"
		return result
	endif
	
	Variable numRows = DimSize(params, 0)
	Variable numColumns = DimSize(params, 1)
	
	key = ParamIsDefault(key) || key <= 0 ? 0 : trunc(key)
	value = ParamIsDefault(value) || value <= 0 ? 1 : trunc(value)
	
	if (key == value)
		// key and value columns must be different integers, stop here
		printf error, "key and value column have the same number, please choose different values"
		return result
	endif
	
	if (key >= numColumns || value >= numColumns)
		printf error, "key or value column is out of range"
		return result
	endif
	
	if (type == 1)
		Make /N=(numRows) /FREE keys = params[p][key]
		Make /N=(numRows) /FREE values = params[p][value]
	
		wfprintf result, "%d:%d;", keys, values
	else
		Make /T/N=(numRows) /FREE keyText = paramText[p][key]
		Make /T/N=(numRows) /FREE valueText = paramText[p][value]

		wfprintf result, "%s:%s;", keyText, valueText
	endif
	
	return result
End

//! Removes empty items from a string list.
// @param theList: a semicolon separated list with values
// @return: a new semicolon separated list with all empty values removed
//-
Function /S removeEmptyItems(theList)
	String theList
	
	String newList = ""
	Variable index = 0
	for (index = 0; index < ItemsInList(theList); index += 1)
		String item = StringFromList(index, theList)
		if (strlen(item) > 0)
			newList = AddListItem(item, newList, ";", Inf)
		endif
	endfor
	return newList
End


//! A general function to extract all unique list items from a text wave as list.
// @param: category: a wave reference to a text wave with the categories
// @return: a semicolon separated list with all the distinct items in the text wave
//- 
Function /S getUniqueItems(category)
	Wave /T category
	
	String result = ""
	
	if (!WaveExists(category) || numpnts(category) == 0)
		return result
	endif
	
	Variable index
	for (index = 0; index < numpnts(category); index += 1)
		result = AddUniqueToList(category[index], result)
	endfor
	
	return result
End


#pragma rtGlobals=1		// Use modern global access method.
#include "CleanersAndCheckers"

// set helper functions for multidimensional waves 
//  written by Roland Bock 9/12/2008
// version 0.3

// adds the columnIndex column of all layers in a 3D wave
Function/S addColumnOfLayers(theWaveName, columnIndex)
	String theWaveName
	Variable columnIndex
	
	Wave theWave = $theWaveName
	Variable rows = DimSize(theWave, 0)
	
	Make /N=(rows) /O $("W_columnSum" + num2str(columnIndex))
	Wave theSumWave = $("W_columnSum" + num2str(columnIndex))
	theSumWave = 0
	
	Variable columns = DimSize(theWave, 1)
	Variable layers = DimSize(theWave, 2)	
	Variable chunks = DimSize(theWave, 3)
	if ((layers  == 0) || (columns == 0) || (columns <= columnIndex) || (chunks > 0))
		return nameOfWave(theSumWave)
	endif
	
	Variable index = 0
	for (index = 0; index < layers; index += 1)
		theSumWave += theWave[p][columnIndex][index]
	endfor
	SetScale /P x, 0, DimDelta(theWave, 0), WaveUnits(theWave, 0), theSumWave
	return nameOfWave(theSumWave)
End

// Averages a 2 dimensional matrix depending on the type. The results are stored in 
// W_matrixAvg for the means, and W_matrixSdev for the corresponding standard deviation
// type:		1: averages each column
//			2: averages each row across the columns
// It returns 1 for success and 0 for failure. The results are written in two waves, W_matrixAvg 
// contains the means, W_matrixSdev contains the standard deviations.
Function averageMatrix(theMatrix, type)
	Wave theMatrix
	Variable type
		
	if (DimSize(theMatrix, 2) > 0 || DimSize(theMatrix, 3) > 0)
		return 0
	endif

	Variable transposeFlag = 0
	Make /N=0 /O W_matrixAvg
	Make /N=0 /O W_matrixSdev
	Make /N=0 /O W_matrixSem
	
	if (type == 2)
		MatrixTranspose theMatrix
		transposeFlag = 1
	endif
	
	Variable index = 0
	Variable loopEnd = DimSize(theMatrix, 1)
	Make /N=(DimSize(theMatrix, 0)) /O avg
	
	for (index = 0; index < loopEnd; index += 1)
		avg[] = theMatrix[p][index]
		WaveStats /Q /W avg
		Wave M_WaveStats
		W_matrixAvg[index] = {M_WaveStats[3]}
		W_matrixSdev[index] = {M_WaveStats[4]}
		W_matrixSem[index] = {M_WaveStats[4]/sqrt(M_WaveStats[0])}
	endfor
	Note /K W_matrixAvg, ("#n:" + num2str(DimSize(theMatrix, 0)))
	deleteGobalWaveMetricsVars()
	KillWaves /Z avg
	if (transposeFlag)
		// return to original form
		MatrixTranspose theMatrix
	endif
	SetScale /p x, 0, DimDelta(theMatrix, 0), WaveUnits(theMatrix, 0), W_matrixAvg, W_matrixSdev
	return 1
End

// Sums the columns of a 3 dimensional wave
// Returns the wave W_matrixColumnSum, row 0 contains the sum of the columns
Function/S sumMatrixColumns(theMatrix)
	Wave theMatrix
	
	if (DimSize(theMatrix, 3) > 0)
		print "** ERROR (sumMatrixColumns): can't deal with chunks."
		return ""
	endif
	
	Variable layers = 0
	if (DimSize(theMatrix, 2) > 0)
		layers = 1
	endif
	
	Variable column = 0
	Variable layer = 0
	
	Make /N=(1, DimSize(theMatrix, 1)) /O W_matrixColumnSum
	if (layers)
		Redimension /N=(-1, -1, DimSize(theMatrix, 2)) W_matrixColumnSum 
	endif
	
	W_matrixColumnSum = 0
	
	for (column = 0; column < DimSize(theMatrix, 1); column += 1)
		SetDimLabel 1, column, $(GetDimLabel($(NameOfWave(theMatrix)), 1, column)), W_matrixColumnSum 
	endfor
	if (layers)
		for (layer = 0; layer < DimSize(theMatrix, 2); layer += 1)
			SetDimLabel 2, layer, $(GetDimLabel($(NameOfWave(theMatrix)), 2, layer)), W_matrixColumnSum 
			for (column = 0; column < DimSize(theMatrix, 1); column += 1)
				SetDimLabel 1, column, $(GetDimLabel($(NameOfWave(theMatrix)), 1, column)), W_matrixColumnSum
			endfor
		endfor
	endif
	
	if (layers)
		for (layer = 0; layer < DimSize(theMatrix, 2); layer += 1)
			for (column = 0; column < DimSize(theMatrix, 1); column += 1)
				Make /N=(DimSize(theMatrix, 0)) /O tmp
				tmp = theMatrix[p][column][layer]
				W_matrixColumnSum[0][column][layer] = sum(tmp)
			endfor
		endfor
	else	
		for (column = 0; column < DimSize(theMatrix, 1); column += 1)
			Make /N=(DimSize(theMatrix, 0)) /O tmp
			tmp[] = theMatrix[p][column]
			W_matrixColumnSum[0][column] = sum(tmp)
		endfor
	endif
	Note /K W_matrixColumnSum, note(theMatrix)
	KillWaves /Z tmp
	return "W_matrixColumnSum"
End

Function ReplaceNumberByNumber(theWave, thisNumber, byThisNumber)
	Wave theWave
	Variable thisNumber, byThisNumber
	
	if (thisNumber == byThisNumber)
		return 0
	endif
	
	Variable rows = DimSize(theWave, 0)
	Variable columns = DimSize(theWave, 1)
	Variable layers = DimSize(theWave, 2)
	Variable chunks = DimSize(theWave, 3)
	
	Variable rowC = 0
	Variable columnC = 0
	Variable layerC = 0
	Variable chunkC = 0
	
	do
		layerC = 0
		do
			columnC = 0
			do
				rowC = 0
				do
					if (theWave[rowC] == thisNumber)
						theWave[rowC] = byThisNumber
					endif
					rowC += 1
				while(rowC < rows)
				columnC += 1
			while(columnC < columns)
			layerC += 1
		while(layerC < layers)
		chunkC += 1
	while(chunkC < chunks)
End

Function ReplaceNaNByNumber(theWave, Number)
	Wave theWave
	Variable Number
	
	Variable rows = DimSize(theWave, 0)
	Variable columns = DimSize(theWave, 1)
	Variable layers = DimSize(theWave, 2)
	Variable chunks = DimSize(theWave, 3)
	
	Variable rowC = 0
	Variable columnC = 0
	Variable layerC = 0
	Variable chunkC = 0
	
	do
		layerC = 0
		do
			columnC = 0
			do
				rowC = 0
				do
					if (numtype(theWave[rowC]) == 2)
						theWave[rowC] = Number
					endif
					rowC += 1
				while(rowC < rows)
				columnC += 1
			while(columnC < columns)
			layerC += 1
		while(layerC < layers)
		chunkC += 1
	while(chunkC < chunks)
End
#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.0
#include <Strings as Lists>

// by Roland Bock, August 2002
// created for SJKimAnalysis, as a general helper.

// **************************************************
// Function to detect the stimulus artefact in a wave and stores the timepoints
// of the found artefacts in a wave. This wave is overwritten with each execution.
// The range has to be given in the time scaling of the wave.
// The length of the artefact is given in seconds.
// The mode determines the direction of the peak
//  mode 		0	(default) positive peak of artefact
//				1	negative peak of artefact
// output		ArteFactWave
//				  containing the points above detectLimit in original wave
Function recognizeArtefact(theWave, searchStart, searchEnd, detectLimit, ArteFactLength, mode)
	String theWave
	Variable searchStart, searchEnd							// determines the range 
	Variable detectLimit
	Variable ArtefactLength									// length of artefact in seconds
	Variable mode = 0
	
	Variable index = 0, counter = 0, oldCounter = 0
	Variable scaling
	Variable pointsOfArtefact									// stores the number of points per artefact
	Variable neg = 0
	Variable beforeAF											// store the last point before artefact
	Variable tempEnd
	wave temp = $theWave

	// convert the time scaling into points
	scaling = deltax(temp)
	searchStart = ceil(searchStart / scaling)	
	searchEnd = ceil(searchEnd / scaling)
	tempEnd = searchEnd
	pointsOfArtefact = ceil(ArtefactLength / scaling) + 2
	Make/N=0/O ArtefactWave
	wave ArtefactWave

	if (mode)
		temp *= -1
	endif
	
	for (index = searchStart; index < searchEnd; index += 1)
		if (temp[index + 1] > (temp[index] + detectLimit))		// is next point above detect limit
			beforeAF = index											// store current point
			oldCounter = counter
			do															// walk through points until found one
				ArtefactWave[counter] = {index + 1}				//    below detectLimit and store all
				index += 1											//    points in a wave
				counter += 1
			while (((temp[beforeAF] + detectLimit) < temp[(index + 1)]) && ((counter - oldCounter) <= pointsOfArtefact))
			searchEnd = tempEnd + ArtefactWave[counter]
			if ((counter - oldCounter) > pointsOfArtefact)
				DeletePoints oldCounter, (counter - oldCounter), ArtefactWave
				counter = oldCounter
			endif
		endif
	endfor
		
	Duplicate/O ArtefactWave, ArtefactWave2
	Wave ArtefactWave2
	ArtefactWave2 += 1
	ArtefactWave2 *= scaling
	if (mode)
		temp *= -1
	endif
	return ArtefactWave	
End

// **************************************************
// Function to initialize the experiment and organize some data folders. It returns the number
// of artefacts it found.
// mode	0	do NOT delete helper wave setStorage (useful for list processing)
//			1	(default) delete helper wave setStorage
Function eliminateArtefact(theWave, points, mode)
	String theWave								// wave to work on 
	Wave points									// a wave containing the timepoints of the artefacts 
	Variable mode = 1

	Wave workWave = $theWave
	Variable index = 0, counter = 1, index2 = 0
	Variable newValue = 0, prevTime = 0, nextTime = 0
	Variable value1, value2						// store data values to determine the average 
	Variable reachedEnd							// value to end the while loop
	Variable numArt = 0							// number of artefacts
	
	if (numpnts(points) != 0)
		if (!exists("setStorage"))						// do not overwrite if exists
			make/N=1/O setStorage					// store the boundaries of the artefact	
			Wave setStorage
			setStorage = 0
			for (index = 0; index < numpnts(points); index += 1)
				if (points[index] > (points[index-1] + 1))
					setStorage[counter] = {index}
					counter += 1
				endif
			endfor
		endif
		
		numArt = numpnts(setStorage)
		index = 0
		counter = 0
	
		for (index = 0; index < numpnts(setStorage); index += 1)
			prevTime = points[setStorage[index]] - 1
			if ((index + 1) >= numpnts(setStorage))
				reachedEnd = rightx(points)
			else
				reachedEnd = setStorage[index + 1]
			endif
			nextTime = points[reachedEnd - 1] + 1
			value1 = workWave[prevTime]
			value2 = workWave[nextTime]
			newValue = (value2 + value1) / 2
			index2 = setStorage[index]
			do
				workWave[points[index2]] = newValue
				index2 += 1
			while(index2 < reachedEnd)
		endfor
	
		if (mode)
			KillWaves/Z setStorage
		endif
	endif
	return numArt
End

// **************************************************
Function eliminateArtefactFromList(List, PointWave, mode)
	String List, PointWave
	Variable mode
	
	String theWave												// contains the wave name as string
	Wave points = $PointWave									// wave that has the points of the artefacts
	Variable index = 0											// index for for-loop
	Variable items = ItemsInList(List)							// ending for for-loop
	
	for (index = 0; index < items; index += 1)
		theWave = GetStrFromList(List, index, ";")
		if (index < (items - 1))
			eliminateArtefact(theWave, points, mode)
		else
			eliminateArtefact(theWave,points, 1)				// delete the helper waves at last
		endif														//     iteration.
	endfor
	
End

// **************************************************
Function ShowTraceForArtefacts(theWave, AFWave)
	String theWave, AFWave
	
	Wave trace = $theWave
	Wave arte = $AFWave
	DoWindow ArtefactControl
	if (V_flag)							// baseline graph does not exist
		DoWindow/K ArtefactControl
	endif
	Display/K=1/M/W=(2,2,20,15) trace as "ArtefactControl"
	DoWindow/C ArtefactControl
	AppendToGraph/R oneswave vs arte
	ModifyGraph mode($theWave) = 4, mode(oneswave) = 1
	ModifyGraph rgb(oneswave) = (0,0,65535)
	ModifyGraph noLabel(right) = 2, axThick(right) = 0
	SetAxis bottom (arte[0] - 0.01), (arte[rightx(arte)] + 0.01)
	SetAxis right 0, 3
	
End

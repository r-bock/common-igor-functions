#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6.2
#pragma version = 1.2

// written by Roland Bock NIAAA / NIH
// ** version 1.2 (October 2018): added a function to quickly normalize all traces in a graph by one value
// ** version 1.1 (October 2018): added a new function 'createCumulativeWave' to make a defined 
//											size cumulative wave without the need to use 2 waves. Will be used
//											within the behavior analysis.
// ** version 1.0 (July 2009)

//   version 1.0 (November 2008)
Function /S makeCumulativeWaves(theDataName, theSortedName, theIndexName)
	String theDataName, theSortedName, theIndexName
	
	if (!WaveExists($theDataName))
		return ""
	endif
	
	Duplicate /O $theDataName, $theSortedName, $theIndexName
	Wave xValues = $theSortedName
	Wave yValues = $theIndexName
	Sort xValues, xValues						// sort the data
	WaveStats /W /Q xValues						// figure out if there are any NaNs or INFs
	Wave M_WaveStats
	
	Redimension /N=(M_WaveStats[0]) xValues, yValues	// throw the NaNs or INFs out
	yValues = p / (M_WaveStats[0] - 1)					// create the index

	KillWaves /Z M_WaveStats
	return theSortedName
end

// Create waves of a defined length (number of bins) by averaging the numbers 
// between the levels
// Returns the name of the wave containing the binned numbers
Function /S binCumulativeWaves(sortedDataName, indexName, [numBins])
	String sortedDataName, indexName
	Variable numBins
	
	if (ParamIsDefault(numBins))
		numBins = 200
	endif
	
	Wave data = $sortedDataName
	Wave indices = $indexName
	
	if (!WaveExists(data) || !WaveExists(indices))
		print "** ERROR: the wave " + sortedDataName + " or " + indexName + " does not exist. STOPPED!"
		return ""
	endif
	if (numBins <= 0)
		print "** ERROR: number of bins has to be a postive number greater than zero. STOPPED!"
		return ""
	endif
	
	String binnedWaveName = sortedDataName + "_bin"
	Make /N=0 /O $binnedWaveName
	Wave bins = $binnedWaveName
	Note /K bins, note($sortedDataName)
	
	WaveStats /W /Q indices
	Wave M_WaveStats
	
	Variable theMax = M_WaveStats[12]
	
	Variable binEdge = 0, prevEdge = 0
	for (binEdge = (theMax/numBins); binEdge <= theMax; binEdge += (theMax/numBins))
		FindLevel /P /Q indices, binEdge
		bins[numpnts(bins)] = {mean(data, ceil(prevEdge), floor(V_LevelX))}
		prevEdge = V_LevelX
	endfor
	
	KillWaves /Z M_WaveStats
	return binnedWaveName
End

// This function uses a list of waves to create the cumulative waves and a binned cumulative wave
// for them. It returns a list of the binned cumulative waves.
Function /S makeBinnedCumulFromWave(theList, [sortedPostFix, indexPostFix, numBins])
	String theList, sortedPostFix, indexPostFix
	Variable numBins
	
	if (strlen(theList) == 0)
		print "** ERROR: the list is empty, nothing to process. STOPPED!"
		return ""
	endif
	if (ParamIsDefault(sortedPostFix))
		sortedPostFix = "_srt"
	endif
	if (ParamIsDefault(indexPostFix))
		indexPostFix = "_ix"
	endif
	if (ParamIsDefault(numBins))
		numBins = 200
	endif
		
	String theResultList = ""
	String theWindowBaseName = "CumulativeWavesGraph"
	String theWindowName
	Variable index = 0
	
	for (index = 0; index < ItemsInList(theList); index += 1)
		String theDataName = StringFromList(index, theList)
		String theSortedName = theDataName + sortedPostFix
		String theIndexName = theDataName + indexPostFix
		makeCumulativeWaves(theDataName, theSortedName, theIndexName)
		theResultList = AddListItem(binCumulativeWaves(theSortedName, theIndexName, numBins=numBins), theResultList)
		if (index == 0)
			Display /N=$theWindowBaseName $theIndexName vs $theSortedName
			// S_name gets set by Display (only in functions) to the real window name.
			theWindowName = S_name
		else
			AppendToGraph /W=$theWindowName $theIndexName vs $theSortedName
		endif
	endfor
	Wave binned = $(StringFromList(0, theResultList))
	Make /N=(numpnts(binned)) /O All_binnedCumul_ix = p / (numpnts(binned) - 1)
	
	return theResultList
End


// - need to check if interp function exists in Igor 6 / 7, otherwise add compiler instructions
// This function creates equal point size cumulative waves so they can be averaged. 

// required parameters:
//	dataWave				the data wave
// resultEndX			a variable defining the x end scale of the result wave
// optional parameters:
// resultName			a string with the name of the new result wave. If it exists, it will be
//							overwritten - default: <dataWave name>_cuml
//	units					a string for the units, (default): "dat" for time units
//	size					a variable defining the total size of the result wave - default: 200
//	resultStartX			a variable defining the start x scale of the result wave - default: 0
//	type					a string either "raw" or "norm" (default)
//								norm:	each wave is scaled from 0 to 1
//								raw:	each index is scaled in whole integers from 1 to its maximum point number
// weights				a numerical wave containing 'weights' to each point in the data wave. Needs to be exact
//							same size as the data wave. 
Function /WAVE createCumulativeWave(dataWave, resultEndX, [size, resultStartX, resultName, units, type, weights])
	Wave dataWave, weights
	String resultName, units, type
	Variable size, resultStartX, resultEndX
	
	Wave result
	Variable keepRaw = 0, hasWeights = 0
	
	if (!WaveExists(dataWave))
		return result
	endif
	if (ParamIsDefault(size) || size <= 0)
		size = 200
	endif
	if (ParamIsDefault(resultStartX))
		resultStartX = 0
	endif
	if (ParamIsDefault(resultName) || strlen(resultName) == 0)
		resultName = NameOfWave(dataWave) + "_cuml"
	endif
	if (ParamIsDefault(units) || strlen(units) == 0)
		units = "dat"
	endif
	if (!ParamIsDefault(type) && strlen(type) > 0)
		strswitch (LowerStr(type))
			case "raw":
				keepRaw = 1
				break
			default:
				keepRaw = 0
		endswitch
	endif
	if (!ParamIsDefault(weights))
		hasWeights = WaveExists(weights) && numpnts(weights) == numpnts(dataWave)
	endif
	
	// make the result wave with the defined size
	DFREF df = GetWavesDataFolderDFR(dataWave)
	Make /N=(size) /O df:$resultName /WAVE=result
	SetScale /I x resultStartX, resultEndX, units, result
	
	// next - make an free index wave so we can interpolate but don't have to worry about cleaning up
	Variable maximum = 1
	Duplicate /FREE /O dataWave, cumulativeIndex

	if (hasWeights)
		cumulativeIndex[0] = weights[0]
		cumulativeIndex[1,] = cumulativeIndex[p - 1] + weights[p]
		
		if (keepRaw)
			maximum = sum(weights)
		else
			cumulativeIndex = cumulativeIndex / sum(weights)
		endif
	else
		if (keepRaw)
			cumulativeIndex = p + 1
			maximum = numpnts(cumulativeIndex)
		else
			cumulativeIndex = (p + 1) / numpnts(cumulativeIndex)
		endif
	endif
	
	result = interp(x, dataWave, cumulativeIndex)
	result = result[p] < 0 ? 0 : ( result[p] > maximum ? maximum : result[p] )
		
	return result
End





Function normalizeAll([theGraph])
	String theGraph
	
	if (ParamIsDefault(theGraph) || strlen(theGraph) == 0)
		theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
	endif
	if (strlen(theGraph) == 0)
		return 0
	endif
	if (WinType(theGraph) != 1)
		return 0
	endif
	
	String theList = TraceNameList(theGraph, ";", 1)
	Variable numWaves = ItemsInList(theList)
	Variable normVar = 0, index = 0
	
	// find the total max
	for (index = 0; index < numWaves; index += 1)
		normVar = max(normVar, WaveMax(TraceNameToWaveRef(theGraph, StringFromList(index, theList))))
	endfor
	
	// now normalize all
	for (index = 0; index < numWaves; index += 1)
		Wave data = TraceNameToWaveRef(theGraph, StringFromList(index, theList))
		data /= normVar
	endfor
	
	return 1
End
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.3
#pragma version = 1.16
#pragma ModuleName = TraceNavigator
#include "RBMinionsVersionControl"
#include "WindowHelper"
#include "ListHelpers"

// written by Roland Bock, June 2014
// * version 1.16 (AUG 2019): added the PanelResolution function to ensure correct display of the Panels but
//									maintain the compatibility with Igor 6 and lower
// * version 1.13 (JUN 2014)


#if Exists("PanelResolution") != 3
static Function PanelResolution(wName)		//compatibility with Igor 7
	String wName
	return 72
End
#endif

// ********************       C O N S T A N T S         ******************

static StrConstant ksWin = "Windows"
static StrConstant ksMac = "Macintosh"
static Strconstant ksPackageName = "RB_TraceNavigator"
static Constant kVersion = 1.15

StrConstant ks_RBTN_PanelName = "TN_NavigatorPanel"
static StrConstant ksPanelTitel = "Trace Navigator"
static StrConstant ksMaxTextBox = "maxText"

Constant k_RBTN_PanelDist = 2
static Constant kPanelMinWidth = 100
static Constant kPanelMaxwidth = 250

static StrConstant ksTraceListPrefix = "traceList_"
static StrConstant ksSelTraceListPrefix = "selTraceList_"
static StrConstant ksTagPrefix = "tgMax_"
static StrConstant ksFillerFuncNamePrefix = "fillFunc_"

static StrConstant ksErrMsg = "** ERROR (%s): %s.\r"

static StrConstant ksTagUDName = "tagList"
static StrConstant ksMaxTagKey = "MAXNUM"
 
#if defined(MACINTOSH)
Strconstant ks_RBTN_PanelFont = "Verdana"
#else
Strconstant ks_RBTN_PanelFont = "Arial"
#endif

// *******************      M E N U             ************************
Menu "Macros"
	SubMenu "Trace Navigation"
		"Add navigator to top graph", TN_addNavPanel2TopGraph()
	End
End

// **********     F O R    D E V E L O P M E N T    O N L Y    ***********

static Function updateVersion()
	VCheck#setVersion(ksPackageName, kVersion)
End

static Function AfterCompileHook()
	RBVC_checkVersion(ksPackageName, kVersion)
	return 0
End

// *******************      S T A T I C S         ************************

static Function /DF getPackage()
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = root:Packages:$ksPackageName
	if (DatafolderRefStatus(package) != 1)
		if (DatafolderRefStatus(package) == 2)
			KillDatafolder /Z package
		endif
		SetDatafolder root:
		if (!DatafolderExists("Packages"))
			NewDatafolder /S Packages
		else
			SetDatafolder Packages
		endif
		NewDatafolder /S $ksPackageName
		DFREF package = GetDatafolderDFR()
	endif
	SetDatafolder cDF
	return package
End	

static Function /WAVE getListWave(postfix)
	String postfix
	
	DFREF package = getPackage()
	String wName = ksTraceListPrefix + postfix
	
	Wave /T aWave = package:$wName
	if (!WaveExists(aWave))
		Make /N=0/T package:$wName /WAVE=aWave
	endif
	return aWave
End

static Function /WAVE getListSelWave(postfix)
	String postfix
	
	DFREF package = getPackage()
	String wName = ksSelTraceListPrefix + postfix
	
	Wave aWave = package:$wName
	if (!WaveExists(aWave))
		Make /N=0 package:$wName /WAVE=aWave
	endif
	return aWave
End

static Function cleanUpAfterKill(theWin)
	String theWin
	
	String theGraph = getHostWinName(theWin)
	Wave /T lbWave = getListWave(theGraph)
	Wave selWave = getListSelWave(theGraph)
	KillWaves lbWave, selWave
	setTagList(theGraph, "")
	String anList = AnnotationList(theGraph)
	Variable index, numItems = ItemsInList(anList)
	for (index = 0; index < numItems; index += 1)
		String anName = StringFromList(index, anList)
		if (cmpstr(anName[0,5], ksTagPrefix) == 0)
			Tag /K/N=$anName/W=$theGraph
		endif
	endfor
	removeFillerFuncVar(theGraph, kill=1)
	
	ModifyGraph /W=$theGraph rgb=(0,0,0)
	SetWindow $(theGraph), hook(modification)=$""
	TextBox /W=$theGraph/K/N=$ksMaxTextBox
	return 1
End

static Function /WAVE fillListWave(lbw, theGraph)
	Wave /T lbw
	String theGraph
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	
	String theList = SortList(TraceNameList(theGraph, ";", 1), ";", 16)
	SetDatafolder package
	String name = makeTWaveFromList(theList)
	Wave listWave = $name
	Duplicate /O/T listWave, lbw
	SetDatafolder cDF
	return lbw
End

static Function /S getTagList(theGraph)
	String theGraph
	
	DoWindow $theGraph
	if (V_flag == 0)
		return ""
	endif
	
	return GetUserData(theGraph, "", ksTagUDName)
End

static Function /S setTagList(theGraph, theList)
	String theGraph, theList
	
	DoWindow $theGraph
	if (V_flag == 0)
		return ""
	endif
	SetWindow $theGraph, userdata($ksTagUDName)=theList
	return theList
End

static Function getMaxTagNum(theGraph)
	String theGraph
	
	String theList = getTagList(theGraph)
	if (strlen(theList) == 0)
		return 0
	endif
	Variable num = NumberByKey(ksMaxTagKey, theList)
	return numtype(num) == 2 ? 0 : num
End	

static Function /S setMaxTagNum(theGraph, num)
	String theGraph
	Variable num
	
	String theList = getTagList(theGraph)
	theList = ReplaceNumberByKey(ksMaxTagKey, theList, num)
	return setTagList(theGraph, theList)
End


static Function /S addTagItem(theGraph, theTrace, theName)
	String theGraph, theTrace, theName
	Variable number
	
	String theList = getTagList(theGraph)
	Variable maxNum = getMaxTagNum(theGraph)
	Variable underscore = strsearch(theName, "_", Inf, 1)
	Variable tagNum = str2num(theName[underscore + 1, strlen(theName) - 1])
	if (maxNum < tagNum)
		maxNum = tagNum
	endif
	theList = setMaxTagNum(theGraph, maxNum)
	theList = ReplaceStringByKey(theTrace, theList, theName)
	return setTagList(theGraph, theList)
End

static Function /S getNextTagName(theGraph)
	String theGraph
	
	Variable maxNum = getMaxTagNum(theGraph)
	return ksTagPrefix + num2str(maxNum + 1)
End

static Function /S updateMaxTagNumFromList(theList)
	String theList
		
	Variable numItems = ItemsInList(theList)
	if (numItems == 0)
		return ""
	endif
	
	String lastItem = StringFromList(numItems - 1, theList)
	Variable underscore = strsearch(lastItem, "_", Inf, 1)
	if (underscore < 0)
		return ""
	endif
	
	lastItem = lastItem[underscore + 1, strlen(lastItem) - 1]
	theList = ReplaceStringByKey(ksMaxTagKey, theList, lastItem)
	return theList
End


static Function /S addMaxTag(theGraph, theTrace)
	String theGraph, theTrace
	
	String tagName = ""
	DoWindow $theGraph
	if (V_flag == 0)
		return tagName
	endif
	Wave aTrace = TraceNameToWaveRef(theGraph, theTrace)
	if (!WaveExists(aTrace))
		return tagName
	endif
	
	String theList = getTagList(theGraph)
	tagName = StringByKey(theTrace, theList)
	if (strlen(tagName) == 0)
		tagName = getNextTagName(theGraph)
	endif
	
	Variable location = NumberByKey("MAXLOC", calcSimpleStats(theTrace, theGraph))
	
	Tag /C/N=$tagName/W=$theGraph/F=2/L=2 $theTrace, location, "\\OY"
	
	return addTagItem(theGraph, theTrace, tagName)
End

static Function /S removeMaxTag(theGraph, theTrace)
	String theGraph, theTrace
	
	DoWindow $theGraph
	if (V_flag == 0)
		return ""
	endif
	
	String theList = getTagList(theGraph)
	if (strlen(theList) == 0)
		return ""
	endif
	String tagName = StringByKey(theTrace, theList)
	if (strlen(tagName) > 0)
		Tag /K/N=$tagName/W=$theGraph
	endif
	theList = RemoveByKey(theTrace, theList)
	theList = updateMaxTagNumFromList(theList)
	theList = setTagList(theGraph, theList)
	
	return theList
End	

static Function adjustPanelElements2Height(width, height, theGraph, [topBorder, bottomBorder])
	Variable width, height, topBorder, bottomBorder
	String theGraph
	
	if (ParamIsDefault(topBorder) || topBorder < 0)
		topBorder = 5
	endif
	if (ParamIsDefault(bottomBorder) || bottomBorder < 0)
		bottomBorder = 0
	endif
	
	Variable leftOffSet = 5
	Variable topOffSet = topBorder
	Variable elementWidth = width - 10
	Variable elementHeight = height - topOffSet - bottomBorder - 35
	ListBox lb_wavelist win=$theGraph#$ks_RBTN_PanelName, pos={leftOffSet,topOffSet}, size={elementWidth, elementHeight}
	elementWidth = 50
	leftOffSet = (width - elementWidth) / 2
	if (leftOffSet + elementWidth + 30 + 5 >= width)
		leftOffSet = (width - 10) - 85
	endif
	topOffSet = elementHeight + topBorder + 5
	elementHeight = 22
	Button btn_close win=$theGraph#$ks_RBTN_PanelName, pos={leftOffSet, topOffSet}, size={elementWidth, elementHeight}
	leftOffSet += elementWidth + 5
	elementWidth = 30
	Checkbox cb_tag win=$theGraph#$ks_RBTN_PanelName, pos={leftOffSet, topOffSet + 5}, size={elementWidth, elementHeight} 
	return 1
End

static Function /S calcSimpleStats(theTrace, theGraph)
	String theTrace, theGraph
	
	String resultFormat = "MAX:%g;MAXLOC:%g;MIN:%g;MINLOC:%g;"
	String result = ""
	
	DoWindow $theGraph
	if (V_flag == 0)
		return result
	endif
	
	Wave aTrace = TraceNameToWaveRef(theGraph, theTrace)
	if (!WaveExists(aTrace))
		return result
	endif
		
	Variable aExists = strlen(CsrInfo(A, theGraph)) > 0
	Variable bExists = strlen(CsrInfo(B, theGraph)) > 0
	
	Variable toStart = 0, toEnd = numpnts(aTrace)
	
	if (aExists)
		toStart = pcsr(A, theGraph)
	endif
	if (bExists)
		toEnd = pcsr(B, theGraph)
	endif
	
	WaveStats /Q/M=1/C=1/W/R=[toStart, toEnd] aTrace
	Wave M_WaveStats
	
	sprintf result, resultFormat, M_WaveStats[%max], M_WaveStats[%maxLoc], M_WaveStats[%min], M_WaveStats[%minLoc]
	return result
End

static Function /S getFillerFunctionVar(theGraph)
	String theGraph
		
	DFREF package = getPackage()
	
	SVAR fillerFuncName = package:$(ksFillerFuncNamePrefix + theGraph)
	if (!SVAR_exists(fillerFuncName))
		String /G package:$(ksFillerFuncNamePrefix + theGraph)= "TraceNavigator#fillListWave"
	endif
	return GetDatafolder(1, package) + (ksFillerFuncNamePrefix + theGraph)
End

static Function /S getFillerFunction(theGraph)
	String theGraph
	
	SVAR funcName = $(getFillerFunctionVar(theGraph))
	return funcName
End

static Function /S setFillerFunction(theGraph, theFuncName)
	String theGraph, theFuncName
	
	if (strlen(theFuncName) == 0)
		return ""
	endif
//	if (WhichListItem(theFuncName, FunctionList("*", ";", "KIND:2,NPARAMS:2,VALTYPE:8")) < 0)
//		return ""
//	endif
	
	SVAR fillerFunc = $(getFillerFunctionVar(theGraph))
	fillerFunc = theFuncName
	return theFuncName
End

static Function removeFillerFuncVar(theWin, [kill])
	String theWin
	Variable kill
	
	if (ParamIsDefault(kill) || kill < 0)
		kill = 0
	else
		kill = kill > 1 ? 1 : round(kill)
	endif
	
	String theGraph = getHostWinName(theWin)
	DoWindow $(theWin)
	if (V_flag > 0 && !kill)
		// window exists, so don't delete
		printf ksErrMsg, "removeFillerFuncVar", "the graph window '" + theGraph + "' has still the navigator panel attached. Can't delete!\r"
		return 0
	endif
	
	DFREF package = getPackage()
	SVAR fillerVar = package:$(ksFillerFuncNamePrefix + theGraph)
	if (SVAR_Exists(fillerVar))
		KillStrings /Z fillerVar
	endif
	return 1
End

static Function /S putTracesOnNewGraph(list, theGraph)
	String list, theGraph
	
	String graphName = ""
	Variable numTraces = ItemsInList(list)
	Variable index
	for (index = 0; index < numTraces; index += 1)
		if (index == 0)
			Display
			graphName = S_name
		endif
		Wave data = TraceNameToWaveRef(theGraph, StringFromList(index, list))
		AppendToGraph /W=$graphName /C=(0,0,0) data
	endfor
	return graphName
End

Function /WAVE RBTN_Prototype_fillListWave(lbw, theGraph)
	Wave /T lbw
	String theGraph
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	
	return lbw
End

// *************    H O O K    F U N C T I O N S    *********************

Function RBTN_reactToPanelEvents(w)
	STRUCT WMWinHookStruct &w
	
	Variable hookResult = 0
	
	switch (w.eventCode)
		case 2:
			hookResult = cleanUpAfterKill(getHostWinName(w.winName))
			break
		case 18:
			HideTools /A
			SetActiveSubWindow ##
			ShowTools /A
			hookResult = 1
			break		
		case 20:
			HideInfo
			SetActiveSubWindow ##
			ShowInfo
			hookResult = 1
			break		
	endswitch
	return hookResult
End

Function RBTN_adjustToGraphMod(w)
	STRUCT WMWinHookStruct &w
	
	Variable hookResult = 0 
	
	switch (w.eventCode)
		case 2:
			hookResult = cleanUpAfterKill(w.winName)
			break
		case 14:
			GetWindow $w.winName activeSW
			String subwindow = S_value
			if (cmpstr(subwindow, ks_RBTN_PanelName) == 0)
				hookResult = cleanUpAfterKill(w.winName)
			endif
			break
	endswitch
	return hookResult
End

Function RBTN_adjustPanelElements(w)
	STRUCT WMWinHookStruct &w
	
	Variable hookResult = 0
	
	switch (w.eventCode)
		case 6:						// resize
			GetWindow $w.winName wsize
			Variable height = V_bottom - V_top
			GetWindow $w.winName#$ks_RBTN_PanelName wsize
			Variable width = V_right - V_left
			MoveSubWindow /W=$w.winName#$ks_RBTN_PanelName fnum=(k_RBTN_PanelDist, 0, width, height)
			adjustPanelElements2Height(width * (ScreenResolution/PanelResolution(w.winName + "#" + ks_RBTN_PanelName)), height * (ScreenResolution/PanelResolution(w.winName + "#" + ks_RBTN_PanelName)), w.winName)
			hookResult = 1
			break
		case 13:					// renamed			
			DFREF package = getPackage()
			Wave /T oldLbWave = package:$(ksTraceListPrefix + w.oldWinName)
			Wave oldSelWave = package:$(ksSelTraceListPrefix + w.oldWinName)
			Wave /T lbWave = getListWave(w.winName)
			Wave selWave = getListSelWave(w.winName)

			Duplicate /O /T oldLbWave, package:$(NameOfWave(lbWave)) /WAVE=lbWave
			Duplicate /O oldSelWave, package:$(NameOfWave(selWave)) /WAVE=selWave
			ListBox lb_wavelist win=$w.winName#$ks_RBTN_PanelName, selWave=selWave, listWave=lbWave

			SVAR fillFunc = $getFillerFunctionVar(w.oldWinName)
			SVAR newFillFunc = $getFillerFunctionVar(w.winName)
			newFillFunc = fillFunc

			KillStrings /Z fillFunc
			KillWaves /Z oldLbWave, oldSelWave
			hookResult = 1
			break
	endswitch
	return hookResult
End

// *******************      P A N E L         ************************

Function TN_addNavPanel2TopGraph()

	String theGraph = StringFromList(0, WinList("*", ";", "WIN:1"))
	TN_addNavigatorPanel(theGraph)
End


Function /S TN_addNavigatorPanel(theGraph)
	String theGraph
	
	DoWindow $theGraph
	if (V_flag == 0)
		// no window of the name exits
		printf ksErrMsg, "TN_addNavigatorPanel", "'" + theGraph + "' doesn't exist. Need an existing window to attach."
		return ""
	endif
	if (V_flag == 2)
		DoWindow /F $theGraph
	endif
	if (WinType(theGraph) != 1)
		printf ksErrMsg, "TN_addNavigatorPanel", "can only attach to a graph. " + theGraph + "is not a graph"
		return ""
	endif
	
	String subwinList = ChildWindowList(theGraph)
	if (WhichListItem(ks_RBTN_PanelName, subwinList) >= 0)
		// panel already exists, so stop here
		return ""
	endif
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
			
	String winInfo = getPixelsFromWindow(theGraph)
	Variable height = NumberByKey("HEIGHT", winInfo)
	Variable width = kPanelMinWidth
		
	NewPanel /K=2/EXT=0 /HOST=$theGraph /W=(k_RBTN_PanelDist, 0, width, height) /N=$ks_RBTN_PanelName as ksPanelTitel
	String panelName = S_name
	
	SetWindow $theGraph, hook(modification)=RBTN_adjustToGraphMod
	SetWindow $theGraph, hook(resize)=RBTN_adjustPanelElements
	SetWindow $theGraph#$panelName, hook(modification)=RBTN_reactToPanelEvents
	
	Wave /T lbWave = getListWave(theGraph)
	Wave selWave = getListSelWave(theGraph)
	
	SVAR fillerFuncName = $(getFillerFunctionVar(theGraph))
	FUNCREF RBTN_Prototype_fillListWave filler = $fillerFuncName
	
	filler(lbWave, theGraph)
	Redimension /N=(numpnts(lbWave)) selWave
	selWave = 0
	
	Duplicate /O selWave, package:tmp /WAVE=tmp
	tmp = strlen(lbWave[p])
	WaveStats /Q/C=1/W/M=1 tmp
	Wave M_WaveStats
	Variable fontWidth = FontSizeStringWidth(ks_RBTN_PanelFont, 10, 0, lbWave[M_WaveStats[%maxLoc]])
	width = fontWidth + 25 + 10	// add scroll frame and panel distance
	width = width < kPanelMinWidth ? kPanelMinWidth : width
	width = width > kPanelMaxWidth ? kPanelMaxWidth : width
	MoveSubWindow /W=$theGraph#$panelName fnum=(k_RBTN_PanelDist, 0, width * (72/Screenresolution), height)
	
	ListBox lb_wavelist win=$theGraph#$panelName, font=$ks_RBTN_PanelFont, fsize=10
	ListBox lb_wavelist win=$theGraph#$panelName, mode=4, selWave=selWave, listWave=lbWave
	ListBox lb_wavelist win=$theGraph#$panelName, proc=lbFunc_showTrace
	
	Button btn_close win=$theGraph#$panelName, title="close", proc=btnFunc_close, font=$ks_RBTN_PanelFont, fsize=12
	
	Checkbox cb_tag win=$theGraph#$panelName, title="tag", font=$ks_RBTN_PanelFont, fsize=10, value=0
	Checkbox cb_tag win=$theGraph#$panelName, proc=cbFunc_tag
	
	adjustPanelElements2Height(width, height, theGraph)
	return panelName
End

// ********************     P A N E L    F U N C T I O N S     *************************

Function lbFunc_showTrace(lb) : ListboxControl
	STRUCT WMListboxAction &lb
	
	String theGraph = "", selWaves = ""
	Variable index = 0
	switch (lb.eventCode)
		case 1:				// mouse down
			if (lb.eventMod & 0x10)		// right mouse click
				theGraph = getHostWinName(lb.win)
				String popupItems = "graph traces;remove traces;"
				
				PopupContextualMenu popupItems
				strswitch (S_selection)
					case "graph traces":
						selWaves = ReplaceString(",", StringByKey("WAVES", lb.userdata), ";")
						putTracesOnNewGraph(selWaves, theGraph)
						Wave /T lw = lb.listWave
						Wave sw = lb.selWave
						sw = 0
						for (index = 0; index < numpnts(lw); index += 1)
							if (WhichListItem(lw[index], selWaves) >= 0)
								sw[index] = 1
							endif
						endfor
						return 1
						break
					case "remove traces":
						selWaves = ReplaceString(",", StringByKey("WAVES", lb.userdata), ";")
						Variable numItems = ItemsInList(selWaves)
						Wave /T lw = lb.listWave
						Wave sw = lb.selWave
												
						Extract /FREE /INDX /O sw, selection, sw == 1
						
						for (index = 0; index < numItems ; index += 1)							
							RemoveFromGraph /W=$theGraph $(StringFromList(index, selWaves))
						endfor
						
						FUNCREF RBTN_Prototype_fillListWave filler = $(getFillerFunction(theGraph))
						filler(lw, theGraph)
						Redimension /N=(numpnts(lw)) sw
						
						Variable firstElement = 0
						if (numpnts(selection) == 0)
							firstElement = 0
						else
							firstElement = selection[0] - 1
							if (firstElement < 0)
								firstElement = 0
							elseif (firstElement >= numpnts(sw))
								firstElement = numpnts(sw) - 1
							endif
						endif
						
						sw = 0
						sw[firstElement] = 1
						ModifyGraph /W=$theGraph rgb($lw[firstElement])=(65535,0,0)
						break
				endswitch
			endif
			break
		case 2:			// mouse up
		case 4:			// cell selection (mouse or arrow keys)
		case 5:			// cell selection plus shift key
			if (lb.eventMod & 0x10)
				return 1
			endif
			theGraph = getHostWinName(lb.win)
			Wave /T lw = lb.listWave
			Wave sw = lb.selWave
			Variable numTraces = numpnts(sw)
			ControlInfo /W=$lb.win cb_tag
			Variable tags = V_value
			
			String textLine = "%s: %g\r"
			String maxTextLine = "", maxText = ""
			
			for (index = 0; index < numTraces; index += 1)
				if (sw[index] == 0)
					ModifyGraph /W=$theGraph rgb($lw[index])=(0,0,0)
					removeMaxTag(theGraph, lw[index])
				else
					ModifyGraph /W=$theGraph rgb($lw[index])=(65535, 0, 0)
					selWaves = AddListItem(lw[index], selWaves, ",", Inf)
					Variable maximum = NumberByKey("MAX", calcSimpleStats(lw[index], theGraph))
					
					String traceList = TraceNameList(theGraph, ";", 1)
					Variable num = ItemsInList(traceList)
					String lastTrace = StringFromList(num - 1, traceList)
					ReorderTraces /W=$theGraph $lastTrace, {$lw[index]}
					ReorderTraces /W=$theGraph $lw[index], {$lastTrace}
					
					sprintf maxTextLine, textLine, lw[index], maximum
					maxText += maxTextLine
					if (tags)
						addMaxTag(theGraph, lw[index])
					endif
				endif
			endfor
			if (strlen(maxText) > 0)
				maxText = RemoveEnding(maxText)
				TextBox /C/N=$ksMaxTextBox/W=$theGraph/A=RT/X=0.00/Y=0.00/F=2 maxText
			else
				TextBox /K/N=$ksMaxTextBox/W=$theGraph
			endif
			lb.userdata = ReplaceStringByKey("WAVES", lb.userdata, selWaves)			
			break
	endswitch
	return 1
End

Function cbFunc_tag(cb) : CheckboxControl
	STRUCT WMCheckboxAction &cb
	
	switch (cb.eventCode)
		case 2:				// mouse button up
			String theGraph = getHostWinName(cb.win)
			String theList = getTagList(theGraph)
			String traceList = TraceNameList(theGraph, ";", 1)
			Variable numTraces = ItemsInList(traceList)
			
			String traceName = "", tagName = ""
			Variable index = 0
			
			
			if (cb.checked)
				DFREF package = getPackage()
				Wave /T lb = package:$(ksTraceListPrefix + theGraph)
				Wave sw = package:$(ksSelTraceListPrefix + theGraph)
				
				Extract /FREE /INDX /O sw, selector, sw == 1
				for (index = 0; index < numpnts(selector); index += 1)
					theList = addMaxTag(theGraph, lb[selector[index]])
				endfor
			else
				if (strlen(theList) == 0)
					return 0
				endif
				for (index = 0; index < numTraces; index += 1)
					traceName = StringFromList(index, traceList)
					tagName = StringByKey(traceName, theList)
					if (strlen(tagName) > 0)
						Tag/K/N=$tagName/W=$theGraph
					endif
				endfor
				theList = ""
				setTagList(theGraph, theList)
			endif
			break
	endswitch
	return 1
End

Function btnFunc_close(b) : ButtonControl
	STRUCT WMButtonAction &b
	
	switch (b.eventCode)
		case 2:
//			cleanUpAfterKill(b.win)
			KillWindow $b.win
			break
	endswitch
	return 1
End

Function btnFunc_toGraph(b) : ButtonControl
	STRUCT WMButtonAction &b
	
	switch (b.eventCode)
		case 2:
			String theGraph = getHostWinName(b.win)
			String waves = ReplaceString(",", StringByKey("WAVES", b.userdata), ";")
			Variable numWaves = ItemsInList(waves)
			String newName = ""
			Variable index
			for (index = 0; index < numWaves; index += 1)
				Wave data = TraceNameToWaveRef(theGraph, StringFromList(index, waves))
				if (index == 0)
					Display data
					newName = S_name
					ModifyGraph /W=$newName rgb=(0,0,0)
				else
					AppendToGraph /W=$newName/C=(0,0,0) data
				endif
			endfor
			break
	endswitch
	return 1
End
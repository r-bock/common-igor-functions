#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6
#pragma version = 1.5
#include "CreatingGraphs"

// by Roland Bock, version 1.5 updated August 2010 using Igor 6 features.
// build on procedures and functions previously made for EOG and Spike analysis, extened 
// with function to find the turning point between two peaks
//
// version 1.2 (August 2002):
// extended feature by a simple function to plot the peak for a given time window of all waves
// of one graph. 

// additional entry into the macro menue, calls one procedure
Menu "Macros"
	"Manual peak analysis...", ManualPeak(,,,1,)
End

// get initial variable setting and call the GetPeaksFromWaves function. The result is a 
// wave "ManualPeakAnalysis" which will displayed in a graph.
Proc ManualPeak(startTime, endTime, scaling, mode, source)
	Variable startTime, endTime
	Variable scaling
	Variable mode
	String source
	Prompt startTime, "Please enter the start time:"
	Prompt endTime, "Please enter the end time:"
	Prompt scaling, "Enter the time interval in seconds between the traces:"
	Prompt mode, "Enter 1 for negative peaks and 0 for positive peaks:"
	Prompt source, "Choose which waves you want to look at:", popup, "top window;all"
	
	String listOption = ""
	if (cmpstr(source, "top window") == 0)
		listOption = "WIN:"
	else
		listOption = ""
	endif
	String List = WaveList("*", ";", listOption)
	if (strlen(List) == 0)
		print "-> Did not find any waves to work with. Stopped!"
		return 0
	endif
	List = SortList(List, ";", 16)
	GetPeaksFromWaves(List, startTime, endTime, scaling, mode)
	ShowManualPeakAnalysis()
End
// ********************************
// simple function to calculate the relation between two points. If the divisor is 0 
// the returned result is also zero.
// mode	0	return Value1/Value2 (default)
//			1	return Value2/Value1
Function GetPointRelation(Value1, Value2, mode)
	Variable Value1, Value2
	Variable mode
	
	Variable result = 0
	if (!mode && (Value1 != 0))						// check for div by 0
		result = Value2/Value1
	else
		if (mode && (Value2 !=0))					// check for div by 0
			result = Value1/Value2
		endif
	endif
	return result
End


// **************************************************
// Function to find the turning point between slow and fast EPSC. Returns the x point (location) of 
// the found turning point. The x location is determined by a line fit from the local maximum of 
// the first derivative (decay slope of the first peak) to the given range. The location when the fitted
// line crosses zero is returned as x location of turning point.
// mode	0	the turning point is a maximum (first derivative from + -> -
//			1	the turning point is a minimum (first derivative from - -> +
Function FindTurningPoint(theWave, startTime, endTime, range, mode)
	String theWave									// the wave name to work on 
	Variable startTime, endTime						// start and end time for searching 
	Variable range									// range for fitting the line
	Variable mode
	
	Variable TurningPoint = 0
	Wave currentWave = $theWave
	Variable scaling = deltax(currentWave)			// get scaling to become independent from time
		
	Duplicate/O currentWave, temp								// work on duplicate not original
	if (mode)
		temp *= -1												// flip for negative turning points
	endif
	Smooth 10, temp												// reduce noise (only interested in trend)
	Differentiate temp											// differentiate
	WaveStats/Q/R=(startTime, endTime) temp				// find local maxium
	CurveFit/Q/N line temp(V_maxloc, (V_maxloc + range))	// fit line from maxium to range
	Wave W_coef
	if (W_coef[1] != 0)
		TurningPoint = -(W_coef[0]/W_coef[1])				// calculate turning point from fit x = -a/b
	endif
	Killwaves/Z temp, W_coef, W_sigma						// get rid of temporary wave
	return TurningPoint								// return turning point
End


// **************************************************
// Extened function to process list of waves searching for the turning point in each one of 
// them. The output is a wave named "SeparationPoints" storing the turning point for each
// single wave.
Function GetAllTurningPoints(List, startTime, endTime, range, mode)
	String List
	Variable startTime, endTime, range
	Variable mode
	
	String theWave
	Variable index = 0
	Make /N=(ItemsInList(List)) /O SeparationPoints = NaN   // empty all original values
	
	for (index = 0; index < ItemsInList(List); index += 1)
		theWave = StringFromList(index, List)
		SeparationPoints[index] = FindTurningPoint(theWave, startTime, endTime, range, mode)
	endfor
End


// **************************************************
// Calculate the peak in the given time window for all waves in the list. The default peak is 
// a negative peak. 
// mode:		0	positive peak
//				1	negative peak
Function GetPeaksFromWaves(List, startTime, endTime, scaling, mode)
	String List
	Variable startTime, endTime, scaling
	Variable mode
	
	String theWave
	Variable index = 0
	Make /N=(ItemsInList(List)) /O ManualPeakAnalysis = NaN
	Wave ManualPeakAnalysis
	
	for (index = 0; index < ItemsInList(List); index += 1)
		theWave = StringFromList(index, List)
		WaveStats/Q/R=(startTime, endTime) $theWave
		if (!mode)
			V_min = V_max
		endif 
		ManualPeakAnalysis[index] = V_min
	endfor
	SetScale/P x, 0, scaling, "s", ManualPeakAnalysis
End
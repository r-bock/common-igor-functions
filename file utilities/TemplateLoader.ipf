#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma IgorVersion = 6.3
#pragma ModuleName = TemplateLoader
#pragma version = 1
#include "RBMinionsVersionControl", version >= 2.4
#include "WindowHelper", version >= 2.7
#include "TimeDateConversions", version >= 2.4

// written by Roland Bock, February 2015
// * version 1 (FEB 2015)

static Constant kVersion = 1
static StrConstant ksErrorMsg = "** ERROR (%s): %s\r"
static StrConstant ksWarnMsg = "* -- WARNING (%s): %s\r"

static StrConstant ksPackageName = "RM Template Loader"
static StrConstant ksPrefsFileName = "RMTemplateLoader.bin"
static Constant kPrefsVersion = 100			// means 1.00

static StrConstant ksPanelName = "RM_TemplateChooserPanel"
static StrConstant ksPanelTitle = "Template Chooser"

static StrConstant tPathName = "RM_templatePathName"

#ifdef MACINTOSH
	static StrConstant ksPanelFont = "Helvetica"
#else
	static StrConstant ksPanelFont = "Arial"
#endif

// ********************************************************
//                          M E N U
// ********************************************************
Menu "Macros"
	"Set template experiment ...", TemplateLoader#displayPanel()
End

// ********************************************************
//                      S T R U C T U R E S 
// ********************************************************

Structure TemplateLoadPrefs
	uint32 version
	int16  default
	char   templateExtension[4]
	char   templateName[60]
	char   templatePath[400]
EndStructure

// ********************************************************
//        P R E F E R E N C E    H A N D L I N G 
// ********************************************************

// this function sets the current default template based on 
// a package name
static Function getRecordID([package])
	String package
	
	Variable id = 0
	if (ParamIsDefault(package) || strlen(package) == 0)
		return id
	endif
	
	strswitch (LowerStr(package))
		case "voltammetry":
			id = 1
			break
		default:
			id = 0
	endswitch
	
	return id
End

static Function loadPrefs(prefs, [package])
	STRUCT TemplateLoadPrefs &prefs
	String package
	
	Variable recordID
	if (ParamIsDefault(package))
		recordID = getRecordID()
	else
		recordID = getRecordID(package=package)
	endif
	
	Variable success = 1
	LoadPackagePreferences ksPackageName, ksPrefsFileName, recordID, prefs
	if (V_flag != 0 || V_bytesRead == 0 || prefs.version != kPrefsVersion)
		defaultPrefs(prefs, setDefault = 1)		
		success = 0
	endif
	return success
End

static Function savePrefs(prefs, [package])
	STRUCT TemplateLoadPrefs &prefs
	String package
	
	Variable recordID
	if (ParamIsDefault(package))
		recordID = getRecordID()
	else
		recordID = getRecordID(package=package)
	endif
	
	SavePackagePreferences ksPackageName, ksPrefsFileName, recordID, prefs
End

static Function defaultPrefs(prefs, [setDefault])
	STRUCT TemplateLoadPrefs &prefs
	Variable setDefault

	setDefault = ParamIsDefault(setDefault) || setDefault == 1 ? 1 : 0

	prefs.version = kPrefsVersion
	prefs.default = setDefault == 1
	prefs.templateExtension = "pxp"
	prefs.templateName = ""
	prefs.templatePath = SpecialDirPath("Documents", 0, 0, 0)
End

// ***************** V E R S I O N    C H E C K E R   ***********

static Function updateVersion()
	VCheck#setVersion(ksPackageName, kVersion)
End

static Function AfterCompileHook()
	RBVC_checkVersion(ksPackageName, kVersion)
	return 0
End

// ***************************************************************
//                  C H O O S E R    P A N E L 
// ***************************************************************

static Function displayPanel()

	DoWindow $ksPanelName
	if (V_flag > 0)
		KillWindow $ksPanelName
	endif
	createPanel(ksPanelName, title=ksPanelTitle)
	return 1
End

static Function createPanel(name, [title])
	String name, title
	
	if (ParamIsDefault(title) || strlen(title) == 0)
		title = ksPanelTitle
	endif
	
	if (strlen(name) == 0)
		name = ksPanelName
		title = ksPanelTitle
	endif
	
	DoWindow $name
	if (V_flag > 1)
		KillWindow $name
	endif
	
	Variable maxWidth = 0, maxHeight = 0
	Variable leftOffset = 10, secLeftOffset = 0, topOffset = 10
	Variable dSpace = 5
	Variable cWidth = 0, cHeight = 20
	
	NewPanel /W=(0, 0, 100, 100) /N=$name /K=1 as title
	
	cWidth = 200
	PopupMenu pu_packageChooser, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, bodyWidth=150
	PopupMenu pu_packageChooser, win=$name, title="package", fSize=12, font=$ksPanelFont
	PopupMenu pu_packageChooser, win=$name, mode=1, popvalue="_default_", value= "_default_;Voltammetry;"
	PopupMenu pu_packageChooser, win=$name, help={"set the template for a specific package"}
	PopupMenu pu_packageChooser, win=$name, proc=TemplateLoader#puFunc_choosePackage

	topOffset += cHeight + dSpace
	cHeight = 22
	cWidth = 130
	Button btn_chooseTemplate, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, title="choose template"
	Button btn_chooseTemplate, win=$name, font=$ksPanelFont
	Button btn_chooseTemplate, win=$name, proc=TemplateLoader#btnFunc_chooseTemplate
	Button btn_chooseTemplate, win=$name, userdata(package)="default"
	
	secLeftOffset = leftOffset + cWidth + dSpace
	Button btn_setTemplate, win=$name, pos={secLeftOffset, topOffset}, size={40, cHeight}, title="set"
	Button btn_setTemplate, win=$name, font=$ksPanelFont
	Button btn_setTemplate, win=$name, proc=TemplateLoader#btnFunc_setTemplate
	Button btn_setTemplate, win=$name, userdata(package)="default"
	
	secLeftOffset += cWidth + dSpace
	Button btn_loadTemplate, win=$name, pos={secLeftOffset, topOffset}, size={60, cHeight}, title="load"
	Button btn_loadTemplate, win=$name, font=$ksPanelFont
	Button btn_loadTemplate, win=$name, proc=TemplateLoader#btnFunc_loadTemplate
	Button btn_loadTemplate, win=$name, userdata(package)="default"
	
	topOffset += cHeight + dSpace
	cWidth = 600
	cHeight = 20
	TitleBox tb_errorDisplay, win=$name, pos={leftOffset, topOffset}, size={cWidth, cHeight}, title=""
	TitleBox tb_errorDisplay, win=$name, fSize=10, font=$ksPanelFont, fColor=(65535, 0, 0), fixedSize=1
	Titlebox tb_errorDisplay, win=$name, anchor=MC, frame=0
	
	topOffset += cHeight + dSpace
	SetVariable sv_showPath, win=$name, pos={leftOffset,topOffset}, size={cWidth, cHeight}, bodyWidth=550
	SetVariable sv_showPath, win=$name, title="path:", fSize=12, fStyle=2, font=$ksPanelFont
	SetVariable sv_showPath, win=$name, frame=0, value=_STR:""
	SetVariable sv_showPath, win=$name, limits={-inf,inf,0}
	maxWidth = 2 * leftOffset + cWidth
	
	topOffset += cHeight + dSpace
	SetVariable sv_showName, win=$name, pos={leftOffset,topOffset}, size={cWidth, cHeight}, bodyWidth=550
	SetVariable sv_showName, win=$name, title="name:", fSize=12, fStyle=2, font=$ksPanelFont
	SetVariable sv_showName, win=$name, frame=0, value=_STR:""
	SetVariable sv_showName, win=$name, limits={-inf,inf,0}
	
	maxHeight = topOffset + cHeight + dSpace

	ControlInfo /W=$name btn_chooseTemplate

	topOffset = V_top
	cWidth = V_Width
	leftOffset = round((maxWidth / 2)) - round((cWidth / 2))
	Button btn_chooseTemplate, win=$name, pos={leftOffset, topOffset}
	leftOffset += cWidth + dSpace
	Button btn_setTemplate, win=$name, pos={leftOffset, topOffset}
	leftOffset += 40 + dSpace
	Button btn_loadTemplate, win=$name, pos={leftOffset, topOffset}
	
	adjustPanelSize(name, pixelWidth=maxWidth, pixelHeight=maxHeight)
	moveWindowOnScreen(name)
	
	STRUCT WMPopupAction pu
	pu.win = name
	pu.eventCode = 2
	pu.popStr = "_default_"
	puFunc_choosePackage(pu)

	return 1
End

// ***************   P A N E L   C O N T R O L    F U N C T I O N S   ***************

static Function btnFunc_chooseTemplate(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch(bn.eventCode)
		case 2:
			String package = GetUserData(bn.win, bn.ctrlName, "package")
			STRUCT TemplateLoadPrefs prefs
			loadPrefs(prefs, package=package)
			
			String error = "", strStruct = "", fileName = "", namePattern = "%s.%s"
			
			Variable refNum
			String msg = "Select a template experiment file"
			String filters = "Igor Experiments (*.pxp, *.uxp):.pxp,.uxp;"
			filters += "All Files:.*;"
			Open /D/R/F=filters/M=msg refNum
			
			if (strlen(S_fileName) > 0)
				prefs.templateName = ParseFilePath(3, S_fileName, ":", 0, 0)
				prefs.templateExtension = ParseFilePath(4, S_fileName, ":", 0, 0)
				prefs.templatePath = ParseFilePath(1, S_fileName, ":", 1, 0)
				StructPut /S prefs, strStruct

				sprintf fileName, namePattern, prefs.templateName, prefs.templateExtension
				SetVariable sv_showPath, win=$bn.win, value=_STR:prefs.templatePath
				SetVariable sv_showName, win=$bn.win, value=_STR:fileName
				TitleBox tb_errorDisplay, win=$bn.win, title=error
			endif
			
			Button btn_setTemplate, win=$bn.win, userdata=strStruct
			
			break
	endswitch
End

static Function btnFunc_setTemplate(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch(bn.eventCode)
		case 2:
			String msg = ""
			if (strlen(bn.userdata) == 0)
				// error, select template first
				msg = "ERROR! No new template selected!"
				TitleBox tb_errorDisplay, win=$bn.win, title=msg
				return 0
			endif
			
			TitleBox tb_errorDisplay, win=$bn.win, title=msg

			STRUCT TemplateLoadPrefs prefs
			StructGet /S prefs, bn.userdata
			
			String fileName = "", namePattern = "%s.%s"
			
			String package = GetUserData(bn.win, bn.ctrlName, "package")
			savePrefs(prefs, package=package)
			
			Button btn_loadTemplate, win=$bn.win, userdata=bn.userdata
			sprintf fileName, namePattern, prefs.templateName, prefs.templateExtension
			SetVariable sv_showPath, win=$bn.win, value=_STR:prefs.templatePath
			SetVariable sv_showName, win=$bn.win, value=_STR:fileName
			break
	endswitch
End

static Function btnFunc_loadTemplate(bn) : ButtonControl
	STRUCT WMButtonAction &bn
	
	switch(bn.eventCode)
		case 2:
			String msg = ""
			if (strlen(bn.userdata) == 0)
				// error, select template first
				msg = "ERROR! No new template selected!"
				TitleBox tb_errorDisplay, win=$bn.win, title=msg
				return 0
			endif
			
			TitleBox tb_errorDisplay, win=$bn.win, title=msg

//			STRUCT TemplateLoadPrefs prefs
//			StructGet /S prefs, bn.userdata
			
//			String fileName = "", namePattern = "%s.%s"
			
			String package = GetUserData(bn.win, bn.ctrlName, "package")
			
//			sprintf fileName, namePattern, prefs.templateName, prefs.templateExtension
//			SetVariable sv_showPath, win=$bn.win, value=_STR:prefs.templatePath
//			SetVariable sv_showName, win=$bn.win, value=_STR:fileName
			getPackageTemplate(package)
			break
	endswitch
End

static Function puFunc_choosePackage(pu) : PopupMenuControl
	STRUCT WMPopupAction &pu
	
	switch (pu.eventCode)
		case 2:
			String value = LowerStr(ReplaceString("_", pu.popStr, ""))
			Button btn_chooseTemplate, win=$pu.win, userdata(package) = value
			Button btn_setTemplate, win=$pu.win, userdata(package) = value
			Button btn_loadTemplate, win=$pu.win, userdata(package) = value
			
			String msg = ""
			String fileName = "", namePattern = "%s.%s"
			String strStruct
			
			STRUCT TemplateLoadPrefs prefs
			Variable success = loadPrefs(prefs, package=value)
			if (success)
				sprintf fileName, namePattern, prefs.templateName, prefs.templateExtension
				SetVariable sv_showPath, win=$pu.win, value=_STR:prefs.templatePath
				SetVariable sv_showName, win=$pu.win, value=_STR:fileName
				msg = ""
				StructPut /S prefs, strStruct
				Button btn_loadTemplate, win=$pu.win, userdata=strStruct
			else
				SetVariable sv_showPath, win=$pu.win, value=_STR:""
				SetVariable sv_showName, win=$pu.win, value=_STR:""
				msg = "No templates have been set for this package!"		
			endif
			TitleBox tb_errorDisplay, win=$pu.win, title=msg
			break
	endswitch
End



// ******************************************************************************
//                      W O R K E R    F U N C T I O N S 
// ******************************************************************************

static Function loadTemplate(name, [path, packed])
	String name, path
	Variable packed
	
	String errormsg
	sprintf errormsg, ksErrorMsg, "TemplateLoader#openTemplate", "%s"
	
	if (ParamIsDefault(packed) || packed < 0)
		packed = 1
	else
		packed = packed >= 1 ? 1 : 0
	endif
	
	// make sure the name does not contain an extension for comparsion
	String eName = ParseFilePath(3, name, ":", 0, 0)
	// extract potential extension
	String extension = ParseFilePath(4, name, ":", 0, 0)
	
	
	// first check if we are still working with the template
	String expName = IgorInfo(1)
	if (cmpstr(expName, eName) == 0)
		// experiment is the template, so assume it needs to be saved as different file and open new one
		String theDate = Secs2Date(DateTime, 0)
		String theTime = ReplaceString(":", Secs2Time(DateTime, 2), "")
		String copyName = expName[0,20] + "_" + correctDateString(theDate) + theTime
		
		SaveExperiment /C /P=home as copyName
	else
		SaveExperiment
	endif
	
	// now we are not working on the template anymore, so check the provided path
	if (ParamIsDefault(path) || strlen(path) == 0)
		PathInfo $tPathName
		if (V_flag == 0)
			PathInfo home
		endif
		if (strlen(S_path) > 0)
			path = S_path
		else
			path = SpecialDirPath("Documents", 0,0,0)
		endif
		NewPath /O/Q/Z $tPathName, path
	else
		// check if the entered path is valid
		GetFileFolderInfo /Z/Q path
		if (V_flag > 0 || V_flag < -1)
			// path is incorrect
			printf errormsg, "The path to '" + path +"' is invalid."
			return 0
		elseif (V_flag == -1)
			printf errormsg, "User canceled the loading."
			return 0
		endif
	endif
	
	// now we have a valid path, time to check the template file name
	if (strlen(extension) == 0)
		if (packed)
			extension = "pxp"
		else
			extension = "uxp"
		endif
	endif
	
	String filePath = ""
	sprintf filePath, "%s%s.%s", path, eName, extension

	GetFileFolderInfo /Z/Q filePath
	if (V_flag > 0 || V_flag < -1)
		printf errormsg, "The template file '" + name + "' does not exist in folder '" + path + "'."
		return 0
	elseif (V_flag == -1)
		printf errormsg, "User canceled the loading."
		return 0
	endif
		
	Execute /P "NEWEXPERIMENT "
	Execute /P ("LOADFILE " + filePath)
		
	return 1
End		


static Function getPackageTemplate(package)
	String package
	
	String errormsg, warnmsg, msg
	sprintf errormsg, ksErrorMsg, "TemplateLoader#getPackageTemplate", "%s"
	sprintf warnmsg, ksWarnMsg, "TemplateLoader#getPackageTemplate", "%s"
	
	strswitch (LowerStr(package))
		case "voltammetry":
		case "default":
			break
		default:
			printf warnmsg, "package '" + package + "' not recognized, loading default template"
	endswitch
	
	STRUCT TemplateLoadPrefs prefs
	Variable success = loadPrefs(prefs, package=package)
	
	if (!success)
		printf errormsg, "No template has been set for package '" + package + "', nothing to load"
		return 0
	endif
		
	return loadTemplate(prefs.templateName, path=prefs.templatePath, packed=cmpstr(prefs.templateExtension, "pxp") == 0)
End
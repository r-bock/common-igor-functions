#pragma rtGlobals=1		// Use modern global access method.
#pragma version = 1.0
#include "ordering_waves"
#include "CreatingGraphs"

// by Roland Bock, August 2002
// Fileloader to load multiple igor binary files created by the AxoData export to Igor function.

//**********************************************************************
// basic loading function for a single binary file. It stores the name of the file in a global
// string variable named "loadingFile"
Function LoadIgorBinary(fileName, pathName)
	String fileName 							// Name of file to load or "" to get dialog
	String pathName 							// Name of path or "" to get dialog
												// load the waves, overwriting existing waves
	String /G loadingFile
	SVAR loadingFile
	LoadWave/W/A/H/D/O/P=$pathName fileName
	loadingFile = S_fileName
	if (V_flag==0) 							// No waves loaded. Perhaps user canceled.
		return -1
	endif
	return 0 									// Signifies success.
End

// extends the basic loading function to take the path of a folder and load all binary
// files into the current experiment.
Function LoadAllIgorBinary(pathName)
	String pathName							// Name of symbolic path or "" to get dialog
	String fileName
	String graphName
	Variable index=0

	if (strlen(pathName)==0) 				// If no path specified, create one
		NewPath/O temporaryPath 			// This will put up a dialog
		pathName = "temporaryPath"
	endif

	Variable result
	do 											// Loop through each file in folder
		fileName = IndexedFile($pathName, index, "IGBW")		// load only igor binarys
		if (strlen(fileName) == 0) 			// No more files?
			break 								// Break out of loop
		endif
		result = LoadIgorBinary(fileName, pathName)
		index += 1
	while (1)
	if (Exists("temporaryPath")) 				// Kill temp path if it exists
		KillPath temporaryPath
	endif
	return 0 									// Signifies success.
End

//**********************************************************************
// Function to easily set the symbolic path to the binary folder
Function SetBinaryPath(pathName)
	String pathName
	
	if (strlen(pathName) == 0)
		NewPath/O binaryFolder							// ask for path if non provided
	else
		NewPath/O binaryFolder, pathName
	endif
End

// Function to easily set the symbolic path to the experiment folder
Function SetExperimentPath(pathName)
	String pathName
	
	if (strlen(pathName) == 0)
		NewPath/O experimentFolder					// ask for path if non provided
	else
		NewPath/O experimentFolder, pathName
	endif
End

// Set the two path from an AppleScript and load the binary file
Function GetBinarysFromAppleScript(binaryPath, experimentPath)
	String binaryPath, experimentPath
	
	SetBinaryPath(binaryPath)
	SetExperimentPath(experimentPath)
	GeneralIgorBinaryLoader("binaryFolder", "experimentFolder")
End


// **************************************************
// delete the potential "Copy", "_9.bwav" parts from the filename when loading Igor
// binarys exported by AxoData.
Function ChangeLoadingName()

	SVAR loadingFile									// global variable created by file loading
	Variable lastChar = 0
	
	lastChar = strsearch(loadingFile, "Copy", 0)
	if (lastChar == -1)
		lastChar = strsearch(loadingFile, "_", 0)
		if (lastChar == -1)
			return -1			
		endif
	endif
	loadingFile = loadingFile[0, lastChar - 1]
	return 0
End


//*******************************************************
// not exactly a file loading function, but often necessary in combination with the file loading
// process - save the current experiment in a certain location
Function SaveThisExperiment(pathName, fileName)
	String pathName, fileName
	
	SVAR loadingFile										// binary file loader generates this global string
	if (strlen(fileName) == 0)							// no fileName provided
		if (exists("loadingFile"))							// if the global string exists set filename to it
			fileName = loadingFile
		else
			fileName = "NoNameProvided"				// indicate that no name was provided 
		endif
	endif
	PathInfo $pathName									// check if path exists
	if (!V_Flag)											// if path does not exists (V_Flag set to 0)
		NewPath/O tempPath								// ask for new path 
		SaveExperiment/P=tempPath as fileName		// save experiment there
	else
		SaveExperiment/P=$pathName as fileName	// otherwise save directly at provided path
	endif
	KillPath/Z tempPath									// get rid of temporary path
End


//*******************************************************
// Function to load Igor binarys from a path and store the experiment file at the experiment path
// All loaded traces are displayed in one graph.
Function GeneralIgorBinaryLoader(pathBinary, pathExperiment)
	String pathBinary, pathExperiment
	
	String List, experimentName, selector
	LoadAllIgorBinary(pathBinary)						// load the binary waves
	List = StringFromList(0, WaveList("*", ";", ""))		// get the name of the first wave
	selector = DetermineBaseWaveName(List, "-") + "*"
	CorrectWaveNumbers(selector, "-")					// correct wave numbers to three digits
	SVAR identifier										// give access to global string
	selector = identifier + "*"							// create new selector
	ChangeLoadingName()								// modify the original name to a nicer one
	SVAR loadingFile
	experimentName = loadingFile + ".pxp"			// add extension to future filename
	List = rightorder(WaveList(selector, ";", ""))		// correct the order of waves in the list
	ShowSingleTracesInOne(List)						// show all waves in a single graph
	SaveThisExperiment(pathExperiment, experimentName)	// save experiment at path with name
End

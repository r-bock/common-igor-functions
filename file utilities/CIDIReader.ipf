#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6.2
#pragma version = 2.5
#pragma hide = 1
#include "TimeDateConversions"

// Written by Roland Bock in July 2008
// ** version 2.5 (July 2017): added a cage - channel list to the loading to make because some channels can be
//                             omitted from the data file and then we have a mismatch to the mouse assignment.
// ** version 2.4 (January 2017): updated the reader to predimension the data matrix to the maximum 16 channels
//                         and scale the x dimension as "s"
// ** version 2.2 (November 2015): fixed some bugs and error messages resulting from opening and 
//                         saving the csv data file with Excel
// ** version 2.1 (September 2011)
// **** version 1.1
//
// **********************************************
// *********    C O N S T A N T S    **********************
// **********************************************
static StrConstant ksPackageName = "CIDIReader"

// Open and read the file (pure text csv-file with header information on top
// Creates the following global variables:
//
// CIDIComment:			String; contents of the comment section
// CIDIfileName:			String; full name of the file
// CIDIdate:					String; system date, when the file was started
// CIDIsessionLength:		Variable; session intervall in seconds
// CIDInumberOfChannels:	Variable; number of used channels
//
// CIDIdata[dataValues][dataRows][channel]:		multidimensional wave containing the data
//								dataRows:		0: Time
//												1: ambulatory counts
//												2: total counts
//								dataValues:		values of the data columns
//								channel:		channel number, starts with zero so enter 0 for 1. channel, 1 for 2. etc													
Function CIDIFileReader(path)
	String path
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = CIDIInit()
	
	SetDatafolder package

	SVAR CIDIComment
	SVAR CIDIfullFileName
	SVAR CIDIfileName
	SVAR CIDIcolumnNames
	SVAR CIDIdate
	SVAR CIDICageNumKeyList
	SVAR CIDIUsedCageNumList
	NVAR CIDIsessionLength
	NVAR CIDInumberOfChannels
	NVAR CIDImaxChannelNumber

	if (!SVAR_Exists(CIDIComment))
		String /G CIDIComment = ""
	else
		CIDIComment = ""
	endif
	if (!SVAR_Exists(CIDIfullFileName))
		String /G CIDIfullFileName = ""
	endif
	if (!SVAR_Exists(CIDIfileName))
		String /G CIDIfileName = "last loaded file"
	endif
	if (!SVAR_Exists(CIDIcolumnNames))
		String /G CIDIcolumnNames = ""
	endif
	if (!SVAR_Exists(CIDIdate))
		String /G CIDIdate = ""
	endif
	if (!SVAR_Exists(CIDICageNumKeyList))
		String /G CIDICageNumKeyList = ""
	endif
	if (!SVAR_Exists(CIDIUsedCageNumList))
		String /G CIDIUsedCageNumList = ""
	endif	
	if (!NVAR_Exists(CIDIsessionLength))
		Variable /G CIDIsessionLength = 0
	endif
	if (!NVAR_Exists(CIDInumberOfChannels))
		Variable /G CIDInumberOfChannels = 0
	endif
	if (!NVAR_Exists(CIDImaxChannelNumber))
		Variable /G CIDImaxChannelNumber = 0
	endif
	
	CIDInumberOfChannels = 0
	CIDImaxChannelNumber = 16
	CIDICageNumKeyList = ""
	CIDIUsedCageNumList = ""
	Variable channelFlag = 0			// flag to count the channels
	Variable commentFlag = 0			// flag to get the comments
	Variable dataFlag = 0				// flag to start reading the data

//	String theChannelList = ""
	Variable refNum, counter, index, i, currentChannel
	String dataPath
	
	PathInfo /S $path
	Open /D /R /M="Choose a file" /T=".csv" refNum
	if (strlen(S_fileName) == 0)
		SetDatafolder cDF
		return 0
	endif	

	CIDIfullFileName = S_fileName
	CIDIfileName = ParseFilePath(0, CIDIfullFileName, ":", 1, 0)
	dataPath = ParseFilePath(1, CIDIfullFileName, ":", 1, 0)
	NewPath /O/Q lastDataPath, dataPath
	
	Open /R refNum as CIDIfullFileName

	if (refNum == 0)
		print "Can not open file"
		CIDIfileName = "wrong file"
		SetDatafolder cDF
		return 0
	endif
	
	String theLine
	
	do
		FReadLine refNum, theLine
		if (strlen(theLine) == 0)
			break
		endif
		theLine = RemoveEnding(theLine)
		if (strsearch(theLine, "Started On:", 0) == 0)
			CIDIdate = StringFromList(0, StringFromList(1, theLine, ","), " ")
		endif
		if (strsearch(theLine, "Session Interval", 0) == 0)
			CIDIsessionLength = timeStr2Secs(StringFromList(1, theLine, ","))
		endif
		if (strsearch(theLine, "[End Channel Setup Info]", 0) > -1)
			channelFlag = 0
			counter = 0
		endif
		if (channelFlag)
			// not used momentarily
			String cage = StringFromList(0, theLine, ",")
			String channel = StringFromList(1, theLine, ",")
			
			Variable numCGStart = strsearch(cage, " ", 0) + 1
			Variable numCHStart = strsearch(channel, " ", 0) + 1 + strlen(cage) + 1
			CIDICageNumKeyList = ReplaceStringByKey(theLine[numCHStart, strlen(theLine)], CIDICageNumKeyList, theLine[numCGStart, strlen(cage) - 1])
//			CIDICageNumList = AddListItem(theLine[numStart, numEnd], CIDICageNumKeyList, ";", Inf)
		endif
		if (strsearch(theLine, "[Channel Setup Info]", 0) > -1)
			channelFlag = 1
			counter = 0
		endif
		if (strsearch(theLine, "[Experiment Comments]", 0) > -1)
			commentFlag = 1
			counter = 0
		endif
		if (strsearch(theLine, "[Experiment Comments End]", 0) > -1)
			commentFlag = 0
			counter = 0
		endif
		if (commentFlag)
			if (counter > 0)
				CIDIcomment += RemoveEnding(theLine, "\r") + "\n"
			endif
			counter += 1
		endif
		if (strsearch(theLine, "[Experiment Data]", 0) > -1)
			dataFlag = 1
			counter = 0
//			Variable dimensions = ItemsInList(CIDICageNumList)
			Variable dimensions = CIDInumberOfChannels < CIDImaxChannelNumber ? CIDImaxChannelNumber : CIDInumberOfChannels
			Make /N=(0, 3, dimensions) /O CIDIdata
		endif
		if (dataFlag)
			if (counter == 1)
				theLine = ReplaceString(",", RemoveEnding(theLine, ","), ";")
				Variable num = ItemsInList(theLine)
				CIDIcolumnNames = RemoveListItem(0, theLine)
				CIDIcolumnNames = RemoveListItem(0, CIDIcolumnNames)
			endif
			if (counter > 1)
				currentChannel = str2num(StringFromList(1, theLine, ","))
				index = str2num(StringFromList(0, theLine, ","))
				if (FindListItem(num2str(currentChannel), CIDIUsedCageNumList) == -1)
					CIDIUsedCageNumList = AddListItem(num2str(currentChannel), CIDIUsedCageNumList, ";", Inf)
				endif
				if (DimSize(CIDIData, 0) <= index) 
					InsertPoints /M=0 index, 1, CIDIData
				endif
				
				CIDIData[index][0][currentChannel - 1] = timeStamp2Secs(StringFromList(2, theLine, ","))		
				CIDIData[index][1][currentChannel - 1] = str2num(StringFromList(3, theLine, ","))
				CIDIData[index][2][currentChannel - 1] = str2num(StringFromList(4, theLine, ","))
			endif
			counter += 1
		endif
	while(1)
	print "-> loaded file " + CIDIfileName + " from '" + dataPath + "' on " + date()
	SetScale /P x, 0, CIDIsessionLength, "s", CIDIdata
	Close refNum

	for (i = 0; i < ItemsInList(CIDIcolumnNames); i += 1)
		Variable l = ItemsInList(CIDIcolumnNames)
		String columnName = StringFromList(i, CIDIcolumnNames)
		SetDimLabel 1, i, $(StringFromList(i, CIDIcolumnNames)), CIDIdata
	endfor
	CIDInumberOfChannels = ItemsInList(CIDIUsedCageNumList)
	index = 0
	for (index = 0; index < CIDInumberOfChannels; index += 1)
		CIDImaxChannelNumber = max(CIDImaxChannelNumber, str2num(StringFromList(index, CIDIUsedCageNumList)))
	endfor
	
	SetDatafolder cDF
	return 1
End // readBehaviourFile()

Function /DF CIDIInit()
	
	DFREF package = root:$"Packages":$ksPackageName
	if (DataFolderRefStatus(package) == 1)
		// the data folder exists, so nothing more to do, just return the reference
		return package
	endif
	
	DFREF cDF = GetDatafolderDFR()
	SetDatafolder root:
	NewDatafolder /S/O $"Packages"
	NewDataFolder /S/O $ksPackageName
	package = GetDatafolderDFR()
	SetDatafolder cDF	
	
	return package
End


Function cleanUpCIDIfiles()
	
	DFREF cDF = GetDatafolderDFR()
	DFREF package = CIDIInit()
	
	SetDatafolder package
	KillWaves /Z CIDIdata
	KillVariables /Z CIDIsessionLength, CIDInumberOfChannels, CIDImaxChannelNumber
	KillStrings /Z CIDIfileName, CIDIComment, CIDIcolumnNames
	SetDatafolder cDF
End
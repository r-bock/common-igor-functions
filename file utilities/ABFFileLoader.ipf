#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6.1
#pragma version = 1.1
#pragma ModuleName = ABFLoader
#include "WindowHelper", version >= 2.8


// by Roland Bock, rolandbock@gmail.com
// ** version 1.1 (AUG 2013)
// version 0.1 (JAN 2012)


// inspired by the LoadABF212 functionality by C.S. Leonard New York Medical College
// load old ABF files (pre pClamp 10 (2006))

// define the header structure to be read from the ABF file, which will determine the data read.

// *****************      S T R U C T U R E S       ********************************

//Structure ABFHeader
//	// file ID and size information (40 bytes)
//										// Offset
//	int32	IFileSignature				//	0 		file type could be "ABF " or "CLPX" or "FTCX"
//	float	fFileVersionNumber			//	4
//	int16	nOperationMode				//	8
//	int32	IActualAcqLength			//	10
//	int16	nNumPointsIgnored			//	14
//	int32	IActualEpisodes				//	16
//	int32	IFileStartDate				//	20
//	int32	IFileStartTime				//	24
//	int32	IStopwatchTime				//	28
//	float	fHeaderVersionNumber		//	32		version number of the header structure
//	int16	nFileType					//	36		numeric equivalent of file type 1 = ABF, 2 = old FETCHEX file, 3 = Old CLAMPEX file
//	int16	nMSBinFormat				//	38		storage format for real numbers in header: 0 = IEEE, 1 = Microsoft binary format (old files only)
//	// file structure (78 bytes)
//	int32	IDataSectionPtr				//	40		block number of start of data section
//	int32	ITagSectionPtr				//	44		block number of start of Tag section (end of data section)
//	int32	INumTagEntries				//	48		number of tag entries
//	int32	IScopeConfigPtr				//	52		block number of ABFScopeConfig structures
//	
//EndStructure


// *******************    C O N S T A N T S     ***********************************

static Strconstant ks_packageName = "ABFFileLoader"

static Strconstant ks_mac = "Macintosh"

static Strconstant ks_sampleInt = "sampleInterval"
static Strconstant ks_totalEpisodes = "totalEpisodes"
static Strconstant ks_episodeSize = "episodeSize"
static Strconstant ks_totalChannels = "totalChannels"
static Strconstant ks_channelNum = "channelNumber"
static Strconstant ks_channelUnit = "channelUnit"
static Strconstant ks_status = "status"
static Strconstant ks_filePath = "filePath"

static Strconstant ksDefProgressBarName = "ABFFileImportProgress"
static Strconstant ksDefProgressBarTitle = "ABF File Import"

// ******************     M E N U    ****************************

Menu "ABF"
	"Import ABF file -Bruxton-", ABFLoader#importABF()
End

// ******************     M A I N    F U N C T I O N S    *****************************

// This function depends on the XOP from Bruxton. 
static Function importABF()

	if (!xopIsLoaded())
		print "** ERROR (importABF): this function depends on the abf xop from Bruxton, that doesn't seem to exist on this computer. Please install it and try again."
		return 0
	endif
		
	Variable episode, channel
	Variable fileIndex = 0
	Variable filesSelected
	
	String fileName, fileList = "", cmd = ""
	String dataWaveName, progressBar
	
	fileList = GetABFFileList()
	if (strlen(fileList) == 0)
		print "No files selected."
		return 0
	endif

	Execute /Z /Q "ModifyBrowser close"

	filesSelected = ItemsInList(fileList, "\r")

	DFREF cDF = GetDatafolderDFR()
	DFREF package = getPackage()
	
	NVAR status = $(getGlobalVariable(ks_status))
	NVAR totalEpisodes = $(getGlobalVariable(ks_totalEpisodes))
	NVAR totalChannels = $(getGlobalVariable(ks_totalChannels))
	NVAR channelNum = $(getGlobalVariable(ks_channelNum))
	NVAR episodeSize = $(getGlobalVariable(ks_episodeSize))
	NVAR sampleInt = $(getGlobalVariable(ks_sampleInt))
	
	SVAR channelUnit = $(getGlobalVariable(ks_channelUnit, type=1))
	SVAR filePath = $(getGlobalVariable(ks_filePath, type = 1))
	
	print "- start ABF file import on", date(), "at", time()
	
	progressBar = getProgressBarPanel(theColor = "blue")

	for (fileIndex = 0; fileIndex < filesSelected; fileIndex += 1)
		filePath = StringFromList(fileIndex, fileList, "\r")
		fileName = ParseFilePath(3, filePath, ":", 0, 0)

		status = 0
		totalEpisodes = 0
		totalChannels = 0
		episodeSize = 0
		
		SetDatafolder package
		
		sprintf cmd, "ABFFileOpen %s, status", ks_filePath
		Execute /Q cmd
		sprintf cmd, "ABFEpisodeGetCount %s, status", ks_totalEpisodes
		Execute /Q cmd
		sprintf cmd, "ABFFileGetSampleInterval %s, status", ks_sampleInt
		Execute /Q cmd
		
		updateProgressBar(0, info = "importing " + num2str(totalEpisodes) + " episodes from file " + fileName)

		for (episode = 1; episode <= totalEpisodes; episode += 1)
			sprintf cmd, "ABFEpisodeSet %g, status", episode
			Execute /Q cmd
			sprintf cmd, "ABFEpisodeGetSampleCount %s, status", ks_episodeSize
			Execute /Q cmd
			sprintf cmd, "ABFChannelGetCount %s, status", ks_totalChannels
			Execute /Q cmd
						
			for (channel = 0; channel < totalChannels; channel += 1)
				sprintf cmd, "ABFChannelGetADC %g, %s, status", channel, ks_channelNum
				Execute /Q cmd
				sprintf cmd, "ABFChannelGetUnits %g, %s, status", channel, ks_channelUnit
				Execute /Q cmd
				
				dataWaveName = fileName + "_" + num2str(episode) + "_ADC" + num2str(channelNum)
				Make /O/D/N=(episodeSize) root:$dataWaveName /WAVE=data
				
				sprintf cmd, "ABFEpisodeRead %g, 0, %g, %s, status", channel, episodeSize, GetWavesDatafolder(data, 2)
				Execute /Q cmd
				SetScale /P x 0, sampleInt, "s", data
				SetScale d 0, 0, channelUnit, data
			endfor
			
			if (mod(episode, 5) == 0)
				updateProgressBar((episode/totalEpisodes))
			endif
		endfor
		updateProgressBar(1)
		Execute /Q "ABFFileClose"
		SetDatafolder cDF
		
		print "-- finished file", fileName, "at", time()
	endfor
	DoWindow /K $progressBar
	return 1
End




// ***********     H E L P E R   /   S T A T I C     F U N C T I O N S    **********************

static Function /S GetABFFileList()
	Variable refNum
	String message = "Please select your ABF data files"
	String output = ""
	String fileFilters = "pClamp Files (*.abf):.abf;All Files:.*;"
	
	Open /D /R /MULT=1 /F=fileFilters /M=message refNum
	output = S_fileName
	
	if (strlen(output) == 0)
		print "File selection canceled."
	else
		output = SortList(output, "\r", 0)
	endif
	return output
End

static Function /DF getPackage()
	DFREF cDF = GetDatafolderDFR()
	DFREF package = root:Packages:$ks_packageName
	if (DatafolderRefStatus(package) == 0)
		// datafolder does not exists
		SetDatafolder root:
		NewDatafolder /O/S Packages
		NewDatafolder /S $ks_packageName
		DFREF package = GetDatafolderDFR()
		SetDatafolder cDF
	endif
	return package
End

static Function /S getGlobalVariable(name, [type])
	String name
	Variable type
	
	if (ParamIsDefault(type) || type < 0)
		type = 0
	else
		type = type > 1 ? 1 : round(type)
	endif
	
	DFREF package = getPackage()
	
	switch (type)
		case 1:
			SVAR str = package:$name
			if (!SVAR_Exists(str))
				String /G package:$name
				SVAR str = package:$name
				str = ""
			endif
			break
		case 0:
		default:
			NVAR var = package:$name
			if (!NVAR_Exists(var))
				Variable /G package:$name
			endif
	endswitch
	return (GetDatafolder(1, package) + name)
End

static Function checkABFXOP()
	// the Bruxton ABF XOP functions all start with ABF, so we can check for them. If we don't have
	//    any, the XOP is not correctly installed or missing.
	String theList = OperationList("ABF*", ";", "external")
	return strlen(theList) > 0
End

static Function xopIsLoaded()
	return checkABFXOP() == 1
End

static Function isMac()
	return cmpstr(IgorInfo(2), ks_mac) == 0
End

static Function /S getProgressBarPanel([theTitle, theName, theColor])
	String theTitle, theName, theColor
	
	if (ParamIsDefault(theTitle) || strlen(theTitle) == 0)
		theTitle = ksDefProgressBarTitle
	endif
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = ksDefProgressBarName
	endif
	if (ParamIsDefault(theColor) || strlen(theColor) == 0)
		theColor = "green"
	endif
	
	STRUCT RGBColor color
	strswitch(theColor)
		case "red":
			color.red = 65535
			color.green = 0
			color.blue = 0
			break
		case "blue":
			color.red = 0
			color.green = 0
			color.blue = 65535
			break
		case "green":
		case "GREEN":
		default:
			color.red = 2
			color.green = 39321
			color.blue = 1
	endswitch
	
	
	NewPanel /W=(5,5,5,5) /K=1 /N=$theName as theTitle
	theName = S_name
	resizeWindowToScreen(theName, ratio=5)
	if (isMac())
		GetWindow $theName wsize
	else
		GetWindow $theName wsizeRM
	endif
	Variable width = V_right - V_left
	Variable height = V_bottom - V_top
	Variable dispWidth = round(width * 0.9)
	Variable dispHeight = round(height * 0.25)
	Variable frameLDistance = round((width - dispWidth) / 2)
	Variable frameTDistance = round((height - dispHeight) * (2/3))
	Variable frameTForText = round(frameTDistance / 2)
	
	ValDisplay progressBar, win=$theName, pos={frameLDistance, frameTDistance}, size={width, dispHeight}
	ValDisplay progressBar, win=$theName, limits={0, 1, 0}, barmisc={0,0}
	ValDisplay progressBar, win=$theName, mode = 3, value=_NUM:0
	ValDisplay progressBar, win=$theName, highColor=(color.red,color.green,color.blue)
	
	TitleBox progressInfo, win=$theName, pos={frameLDistance, round(frameTDistance / 2)}, size={dispWidth, dispHeight}
	TitleBox progressInfo, win=$theName, font="Verdana", fSize=10, frame=0
	Titlebox progressInfo, win=$theName, title = ""
	
	DoUpdate /W=$theName /E=1
	moveWindowOnScreen(theName)
	return theName
End

static Function updateProgressbar(progress, [info, theName])
	Variable progress
	String theName, info
	
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = ksDefProgressBarName
	endif
	
	ValDisplay progressBar, win=$theName, value=_NUM:progress
	
	if (!ParamIsDefault(info))
		Titlebox progressInfo, win=$theName, title=info
	endif
	DoWindow /F $theName
	DoUpdate /W=$theName
	return 0
End

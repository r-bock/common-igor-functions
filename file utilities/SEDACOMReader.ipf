﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma ModuleName = SEDACOM
#pragma IgorVersion = 7
#pragma version = 1.2
#include "ListHelpers"
#include "TimeDateConversions"

// Written by Roland Bock in December 2018
// 
// version 1.2 (JULY 2019): needed to add a conditional for the date conversion to ensure that the old formats are imported
//									without any problems.
// version 1.1 (JULY 2019): needed to update the date determination, because the export changed the date columns in the 
//									excel files. Dates can now be either DD/MM/YYYY or DD-MM-YYYY. The latter is not recognized by
//									the Igor Excel import as a valid date, so it was required to import it as text and convert it to
//									the Igor date format.

// **************************************************************
// ***********    C O N S T A N T S        **********************
// **************************************************************

Strconstant ksSEDACOMPathName = "SEDACOMDataPath"

static Strconstant ksPackageName = "SEDACOMReader"
static Strconstant ksLoadFolderName = "lastImport"
static Strconstant ksImportTableName = "SEDACOMLastImportTable"

static Strconstant ksPLColList = "Cage;Mouse"

// ------ wave names of import ------
Strconstant ksSEDACOM_i_cage = "Cage"						// number
Strconstant ksSEDACOM_i_projcode = "Project_code"		// text
Strconstant ksSEDACOM_i_expter = "Experimenter"			// text
Strconstant ksSEDACOM_i_chal = "Challenge"					// text
Strconstant ksSEDACOM_i_dose = "Dose"						// text
Strconstant ksSEDACOM_i_date = "X_Date"						// number
Strconstant ksSEDACOM_i_hTime = "Header_Time"				// number
Strconstant ksSEDACOM_i_exp = "Exper_"						// number
Strconstant ksSEDACOM_i_intNum = "Intervals_number"		// number
Strconstant ksSEDACOM_i_dur = "Duration"					// number
Strconstant ksSEDACOM_i_start = "Start_at"					// number
Strconstant ksSEDACOM_i_ocage = "O_Cage"					// number
Strconstant ksSEDACOM_i_frame = "Frame"						// text
Strconstant ksSEDACOM_i_level = "Level"						// level
Strconstant ksSEDACOM_i_serial = "Serial"					// text
Strconstant ksSEDACOM_i_int = "Inter_"						// number
Strconstant ksSEDACOM_i_smov = "S_Mov_"						// number
Strconstant ksSEDACOM_i_fmov = "F_Mov_"						// number
Strconstant ksSEDACOM_i_sste = "S_Ste_"						// number
Strconstant ksSEDACOM_i_fste = "F_Ste_"						// number
Strconstant ksSEDACOM_i_srear = "S_Rea_"					// number
Strconstant ksSEDACOM_i_frear = "F_Rea_"					// number

// *****************************************************************

Function LA_IRActimeterXLReader(path)
	String path
	
	if (strlen(path) == 0)
		path = ksSEDACOMPathName
	endif

	DFREF cDF = GetDataFolderDFR()
	DFREF package = init()
	DFREF importFolder = cleanImportFolder()

	SetDatafolder package
	
	SVAR SEDACOMLastFile
	SVAR SEDACOMLastPath
	SVAR SEDACOMWaveNames
	SVAR SEDACOMCageNums
	SVAR SEDACOMExperiments
	SVAR SEDACOMDateList
	NVAR SEDACOMDuration
	NVAR SEDACOMInterval
	
	Variable refNum
	String filter = "Excel Files (*.xls,*.xlsx):.xls,.xlsx;"

	PathInfo $path
	
	if (V_flag)
		Open /F=filter /P=$path /D/R refNum
	else
		Open /F=filter /D/R refNum
	endif	
	
	if (strlen(S_fileName) == 0)
		print " - user canceled file load on " + date() + " at " + time()
		return 0
	endif
	
	String fullfilePath = ParseFilePath(5, S_fileName, ":", 0, 0)
	SEDACOMLastPath = ParseFilePath(1, fullFilePath, ":", 1, 0)
	SEDACOMLastFile = ParseFilePath(0, fullFilePath, ":", 1, 0)
	
	NewPath /O/Q $path, SEDACOMLastPath			// update the path to current data folder
	
	XLLoadWave /J=3/P=$path SEDACOMLastFile		// first get some info of the excel file
	String sheet = StringByKey("NAME", S_value)
   String rangeStart = StringByKey("FIRST", S_value)
	String rangeEnd = StringByKey("LAST", S_value)
		
   KillWindow /Z $ksImportTableName  // kill the table before using the excel load function

	SetDatafolder importFolder
   
//	XLLoadWave /Q/P=$path/S=sheet /R=($rangeStart,$rangeEnd)/COLT="1N5T2D2T3N1D1N1T1N1T9N"/W=1/O/D/T SEDACOMLastFile
	XLLoadWave /Q/P=$path/S=sheet /R=($rangeStart,$rangeEnd)/COLT="1N6T1D2T3N1D1N1T1N1T9N"/W=1/O/D/T SEDACOMLastFile
	if (V_flag == 0)
		print "- user canceled Excel file load on " + date() + " at " + time()
		return 0
	endif
	SVAR S_name
	DoWindow /W=$S_name/C $ksImportTableName
	GetWindow $ksImportTableName title
	DoWindow /T $ksImportTableName, SEDACOMLastFile + ": " + S_value
	ModifyTable /W=$ksImportTableName format(importFolder:Header_Time)=7
	ModifyTable /W=$ksImportTableName format(root:Packages:SEDACOMReader:lastImport:Start_at)=7
//	ModifyTable /W=$ksImportTableName format(root:Packages:SEDACOMReader:lastImport:X_Date)=6
	
	SEDACOMWaveNames = S_waveNames
	getCageNumberList(reset = 1)
	getExperimentNums(reset = 1)
	convertTextW2DateW()
	getDateList(reset = 1)
	Wave duration = $"Duration"
	convertExcelDuration2Secs(duration)
	
	SetDatafolder cDF
	
	return 1
End

Function /S SEDACOM_getCageNums()
	return getGlobalByName("SEDACOMCageNums")
End

Function /S SEDACOM_getLastFile()
	return getGlobalByName("SEDACOMLastFile")
End

Function /S SEDACOM_getExpDateList()
	return getGlobalByName("SEDACOMDateList")
End

// **************************************************************
// ***  S T A T I C    H E L P E R    F U N C T I O N S  ********
// **************************************************************

static Function /DF init([force])
	Variable force
	
	force = ParamIsDefault(force) || force < 1 ? 0 : 1

	DFREF package = root:$"Packages":$ksPackageName
	if (DatafolderRefStatus(package) == 1 && !force)
		// the data folder exists, init has been run before, so just return the folder
		return package
	endif
	
	DFREF cDF = GetDatafolderDFR()
	SetDatafolder root:
	NewDatafolder /S/O $"Packages"
	NewDataFolder /S/O $ksPackageName
	package = GetDataFolderDFR()

	SVAR SEDACOMLastFile				// last loaded file name
	SVAR SEDACOMLastPath				// last loaded file path
	SVAR SEDACOMWaveNames				// wave names of the last import
	SVAR SEDACOMCageNums				// numbers of the cages used in the last import
	SVAR SEDACOMExperiments			// list of experiment numbers in the file
	SVAR SEDACOMDateList				// list of experiment dates in the imported file
	NVAR SEDACOMDuration				// interval duration of the last file 
	NVAR SEDACOMInterval				// number of intervals of the last file
		
	if (!SVAR_Exists(SEDACOMLastFile))
		String /G SEDACOMLastFile = ""
	endif
	if (!SVAR_Exists(SEDACOMLastPath))
		String /G SEDACOMLastPath = ""
	endif
	if (!SVAR_Exists(SEDACOMWaveNames))
		String /G SEDACOMWaveNames = ""
	endif
	if (!SVAR_Exists(SEDACOMCageNums))
		String /G SEDACOMCageNums = ""
	endif
	if (!SVAR_Exists(SEDACOMExperiments))
		String /G SEDACOMExperiments = ""
	endif
	if (!SVAR_Exists(SEDACOMDateList))
		String /G SEDACOMDateList = ""
	endif	
	if (!NVAR_Exists(SEDACOMDuration))
		Variable /G SEDACOMDuration = NaN
	endif
	if (!NVAR_Exists(SEDACOMDuration))
		Variable /G SEDACOMInterval = NaN
	endif

	SetDatafolder cDF
	
	return package
End

static Function /DF getImportFolder()
	DFREF package = init()
	DFREF importFolder = package:$ksLoadFolderName
	if (DatafolderRefStatus(importFolder) == 1)
		return importFolder
	endif
	
	DFREF cDF = GetDataFolderDFR()
	SetDatafolder package
	NewDataFolder /S/O $ksLoadFolderName
	importFolder = GetDatafolderDFR()
	SetDatafolder cDF
	
	return importFolder
End

static Function /DF cleanImportFolder()
	DFREF importFolder = getImportFolder()
	DFREF cDF = GetDataFolderDFR()
	
	SetDatafolder importFolder
	KillWaves /A/Z
	KillVariables /A/Z
	KillStrings /A/Z
	SetDataFolder cDF
	
	return importFolder
End

static Function /S getGlobalByName(name)
	String name
	return GetDatafolder(1, init()) + name
End
	

static Function /S getUniqueListFromWave(theWave, theVarName, [format, reset])
	String theWave, theVarName, format
	Variable reset
	
	if (strlen(theWave) == 0 || strlen(theVarName) == 0)
		return ""
	endif
	if (ParamIsDefault(format) || strlen(format) == 0)
		format = "number"
	endif
	
	reset = ParamIsDefault(reset) || reset < 1 ? 0 : 1
		
	strswitch (LowerStr(format))
		case "date":
		case "time":
		case "number":
			format = LowerStr(format)
			break
		default:
			format = "number"
	endswitch
	
	DFREF cDF = GetDataFolderDFR()
	DFREF package = init()
	DFREF import = getImportFolder()
	
	SetDatafolder package
	
	SVAR resultVar = $theVarName
	if (!SVAR_Exists(resultVar))
		return ""
	endif

	if (reset)
		resultVar = ""
	endif

	SVAR SEDACOMWaveNames
	
	SetDatafolder import
	
	Wave cages = $theWave
	if (!WaveExists(cages))
		return ""
	endif
	
	Variable length = numpnts(cages)
	Variable index
	
	strswitch (format)
		case "date":
			for (index = 0; index < length; index += 1)
				resultVar = AddUniqueToList(correctDateString(Secs2Date(cages[index], 0), type = 1, separator = "/"), resultVar)
			endfor
			break
		case "time":
			for (index = 0; index < length; index += 1)
				resultVar = AddUniqueToList(Secs2Time(cages[index], 3), resultVar)
			endfor
			break
		case "number":
		default:
			for (index = 0; index < length; index += 1)
				resultVar = AddUniqueToList(num2str(cages[index]), resultVar)
			endfor
	endswitch	

	SetDatafolder cDF
	
	return resultVar
End

static Function /S getCageNumberList([reset])
	Variable reset	
	return getUniqueListFromWave(ksSEDACOM_i_cage, "SEDACOMCageNums", reset = reset)
End

static Function /S getExperimentNums([reset])
	Variable reset
	return getUniqueListFromWave(ksSEDACOM_i_exp, "SEDACOMExperiments", reset = reset)
End

static Function /S getDateList([reset])
	Variable reset
	return getUniqueListFromWave(ksSEDACOM_i_date, "SEDACOMDateList", format = "date", reset = reset)
End


static Function convertTextW2DateW([name])
	String name
	
	if (ParamIsDefault(name) || strlen(name) == 0)
		name = ksSEDACOM_i_date
	endif
	
	DFREF cDF = GetDataFolderDFR()
	DFREF package = init()
	DFREF import = getImportFolder()
	
	Wave /T textDateWave = import:$name
	if (!WaveExists(textDateWave))
		return 0
	endif
	
	SetDataFolder import

	String textDateName = NameOfWave(textDateWave)
	String newTDName = textDateName + "_txt"
	
	if (WaveExists($newTDName))
		// an older wave already exists, so kill it
		KillWaves /Z $newTDName
	endif
	
	Rename $textDateName, $newTDName
	Make /N=(numpnts(textDateWave)) /D import:$name /WAVE=dateWave
	
	String separator = determineDateSeparator(textDateWave[0])
	if (strlen(separator) == 0)
		// excel date came in as date serial number, convert it back and figure it out
		Make /N=(numpnts(textDateWave)) /O/FREE datesFromSerialNums
		datesFromSerialNums = str2num(textDateWave)
		datesFromSerialNums *= 24 * 3600		// converts days to seconds
		datesFromSerialNums -= 24 * 3600 * 365.5 * 4		// account for 4 years difference (Excel start at 1900)
		
		dateWave = timeStamp2Secs(correctDateString(Secs2Date(datesFromSerialNums[p], -2), separator = "-", flip = 1), type = 2)
	else
		dateWave = timeStamp2Secs(correctDateString(textDateWave[p], type = 1, separator = separator), type = 2)
	endif
	
	SetDataFolder cDF
	
	return 1
End
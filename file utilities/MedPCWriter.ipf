﻿#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma ModuleName = MPCWriter
#pragma IgorVersion = 8
#pragma version = 1
#pragma hide = 1
#include "MedPCReader", version >= 2.4
#include "ListHelpers", version >= 5.4
#include "RBFileUtil", version >= 3.5

// Written by Roland Bock
// * version 1 (February 2021)

// This file is an companion file to the MedPCReader and writes data in the MedPC file format. 
// It is intended as an emergency use function, when an original data file got lost.
//
// The "MedPCReader" file is included to make use of some defined constants.

static Constant kSections = 5

// standardized feedback functions.
static Function /S getMsgStr(formatStr, functionName)
	String formatStr, functionName
	
	String msgStr = ""
	sprintf msgStr, formatStr, functionName, "%s"
	return msgStr
End

static Function /S getErrorStr(funcName)
	String funcName
	
	String format = "** ERROR (%s): %s!"
	return getMsgStr(format, funcName)
End

static Function /S getWarningStr(funcName)
	String funcName
	
	String format = "-- WARNING (%s): %s."
	return getMsgStr(format, funcName)
End


Menu "Macros"
	"Write MedPC as csv", MPC_transformCSV()
End

// This function writes the data of one animal into a text file in the same structure than
// the original file.
//
// parameters
// theSubject		string	subject whose data to be exported
// 
// optional
// path				string	name of the symbolic path to export the file to
Function MPC_writeSubject2File(theSubject, [path])
	String theSubject, path

	String errormsg = getErrorStr("MPC_writeSubject2File")
	String warnmsg = getWarningStr("MPC_writeSubject2File")
	
	String feedback = "-- exported data from %s to file %s\r    to %s\r     on %s at %s\r"

	DFREF subject = $theSubject
	if (DatafolderRefstatus(subject) != 1)
		printf errormsg, "'" + theSubject + "' is not valid - stopped."
		return 0
	endif
	
	if (ParamIsDefault(path) || strlen(path) == 0)
		path = "rbLastOpenedPath"
	endif
	PathInfo $path
	if (!V_flag)
		// path does not exists, so default to desktop
		NewPath /Q $path, SpecialDirPath("Desktop", 0, 0, 0)
	endif
	PathInfo $path
	
	DFREF cDF = GetDataFolderDFR()
	SetDatafolder subject	
	String daysList = SortListByDate2(StringList("*_header_*", ";"))
	SetDatafolder cDF

	Variable numDays = ItemsInList(daysList)
	String day = ""
	
	if (strlen(daysList) == 0)
		printf errormsg, "no header information, nothing to process - stopped."
		return 0
	endif

	String fileName = theSubject + "_igor.sad"
	Variable refNum = 0, index = 0, error = 0
	
	String spacer = W_LINEEND + U_LINEEND + W_LINEEND + U_LINEEND + W_LINEEND + U_LINEEND
	String blockPattern = "%s"
	String firstLine = "File: %s" + W_LINEEND + U_LINEEND
	
	Open /C="R*ch" /P=$path refNum as fileName
	fprintf refNum, firstLine, ReplaceString(":", (S_path + fileName), "\\")

	for (index = 0; index < numDays; index += 1)
		String theDate = getDateFromName(StringFromList(index, daysList))
		if (strlen(theDate) == 0)
			continue
		endif
	
		fprintf refNum, blockPattern, spacer	
		error = MPC_dayData2Str(refNum, subject, theDate)
		if (error == 0)
			// something went wrong, break the loop to close the file
			break
		endif
	endfor
	
	Close refNum	
	
	printf feedback, theSubject, fileName, S_path, date(), time()
	
	return 1
End


static Function MPC_dayData2Str(refNum, subject, day)
	Variable refNum
	DFREF subject
	String day
	
	String errormsg = getErrorStr("MPC_dayData2Str")
	String warnmsg = getWarningStr("MPC_dayData2Str")
	
	if (DatafolderRefStatus(subject) != 1)
		printf errormsg, "subject datafolder does not exist - stopped."
		return 0
	endif
	if (strlen(day) == 0)
		printf errormsg, "no data matching the date '" + day + "' found, could be wrong date format - stopped."
		return 0
	endif

	DFREF cDF = GetDataFolderDFR()
	SetDataFolder subject
	String sList = SortListByDate2(StringList("*_header_*_" + day, ";"))	// make a list of the header strings
	
	Variable numHeaders = ItemsInList(sList)
	
	if (strlen(sList) == 0)
		// we have no header information, so we can stop right here
		printf errormsg, "no header information about day '" + day + "' - stopped."
		SetDatafolder cDF
		return 0
	endif
	
	sList = sortListBySNum(sList)
	
	String varPattern = "%s: %11.3f" + W_LINEEND + U_LINEEND
	String whPattern = "%s:" + W_LINEEND + U_LINEEND
	String wvPattern = "%6d: %11.3f %11.3f %11.3f %11.3f %11.3f" + W_LINEEND + U_LINEEND
	String wrhPattern = "%6d: %11.3f"
	String wrvPattern = " %11.3f"
	String linePattern = "%s"
	String lines = ""
	
	String errMessage = "", comment = ""
	Variable err = 0
	Variable index = 0, values = 0, i = 0, r = 0
	for (index = 0; index < numHeaders; index += 1)
		String headerName = StringFromList(index, sList)
		String session = getSessionNumberFromName(headerName)
		SVAR header = subject:$headerName
		lines = RemoveByKey("file", header, "=", U_LINEEND)
		comment = "\\" + RemoveEnding(TrimString(StringByKey("comment", lines, "=", U_LINEEND)), ",")
		lines = RemoveByKey("comment", lines, "=", U_LINEEND)
		lines = ReplaceString("=", lines, ": ")
		lines = ReplaceString(U_LINEEND, lines, (W_LINEEND + U_LINEEND))
		
		try
			fprintf refNum, "%s", lines
			AbortOnRTE
		catch
			err = GetRTError(1)
			errMessage = GetErrMessage(err)
			printf "- while trying to write, got the following error: %s\r", errMessage
		endtry

		String varName = "", varLbl = "", searchStr = ""
		sprintf searchStr, "*_%s_%s", session, day
			
		String vList = SortListByDate2(VariableList(searchStr, ";", 4))	// make a list all variables
		Variable numVars = ItemsInList(vList)
		vList = sortListByVarLbl(vList)			// make sure the variables are in alphabetical order
			
		for (values = 0; values < numVars; values += 1)
			varName = StringFromList(values, vList)
			varLbl = getVarLblFromName(varName)
			NVAR dataVar = subject:$varName
			fprintf refNum, varPattern, varLbl, dataVar
		endfor

		String wList = SortListByDate2(WaveList(searchStr, ";", ""))			// make a list of all waves
		Variable numWaves = ItemsInList(wList)
		wList = sortListByVarLbl(wList)

		for (values = 0; values < numWaves; values += 1)
			varName = StringFromList(values, wList)
			varLbl = getVarLblFromName(varName)
			Wave dataWave = subject:$varName
			
			if (strsearch(varName, "_FR_", 0) > 0 || strsearch(varName, "_FP_", 0) > 0)
				continue
			endif
			
			fprintf refNum, whPattern, varLbl
			if (numpnts(dataWave) == 0)
				continue
			endif
			
			Variable sections = floor(numpnts(dataWave) / kSections)
			Variable rest = mod(numpnts(dataWave), kSections)
			Variable restIdx = numpnts(dataWave) - rest
			
			for (i = 0; i < (sections * kSections); i += kSections)
				fprintf refNum, wvPattern, i, dataWave[i], dataWave[i + 1], dataWave[i + 2], dataWave[i + 3], dataWave[i + 4]	
			endfor
			
			if (rest > 0)
				for (r = 0; r < rest; r += 1)
					if (r == 0)
						fprintf refNum, wrhPattern, i, dataWave[restIdx + r]
					else
						fprintf refNum, wrvPattern, dataWave[restIdx + r]
					endif
				endfor
				fprintf refNum, linePattern, W_LINEEND + U_LINEEND
			endif
		endfor
		fprintf refNum, linePattern, comment + W_LINEEND + U_LINEEND
	endfor

	SetDatafolder cDF
	return 1
End

//! Write out the contens of the MedPC data folder (where the loaded MedPC data ends up) as a 
// csv file into the local users document folder.
// @param fName: a optional string for the file name.
// @param found: a flag (default: TRUE) to use the file name from the loaded data MedPC data file
// @return: true (1) for success or false (0) for failure/error
//-
Function MPC_transformCSV([fName, found])
	String fName
	Variable found
	
	if (ParamIsDefault(fName) || strlen(fName) == 0)
		fName = "MedPC2ExcelExport"
	endif
	found = ParamIsDefault(found) || found >= 1 ? 1 : 0
	
	
	DFREF cDF = GetDataFolderDFR()
	DFREF load = $"root:MedPC"
	
	if (DataFolderRefStatus(load) == 0)
		// MedPC folder does not exists, nothing has been imported yet.
		return 0
	endif
	
	String vList = "", wList = ""
	SetDatafolder load
	String headerList = StringList("MPC_*_header", ";")
	SetDatafolder cDF
	
	Variable numHeaders = ItemsInList(headerList)
	if (numHeaders == 0)
		// without headers we can do anything, nothing loaded yet.
		return 0
	endif
	
	SVAR theFName = load:$"MPC_fileName"
	if (found)
		fName = theFName
	endif
	
	String line = "", linePattern = "%s,%s\r", lineNumPattern = "%s,%g\r"
	
	Variable index = 0, vars = 0, arrays = 0
	for (index = 0; index < numHeaders; index += 1)
		String headerName = StringFromList(index, headerList)
		String searchStr = ReplaceString("header", headerName, "*")
		SVAR curHeader = load:$headerName
		sprintf line, linePattern, "header", ReplaceString("=", ReplaceString("\n", curHeader, ","), ":")
	
		writeText2File(line, thePath = "rbExcelExport", theName = fName, verbose = 0, add = 1)
		
		SetDatafolder load
		vList = VariableList(searchStr, ";", 4)
		wList = WaveList(searchStr, ";", "")
		SetDatafolder cDF
		
		Variable numVars = ItemsInList(vList)
		Variable numWaves = ItemsInList(wList)
		
		if (numVars > 0)
			vList = SortList(vList)
			for (vars = 0; vars < numVars; vars += 1)
				String varName = StringFromList(vars, vList)
				String var = varName[strlen(varName) - 1, Inf]
				NVAR curVar = load:$varName
				sprintf line, lineNumPattern, var, curVar
				writeText2File(line, thePath = "rbExcelExport", theName = fName, verbose = 0, add = 1)	
			endfor
		endif
		
		if (numWaves > 0)
			wList = SortList(wList)
			for (arrays = 0; arrays < numWaves; arrays += 1)
				String wName = StringFromList(arrays, wList)
				String w = wName[strlen(wName) - 1, Inf]
				Wave curWave = load:$wName
				
				String values = ""
				wfprintf values, "%g,", curWave
				
				sprintf line, linePattern, w, values
				writeText2File(line, thePath = "rbExcelExport", theName = fName, verbose = 0, add = 1)	
			endfor
		endif
		
		writeText2File("\r\r", thePath = "rbExcelExport", theName = fName, verbose = 0, add = 1)
	endfor
	
	return 1
End


// *******************   HELPER FUNCTIONS **********************

static Function /S sortListBySNum(theList)
	String theList
	
	String result = ""
	if (strlen(theList) == 0 || ItemsInList(theList) == 1)
		return theList
	endif
	
	Wave /T orgList = ListToTextWave(theList, ";")
	Make /N=(numpnts(orgList))/O/FREE/T sorter
	sorter = getSessionNumberFromName(orgList[p])
	Sort /A sorter, orgList
	Variable index = 0
	for (index = 0; index < numpnts(orgList); index += 1)
		result = AddListItem(orgList[index], result, ";", index)
	endfor
	return result
End

static Function /S sortListByVarLbl(theList)
	String theList
	
	String result = ""
	
	if (strlen(theList) == 0 || ItemsInList(theList) == 1)
		return theList
	endif
	
	Wave /T orgList = ListToTextWave(theList, ";")
	Make /N=(numpnts(orgList))/O/FREE/T vars, sessions, sorter
	vars = getVarLblFromName(orgList[p])
	sessions = getSessionNumberFromName(orgList[p])
	sorter = sessions + vars
	Sort /A sorter, orgList
	Variable index = 0
	for (index = 0; index < numpnts(orgList); index += 1)
		result = AddListItem(orgList[index], result, ";", index)
	endfor
	return result
End

//! This function returns the date from a wave name with the following naming scheme:
// Protocol_VariableName_S#_Date, i.e. SAT_A_S1_090601.
// @param theName: wave name
// @return: the data in the form YYMMDD from the wave name
//-
static Function /S getDateFromName(theName)
	String theName
	
	if (strlen(theName) == 0)
		return ""
	endif
	
	Variable underScore = strsearch(theName, "_", Inf, 1)
	if (underScore > -1)
		return theName[underScore + 1, strlen(theName)]
	else
		return ""
	endif
End

static Function /S getSessionNumberFromName(theName)
	String theName
	
	if (strlen(theName) == 0)
		return ""
	endif
	
	String theSession = ""
	Variable listItems = ItemsInList(theName, "_")
	if ( listItems >= 3 )
		// for sessions we need 3 underscores, creating 4 list items
		theSession = StringFromList(2, theName, "_")
	endif
	return theSession	
End

static Function /S getVarLblFromName(theName)
	String theName
	
	String result = ""
	
	if (strlen(theName) == 0)
		return result
	endif
	
	Variable listItems = ItemsInList(theName, "_")
	if (listItems >= 3)
		// we need 3 underscores to create 4 list items
		result = StringFromList(1, theName, "_")
	endif
	
	return result
End
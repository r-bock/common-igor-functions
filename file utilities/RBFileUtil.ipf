#pragma rtGlobals=1		// Use modern global access method.
#pragma IgorVersion = 6.1
#pragma version = 3.5
#pragma hide = 1
#include "ListHelpers"

// by Roland Bock
// version 3.5 (Mar 2021): fixed the long format output, formerly SPSS, of stats values.
// version 3.45 (Jan 2017): fixed line break for cross platform value export and added 2 file path
//                  conversion functions, specific for Win or Mac 
// version 3.4 (Nov 2016): fixed a bug in write indexed CSV1 that would duplicate the first wave
// version 3.3 (July 2014): added a generic file loader
// version 3.2 (June 2013)
// version 3 (May 2012)
// * version 1.0 (August 2002)

// Collection of file loading and experiment utilities


// ***********************************************************************
// **************                                      C O N S T A N T S 
// ***********************************************************************

#if defined(MACINTOSH)
static Strconstant ksLineEnd = "\r"
#else
static Strconstant ksLineEnd = "\r\n"
#endif

static strConstant ksComma = ","

StrConstant ksLastLoadedPath = "rbLastOpenedPath"

// General binary loader. Creates an packed Igor experiment file with all binarys from one folder
// loaded in.
Function GeneralBinaryLoader(binaryPath, experimentPath)
	String binaryPath, experimentPath
	
	String setPathCmd
	sprintf setPathCmd, "GetBinarysFromAppleScript(\"%s\", \"%s\")", binaryPath, experimentPath 
	Execute/P "NEWEXPERIMENT "
	Execute/P "INSERTINCLUDE \"FileLoader\""
	Execute/p "COMPILEPROCEDURES "
	Execute/P setPathCmd
End

// experiment executer for Sang Jeongs Analysis

Function DoSJKimAnalysis(binaryPath, experimentPath)
	String binaryPath, experimentPath
		
	String setPathCmd
	sprintf setPathCmd, "GetSJKBinarysFromAppleScript(\"%s\", \"%s\")", binaryPath, experimentPath 
	Execute/P "NEWEXPERIMENT "					// creates a new experiment
	Execute/P "INSERTINCLUDE \"SJKimAnalysis\""
	Execute/P "COMPILEPROCEDURES "
	Execute/P setPathCmd
	Execute/P "AnalysisSJK(WaveList(\"Im_*\", \";\", \"\"))"
End


Function /S selectPathForWrite([thePath, ask])
	String thePath
	Variable ask
	
	if (ParamIsDefault(thePath) || strlen(thePath) == 0)
		thePath = ksLastLoadedPath
	endif	
	ask = ParamIsDefault(ask) || ask < 1 ? 0 : 1

	PathInfo $thePath	
	if (!V_flag || ask)
		// Path does not exists
		if (!V_flag)
			NewPath /Q $thePath, SpecialDirPath("Documents", 0,0,0)
		endif
		NewPath /M="Choose a destination folder ..." /O $thePath
	else
		Variable pathLevels = ItemsInList(S_path, ":")
		if (pathLevels <= 1)		
			// main level, probably unable to have write permissons, reset path to "Documents"
			NewPath /O/Q $thePath, SpecialDirPath("Documents", 0,0,0)
		endif			
	endif
	return thePath
End

Function /S FU_convertPath2Unix(path, [escSpecial])
	String path
	Variable escSpecial
	
	escSpecial = ParamIsDefault(escSpecial) || escSpecial > 0 ? 1 : 0
	
	path = ReplaceString(":", path, "/")
	String home = ReplaceString(":", ParseFilePath(1, SpecialDirPath("Documents", 0,0,0), ":", 1,0), "/")
	Variable homeLength = strlen(home)
	
	if (cmpstr(home, path[0, homeLength - 1]) == 0)
		path[0, homeLength - 2] = "~"
	else
		path = "/Volumes/" + path
	endif
	
	if (escSpecial)
		path = ReplaceString(" ", path, "\\\ ")
		path = ReplaceString("(", path, "\\\(")
		path = ReplaceString(")", path, "\\\)")
		path = ReplaceString("[", path, "\\\[")
		path = ReplaceString("[", path, "\\\]")
	endif
		
	return path
End

Function /S FU_convertPath2Win(path)
	String path
	
	if (strsearch(path, ":", 0) == 1)
		// we have a windows path with a single drive letter
		path = ReplaceString(":", path, "\\")
		path[1] = ":"			// insert the colon for the drive letter
	elseif (strsearch(path, "\\", 0) == 0)
		// we have a windows UNC path in the igor notation "\\server\path:to:folder:"
		path = ReplaceString(":", ReplaceString("\\", path, ":"), "\\")
	endif
	
	return path
End

// Write a set of files from subject folders into a csv file
//
// INPUTS:
//      required:
// theSubjList			a list of subject names (matching data folders) for the data waves
// 	
//	   optional:
// dataNames			a list of data waves that are in each subject folder (have the same name for each subject)
// thePath				the name of the simbolic path to the data folder
// theFolder			a string of the path to the data folder for the future data file
//						default: users home folder / documents
// theName				a name for the new data file (if it exists, it will be overwritten without warning)
//							the ending ".csv" will be added to the file name automatically
// valueWaveList		a list of text waves with extra information for each subject (like sex or genotype).
//							the dimension labels have to match the subjects
// separator			the separator to be used for the csv file
//						default:	"," (comma)
// verbose				0:	do not print full file name and date
//						1:	(default) print full file name and date
Function /S write_IndexedCSVFile1(theSubjList, [repeatedList, theFolder, thePath, theName, valueWaveList, separator, verbose, translate])
	String theFolder, thePath, theName, theSubjList, repeatedList, valueWaveList, separator
	Variable verbose, translate
	
	if (ParamIsDefault(verbose) || (verbose != 0 && verbose != 1))
		verbose = 1
	endif
	if (ParamIsDefault(translate) || (translate != 0 && translate != 1))
		translate = 0
	endif
	if (ParamIsDefault(thePath) || strlen(theFolder) == 0)
		thePath = "rbLastOpenedPath"
	endif
	PathInfo $thePath
	if (!V_flag)
		// Path does not exists
		if (ParamIsDefault(theFolder) || strlen(theFolder) == 0)
			NewPath /Q $thePath, SpecialDirPath("Documents", 0,0,0)
		else
			NewPath /Q $thePath, theFolder
		endif
	elseif (!ParamIsDefault(theFolder) && strlen(theFolder) > 0)
		NewPath /O /Q $thePath, theFolder	
	endif
	
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = "DATA_indexed.csv"
	else
		Variable theDot = strsearch(theName, ".", strlen(theName), 1)
		if (theDot == -1 || (strlen(theName) - theDot) > 5)
			// either no file ending found or the dot occurs past the last 4 characters, so currently assume it does not 
			// designate a file identifier.
			theName += ".csv"
		endif
	endif
	if (ParamIsDefault(separator) || strlen(separator) == 0)
		separator = ksComma
	endif
	if (ParamIsDefault(repeatedList))
		repeatedList = ""
	endif
	if (ParamIsDefault(valueWaveList))
		valueWaveList = ""
	endif
	
	DFREF cDF = GetDatafolderDFR()
	String output = "", number = ""
	
	Variable subject, stati, waves, index, lines, missing
	
	// all the following just to make sure that we have an equal number of columns for all lines
	Make /N=(ItemsInList(repeatedList)) /FREE counters = 0			// count max data wave length
	Make /N=(ItemsInList(repeatedList) + 1) /O /T /FREE headerLine	// text wave for header lines
	Make /N=(ItemsInList(theSubjList), ItemsInList(repeatedList) + 1) /O /T /FREE lineParts 		// wave parts of converted data waves
	if (translate)
		// this wave of wave references is going to hold the waves we need to translate 
		Make /N=(ItemsInList(valueWaveList)) /O /FREE /WAVE translators
		for (stati = 0; stati < ItemsInList(valueWaveList); stati += 1)
			Make /N=0 /O /FREE singleTrans
			if (WaveType($(StringFromList(stati, valueWaveList)), 1) == 2)
				String theValues = makeListFromWave(StringFromList(stati, valueWaveList), unique=1)
				Redimension /N=(ItemsInList(theValues)) singleTrans
				singleTrans = p+1
				for (index = 0; index < numpnts(singleTrans); index += 1)
					SetDimLabel 0, index, $(StringFromList(index, theValues)), singleTrans
				endfor
				translators[stati] = singleTrans
			endif			
		endfor
	endif

	headerLine[0] = "subject" + separator
	for (stati = 0; stati < ItemsInList(valueWaveList); stati += 1)
		switch (WaveType($(StringFromList(stati, valueWaveList)), 1))
			case 1:	// wave is a value wave with a single value for each subject
				headerLine[0] += StringFromList(stati, valueWaveList) + separator	// add the wave name as column label for the value
				break
			case 2:	// wave is a text wave and has status information for each subject
				headerLine[0] += StringFromList(stati, valueWaveList) + separator	// add the wave name as column label for the status
				break
		endswitch
	endfor
	
	for (subject = 0; subject < ItemsInList(theSubjList); subject += 1)
		String theSubject = StringFromList(subject, theSubjList)		// get the first subject
		output = theSubject + separator										// add the subject name to the beginning of the line
		
		for (stati = 0; stati < ItemsInList(valueWaveList); stati += 1)
			switch (WaveType($(StringFromList(stati, valueWaveList)), 1))
				case 1:	// wave is a value wave with a single value for each subject
					Wave theVWave = $(StringFromList(stati, valueWaveList))
					output += num2str(theVWave[%$theSubject]) + separator			// retrieve the status from the status wave and add it to the line
					break
				case 2:	// wave is a text wave and has status information for each subject
					Wave /T theSWave = $(StringFromList(stati, valueWaveList))
					if (translate)
						Wave translator = translators[stati]
						output += num2str(translator[%$(theSWave[%$theSubject])]) + separator
					else
						output += theSWave[%$theSubject] + separator			// retrieve the status from the status wave and add it to the line
					endif
					break
			endswitch
		endfor														//    this requires subject matching dimension labels in the status wave
		
		lineParts[subject][0] = output
		output = ""
	
		SetDatafolder $theSubject										// now go into the subject folder to retrieve the data
		for (waves = 0; waves < ItemsInList(repeatedList); waves += 1)
			Wave data = $(StringFromList(waves, repeatedList))
			if (!WaveExists(data))
				// do stuff
				continue												// data wave does not exist, so I need to add empty spaces
			endif
			output = ""
			if (counters[waves] == 0)
				counters[waves] = numpnts(data)						// retrieve the first data length and remember it
				for (missing = 0; missing < numpnts(data); missing += 1)
					if (ItemsInList(repeatedList) == 1)
						headerLine[waves + 1] += "D" + num2str(missing) + separator
					else
						headerLine[waves + 1] += "t" + num2str(waves + 1) + "D" + num2str(missing) + separator
					endif
				endfor				
			endif
			
			if (counters[waves] < numpnts(data))
				// the current data is longer as all previous data parts, so we need to add empty slots (commas) to each previous line until
				//		they match the current data length
				Variable diff = numpnts(data) - counters[waves]
				for (lines = 0; lines < DimSize(lineParts, 0); lines += 1)
					for (missing = counters[waves]; missing < numpnts(data); missing += 1)
						if (lines == 0)
							if (ItemsInList(repeatedList) == 1)
								headerLine[waves + 1] += "D" + num2str(missing) + separator
							else
								headerLine[waves + 1] += "t" + num2str(waves + 1) + "D" + num2str(missing) + separator
							endif
						endif
						if (strlen(lineParts[lines][waves + 1]) != 0)
							lineParts[lines][waves + 1] += separator
						endif
					endfor
				endfor
				counters[waves] = numpnts(data)
			endif
			
			// now add the data to the file
			for (index = 0; index < numpnts(data); index += 1)
				output += num2str(data[index]) + separator
			endfor
			
			if (counters[waves] > numpnts(data))
				// the current data wave is shorter than the previous ones, so add empty slots
				for (lines = numpnts(data); lines < counters[waves]; lines += 1)
					output += separator
				endfor
			endif
			
			lineParts[subject][waves + 1] = output
		endfor

		SetDatafolder cDF
	endfor

	// now put all lines together
	String theHeaderLine = ""
		
	Variable refNum
	Open /P=$thePath refNum as theName	// open the file (and overwrite if it exists)

	Variable parts = 0
	for (lines = 0; lines < DimSize(lineParts, 0); lines += 1)
		String theLine = ""
		for (parts = 0; parts < DimSize(lineParts, 1); parts += 1)
			if (lines == 0)
				theHeaderLine += headerLine[parts]
			endif
			theLine += lineParts[lines][parts]	
		endfor
		theLine += ksLineEnd
		if (lines == 0)
			fprintf refNum, "%s", (theHeaderLine + ksLineEnd)
		endif
		fprintf refNum, "%s", theLine
	endfor
	Close refNum

	if (verbose)
		print "-> exported to: \"" + S_fileName + "\" on " + date() + " using indexed file 1 (one line for each subject)."
	endif
	return S_fileName
End

//! This is the companion function to write_IndexedCSVFile1, but writes the data out in long format, i.e. each measurement
// value is written on a separate line. The output file will either have 4 or 5 columns. The 5 column output is created
// when one or more summary category waves are included in the output to avoid mixing categorial factor elements with 
// numeric output.
// @param theSubjList: required - a list of subject names (data folders) for the data export
// @param dataList: a list of data waves specific for each subject (must be the same name for each subject)
// @param theFolder: a string path to the folder for the result file
// @param thePath: an igor path name for the result file
// @param theName: a file name for the result file, an existing file will be overwritten, the file ending .csv will be added
// @param valueWaveList: a list of value (either categorial or summary values) waves, i.e. each subject has one entry and can be found through the dimension labels
// @param separator: a single character string used as separator of the values in the output file, default is ","
// @param verbose: a flag to suppress user feedback in the history window (default is 1: provide feedback)
// @param translate: a flag to indicate if the categories should be translated into numbers (1: TRUE) or not (default: 0: FALSE)
// @param add: a flag to append (1: TRUE) the data to an existing file or not (default: 0: FALSE -> overwrite) 
// @return: the file name of the output file
//-
Function /S write_IndexedCSVFile2(theSubjList, [dataList, theFolder, thePath, theName, valueWaveList, separator, verbose, translate, add])
	String theFolder, thePath, theName, theSubjList, dataList, valueWaveList, separator
	Variable verbose, translate, add
	
	
	verbose = ParamIsDefault(verbose) || verbose > 0 ? 1 : 0
	translate = ParamIsDefault(translate) || translate < 1 ? 0 : 1
	add = ParamIsDefault(add) || add < 1 ? 0 : 1
	
	if (ParamIsDefault(thePath) || strlen(theFolder) == 0)
		thePath = "rbLastOpenedPath"
	endif
	PathInfo $thePath
	if (!V_flag)
		// Path does not exists
		if (ParamIsDefault(theFolder) || strlen(theFolder) == 0)
			NewPath /Q $thePath, SpecialDirPath("Documents", 0,0,0)
		else
			NewPath /Q $thePath, theFolder
		endif
	elseif (!ParamIsDefault(theFolder) && strlen(theFolder) > 0)
		NewPath /O /Q $thePath, theFolder	
	endif
	
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = "DATA_long_format.csv"
	else
		Variable theDot = strsearch(theName, ".", strlen(theName), 1)
		if (theDot == -1 || (strlen(theName) - theDot) > 5)
			// either no file ending found or the dot occurs past the last 4 characters, so currently assume it does not 
			// designate a file identifier.
			theName += ".csv"
		endif
	endif
	if (ParamIsDefault(separator) || strlen(separator) == 0)
		separator = ksComma
	endif
	if (ParamIsDefault(dataList))
		dataList = ""
	endif
	if (ParamIsDefault(valueWaveList))
		valueWaveList = ""
	endif
	
	Variable valueFlag = 0						// a flag to set if we have summary or categories or both
	Variable index
	Variable numValueWaves = ItemsInList(valueWaveList)
	Variable numDataWaves = ItemsInList(dataList)
	
	// sanity check
	if (numValueWaves + numDataWaves == 0)
		// no lists to export given, stop here
		printf "** ERROR: both the value list and data list is empty, nothing to export -> stopped!"
		return ""
	endif
	
	if (translate)
		Make /N=(numValueWaves) /O/FREE/WAVE translators
	endif
	
	if (numValueWaves > 0)
		// check if we have category waves or only values
		for (index = 0; index < numValueWaves; index += 1)
			if (WaveType($StringFromList(index, valueWaveList), 1) == 1)
				// is variable wave -> summary value wave
				valueFlag = valueFlag | 2^0
			elseif (WaveType($StringFromList(index, valueWaveList), 1) == 2)
				// is text wave -> category wave
				valueFlag = valueFlag | 2^1
				if (translate)
					Make /N=0/O/FREE singleTrans
					String theValues = makeListFromWave(StringFromList(index, valueWaveList), unique=1)
					Redimension /N=(ItemsInList(theValues)) singleTrans
					singleTrans = p + 1
					for (index = 0; index < numpnts(singleTrans); index += 1)
						SetDimLabel 0, index, $(StringFromList(index, theValues)), singleTrans
					endfor
					translators[index] = singleTrans
				endif				
			endif
		endfor
	endif	
	
	// Prepare the output format
	String headerLine = "", dataLine = "", dataPattern = ""

	if ((valueFlag & 2^1) != 0)
		// we have category waves, so we don't want to mix the category and the values and need an extra column
		headerLine = "subject" + separator + "measurement" + separator + "status" + separator + "row" + separator + "data"
		dataPattern = "%s" + separator + "%s" + separator + "%s" + separator + "%d" + separator + "%g"
	else
		headerLine = "subject" + separator + "measurement" + separator + "row" + separator + "data"
		dataPattern = "%s" + separator + "%s" + separator + "%d" + separator + "%g"	
	endif
		
	DFREF cDF = GetDatafolderDFR()
	String output = "", number = ""
	
	Variable subject, waves, value, row
	String category = "", line = "", wName = ""
	
	// all the following just to make sure that we have an equal number of columns for all lines
	Make /N=1 /O /T /FREE theLines								 		// wave parts of converted data waves	 
 	theLines[0] = headerLine
 	
	for (subject = 0; subject < ItemsInList(theSubjList); subject += 1)
		String theSubject = StringFromList(subject, theSubjList)		// get the first subject
		
		// first process all individual value list, i.e. those waves that have a single value for 
		//    each subject
		for (waves = 0; waves < numValueWaves; waves += 1)	
			wName = StringFromList(waves, valueWaveList)
			switch (WaveType($wName, 1))
				case 1:
					category = ""
					Wave theVWave = $wName
					value = theVWave[%$theSubject]
					break
				case 2:
					value = Nan
					Wave /T theSWave = $wName
					if (translate)
						Wave translator = translators[index]
						category = num2str(translator[%$(theSWave[%$theSubject])])
					else
						category = theSWave[%$theSubject]
					endif
					break
				default:
					value = Nan
					category = ""
			endswitch
			
			if (cmpstr(wName[0,3], "All_") == 0)
				wName[0,3] = ""
			endif

			if ((valueFlag & 2^1) != 0)
				sprintf line, dataPattern, theSubject, wName, category, 1, value
			else
				sprintf line, dataPattern, theSubject, wName, 1, value
			endif
			
			// now add the line to the lines wave for later output
			theLines[numpnts(theLines)] = {line}
		endfor
		
		// now start to process the data value waves for each subject
		DFREF sDF = $theSubject
		if (DatafolderRefStatus(sDF) != 1)
			// the datafolder does not exist or is a free folder, i.e. not a general datafolder - nothing more to do
			continue
		endif
		
		for (waves = 0; waves < numDataWaves; waves += 1)
			wName = StringFromList(waves, dataList)
			if (WaveType(sDF:$wName, 1) != 1)
				continue
			endif
			
			Wave data = sDF:$wName
			
			for (row = 0; row < numpnts(data); row += 1)
				if ((valueFlag & 2^1) != 0)
					// we have a category column
					sprintf line, dataPattern, theSubject, wName, "", row + 1, data[row]
				else
					sprintf line, dataPattern, theSubject, wName, row + 1, data[row]
				endif
				theLines[numpnts(theLines)] = {line}
			endfor
		endfor
	endfor

	// now put all lines together
	Variable refNum, lines, numCols, fidx = 0
	if (add)
		GetFileFolderInfo /P=$thePath/Q/Z theName
		if (V_flag == 0 && V_isFile)
			// file exists, so check if it has 4 or 5 columns
			Open /P=$thePath /R refNum as theName
			FReadLine refNum, line
			Close refNum
			numCols = ItemsInList(line, ",")
						
			if (((valueFlag & 2^1) != 0 && numCols == 4) || (valueFlag < 2) && numCols == 5)
				String namePattern = "%s_cp%d.csv"
				String nFName = ""
				theName[strlen(theName) - 5, strlen(theName)] = ""
				
				do
					fidx += 1
					sprintf nFName, namePattern, theName, fidx
					GetFileFolderInfo /P=$thePath /Q/Z nFName
				while(V_flag == 0)

				Open /P=$thePath refNum as nFName
			else
				Open /A/P=$thePath refNum as theName
			endif
			
		elseif (V_isFolder)
			print "** ERROR: desired file name is a folder."
			return ""
		endif
		
	else
		Open /P=$thePath refNum as theName	// open the file (and overwrite if it exists)
	endif
	
	for (lines = 0; lines < DimSize(theLines, 0); lines += 1)
		String theLine = theLines[lines] + ksLineEnd
		fprintf refNum, "%s", theLine
	endfor
	Close refNum

	if (verbose)
		print "-> exported to: \"" + S_fileName + "\" on " + date() + " using indexed file 2 (stats in long format)"
	endif
	return S_fileName
End

Function /S writeText2File(text, [thePath, theName, verbose, add])
	String text, thePath, theName
	Variable verbose, add
	
	if (ParamIsDefault(thePath) || strlen(thePath) == 0)
		thePath = selectPathForWrite()
	endif
	PathInfo $thePath
//	if (!V_flag)
		// path does not exists
		thePath = selectPathForWrite(thePath=thePath)
		PathInfo $thePath
//	endif
	
	
	String pathToFile = S_path
	
	if (ParamIsDefault(theName) || strlen(theName) == 0)
		theName = "TrialData4FMImport.csv"
	else
		Variable theDot = strsearch(theName, ".", Inf, 1)
		if (theDot == -1 || (strlen(theName) - theDot) > 5)
			// either no file ending found or the dot occurs past the last 4 characters, so currently assume it does not 
			// designate a file identifier.
			theName += ".csv"
		endif
	endif
	verbose = ParamIsDefault(verbose) || verbose >= 1 ? 1 : 0	
	add = ParamIsDefault(add) || add < 1 ? 0 : 1 
	
	Variable refNum
	if (add)
		Open /P=$thePath /A refNum as theName
	else
		Open /P=$thePath refNum as theName
	endif
	
	fprintf refNum, "%s", text
	Close refNum
	
	if (verbose)
		print "->  exported to: \"" +  ParseFilePath(0, S_fileName, ":", 1, 0) + "\" on", date(), time(), "to \'" + pathToFile + "\'."
	endif
	
	return theName
End


Function /S GenericFileFolderChooser([msg, extension, folder, verbose])
	String msg, extension
	Variable folder, verbose
	
	if (ParamIsDefault(verbose) || verbose > 1)
		verbose = 1
	else
		verbose = verbose <= 0 ? 0 : round(verbose)
	endif
	if (ParamIsDefault(folder) || folder < 0)
		folder = 0
	else
		folder = folder >= 1 ? 1 : round(folder)
	endif

	if (ParamIsDefault(msg) || strlen(msg) == 0)
		if (folder)
			msg = "Please choose a data folder"
		else
			msg = "Please choose your data files"
		endif
	endif

	Variable generic = 0
	if (ParamIsDefault(extension) || strlen(extension) == 0)
		extension = ".txt,.dat,.csv"
		generic = 1
	endif

	extension = ReplaceString("*", extension, "")

	String path = "", theFileList = ""
	String fileFilter = "Data Files (*.txt, *.dat, *.csv):.txt,.dat,.csv;All Files:.*;"

	Variable refNum = 0, index = 0, aborted = 0
	
	PathInfo /S $ksLastLoadedPath
	Variable pathExists = V_flag == 1	
	
	if (folder)
		NewPath /M=msg/Q/O $ksLastLoadedPath
		if (V_flag == -1)
			// user aborted
			print "--- user canceled data folder selection."
			return ""
		endif
		
		PathInfo /S $ksLastLoadedPath
		path = S_path

		Variable numExt = ItemsInList(extension, ",")
		for (index = 0; index < numExt; index += 1)
			theFileList += IndexedFile($ksLastLoadedPath, -1, StringFromList(index, extension, ","))		
		endfor
		theFileList = RemoveEnding(theFileList, ";")
		theFileList = path + ReplaceString(";", theFileList, ";" + path)
	else
		if (!generic)
			String extDisp = ReplaceString(".", extension, "*.")
			extDisp = ReplaceString(",", extDisp, ", ")
			fileFilter = "Specific Files (" + extDisp + "):" + extension + ";" + fileFilter
		endif
				
		if (pathExists)
			Open /P=$ksLastLoadedPath /D/R/M=msg/MULT=1/F=fileFilter refNum
		else
			Open /D/R/M=msg/MULT=1/F=fileFilter refNum
		endif
		
		if (strlen(S_fileName) == 0)
			// user aborted
			aborted = 1
		endif
		
		if (!aborted)
			theFileList = ReplaceString("\r", S_fileName, ";")
			path = ParseFilePath(1, StringFromList(0, theFileList), ":", 1, 0)
			NewPath /Q/O $ksLastLoadedPath, path

			if (ItemsInList(theFileList) > 1)
				theFileList = SortList(theFileList)
			endif
		endif
	endif
	
	if (verbose)
		if (aborted)
			print "--- user canceled data file selection."
		else
			printf "--- found %g files with the extensions [%s] on %s, %s \r", ItemsInList(theFileList), ReplaceString(",", extension, ", "), date(), time()
			printf "---> in path %s \r", path
		endif
	endif
		
	return theFileList
End